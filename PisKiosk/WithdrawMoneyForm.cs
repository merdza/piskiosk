﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class WithdrawMoneyForm : PisKiosk.MainForm
    {
        public WithdrawMoneyForm()
        {
            InitializeComponent();
            //this.TopMost = true;
            LangsVisible = false;
            ReactOnCardRemoved = false;
        }

        public double MoneyTotal { get; protected set; } //ukupno novca u kiosku od poslednjeg podizanja
        public double MoneyInsertedToday { get; protected set; } //koliko novca je uplaceno na danasnji dan. Ovo je bitno da bi eventualno mogli da vrate taj novac u kiosk jer su im blagajne obicno vezane za dan, a danasnji dan nije jos zavrsen, tako da ce da predaju novac i izvestaje za prethodne dane, zakljucno sa jucerasnjim.
        public double MoneyWithdrawn { get; protected set; } //ukupno novca u kiosku od poslednjeg podizanja
        public virtual void getMoneyInformation(out double total, out double totalUntilToday) 
        {
            total = 0;
            totalUntilToday = 0;
        }
    }
}
