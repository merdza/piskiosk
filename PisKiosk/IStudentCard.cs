﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisKiosk
{
    public interface IStudentCard
    {
        void initializeCommunicationAdapter();
        void finalizeCommunicationAdapter();
        int isCardPresentAdapter();
        CardType getCardTypeAdapter();
        int isLoggedInAdapter();
        void loginAdapter(string username, string password);
        void logoutAdapter();
        void getDataAdapter();
        string getCardSerialAdapter();
        string getFirstNameAdapter();
        string getLastNameAdapter();
        string getCardOwnerNameAdapter();
        string getPersonalNumberAdapter();
        string getPhotoNameAdapter();
        string getCardNumberAdapter();
        int getExpiryDateDayAdapter();
        int getExpiryDateMonthAdapter();
        int getExpiryDateYearAdapter();
        int getOperaterIDAdapter();
        string getOfficialCardNumberAdapter();
    }
}
