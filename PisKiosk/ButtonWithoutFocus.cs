﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk
{
    public class ButtonWithoutFocus : Button
    {
        public ButtonWithoutFocus()
            : base()
        {
            this.SetStyle(ControlStyles.Selectable, false);
            
        }

        protected override bool ShowFocusCues
        {
            get
            {
                return false;
            }
        }
    }
}
