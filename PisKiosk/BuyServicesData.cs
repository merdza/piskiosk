﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk
{
    public class BuyServicesData
    {
        public double CardMoneyBalance { get; set; } //kes na kartici pre kupovine
        public double MoneyBalanceAfterBuyingSelected { get; set; } //kes koji bi ostao ako bi korisnik kupio ono sto je odabrao

        public List<Service> Services { get; set; } // lista mogucih obroka za kupovinu obroka

        public BuyServicesData()
        {
            Services = new List<Service>();
        }

        public void resetBuyServicesData()
        {
            CardMoneyBalance = 0;
            MoneyBalanceAfterBuyingSelected = CardMoneyBalance;

            Services.Clear();
        }

        public void addService(Service s)
        {
            Services.Add(s);
        }

        public Service getNextService(Service s)
        {
            return Services[(Services.IndexOf(s) + 1) == Services.Count ? 0 : (Services.IndexOf(s) + 1)];
        }

        public Service getPreviousService(Service s)
        {
            return Services[(Services.IndexOf(s) - 1) == (-1) ? (Services.Count - 1) : (Services.IndexOf(s) - 1)];
        }

        public virtual void fillServicesFromDB() {}

        public double getTotalPrice()
        {
            double ret = 0;

            foreach (var service in Services)
            {
                ret += service.Amount * service.Price;
            }

            return ret;
        }

        public void changeServiceAmounts(Service s, int amountChange)
        {
            s.Amount += amountChange;
            MoneyBalanceAfterBuyingSelected -= amountChange * s.Price;

            Program.KioskApplication.log.Info("Service amount changed. Service: " + s.Name + ", price: " + s.Price.ToString() + " x " + s.Amount.ToString() + " = " + (s.Price * s.Amount).ToString() + ", current cash balance if purchased now: " + MoneyBalanceAfterBuyingSelected.ToString());
        }

        public bool hasEnoughMoneyToBuy(Service s, int amount)
        {
            bool ret = false;
            if ((MoneyBalanceAfterBuyingSelected - amount * s.Price) >= 0)
            {
                ret = true;
            }
            return ret;
        }
    }
}
