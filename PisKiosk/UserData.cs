﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk
{
    public enum CardType { Unknown, Student, Official }
    public abstract class UserData
    {
        protected string userName;
        protected string personalNumber;
        protected string cardNumber;
        protected string cardSerialNumber;
        protected double cashOnCard;
        protected string photo;
        protected CardType cardType;

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public string PersonalNumber
        {
            get { return personalNumber; }
            set { personalNumber = value; }
        }

        public string CardNumber
        {
            get { return cardNumber; }
            set { cardNumber = value; }
        }

        public string CardSerialNumber
        {
            get { return cardSerialNumber; }
            set { cardSerialNumber = value; }
        }

        public double CashOnCard
        {
            get { return cashOnCard; }
            set { cashOnCard = value; }
        }

        public string Photo
        {
            get { return photo; }
            set { photo = value; }
        }

        public CardType CardType
        {
            get { return cardType; }
            set { cardType = value; }
        }

        public UserData() { }
        public UserData(string un, string pn, string cn, string csn, string p)
        {
            setUserData(un, pn, cn, csn, p);
        }

        public virtual void setUserData(string un, string pn, string cn, string csn, string p)
        {
            UserName = un;
            PersonalNumber = pn;
            CardNumber = cn;
            CardSerialNumber = csn;
            Photo = p;
        }

        public virtual void readCardData()
        {
            try
            {
                CardInterface.getInstance().CardInterf.getDataAdapter();
                string nm = CardInterface.getInstance().CardInterf.getCardOwnerNameAdapter();
                string pn = CardInterface.getInstance().CardInterf.getPersonalNumberAdapter();
                string cn = CardInterface.getInstance().CardInterf.getCardNumberAdapter();
                string cs = CardInterface.getInstance().CardInterf.getCardSerialAdapter();
                //double b = CardInterface.getInstance().CardInterf.getCashBalanceAdapter();
                string p = CardInterface.getInstance().CardInterf.getPhotoNameAdapter().Trim();

                setUserData(nm, pn, cn, cs, p);
            }
            catch (Exception ex)
            {
                throw new Exception(guiLang.UserData_messageErrorReadingCard + " " + ex.Message);
            }
        }

        public virtual void resetUserData()
        {
            UserName = "";
            PersonalNumber = "";
            CardNumber = "";
            CardSerialNumber = "";
            CashOnCard = 0;
            Photo = "";
            CardType = CardType.Unknown;

        }

        public virtual bool isOperatorForThisKiosk(Operator op)
        {
            return false;
        }
    }
}
