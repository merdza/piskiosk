﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PisKiosk
{
    public partial class CashInsertForm : MainForm
    {
        protected List<InsertedBankNote> insertedBankNotes;
        protected List<string> supportedBankNotes;
        protected MainForm whoCalledMe;  // and where should I return?

        public bool BankNoteInserted { get; protected set; }  //indikacija da je validator ocitao novcanicu i jos uvek je nije obradio.
                                                            //ponistava se kada se uradi credit novcanice ili reject
                                                            //ako je promenljiva true duze od 5 sekundi, daje se upozorenje da je novcanica progutana

        public void getSupportedBankNotes()
        {
            supportedBankNotes = new List<string>();

            XmlSettings.XmlSettings.getArraySettings(Program.KioskApplication.SettingsFile, "banknote", ref supportedBankNotes);
        }

        //private void PaintBorderlessGroupBox(object sender, PaintEventArgs p)
        //{
        //    GroupBox box = (GroupBox)sender;
        //    //p.Graphics.Clear(Program.KioskApplication.BackgroundColor);
        //    p.Graphics.DrawString(box.Text, box.Font, Brushes.White, 0, 0);
        //}

        protected CashInsertForm()
        {
            InitializeComponent();

            LangsVisible = false;
            ReactOnCardRemoved = true;

            //groupBoxInsertedBankNotes.Paint += PaintBorderlessGroupBox;

            getSupportedBankNotes();

            insertedBankNotes = new List<InsertedBankNote>();

            //this.BackColor = Program.pkf.BackgroundColor;

            //lblTitle.ForeColor = Program.pkf.TextForeColor2;

            this.BackColor = Program.KioskApplication.BackgroundColor;

            listViewSupportedNotes.BackColor = Program.KioskApplication.BackgroundColor;
            listViewSupportedNotes.ForeColor = Program.KioskApplication.TextForeColor;
            
            //visina kontrole za prikaz podrzanih novcanica se menja u zavisnosti od broja podrzanih novcanica
            //do velicina sam dosao metodom trial and error :-)
            listViewSupportedNotes.Height = supportedBankNotes.Count * 60;
            groupBoxSupportedBankNotes.Height = listViewSupportedNotes.Height + 80;
            listBoxInsertedNotes.Height = listViewSupportedNotes.Height;
            groupBoxInsertedBankNotes.Height = listViewSupportedNotes.Height + 80;
            lblEquals.Height = listViewSupportedNotes.Height;
            lblTimes.Height = listViewSupportedNotes.Height;
            lblInsertedTotal.Height = listViewSupportedNotes.Height;
            groupBoxTotal.Height = listViewSupportedNotes.Height + 80;
            

            listBoxInsertedNotes.BackColor = Program.KioskApplication.BackgroundColor;
            listBoxInsertedNotes.ForeColor = Program.KioskApplication.TextForeColor;

            lblInsertedTotal.BackColor = Program.KioskApplication.BackgroundColor;
            lblInsertedTotal.ForeColor = Program.KioskApplication.TextForeColor2;

            lblEquals.ForeColor = Program.KioskApplication.TextForeColor2;
            lblTimes.ForeColor = Program.KioskApplication.TextForeColor2;

            lblInsertedTotal.Text = "0";

            lblCardMoneyBalance.ForeColor = Program.KioskApplication.TextForeColor2;

            btnEndInsertingMoney.BackColor = Program.KioskApplication.TextForeColor2;
            btnEndInsertingMoney.ForeColor = Program.KioskApplication.BackgroundColor;

            this.Refresh();
            Application.DoEvents();
        }

        private void setGuiLabels()
        {
            lblTitle.Text = guiLang.CashInsertForm_Label_lblTitle1 + Environment.NewLine + guiLang.CashInsertForm_Label_lblTitle2;
            groupBoxSupportedBankNotes.Text = guiLang.CashInsertForm_GroupBox_SupportedBankNotes + " (" + Program.KioskApplication.CurrencyCode + ")";
            groupBoxInsertedBankNotes.Text = guiLang.CashInsertForm_GroupBox_InsertedBankNotes;
            groupBoxTotal.Text = guiLang.CashInsertForm_GroupBox_Total + " (" + Program.KioskApplication.CurrencyCode + ")";

            btnEndInsertingMoney.Text = guiLang.CashInsertForm_Button_btnEndInsertingMoney;

            this.Refresh();
            Application.DoEvents();
        }

        //private static CashInsertForm instance = null;

        //public static CashInsertForm getInstance(MainForm w)
        //{
        //    if (instance == null)
        //    {
        //        instance = new CashInsertForm();
        //    }
        //    instance.whoCalledMe = w;
        //    return instance;
        //}

        private void CashInsertForm_Load(object sender, EventArgs e)
        {
            try
            {
                listSupportedNotes();
                InitValidator();
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Validator could not be initialized. Error: " + ex.Message, ex);
                Program.KioskApplication.showFormInPanel(whoCalledMe);
                //throw ex;  ako ovo ostane, puca program zbog unhandeled exceptiona
            }
        }

        private void listSupportedNotes()
        {
            foreach (string note in supportedBankNotes)
            {
                listViewSupportedNotes.Items.Add(note);
            }
        }

        //public int[] getSupportedBankNotes()
        //{
        //    return supportedBankNotes;
        //}

        private void InitValidator()
        {
            try
            {
                //ValidatorAdapter.getInstance().OpenValidatorPort();
                ValidatorAdapter.getInstance().OnLog = (text) =>
                {
                    SafeInvokeHelper.Invoke(this, "writeLog", text);
                };

                ValidatorAdapter.getInstance().OnException = (ex) =>
                {
                    SafeInvokeHelper.Invoke(this, "handleException", ex);
                };

                ValidatorAdapter.getInstance().OnRejectedNote = () =>
                {
                    SafeInvokeHelper.Invoke(this, "bankNoteRejected");
                };

                ValidatorAdapter.getInstance().OnCreditNote = (bankNote) =>
                {
                    SafeInvokeHelper.Invoke(this, "bankNoteCredited", bankNote);
                };

                ValidatorAdapter.getInstance().OnReadNote = (bankNote) =>
                {
                    SafeInvokeHelper.Invoke(this, "bankNoteRead", bankNote);
                };

                ValidatorAdapter.getInstance().OnValidatorStatusChange = (status, bankNote) =>
                {
                    status += " " + Program.KioskApplication.ValidatorPort + " " + bankNote.ToString();
                    SafeInvokeHelper.Invoke(this, "setValidatorStatusInfo", status);
                };
            }
            catch (Exception ex)
            {
                // SCLogManager.WriteToLog("ERROR INIT BANK NOTE VALIDATOR");
                //ValidatorAdapter.getInstance().Enabled = false;
                //HandleException(ex);
                //Console.WriteLine(ex.Message);
                throw new Exception("Init validator error. " + ex.Message);
            }
        }

        private void CashInsertForm_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                try
                {
                    //natpisi na formi se menjaju svaki put kada se prikaze forma jer u medjuvremenu moze da se promeni jezik
                    setGuiLabels();
                    setCashBalance();
                    startValidator();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error(ex.Message, ex);
                    Program.KioskApplication.showFormInPanel(whoCalledMe);
                    MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
                }
            }
            else
            {
                stopValidator();
                resetGui();
            }
        }

        protected virtual void setCashBalance()
        { 

        }
        
        private void startValidator()
        {
            try
            {
                ValidatorAdapter.getInstance().Start();
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Start validator error: " + ex, ex);
                MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
                throw ex;
            }
        }

        private void stopValidator()
        {
            try
            {
                ValidatorAdapter.getInstance().Stop();
            }
            catch (Exception ex)
            {
                //nije uspelo zaustavljanje validatora, ali to mozda moze da prodje bez upozorenja korisniku
                Program.KioskApplication.log.Error("Stop validator error: " + ex, ex);
            }
        }

        private void resetGui()
        {
            insertedBankNotes.Clear();
            listBoxInsertedNotes.Items.Clear();
            lblInsertedTotal.Text = "0";
            emphasize(9, false);//novcanica od 9 sigurno ne postoji, tako da ce da se ukloni selekcija svih novcanica

        }

        public void handleException(string ex)
        {
            //listBoxException.Items.Add(ex);
            //listBoxException.TopIndex = listBoxException.Items.Count - 1;
            //this.Refresh();
            //Application.DoEvents();
            Program.KioskApplication.log.Error("Validator exception: " + ex);
        }

        public void writeLog(string logEntry)
        {
            //    listBoxLog.Items.Add(logEntry);
            //    listBoxLog.TopIndex = listBoxLog.Items.Count - 1;
            //    this.Refresh();
            //    Application.DoEvents();
            Program.KioskApplication.log.Debug("Validator event: " + logEntry);
        }

        public void bankNoteRejected()
        {
            bankNoteProcessed();
            //btnEndInsertingMoney.Enabled = true;
            Program.KioskApplication.log.Info("Banknote rejected.");
            MessageClassic.getInstance(guiLang.CashInsertForm_RejectedNote_Warning, MessageType.Warning).Show();
        }

        public void bankNoteRead(int note)
        {
            Program.KioskApplication.log.Info("Detected banknote " + note.ToString());
            
            //postavi flag da je novcanica ubacena
            bankNoteInserted();

            //proveri da li je ocitana novcanica medju dozvoljenim novcanicama
            if (!supportedBankNotes.Contains(note.ToString()))
            {
                Program.KioskApplication.log.Info("Banknote " + note.ToString() + " is not supported.");
                ValidatorAdapter.getInstance().Reject();
            }

            //oznaci novcanicu na formi da je ubacena, a jos nije skroz obradjena
            emphasize(note, false);
        }

        public virtual void bankNoteCredited(int bankNote)
        {
            
        }

        protected void bankNoteInserted()
        {
            BankNoteInserted = true;
            //btnEndInsertingMoney.Enabled = false;
            timerBankNoteInserted.Start();
        }

        //poziva se kada je zavrsena obrada ubacene novcanice, bez obzira na ishod obrade
        protected void bankNoteProcessed()
        {
            BankNoteInserted = false;
            timerBankNoteInserted.Stop();
        }

        public void emphasize(int note, bool isCredited)
        {
            foreach (ListViewItem item in listViewSupportedNotes.Items)
            {
                item.BackColor = Program.KioskApplication.BackgroundColor;
            }


            var noteInListView = listViewSupportedNotes.FindItemWithText(note.ToString(), false, 0, false);

            if (noteInListView != null)
            {
                if (noteInListView.Text.Equals(note.ToString()))
                {
                    if (isCredited)
                    {
                        noteInListView.BackColor = Program.KioskApplication.TextForeColor2;
                    }
                    else
                    {
                        Color lightClr = ControlPaint.LightLight(Program.KioskApplication.TextForeColor2);  //daj svetliju nijansu
                        noteInListView.BackColor = lightClr;                        
                    }
                }
            }
        }


        public void setValidatorStatusInfo(string status)
        {
            listBoxValidatorStatus.Items.Add(status);
            listBoxValidatorStatus.TopIndex = listBoxValidatorStatus.Items.Count - 1;
            this.Refresh();
            Application.DoEvents();
        }

        public void addNoteToScreen(int note)
        {
            //listBoxNotes.Items.Clear();
            listBoxInsertedNotes.Items.Clear();

            int total = 0;
            
            foreach (string bn in supportedBankNotes)
            {
                int bnInt = Convert.ToInt32(bn);
                //nadji koliko ima takvih novcanica medju ubacenim novcanicama, pa ispisi broj
                int cnt = insertedBankNotes.Where(s => s.Value == bnInt).Count();
                if (cnt > 0)
                {
                    listBoxInsertedNotes.Items.Add(cnt);
                }
                else
                {
                    listBoxInsertedNotes.Items.Add("");
                }

                total += cnt * bnInt;
            }

            lblInsertedTotal.Text = total.ToString();

            setCashBalance();

            this.Refresh();
            Application.DoEvents();
        }

        private void CashInsertForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                //ValidatorAdapter.getInstance().ExitValidator();
                ValidatorAdapter.getInstance().Stop();
            }
            catch (Exception)
            {
                //ma iskuliraj
            }
        }

        private void listViewSupportedNotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            listViewSupportedNotes.SelectedIndices.Clear(); //onemogucava selektovanje novcanica u ovom listView-u
        }

        private void groupBoxSupportedBankNotes_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Program.KioskApplication.TextForeColor, Program.KioskApplication.TextForeColor);
        }

        private void groupBoxInsertedBankNotes_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Program.KioskApplication.TextForeColor, Program.KioskApplication.TextForeColor);
        }

        private void groupBoxTotal_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Program.KioskApplication.TextForeColor, Program.KioskApplication.TextForeColor);
        }


        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Clear text and border
                g.Clear(this.BackColor);

                // Draw text
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }



        private void timerBankNoteInserted_Tick(object sender, EventArgs e)
        {
            //proslo je 10 sekundi od kad je ubacena novcanica
            //ako je uopste pozvana ova metoda, to znaci da novcanica jos uvek nije obradjena (jer se timer gasi u metodi bankNoteProcessed())
            bankNoteProcessed();
            Program.KioskApplication.log.Error("MONEY PROBLEM! Banknote is LOST. 10 seconds passed since it was inserted and nothing happened.");
            MessageClassic.getInstance(guiLang.CashInsertForm_LostBankNote_Warning, MessageType.Warning).Show();
        }
    }
}
