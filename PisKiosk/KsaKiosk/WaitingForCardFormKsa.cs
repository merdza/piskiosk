﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.KsaKiosk
{
    public partial class WaitingForCardFormKsa : PisKiosk.WaitingForCardForm
    {
        public WaitingForCardFormKsa()
        {
            InitializeComponent();
        }

        private static WaitingForCardFormKsa instance = null;
        public static WaitingForCardFormKsa getInstance()
        {
            if (instance == null)
            {
                instance = new WaitingForCardFormKsa();
            }
            return instance;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.showFormInPanel(UserDataFormKsa.getInstance());
        }

        public override void cardInserted()
        {
            Program.KioskApplication.showFormInPanel(UserDataFormKsa.getInstance());
        }
    }
}
