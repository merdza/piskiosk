﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.KsaKiosk
{
    public partial class WithdrawMoneyFormKsa : PisKiosk.WithdrawMoneyForm
    {
        public WithdrawMoneyFormKsa()
        {
            InitializeComponent();
        }

        private static WithdrawMoneyFormKsa instance = null;
        public static WithdrawMoneyFormKsa getInstance()
        {
            if (instance == null)
            {
                instance = new WithdrawMoneyFormKsa();
            }
            return instance;
        }

        public override void getMoneyInformation(out double total, out double totalUntilToday)
        {
            //todo
            total = 0;
            totalUntilToday = 0;
            //.getMoneyInformation(out total, out totalUntilToday);
        }
    }
}
