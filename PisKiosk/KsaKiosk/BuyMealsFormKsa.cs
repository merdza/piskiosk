﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.KsaKiosk
{
    public partial class BuyMealsFormKsa : PisKiosk.BuyMealsForm
    {
        public BuyMealsFormKsa()
        {
            InitializeComponent();
            this.BackColor = Program.KioskApplication.BackgroundColor;
        }

        private static BuyMealsFormKsa instance = null;
        public static BuyMealsFormKsa getInstance()
        {
            if (instance == null)
            {
                instance = new BuyMealsFormKsa();
            }
            return instance;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.showFormInPanel(UserDataFormKsa.getInstance());
        }
    }
}
