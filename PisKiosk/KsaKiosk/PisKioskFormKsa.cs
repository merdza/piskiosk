﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.KsaKiosk
{
    public partial class PisKioskFormKsa : PisKiosk.PisKioskForm
    {
        private static PisKioskFormKsa instance = null;

        public static PisKioskFormKsa getInstance()
        {
            if (instance == null)
            {
                instance = new PisKioskFormKsa();
            }
            return instance;
        }

        private PisKioskFormKsa():base()
        {
            InitializeComponent();

            UserData = new UserDataKsa();
        }

        public override void setKioskOperater()
        {
            
        }

        public override WithdrawMoneyForm getWithdrawMoneyForm()
        {
            return WithdrawMoneyFormKsa.getInstance();
        }

        public override void loadKioskApplication(PanelFormType pft)
        {
            MainForm form = WaitingForCardFormKsa.getInstance();
            showFormInPanel(form);
        }
    }
}
