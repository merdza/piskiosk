﻿namespace PisKiosk.KsaKiosk
{
    partial class UserDataFormKsa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.photoBox)).BeginInit();
            this.panelMealsData.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnInsertCash
            // 
            this.btnInsertCash.Click += new System.EventHandler(this.btnInsertCash_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "Title - UserDataForm Ksa";
            // 
            // UserDataFormKsa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1150, 650);
            this.Name = "UserDataFormKsa";
            this.Load += new System.EventHandler(this.UserDataFormKsa_Load);
            this.VisibleChanged += new System.EventHandler(this.UserDataFormKsa_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.photoBox)).EndInit();
            this.panelMealsData.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
