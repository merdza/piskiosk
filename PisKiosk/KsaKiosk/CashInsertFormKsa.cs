﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.KsaKiosk
{
    public partial class CashInsertFormKsa : PisKiosk.CashInsertForm
    {
        public CashInsertFormKsa():base()
        {
            InitializeComponent();

            lblKioskApp.Text = "Ksa";
        }


        private static CashInsertFormKsa instance = null;

        public static CashInsertFormKsa getInstance(MainForm w)
        {
            if (instance == null)
            {
                instance = new CashInsertFormKsa();
            }
            instance.whoCalledMe = w;
            return instance;
        }

        public override void bankNoteCredited(int bankNote)
        {
            try
            {
                InsertedBankNote newBankNote = new InsertedBankNoteKsa();

                newBankNote.insertBankNote(bankNote);
                insertedBankNotes.Add(newBankNote);

                emphasize(bankNote, true);

                addNoteToScreen(bankNote);
                bankNoteProcessed();
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Error inserting banknote. Banknote is in the kiosk, but it was not recorded at the card or in the database. " + ex.Message, ex);
                MessageClassic.getInstance(guiLang.CashInsertForm_LostBankNote_Warning, MessageType.Error).Show();
            }
        }

    }
}
