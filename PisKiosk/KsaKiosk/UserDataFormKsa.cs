﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.KsaKiosk
{
    public partial class UserDataFormKsa : PisKiosk.UserDataForm
    {
        public UserDataFormKsa()
        {
            InitializeComponent();
        }

        private static UserDataFormKsa instance = null;
        public static UserDataFormKsa getInstance()
        {
            if (instance == null)
            {
                instance = new UserDataFormKsa();
            }
            return instance;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.showFormInPanel(BuyMealsFormKsa.getInstance());
        }

        public override void readCardData()
        {
            userData = new UserDataKsa();
            ((UserDataKsa)userData).readCardData();
        }

        private void UserDataFormKsa_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                setLabels();
                readCardData();
                showUserData();
            }
        }

        private void UserDataFormKsa_Load(object sender, EventArgs e)
        {
        }

        private void btnBuyFood_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.showFormInPanel(BuyMealsFormKsa.getInstance());
        }

        private void btnInsertCash_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.showFormInPanel(CashInsertFormKsa.getInstance(this));
        }
    }
}
