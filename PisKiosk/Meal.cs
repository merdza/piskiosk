﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk
{
    public abstract class Meal
    {
        public int Month { get; set; }  //mesec u kom obrok treba da se potrosi, odnosno za koji je kupljen
        public int Year { get; set; }  //godina u kojoj obrok treba da se potrosi, odnosno za koju je kupljen
        public MealType Type { get; set; }  // dorucak, rucak ili vecera
        public double Price { get; set; }  // jedinicna cena obroka
        public int Amount { get; set; }  // odabrana kolicina obroka
        public int MealsAllowed { get; set; }  //koliko obroka sme da kupi na osnovu onoga sto je izracunao MealsChecker


        public Meal() { }
        public Meal(int m, int y, MealType mt, double p, int a = 0)
        {
            Month = m;
            Year = y;
            Type = mt;
            Price = p;
            Amount = a;
            setMealsAllowed();
        }
        public string getMealTypeString()
        {
            string ret = "";
            switch (Type)
            {
                case MealType.Breakfast:
                    ret = guiLang.Breakfast;
                    break;
                case MealType.Lunch:
                    ret = guiLang.Lunch;
                    break;
                case MealType.Dinner:
                    ret = guiLang.Dinner;
                    break;
                default:
                    break;
            }
            return ret;
        }

        public abstract void setMealsAllowed();

    }

    public enum MealType { Breakfast, Lunch, Dinner };
}
