﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Printing;
using System.ServiceProcess;

namespace PisKiosk
{
    public static class PrintSupport
    {
        internal static void SpotTroubleUsingProperties(PrintQueue pq)
        {
            if (pq.HasPaperProblem)
            {
                throw new Exception(guiLang.printerErrorHasPaperProblem);
            }
            if (!(pq.HasToner))
            {
                throw new Exception(guiLang.printerErrorHasNotToner);
            }
            if (pq.IsDoorOpened)
            {
                throw new Exception(guiLang.printerErrorIsDoorOpened);
            }
            if (pq.IsInError)
            {
                throw new Exception(guiLang.printerErrorIsInError);
            }
            if (pq.IsNotAvailable)
            {
                throw new Exception(guiLang.printerErrorIsNotAvailable);
            }
            if (pq.IsOffline)
            {
                throw new Exception(guiLang.printerErrorIsOffline);
            }
            if (pq.IsOutOfMemory)
            {
                throw new Exception(guiLang.printerErrorIsOutOfMemory);
            }
            if (pq.IsOutOfPaper)
            {
                throw new Exception(guiLang.printerErrorIsOutOfPaper);
            }
            if (pq.IsOutputBinFull)
            {
                throw new Exception(guiLang.printerErrorIsOutputBinFull);
            }
            if (pq.IsPaperJammed)
            {
                throw new Exception(guiLang.printerErrorIsPaperJammed);
            }
            if (pq.IsPaused)
            {
                throw new Exception(guiLang.printerErrorIsPaused);
            }
            if (pq.IsTonerLow)
            {
                throw new Exception(guiLang.printerErrorIsTonerLow);
            }
            if (pq.NeedUserIntervention)
            {
                throw new Exception(guiLang.printerErrorNeedUserIntervention);
            }

            // Check if queue is even available at this time of day
            // The following method is defined in the complete example.
            ReportAvailabilityAtThisTime(pq);

        }
            // Check for possible trouble states of a printer using its properties
            internal static void SpotTroubleUsingProperties(ref String statusReport, PrintQueue pq)
            {
                statusReport += pq.QueueStatus;
                if (pq.HasPaperProblem)
                {
                    statusReport = statusReport + "Has a paper problem. ";
                }
                if (!(pq.HasToner))
                {
                    statusReport = statusReport + "Is out of toner. ";
                }
                if (pq.IsDoorOpened)
                {
                    statusReport = statusReport + "Has an open door. ";
                }
                if (pq.IsInError)
                {
                    statusReport = statusReport + "Is in an error state. ";
                }
                if (pq.IsNotAvailable)
                {
                    statusReport = statusReport + "Is not available. ";
                }
                if (pq.IsOffline)
                {
                    statusReport = statusReport + "Is off line. ";
                }
                if (pq.IsOutOfMemory)
                {
                    statusReport = statusReport + "Is out of memory. ";
                }
                if (pq.IsOutOfPaper)
                {
                    statusReport = statusReport + "Is out of paper. ";
                }
                if (pq.IsOutputBinFull)
                {
                    statusReport = statusReport + "Has a full output bin. ";
                }
                if (pq.IsPaperJammed)
                {
                    statusReport = statusReport + "Has a paper jam. ";
                }
                if (pq.IsPaused)
                {
                    statusReport = statusReport + "Is paused. ";
                }
                if (pq.IsTonerLow)
                {
                    statusReport = statusReport + "Is low on toner. ";
                }
                if (pq.NeedUserIntervention)
                {
                    statusReport = statusReport + "Needs user intervention. ";
                }

                // Check if queue is even available at this time of day
                // The following method is defined in the complete example.
                ReportAvailabilityAtThisTime(ref statusReport, pq);

            }//end SpotTroubleUsingProperties

            // Check for possible trouble states of a printer using the flags of the QueueStatus property
            internal static void SpotTroubleUsingQueueAttributes(ref String statusReport, PrintQueue pq)
            {
                if ((pq.QueueStatus & PrintQueueStatus.PaperProblem) == PrintQueueStatus.PaperProblem)
                {
                    statusReport = statusReport + "Has a paper problem. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.NoToner) == PrintQueueStatus.NoToner)
                {
                    statusReport = statusReport + "Is out of toner. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.DoorOpen) == PrintQueueStatus.DoorOpen)
                {
                    statusReport = statusReport + "Has an open door. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.Error) == PrintQueueStatus.Error)
                {
                    statusReport = statusReport + "Is in an error state. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.NotAvailable) == PrintQueueStatus.NotAvailable)
                {
                    statusReport = statusReport + "Is not available. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.Offline) == PrintQueueStatus.Offline)
                {
                    statusReport = statusReport + "Is off line. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.OutOfMemory) == PrintQueueStatus.OutOfMemory)
                {
                    statusReport = statusReport + "Is out of memory. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.PaperOut) == PrintQueueStatus.PaperOut)
                {
                    statusReport = statusReport + "Is out of paper. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.OutputBinFull) == PrintQueueStatus.OutputBinFull)
                {
                    statusReport = statusReport + "Has a full output bin. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.PaperJam) == PrintQueueStatus.PaperJam)
                {
                    statusReport = statusReport + "Has a paper jam. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.Paused) == PrintQueueStatus.Paused)
                {
                    statusReport = statusReport + "Is paused. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.TonerLow) == PrintQueueStatus.TonerLow)
                {
                    statusReport = statusReport + "Is low on toner. ";
                }
                if ((pq.QueueStatus & PrintQueueStatus.UserIntervention) == PrintQueueStatus.UserIntervention)
                {
                    statusReport = statusReport + "Needs user intervention. ";
                }

                // Check if queue is even available at this time of day
                // The method below is defined in the complete example.
                ReportAvailabilityAtThisTime(ref statusReport, pq);
            }

            private static void ReportAvailabilityAtThisTime(ref String statusReport, PrintQueue pq)
            {
                if (pq.StartTimeOfDay != pq.UntilTimeOfDay) // If the printer is not available 24 hours a day
                {
                    DateTime utcNow = DateTime.UtcNow;
                    Int32 utcNowAsMinutesAfterMidnight = (utcNow.TimeOfDay.Hours * 60) + utcNow.TimeOfDay.Minutes;

                    // If now is not within the range of available times . . .
                    if (!((pq.StartTimeOfDay < utcNowAsMinutesAfterMidnight)
                      &&
                      (utcNowAsMinutesAfterMidnight < pq.UntilTimeOfDay)))
                    {
                        statusReport = statusReport + " Is not available at this time of day. ";
                    }
                }
            }

            private static void ReportAvailabilityAtThisTime(PrintQueue pq)
            {
                if (pq.StartTimeOfDay != pq.UntilTimeOfDay) // If the printer is not available 24 hours a day
                {
                    DateTime utcNow = DateTime.UtcNow;
                    Int32 utcNowAsMinutesAfterMidnight = (utcNow.TimeOfDay.Hours * 60) + utcNow.TimeOfDay.Minutes;

                    // If now is not within the range of available times . . .
                    if (!((pq.StartTimeOfDay < utcNowAsMinutesAfterMidnight)
                      &&
                      (utcNowAsMinutesAfterMidnight < pq.UntilTimeOfDay)))
                    {
                        throw new Exception(guiLang.printerErrorNotAvailableTimeOfTheDay);
                    }
                }
            }

            public static void getPrinterErrors()
            {
                try
                {
                    // Create a PrintServer "theServer" must be a print server to which the user has full print access.
                    PrintServer myPrintServer = new PrintServer();

                    //koliko shvatam, na ovaj nacin mogu da se koriste samo lokalni printeri, ne i mrezni printeri

                    // List the print server's queues
                    PrintQueueCollection myPrintQueues = myPrintServer.GetPrintQueues();
                    foreach (PrintQueue pq in myPrintQueues)
                    {
                        SpotTroubleUsingProperties(pq);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            public static void restartSpooler()
            {
                try
                {
                    //stop spooler
                    ServiceController service = new ServiceController("Spooler");
                    if ((!service.Status.Equals(ServiceControllerStatus.Stopped)) && (!service.Status.Equals(ServiceControllerStatus.StopPending)))
                    {
                        service.Stop();
                        service.WaitForStatus(ServiceControllerStatus.Stopped);
                    }

                    //start spooler
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running);
                }
                catch (Exception ex)
                {                    
                    throw new Exception("Restarting printer spooler service failed. " + ex.Message);
                }
            }

            public static void deletePrintingJobs()
            {
                try
                {
                    /*
                    Dim ps = New LocalPrintServer(PrintSystemDesiredAccess.AdministrateServer)
                    Dim pq = New PrintQueue(ps, ps.DefaultPrintQueue.FullName, PrintSystemDesiredAccess.AdministratePrinter)

                    If pq.NumberOfJobs > 0 Then
                    pq.Purge()
                    End If
                     */

                    LocalPrintServer ps = new LocalPrintServer(PrintSystemDesiredAccess.AdministrateServer);
                    PrintQueue pq = new PrintQueue(ps, ps.DefaultPrintQueue.FullName, PrintSystemDesiredAccess.AdministratePrinter);

                    if (pq.NumberOfJobs > 0)
                    {
                        pq.Purge();
                    }


                    //// Create a PrintServer "theServer" must be a print server to which the user has full print access.
                    //PrintServer myPrintServer = new PrintServer();

                    ////koliko shvatam, na ovaj nacin mogu da se koriste samo lokalni printeri, ne i mrezni printeri

                    //// List the print server's queues
                    //PrintQueueCollection myPrintQueues = myPrintServer.GetPrintQueues();
                    //foreach (PrintQueue pq in myPrintQueues)
                    //{
                    //    pq.Purge();
                    //}
                }
                catch (Exception ex)
                {
                    throw new Exception("Deleting printing jobs failed. " + ex.Message);
                }
            }
    }
}
