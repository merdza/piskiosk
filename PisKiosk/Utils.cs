﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk
{
    public static class Utils
    {
        public static void getCultureNames()
        {
            // ispisi u konzolu kodove svih kulture
            List<string> list = new List<string>();
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
            {
                string specName = "(none)";
                try { specName = CultureInfo.CreateSpecificCulture(ci.Name).Name; }
                catch { }
                list.Add(String.Format("{0,-12}{1,-12}{2}", ci.Name, specName, ci.EnglishName));
            }

            list.Sort();  // sort by name

            // write to console
            Console.WriteLine("CULTURE   SPEC.CULTURE  ENGLISH NAME");
            Console.WriteLine("--------------------------------------------------------------");
            foreach (string str in list)
                Console.WriteLine(str);
        }

        public static string getMonthString(int m)
        {
            string ret = "";
            switch (m)
            {
                case 1:
                    ret = guiLang.Utils_Month_Jan;
                    break;
                case 2:
                    ret = guiLang.Utils_Month_Feb;
                    break;
                case 3:
                    ret = guiLang.Utils_Month_Mar;
                    break;
                case 4:
                    ret = guiLang.Utils_Month_Apr;
                    break;
                case 5:
                    ret = guiLang.Utils_Month_May;
                    break;
                case 6:
                    ret = guiLang.Utils_Month_Jun;
                    break;
                case 7:
                    ret = guiLang.Utils_Month_Jul;
                    break;
                case 8:
                    ret = guiLang.Utils_Month_Aug;
                    break;
                case 9:
                    ret = guiLang.Utils_Month_Sep;
                    break;
                case 10:
                    ret = guiLang.Utils_Month_Oct;
                    break;
                case 11:
                    ret = guiLang.Utils_Month_Nov;
                    break;
                case 12:
                    ret = guiLang.Utils_Month_Dec;
                    break;
                case 13:
                    ret = guiLang.Utils_Month_Jan;
                    break;
            }
            return ret;
        }

        //nacrtaj border na TabControl
        public static void drawTabControlBorder(object sender, System.Windows.Forms.PaintEventArgs e, System.Windows.Forms.UserControl uc)
        {
            System.Drawing.Rectangle r = e.ClipRectangle;
            r = new System.Drawing.Rectangle(r.X, r.Y, r.Width, r.Height);

            System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(Program.KioskApplication.TextForeColor);
            System.Drawing.Graphics formGraphics;
            formGraphics = uc.CreateGraphics();
            formGraphics.DrawRectangle(new System.Drawing.Pen(myBrush, 2), r);
            myBrush.Dispose();
            formGraphics.Dispose();
        }

        public static string getCurrencyString(double price, bool space)
        {
            //space = da li se prikazuje sa spejsom izmedju cifre i oznake novca ili ne
            string currencyBeforeAmount = XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "currencyBeforeAmount");
            string currency = XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "currency");

            string ret = "";

            string spaceString = "";
            if (space)
            {
                spaceString = " ";                
            }

            if (currencyBeforeAmount.Equals("1"))
            {
                ret = currency + spaceString + price.ToString("N");
            }
            else
            {
                ret = price.ToString("N") + spaceString + currency;
            }

            return ret;
        }

        public static string getCurrencyString(string price)
        {
            string currencyBeforeAmount = XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "currencyBeforeAmount");
            string currency = XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "currency");

            string ret = "";

            if (currencyBeforeAmount.Equals("1"))
            {
                ret = currency + " " + price;
            }
            else
            {
                ret = price + " " + currency;
            }

            return ret;
        }

        public static string addLeadingZeros(int i, int totalNumbers)
        {
            try
            {
                int mul = 10;
                string lead = "";
                for (int j = totalNumbers; j > 1; j--)
                {
                    if (i < mul)
                    {
                        for (int k = 0; k < j - 1; k++)
                        {
                            lead += "0";
                        }
                        return lead + i.ToString();
                    }
                    mul *= 10;
                }
                return i.ToString();
            }
            catch (Exception)
            {
                //ma ovo nema sanse da se desi
                //try catch je samo da ne pukne program
                return "0";
            }
        }

        public static string makeFixedLengthString(string s, int length)
        {
            //koristi se za pravljenje stringa fiksne duzine koji se npr. koristi za racune
            //ako je string kraci od potrebne duzine, dodaju mu se spejsovi
            //ako je duzi, skracuje se na potrebnu duzinu
            try
            {
                if (s.Length < length)
                {
                    int len = s.Length;
                    for (int i = 0; i < length - len; i++)
                    {
                        s += " ";
                    }
                }
                else if (s.Length > length)
                {
                    s = s.Substring(0, length);
                }
                return s;
            }
            catch (Exception)
            {
                //ma ovo nema sanse da se desi
                //try catch je samo da ne pukne program
                return "0";
            }
        }

        public static void KillProcess(string processName)
        {
            try
            {
                Process[] processes = Process.GetProcessesByName(processName);
                foreach (var process in processes)
                {
                    process.Kill();
                }
            }
            catch (Exception)
            {
                //nije uspelo ubijanje procesa. ne treba prijavljivati gresku
            }
        }

    }
}
