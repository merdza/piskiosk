﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class WaitingForCardForm : MainForm
    {
        protected WaitingForCardForm()
        {
            InitializeComponent();
            //this.TopMost = true;
            LangsVisible = true;
            ReactOnCardRemoved = true;
        }

        //private static WaitingForCardForm instance = null;
        //public static WaitingForCardForm getInstance()
        //{
        //    if (instance == null)
        //    {
        //        instance = new WaitingForCardForm();
        //    }
        //    return instance;
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            cardInserted();
        }

        public virtual void cardInserted() { }

        private void WaitingForCardForm_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.BackColor = Program.KioskApplication.BackgroundColor;
                lblInsertCard.ForeColor = Program.KioskApplication.TextForeColor2;
                lblInsertCard.Text = guiLang.WaitingforCardForm_Label_lblInsertCard;

                //labela ce da se krece proizvoljno po ekranu
                timerLabelMove.Start();
            }
            else
            {
                timerLabelMove.Stop();
            }
        }

        private Point getPanelCoordinates()
        {
            Point panelLocation = new Point();

            Point max = new Point(this.Size - panelTitle.Size);
            Random rnd = new Random();
            panelLocation.X = rnd.Next(max.X);
            panelLocation.Y = rnd.Next(max.Y);

            return panelLocation;
        }

        private void timerLabelMove_Tick(object sender, EventArgs e)
        {
            panelTitle.Location = getPanelCoordinates();
            this.Refresh();
            Application.DoEvents();
        }

        private void panelTitle_Paint(object sender, PaintEventArgs e)
        {
            //ControlPaint.DrawBorder(e.Graphics, this.panelTitle.ClientRectangle, Color.DarkBlue, ButtonBorderStyle.Solid);
        }

        private void panelTitle_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void btnPrinterStatus_Click(object sender, EventArgs e)
        {
            try
            {	        
                Program.KioskApplication.checkPrinterStatus();

                MessageClassic.getInstance("Printer status ok!", MessageType.Information).Show();
	        }
	        catch (Exception ex)
	        {
                MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
	        }
        }

        private void btnPrintReceipt_Click(object sender, EventArgs e)
        {
            try
            {
                printSampleReceipt();
                MessageClassic.getInstance("Receipt printed!", MessageType.Information).Show();
            }
            catch (Exception ex)
            {
                MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
            }
        }

        public void printSampleReceipt()
        {
            try
            {
                //UserDataScBgd ud = UserDataFormScBgd.getInstance().ud;

                string receiptTitle = guiLang.Receipt_Title_Money_Inserted;

                string receiptNumber = "";

                DateTime now = DateTime.Now;

                string timeStamp = Utils.addLeadingZeros(now.Day, 2) + @"." + Utils.addLeadingZeros(now.Month, 2) + @"." + now.Year.ToString() + @". " + Utils.addLeadingZeros(now.Hour, 2) + @":" + Utils.addLeadingZeros(now.Minute, 2) + @":" + Utils.addLeadingZeros(now.Second, 2);//KSA date format dd/mm/yyy

                double insertedTotal = 0;
                List<ReceiptItem> receiptItems = new List<ReceiptItem>();

                
                receiptItems.Add(new ReceiptItem(guiLang.Receipt_BankNote + " " + Utils.getCurrencyString(Convert.ToInt32(10), true), 1, Convert.ToDouble(10)));
                insertedTotal += 10;

                receiptItems.Add(new ReceiptItem(guiLang.Receipt_BankNote + " " + Utils.getCurrencyString(Convert.ToInt32(500), true), 1, Convert.ToDouble(500)));
                insertedTotal += 500;

                receiptItems.Add(new ReceiptItem(guiLang.Receipt_BankNote + " " + Utils.getCurrencyString(Convert.ToInt32(100), true), 1, Convert.ToDouble(100)));
                insertedTotal += 100;

                //Program.KioskApplication
                Receipt rp = new ScBgdKiosk.ReceiptSample(receiptTitle, receiptNumber, "041405254", insertedTotal, timeStamp, receiptItems);
                rp.printReceipt();
            }
            catch (Exception ex)
            {
                string err = "Error printing receipt. " + " " + ex.Message;
                Program.KioskApplication.log.Error(err, ex);
                throw new Exception(err);
            }
        }
    }

}
