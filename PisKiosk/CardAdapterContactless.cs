﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisKiosk
{
    public class CardAdapterContactless : IStudentCard
    {

        static object locker = new object();
        public void getDataRestaurantVersionAdapter(byte[] mealsArray, byte[] eatenMealsArray)
        {
            lock (locker)
            {
                try
                {
                    //MifareProjekat.MifareIDCap.getDataRestaurantVersion(mealsArray, eatenMealsArray);
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public void setDataRestaurantVersionAdapter(byte[] mealsArray, byte[] eatenMealsArray, int token)
        {
            lock (locker)
            {
                try
                {
                    //MifareProjekat.MifareIDCap.setDataRestaurantVersion(mealsArray, eatenMealsArray, token);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public int getDataRestaurantReturnVersionAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getDataRestaurantReturnVersion();
                    return 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public int isCardPresentAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.isCardPresent();
                    return 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public string getFirstNameAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getFirstName();
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public string getLastNameAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getLastName();
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public string getCardOwnerNameAdapter()
        {
            try
            {
                string result = getFirstNameAdapter() + " " + getLastNameAdapter();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string getPersonalNumberAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getPersonalNumber();
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public string getPhotoNameAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getPhotoName();
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public string getCardNumberAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getCardNumber();
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public string getCardSerialAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getCardSerial();
                    return string.Empty;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public int getValidAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getValid();
                    return 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public int getExpiryDateDayAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getExpiryDateDay();
                    return 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public int getExpiryDateMonthAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getExpiryDateMonth();
                    return 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public int getExpiryDateYearAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.getExpiryDateYear();
                    return 0;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public void initializeCommunicationAdapter()
        {
            lock (locker)
            {
                try
                {
                    //MifareProjekat.MifareIDCap.startListener();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public void finalizeCommunicationAdapter()
        {
            lock (locker)
            {
                try
                {
                    //MifareProjekat.MifareIDCap.stopListener();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public bool isOfficialAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.isOfficial();
                    return false;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public bool isStudentCardAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.isStudentCard();
                    return false;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public bool isGuestCardAdapter()
        {
            lock (locker)
            {
                try
                {
                    //return MifareProjekat.MifareIDCap.isGuestCard();
                    return false;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        //ovde su implementacije funkcija interfejsa koje za sada nemam
        public int decCashAdapter(double amount, string pin) { return 0; }
        public int incCashAdapter(double amount, string pin) { return 0; }
        public void getDataAdapter() {}
        public double getCashBalanceAdapter() { return 0; }
        public int checkUserPinAdapter(string pin) { return 0; }
        public CardType getCardTypeAdapter() 
        {
            if (isStudentCardAdapter())
            {
                return CardType.Student;     
            }
            else if (isOfficialAdapter())
            {
                return CardType.Official;
            }
            else
            {
                throw new Exception("Wrong card type");
            }
            
        }
        public string getOfficialCardNumberAdapter() { return ""; }
        public int isLoggedInAdapter() { return 0; }
        public void loginAdapter(string username, string password) { }
        public void logoutAdapter() { }
        public int getOperaterIDAdapter() { return 0; }

        public void connect()
        {
            lock (locker)
            {
                try
                {
                    //MifareProjekat.MifareIDCap.connect();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        }
        public void disconnect()
        {
            lock (locker)
            {
                try
                {
                    //MifareProjekat.MifareIDCap.disconnect();
                }
                catch (Exception)
                {
                    //throw new Exception(ex.Message, ex);
                }
            }
        }
    }
}