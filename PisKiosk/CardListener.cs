using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace PisKiosk
{
    public class CardListener
    {
        public void listen()
        {
            int present; //trenutno stanje u citacu kartica
            int presentPrev;//1 - present; //prethodno stanje u citacu kartica
            if (XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "cardInterface").Equals("1"))
            {
                //kontaktni cip
                present = 0; 
                presentPrev = 0;
            }
            else
            {
                //beskontaktni cip
                present = 1;
                presentPrev = 1;
            }
            
            IStudentCard cardInterface = CardInterface.getInstance().CardInterf; //da li se radi sa kontaktnom ili beskontaktnom karticom

            while (!shouldStop)
            {
                try
                {
                    present = cardInterface.isCardPresentAdapter();
                    if ((present != presentPrev))
                    {
                        if (present == 0)
                        {
                            /*izbacena kartica*/
                            if (cardInterface.GetType().Equals(typeof(SomborKiosk.CardAdapterContactlessSombor)))
                            {
                                SomborKiosk.CardAdapterContactlessSombor ci = (SomborKiosk.CardAdapterContactlessSombor)cardInterface;
                                ci.disconnect();
                            }
                            SafeInvokeHelper.Invoke(Program.KioskApplication, "cardRemoved", null);
                        }
                        else if (present == 1)
                        {
                            /*ubacena kartica*/
                            if (Program.KioskApplication.isHandleCreated())
                            {
                                SafeInvokeHelper.Invoke(Program.KioskApplication, "cardInserted", null);
                            }
                            else
                            {
                                if (!Login.getInstance().IsHandleCreated)
                                {
                                    Thread.Sleep(500);
                                }
                                SafeInvokeHelper.Invoke(Login.getInstance(), "cardInserted", null);
                            }
                            
                        }
                    }

                    presentPrev = present;
                    Thread.Sleep(500); //cekaj pola sekunde na promenu dogadjaja u citacu

                }
                catch (Exception)
                {
                }
            }
        }

        public void requestStop()
        {
            shouldStop = true;
        }

        private volatile bool shouldStop;
    }
}

