﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk
{
    public class Operator
    {
        public int OperaterId { get; set; }
        public string OperaterCardSerialNumber { get; set; }
        public bool Initialized { get; set; }  //da li je dobijena informacija o operateru kioska iz baze

        public Operator(int opId, string sn)
        {
            OperaterId = opId;
            OperaterCardSerialNumber = sn;
        }

        public Operator(int opId)
        {
            OperaterId = opId;
            OperaterCardSerialNumber = null;
        }

        public Operator(bool initialized)
        {
            Initialized = initialized;
        }

       
    }
}
