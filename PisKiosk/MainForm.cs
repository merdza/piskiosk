﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class MainForm : Form
    {
        protected MainForm()
        {
            InitializeComponent();

            this.TopLevel = false;

            FormBusy = false;
            
            //this.BackColor = Program.KioskApplication.BackgroundColor;
            //lblTitle.ForeColor = Program.KioskApplication.TextForeColor2;

            //temp
            //this.BackColor = Color.Yellow;
            //lblTitle.ForeColor = Color.Tomato;
        }

        public bool LangsVisible { get; set; }

        public bool ReactOnCardRemoved { get; set; }

        public bool FormBusy { get; set; }

        //private static MainForm instance = null;

        //private static MainForm getInstance()
        //{
        //    if (instance == null)
        //    {
        //        instance = new MainForm();
        //    }
        //    return instance;
        //}
    }
}
