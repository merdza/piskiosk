﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk
{
    public class Service
    {
        public string Name {get; set;}
        public double Price { get; set; }

        public int ServiceDBId { get; set; }

        public int BoardingDBId { get; set; }  //IDPlacanjeBoravka
        public int PersonalDamageDBId { get; set; }  //IDLicnaOstecenja

        public bool MultiPurchasable { get; set; }  //Moze da se kupi vise od jednom ili ne

        public int Amount { get; set; }  // odabrana kolicina
        
        public Service(){}
        public Service(string n, double p, int dbId, int bid, int pdid, bool mp, int a = 0)
        {
            Name = n;
            Price = p;
            Amount = a;
            ServiceDBId = dbId;
            BoardingDBId = bid;
            PersonalDamageDBId = pdid;
            MultiPurchasable = mp;
        }
    }
}