﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk
{
    public class DBAdapter
    {
        public DBAdapter() { }

        protected static string getProperty(string name)
        {
            string settingsFileName = Program.KioskApplication.SettingsFile;
            return XmlSettings.XmlSettings.getSetting(settingsFileName, name);
        }

        /// <summary>
        /// Check DB connection
        /// </summary>
        /// <returns></returns>
        public static bool IsServerConnected()
        {
            string sqlConnectString = getProperty("connectionString");
            using (var dbConnection = new SqlConnection(sqlConnectString))
            {
                try
                {
                    dbConnection.Open();
                    return true;
                }
                catch (SqlException)
                {
                    return false;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}
