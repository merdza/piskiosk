﻿namespace PisKiosk
{
    partial class Message
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelMessage = new System.Windows.Forms.Panel();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblMessageTitle = new System.Windows.Forms.Label();
            this.lblSecondsTillClose = new System.Windows.Forms.Label();
            this.panelMessage.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMessage
            // 
            this.panelMessage.Controls.Add(this.lblSecondsTillClose);
            this.panelMessage.Controls.Add(this.lblMessage);
            this.panelMessage.Location = new System.Drawing.Point(12, 100);
            this.panelMessage.Name = "panelMessage";
            this.panelMessage.Size = new System.Drawing.Size(1155, 436);
            this.panelMessage.TabIndex = 0;
            // 
            // lblMessage
            // 
            this.lblMessage.Font = new System.Drawing.Font("Segoe UI Light", 24F, System.Drawing.FontStyle.Bold);
            this.lblMessage.Location = new System.Drawing.Point(4, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(1151, 436);
            this.lblMessage.TabIndex = 0;
            this.lblMessage.Text = "label1";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnClose
            // 
            this.btnClose.FlatAppearance.BorderSize = 2;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Segoe UI Light", 50F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(1067, 50);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 100);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "X";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // lblMessageTitle
            // 
            this.lblMessageTitle.AutoSize = true;
            this.lblMessageTitle.Font = new System.Drawing.Font("Segoe UI Light", 40F, System.Drawing.FontStyle.Bold);
            this.lblMessageTitle.Location = new System.Drawing.Point(12, 67);
            this.lblMessageTitle.Name = "lblMessageTitle";
            this.lblMessageTitle.Size = new System.Drawing.Size(0, 72);
            this.lblMessageTitle.TabIndex = 2;
            this.lblMessageTitle.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lblSecondsTillClose
            // 
            this.lblSecondsTillClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSecondsTillClose.AutoSize = true;
            this.lblSecondsTillClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblSecondsTillClose.Location = new System.Drawing.Point(1103, 397);
            this.lblSecondsTillClose.Name = "lblSecondsTillClose";
            this.lblSecondsTillClose.Size = new System.Drawing.Size(27, 20);
            this.lblSecondsTillClose.TabIndex = 1;
            this.lblSecondsTillClose.Text = "10";
            this.lblSecondsTillClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Message
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1179, 548);
            this.Controls.Add(this.lblMessageTitle);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.panelMessage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Message";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Message";
            this.panelMessage.ResumeLayout(false);
            this.panelMessage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.Panel panelMessage;
        protected System.Windows.Forms.Button btnClose;
        protected System.Windows.Forms.Label lblMessage;
        protected System.Windows.Forms.Label lblMessageTitle;
        protected System.Windows.Forms.Label lblSecondsTillClose;
    }
}