﻿namespace PisKiosk
{
    partial class UserDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            PisKiosk.Office2010Blue office2010Blue1 = new PisKiosk.Office2010Blue();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDataForm));
            this.lblCardOwner = new System.Windows.Forms.Label();
            this.lblCardNumberLabel = new System.Windows.Forms.Label();
            this.lblCashAvailableLabel = new System.Windows.Forms.Label();
            this.panelMealsData = new System.Windows.Forms.Panel();
            this.lblMealsTitle = new System.Windows.Forms.Label();
            this.btnBuyFood = new PisKiosk.XButton();
            this.lblHasMealsForNextMonth = new System.Windows.Forms.Label();
            this.lblMealsSpentToday = new System.Windows.Forms.Label();
            this.lblAllowedMealsToBuy = new System.Windows.Forms.Label();
            this.lblDinner = new System.Windows.Forms.Label();
            this.lblLunch = new System.Windows.Forms.Label();
            this.lblHasMealsForNextMonthDinner = new System.Windows.Forms.Label();
            this.lblHasMealsForNextMonthLunch = new System.Windows.Forms.Label();
            this.lblHasMealsForNextMonthBreakfast = new System.Windows.Forms.Label();
            this.lblMealsSpentTodayDinner = new System.Windows.Forms.Label();
            this.lblMealsSpentTodayLunch = new System.Windows.Forms.Label();
            this.lblMealsSpentTodayBreakfast = new System.Windows.Forms.Label();
            this.lblAllowedMealsToBuyDinner = new System.Windows.Forms.Label();
            this.lblAllowedMealsToBuyLunch = new System.Windows.Forms.Label();
            this.lblAllowedMealsToBuyBreakfast = new System.Windows.Forms.Label();
            this.lblHasMealsDinner = new System.Windows.Forms.Label();
            this.lblHasMealsLunch = new System.Windows.Forms.Label();
            this.lblHasMealsBreakfast = new System.Windows.Forms.Label();
            this.lblBreakfast = new System.Windows.Forms.Label();
            this.lblHasMeals = new System.Windows.Forms.Label();
            this.btnInsertCash = new PisKiosk.XButton();
            this.lblCardNumber = new System.Windows.Forms.Label();
            this.lblCashAvailable = new System.Windows.Forms.Label();
            this.photoBox = new System.Windows.Forms.PictureBox();
            this.btnOtherServices = new PisKiosk.XButton();
            this.panelMealsData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.photoBox)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(20, 9);
            this.lblTitle.Size = new System.Drawing.Size(44, 22);
            this.lblTitle.Text = "Title - UserDataForm";
            // 
            // lblCardOwner
            // 
            this.lblCardOwner.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCardOwner.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold);
            this.lblCardOwner.Location = new System.Drawing.Point(2, 10);
            this.lblCardOwner.Name = "lblCardOwner";
            this.lblCardOwner.Size = new System.Drawing.Size(503, 109);
            this.lblCardOwner.TabIndex = 3;
            this.lblCardOwner.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblCardNumberLabel
            // 
            this.lblCardNumberLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCardNumberLabel.Font = new System.Drawing.Font("Segoe UI Light", 26F, System.Drawing.FontStyle.Bold);
            this.lblCardNumberLabel.Location = new System.Drawing.Point(12, 119);
            this.lblCardNumberLabel.Name = "lblCardNumberLabel";
            this.lblCardNumberLabel.Size = new System.Drawing.Size(493, 41);
            this.lblCardNumberLabel.TabIndex = 3;
            this.lblCardNumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCashAvailableLabel
            // 
            this.lblCashAvailableLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCashAvailableLabel.Font = new System.Drawing.Font("Segoe UI Light", 26F, System.Drawing.FontStyle.Bold);
            this.lblCashAvailableLabel.Location = new System.Drawing.Point(12, 213);
            this.lblCashAvailableLabel.Name = "lblCashAvailableLabel";
            this.lblCashAvailableLabel.Size = new System.Drawing.Size(493, 43);
            this.lblCashAvailableLabel.TabIndex = 3;
            this.lblCashAvailableLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelMealsData
            // 
            this.panelMealsData.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelMealsData.Controls.Add(this.lblMealsTitle);
            this.panelMealsData.Controls.Add(this.btnBuyFood);
            this.panelMealsData.Controls.Add(this.lblHasMealsForNextMonth);
            this.panelMealsData.Controls.Add(this.lblMealsSpentToday);
            this.panelMealsData.Controls.Add(this.lblAllowedMealsToBuy);
            this.panelMealsData.Controls.Add(this.lblDinner);
            this.panelMealsData.Controls.Add(this.lblLunch);
            this.panelMealsData.Controls.Add(this.lblHasMealsForNextMonthDinner);
            this.panelMealsData.Controls.Add(this.lblHasMealsForNextMonthLunch);
            this.panelMealsData.Controls.Add(this.lblHasMealsForNextMonthBreakfast);
            this.panelMealsData.Controls.Add(this.lblMealsSpentTodayDinner);
            this.panelMealsData.Controls.Add(this.lblMealsSpentTodayLunch);
            this.panelMealsData.Controls.Add(this.lblMealsSpentTodayBreakfast);
            this.panelMealsData.Controls.Add(this.lblAllowedMealsToBuyDinner);
            this.panelMealsData.Controls.Add(this.lblAllowedMealsToBuyLunch);
            this.panelMealsData.Controls.Add(this.lblAllowedMealsToBuyBreakfast);
            this.panelMealsData.Controls.Add(this.lblHasMealsDinner);
            this.panelMealsData.Controls.Add(this.lblHasMealsLunch);
            this.panelMealsData.Controls.Add(this.lblHasMealsBreakfast);
            this.panelMealsData.Controls.Add(this.lblBreakfast);
            this.panelMealsData.Controls.Add(this.lblHasMeals);
            this.panelMealsData.Location = new System.Drawing.Point(1, 350);
            this.panelMealsData.Name = "panelMealsData";
            this.panelMealsData.Size = new System.Drawing.Size(1147, 298);
            this.panelMealsData.TabIndex = 5;
            this.panelMealsData.Paint += new System.Windows.Forms.PaintEventHandler(this.panelMealsData_Paint);
            this.panelMealsData.Resize += new System.EventHandler(this.panelMealsData_Resize);
            // 
            // lblMealsTitle
            // 
            this.lblMealsTitle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblMealsTitle.Font = new System.Drawing.Font("Segoe UI Light", 26F, System.Drawing.FontStyle.Bold);
            this.lblMealsTitle.Location = new System.Drawing.Point(11, 13);
            this.lblMealsTitle.Name = "lblMealsTitle";
            this.lblMealsTitle.Size = new System.Drawing.Size(1122, 43);
            this.lblMealsTitle.TabIndex = 2;
            // 
            // btnBuyFood
            // 
            this.btnBuyFood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            office2010Blue1.BorderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonMouseOverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.ButtonMouseOverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonMouseOverColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            office2010Blue1.ButtonMouseOverColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            office2010Blue1.ButtonNormalColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.ButtonNormalColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonNormalColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            office2010Blue1.ButtonNormalColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            office2010Blue1.ButtonSelectedColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.ButtonSelectedColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonSelectedColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            office2010Blue1.ButtonSelectedColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            office2010Blue1.HoverTextColor = System.Drawing.Color.White;
            office2010Blue1.SelectedTextColor = System.Drawing.Color.White;
            office2010Blue1.TextColor = System.Drawing.Color.White;
            this.btnBuyFood.ColorTable = office2010Blue1;
            this.btnBuyFood.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnBuyFood.Image = ((System.Drawing.Image)(resources.GetObject("btnBuyFood.Image")));
            this.btnBuyFood.Location = new System.Drawing.Point(957, 111);
            this.btnBuyFood.Name = "btnBuyFood";
            this.btnBuyFood.Size = new System.Drawing.Size(176, 100);
            this.btnBuyFood.TabIndex = 1;
            this.btnBuyFood.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnBuyFood.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnBuyFood.UseVisualStyleBackColor = true;
            // 
            // lblHasMealsForNextMonth
            // 
            this.lblHasMealsForNextMonth.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblHasMealsForNextMonth.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblHasMealsForNextMonth.Location = new System.Drawing.Point(4, 251);
            this.lblHasMealsForNextMonth.Name = "lblHasMealsForNextMonth";
            this.lblHasMealsForNextMonth.Size = new System.Drawing.Size(389, 33);
            this.lblHasMealsForNextMonth.TabIndex = 0;
            this.lblHasMealsForNextMonth.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMealsSpentToday
            // 
            this.lblMealsSpentToday.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblMealsSpentToday.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblMealsSpentToday.Location = new System.Drawing.Point(4, 218);
            this.lblMealsSpentToday.Name = "lblMealsSpentToday";
            this.lblMealsSpentToday.Size = new System.Drawing.Size(389, 33);
            this.lblMealsSpentToday.TabIndex = 0;
            this.lblMealsSpentToday.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAllowedMealsToBuy
            // 
            this.lblAllowedMealsToBuy.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAllowedMealsToBuy.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblAllowedMealsToBuy.Location = new System.Drawing.Point(4, 185);
            this.lblAllowedMealsToBuy.Name = "lblAllowedMealsToBuy";
            this.lblAllowedMealsToBuy.Size = new System.Drawing.Size(389, 33);
            this.lblAllowedMealsToBuy.TabIndex = 0;
            this.lblAllowedMealsToBuy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDinner
            // 
            this.lblDinner.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblDinner.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblDinner.Location = new System.Drawing.Point(773, 66);
            this.lblDinner.Name = "lblDinner";
            this.lblDinner.Size = new System.Drawing.Size(190, 41);
            this.lblDinner.TabIndex = 0;
            this.lblDinner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLunch
            // 
            this.lblLunch.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblLunch.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblLunch.Location = new System.Drawing.Point(581, 66);
            this.lblLunch.Name = "lblLunch";
            this.lblLunch.Size = new System.Drawing.Size(190, 41);
            this.lblLunch.TabIndex = 0;
            this.lblLunch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHasMealsForNextMonthDinner
            // 
            this.lblHasMealsForNextMonthDinner.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblHasMealsForNextMonthDinner.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblHasMealsForNextMonthDinner.Location = new System.Drawing.Point(772, 251);
            this.lblHasMealsForNextMonthDinner.Name = "lblHasMealsForNextMonthDinner";
            this.lblHasMealsForNextMonthDinner.Size = new System.Drawing.Size(190, 33);
            this.lblHasMealsForNextMonthDinner.TabIndex = 0;
            this.lblHasMealsForNextMonthDinner.Text = "0";
            this.lblHasMealsForNextMonthDinner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHasMealsForNextMonthLunch
            // 
            this.lblHasMealsForNextMonthLunch.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblHasMealsForNextMonthLunch.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblHasMealsForNextMonthLunch.Location = new System.Drawing.Point(581, 251);
            this.lblHasMealsForNextMonthLunch.Name = "lblHasMealsForNextMonthLunch";
            this.lblHasMealsForNextMonthLunch.Size = new System.Drawing.Size(190, 33);
            this.lblHasMealsForNextMonthLunch.TabIndex = 0;
            this.lblHasMealsForNextMonthLunch.Text = "0";
            this.lblHasMealsForNextMonthLunch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHasMealsForNextMonthBreakfast
            // 
            this.lblHasMealsForNextMonthBreakfast.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblHasMealsForNextMonthBreakfast.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblHasMealsForNextMonthBreakfast.Location = new System.Drawing.Point(389, 251);
            this.lblHasMealsForNextMonthBreakfast.Name = "lblHasMealsForNextMonthBreakfast";
            this.lblHasMealsForNextMonthBreakfast.Size = new System.Drawing.Size(190, 33);
            this.lblHasMealsForNextMonthBreakfast.TabIndex = 0;
            this.lblHasMealsForNextMonthBreakfast.Text = "0";
            this.lblHasMealsForNextMonthBreakfast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMealsSpentTodayDinner
            // 
            this.lblMealsSpentTodayDinner.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblMealsSpentTodayDinner.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblMealsSpentTodayDinner.Location = new System.Drawing.Point(772, 218);
            this.lblMealsSpentTodayDinner.Name = "lblMealsSpentTodayDinner";
            this.lblMealsSpentTodayDinner.Size = new System.Drawing.Size(190, 33);
            this.lblMealsSpentTodayDinner.TabIndex = 0;
            this.lblMealsSpentTodayDinner.Text = "0";
            this.lblMealsSpentTodayDinner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMealsSpentTodayLunch
            // 
            this.lblMealsSpentTodayLunch.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblMealsSpentTodayLunch.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblMealsSpentTodayLunch.Location = new System.Drawing.Point(581, 218);
            this.lblMealsSpentTodayLunch.Name = "lblMealsSpentTodayLunch";
            this.lblMealsSpentTodayLunch.Size = new System.Drawing.Size(190, 33);
            this.lblMealsSpentTodayLunch.TabIndex = 0;
            this.lblMealsSpentTodayLunch.Text = "0";
            this.lblMealsSpentTodayLunch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMealsSpentTodayBreakfast
            // 
            this.lblMealsSpentTodayBreakfast.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblMealsSpentTodayBreakfast.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblMealsSpentTodayBreakfast.Location = new System.Drawing.Point(389, 218);
            this.lblMealsSpentTodayBreakfast.Name = "lblMealsSpentTodayBreakfast";
            this.lblMealsSpentTodayBreakfast.Size = new System.Drawing.Size(190, 33);
            this.lblMealsSpentTodayBreakfast.TabIndex = 0;
            this.lblMealsSpentTodayBreakfast.Text = "0";
            this.lblMealsSpentTodayBreakfast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAllowedMealsToBuyDinner
            // 
            this.lblAllowedMealsToBuyDinner.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblAllowedMealsToBuyDinner.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblAllowedMealsToBuyDinner.Location = new System.Drawing.Point(772, 185);
            this.lblAllowedMealsToBuyDinner.Name = "lblAllowedMealsToBuyDinner";
            this.lblAllowedMealsToBuyDinner.Size = new System.Drawing.Size(190, 33);
            this.lblAllowedMealsToBuyDinner.TabIndex = 0;
            this.lblAllowedMealsToBuyDinner.Text = "0";
            this.lblAllowedMealsToBuyDinner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAllowedMealsToBuyLunch
            // 
            this.lblAllowedMealsToBuyLunch.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblAllowedMealsToBuyLunch.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblAllowedMealsToBuyLunch.Location = new System.Drawing.Point(581, 185);
            this.lblAllowedMealsToBuyLunch.Name = "lblAllowedMealsToBuyLunch";
            this.lblAllowedMealsToBuyLunch.Size = new System.Drawing.Size(190, 33);
            this.lblAllowedMealsToBuyLunch.TabIndex = 0;
            this.lblAllowedMealsToBuyLunch.Text = "0";
            this.lblAllowedMealsToBuyLunch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAllowedMealsToBuyBreakfast
            // 
            this.lblAllowedMealsToBuyBreakfast.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblAllowedMealsToBuyBreakfast.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblAllowedMealsToBuyBreakfast.Location = new System.Drawing.Point(389, 185);
            this.lblAllowedMealsToBuyBreakfast.Name = "lblAllowedMealsToBuyBreakfast";
            this.lblAllowedMealsToBuyBreakfast.Size = new System.Drawing.Size(190, 33);
            this.lblAllowedMealsToBuyBreakfast.TabIndex = 0;
            this.lblAllowedMealsToBuyBreakfast.Text = "0";
            this.lblAllowedMealsToBuyBreakfast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHasMealsDinner
            // 
            this.lblHasMealsDinner.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblHasMealsDinner.Font = new System.Drawing.Font("Segoe UI Light", 40F, System.Drawing.FontStyle.Bold);
            this.lblHasMealsDinner.Location = new System.Drawing.Point(772, 107);
            this.lblHasMealsDinner.Name = "lblHasMealsDinner";
            this.lblHasMealsDinner.Size = new System.Drawing.Size(190, 69);
            this.lblHasMealsDinner.TabIndex = 0;
            this.lblHasMealsDinner.Text = "0";
            this.lblHasMealsDinner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHasMealsLunch
            // 
            this.lblHasMealsLunch.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblHasMealsLunch.Font = new System.Drawing.Font("Segoe UI Light", 40F, System.Drawing.FontStyle.Bold);
            this.lblHasMealsLunch.Location = new System.Drawing.Point(581, 107);
            this.lblHasMealsLunch.Name = "lblHasMealsLunch";
            this.lblHasMealsLunch.Size = new System.Drawing.Size(190, 69);
            this.lblHasMealsLunch.TabIndex = 0;
            this.lblHasMealsLunch.Text = "0";
            this.lblHasMealsLunch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHasMealsBreakfast
            // 
            this.lblHasMealsBreakfast.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblHasMealsBreakfast.Font = new System.Drawing.Font("Segoe UI Light", 40F, System.Drawing.FontStyle.Bold);
            this.lblHasMealsBreakfast.Location = new System.Drawing.Point(389, 107);
            this.lblHasMealsBreakfast.Name = "lblHasMealsBreakfast";
            this.lblHasMealsBreakfast.Size = new System.Drawing.Size(190, 69);
            this.lblHasMealsBreakfast.TabIndex = 0;
            this.lblHasMealsBreakfast.Text = "0";
            this.lblHasMealsBreakfast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBreakfast
            // 
            this.lblBreakfast.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblBreakfast.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblBreakfast.Location = new System.Drawing.Point(389, 66);
            this.lblBreakfast.Name = "lblBreakfast";
            this.lblBreakfast.Size = new System.Drawing.Size(190, 41);
            this.lblBreakfast.TabIndex = 0;
            this.lblBreakfast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHasMeals
            // 
            this.lblHasMeals.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblHasMeals.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblHasMeals.Location = new System.Drawing.Point(4, 129);
            this.lblHasMeals.Name = "lblHasMeals";
            this.lblHasMeals.Size = new System.Drawing.Size(389, 33);
            this.lblHasMeals.TabIndex = 0;
            this.lblHasMeals.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnInsertCash
            // 
            this.btnInsertCash.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnInsertCash.ColorTable = office2010Blue1;
            this.btnInsertCash.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnInsertCash.Image = ((System.Drawing.Image)(resources.GetObject("btnInsertCash.Image")));
            this.btnInsertCash.Location = new System.Drawing.Point(958, 43);
            this.btnInsertCash.Name = "btnInsertCash";
            this.btnInsertCash.Size = new System.Drawing.Size(176, 100);
            this.btnInsertCash.TabIndex = 1;
            this.btnInsertCash.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnInsertCash.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnInsertCash.UseVisualStyleBackColor = true;
            // 
            // lblCardNumber
            // 
            this.lblCardNumber.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCardNumber.Font = new System.Drawing.Font("Segoe UI Light", 26F, System.Drawing.FontStyle.Bold);
            this.lblCardNumber.Location = new System.Drawing.Point(12, 160);
            this.lblCardNumber.Name = "lblCardNumber";
            this.lblCardNumber.Size = new System.Drawing.Size(493, 39);
            this.lblCardNumber.TabIndex = 3;
            this.lblCardNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCashAvailable
            // 
            this.lblCashAvailable.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCashAvailable.Font = new System.Drawing.Font("Segoe UI Light", 26F, System.Drawing.FontStyle.Bold);
            this.lblCashAvailable.Location = new System.Drawing.Point(12, 256);
            this.lblCashAvailable.Name = "lblCashAvailable";
            this.lblCashAvailable.Size = new System.Drawing.Size(493, 40);
            this.lblCashAvailable.TabIndex = 3;
            this.lblCashAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // photoBox
            // 
            this.photoBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.photoBox.Location = new System.Drawing.Point(646, 12);
            this.photoBox.Name = "photoBox";
            this.photoBox.Size = new System.Drawing.Size(260, 320);
            this.photoBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.photoBox.TabIndex = 4;
            this.photoBox.TabStop = false;
            // 
            // btnOtherServices
            // 
            this.btnOtherServices.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOtherServices.ColorTable = office2010Blue1;
            this.btnOtherServices.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnOtherServices.Image = ((System.Drawing.Image)(resources.GetObject("btnOtherServices.Image")));
            this.btnOtherServices.Location = new System.Drawing.Point(958, 189);
            this.btnOtherServices.Name = "btnOtherServices";
            this.btnOtherServices.Size = new System.Drawing.Size(176, 100);
            this.btnOtherServices.TabIndex = 6;
            this.btnOtherServices.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnOtherServices.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnOtherServices.UseVisualStyleBackColor = true;
            // 
            // UserDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1150, 650);
            this.Controls.Add(this.lblCashAvailableLabel);
            this.Controls.Add(this.lblCardNumberLabel);
            this.Controls.Add(this.btnOtherServices);
            this.Controls.Add(this.btnInsertCash);
            this.Controls.Add(this.panelMealsData);
            this.Controls.Add(this.photoBox);
            this.Controls.Add(this.lblCashAvailable);
            this.Controls.Add(this.lblCardNumber);
            this.Controls.Add(this.lblCardOwner);
            this.Name = "UserDataForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Controls.SetChildIndex(this.lblCardOwner, 0);
            this.Controls.SetChildIndex(this.lblCardNumber, 0);
            this.Controls.SetChildIndex(this.lblCashAvailable, 0);
            this.Controls.SetChildIndex(this.photoBox, 0);
            this.Controls.SetChildIndex(this.panelMealsData, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.btnInsertCash, 0);
            this.Controls.SetChildIndex(this.btnOtherServices, 0);
            this.Controls.SetChildIndex(this.lblCardNumberLabel, 0);
            this.Controls.SetChildIndex(this.lblCashAvailableLabel, 0);
            this.panelMealsData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.photoBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Label lblCardOwner;
        protected System.Windows.Forms.PictureBox photoBox;
        protected System.Windows.Forms.Label lblCardNumberLabel;
        protected System.Windows.Forms.Label lblCashAvailableLabel;
        protected System.Windows.Forms.Panel panelMealsData;
        protected System.Windows.Forms.Label lblHasMeals;
        protected System.Windows.Forms.Label lblMealsSpentToday;
        protected System.Windows.Forms.Label lblAllowedMealsToBuy;
        protected System.Windows.Forms.Label lblHasMealsForNextMonth;
        protected System.Windows.Forms.Label lblBreakfast;
        protected System.Windows.Forms.Label lblDinner;
        protected System.Windows.Forms.Label lblLunch;
        protected System.Windows.Forms.Label lblCardNumber;
        protected System.Windows.Forms.Label lblCashAvailable;
        protected System.Windows.Forms.Label lblHasMealsBreakfast;
        protected System.Windows.Forms.Label lblAllowedMealsToBuyBreakfast;
        protected System.Windows.Forms.Label lblMealsSpentTodayBreakfast;
        protected System.Windows.Forms.Label lblHasMealsForNextMonthBreakfast;
        protected System.Windows.Forms.Label lblHasMealsDinner;
        protected System.Windows.Forms.Label lblHasMealsLunch;
        protected System.Windows.Forms.Label lblAllowedMealsToBuyLunch;
        protected System.Windows.Forms.Label lblAllowedMealsToBuyDinner;
        protected System.Windows.Forms.Label lblMealsSpentTodayLunch;
        protected System.Windows.Forms.Label lblHasMealsForNextMonthLunch;
        protected System.Windows.Forms.Label lblMealsSpentTodayDinner;
        protected System.Windows.Forms.Label lblHasMealsForNextMonthDinner;
        protected XButton btnBuyFood;
        protected XButton btnInsertCash;
        protected XButton btnOtherServices;
        protected System.Windows.Forms.Label lblMealsTitle;
    }
}
