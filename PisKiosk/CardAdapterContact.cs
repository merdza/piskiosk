﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace PisKiosk
{
    public class CardAdapterContact : IStudentCard
    {
        static object locker = new object();
        protected const string dllName = "idcap.dll";

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int getErrorCode();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern void startListener();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern void stopListener();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int isCardPresent();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int getCardType();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int isLoggedIn();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern void login([MarshalAs(UnmanagedType.BStr)] string username, [MarshalAs(UnmanagedType.BStr)] string password);
        
        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern void logout();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern void getData();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public static extern string getCardSerial();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public static extern string getCardSerialNumber();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public static extern string getFirstName();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public static extern string getLastName();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public static extern string getPersonalNumber();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public static extern string getPhotoName();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public static extern string getCardNumber();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int getExpiryDateDay();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int getExpiryDateMonth();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int getExpiryDateYear();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int getOperaterID();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern string getOfficialCardNumber();

        /*
        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int changeUserPin([MarshalAs(UnmanagedType.BStr)] string oldPin, [MarshalAs(UnmanagedType.BStr)] string newPin);

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int isUserPinChanged();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int changeUserPinInitially([MarshalAs(UnmanagedType.BStr)] string newPin);

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int checkUserPin([MarshalAs(UnmanagedType.BStr)] string pin);

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern double getCashBalance();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int incCash(double amount, [MarshalAs(UnmanagedType.BStr)] string pin);

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern int decCash(double amount, [MarshalAs(UnmanagedType.BStr)] string pin);
        */


        public virtual void initializeCommunicationAdapter()
        {
            lock (locker)
            {
                startListener();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
            }
        }

        public void finalizeCommunicationAdapter()
        {
            lock (locker)
            {
                try
                {
                    stopListener();
                }
                catch (Exception)
                {
                    throw new Exception();
                }
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
            }
        }

        public int isCardPresentAdapter()
        {
            lock (locker)
            {
                int result = isCardPresent();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public CardType getCardTypeAdapter()
        {
            lock (locker)
            {
                CardType ret = CardType.Student;
                int result = getCardType();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }

                if (result == 2)
                {
                    ret = CardType.Official;
                }

                return ret;
            }
        }

        public int isLoggedInAdapter()
        {
            lock (locker)
            {
                int result = isLoggedIn();
                int err = getErrorCode();

                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public void loginAdapter(string username, string password)
        {
            lock (locker)
            {
                login(username, password);
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
            }
        }

        public void logoutAdapter()
        {
            lock (locker)
            {
                logout();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
            }
        }

        public void getDataAdapter()
        {
            try
            {
                getData();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string getCardSerialAdapter()
        {
            lock (locker)
            {
                string result = string.Empty;

                switch (Program.Kiosk)
                {
                    
                    case 1:
                        //SC Bgd
                        result = getCardSerial();
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        //sc ns
                        result = getCardSerialNumber();
                        break;
                    default:
                        throw new Exception("Card serial number could not be called.");

                }


                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public string getFirstNameAdapter()
        {
            lock (locker)
            {
                string result = getFirstName();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public string getLastNameAdapter()
        {
            lock (locker)
            {
                string result = getLastName();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public string getCardOwnerNameAdapter()
        {
            try
            {
                string result = getFirstNameAdapter() + " " + getLastNameAdapter();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string getPersonalNumberAdapter()
        {
            lock (locker)
            {
                string result = getPersonalNumber();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public string getPhotoNameAdapter()
        {
            lock (locker)
            {
                string result = getPhotoName();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public string getCardNumberAdapter()
        {
            lock (locker)
            {
                string result = getCardNumber();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public int getExpiryDateDayAdapter()
        {
            lock (locker)
            {
                int result = getExpiryDateDay();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public int getExpiryDateMonthAdapter()
        {
            lock (locker)
            {
                int result = getExpiryDateMonth();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public int getExpiryDateYearAdapter()
        {
            lock (locker)
            {
                int result = getExpiryDateYear();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public int getOperaterIDAdapter()
        {
            lock (locker)
            {
                int result = getOperaterID();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public string getOfficialCardNumberAdapter()
        {
            lock (locker)
            {
                string result = getOfficialCardNumber();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }
        
        /*
        public double getCashBalanceAdapter()
        {
            try
            {
                double balance = getCashBalance();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return balance;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int checkUserPinAdapter(string pin)
        {
            try
            {
                int pinOk = checkUserPin(pin);
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return pinOk;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int decCashAdapter(double amount, string pin)
        {
            try
            {
                int pinOk = decCash(amount, pin);
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return pinOk;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int incCashAdapter(double amount, string pin)
        {
            try
            {
                int pinOk = incCash(amount, pin);
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return pinOk;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        */
    }
}
