﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class UserDataForm : PisKiosk.MainForm
    {
        protected UserDataForm()
        {
            InitializeComponent();
            //this.TopMost = true;

            LangsVisible = true;
            ReactOnCardRemoved = true;

            BackColor = Program.KioskApplication.BackgroundColor;
            //btnBuyFood.BackColor = Program.KioskApplication.TextForeColor2;
            //btnBuyFood.ForeColor = Program.KioskApplication.BackgroundColor;

            btnBuyFood.BackColor = Color.Yellow;
            btnBuyFood.ForeColor = Color.Red;

            btnInsertCash.BackColor = Program.KioskApplication.TextForeColor2;
            btnInsertCash.ForeColor = Program.KioskApplication.BackgroundColor;

            btnOtherServices.BackColor = Program.KioskApplication.TextForeColor2;
            btnOtherServices.ForeColor = Program.KioskApplication.BackgroundColor;
            
            lblCardOwner.BackColor = Program.KioskApplication.BackgroundColor;
            lblCardOwner.ForeColor = Program.KioskApplication.TextForeColor;

            lblCardNumberLabel.BackColor = Program.KioskApplication.BackgroundColor;
            lblCardNumberLabel.ForeColor = Program.KioskApplication.TextForeColor2;

            lblCardNumber.BackColor = Program.KioskApplication.BackgroundColor;
            lblCardNumber.ForeColor = Program.KioskApplication.TextForeColor;

            lblCashAvailableLabel.BackColor = Program.KioskApplication.BackgroundColor;
            lblCashAvailableLabel.ForeColor = Program.KioskApplication.TextForeColor2;

            lblCashAvailable.BackColor = Program.KioskApplication.BackgroundColor;
            lblCashAvailable.ForeColor = Program.KioskApplication.TextForeColor;

            lblMealsTitle.ForeColor = Program.KioskApplication.TextForeColor2;
            lblBreakfast.ForeColor = Program.KioskApplication.TextForeColor2;
            lblLunch.ForeColor = Program.KioskApplication.TextForeColor2;
            lblDinner.ForeColor = Program.KioskApplication.TextForeColor2;
            lblHasMeals.ForeColor = Program.KioskApplication.TextForeColor2;
            lblAllowedMealsToBuy.ForeColor = Program.KioskApplication.TextForeColor2;
            lblMealsSpentToday.ForeColor = Program.KioskApplication.TextForeColor2;
            lblHasMealsForNextMonth.ForeColor = Program.KioskApplication.TextForeColor2;

            lblMealsTitle.BackColor = Program.KioskApplication.BackgroundColor;
            lblBreakfast.BackColor = Program.KioskApplication.BackgroundColor;
            lblLunch.BackColor = Program.KioskApplication.BackgroundColor;
            lblDinner.BackColor = Program.KioskApplication.BackgroundColor;
            lblHasMeals.BackColor = Program.KioskApplication.BackgroundColor;
            lblAllowedMealsToBuy.BackColor = Program.KioskApplication.BackgroundColor;
            lblMealsSpentToday.BackColor = Program.KioskApplication.BackgroundColor;
            lblHasMealsForNextMonth.BackColor = Program.KioskApplication.BackgroundColor;

            lblHasMealsBreakfast.ForeColor = Program.KioskApplication.TextForeColor;
            lblAllowedMealsToBuyBreakfast.ForeColor = Program.KioskApplication.TextForeColor;
            lblMealsSpentTodayBreakfast.ForeColor = Program.KioskApplication.TextForeColor;
            lblHasMealsForNextMonthBreakfast.ForeColor = Program.KioskApplication.TextForeColor;

            lblHasMealsLunch.ForeColor = Program.KioskApplication.TextForeColor;
            lblAllowedMealsToBuyLunch.ForeColor = Program.KioskApplication.TextForeColor;
            lblMealsSpentTodayLunch.ForeColor = Program.KioskApplication.TextForeColor;
            lblHasMealsForNextMonthLunch.ForeColor = Program.KioskApplication.TextForeColor;

            lblHasMealsDinner.ForeColor = Program.KioskApplication.TextForeColor;
            lblAllowedMealsToBuyDinner.ForeColor = Program.KioskApplication.TextForeColor;
            lblMealsSpentTodayDinner.ForeColor = Program.KioskApplication.TextForeColor;
            lblHasMealsForNextMonthDinner.ForeColor = Program.KioskApplication.TextForeColor;

            lblHasMealsBreakfast.BackColor = Program.KioskApplication.BackgroundColor;
            lblAllowedMealsToBuyBreakfast.BackColor = Program.KioskApplication.BackgroundColor;
            lblMealsSpentTodayBreakfast.BackColor = Program.KioskApplication.BackgroundColor;
            lblHasMealsForNextMonthBreakfast.BackColor = Program.KioskApplication.BackgroundColor;

            lblHasMealsLunch.BackColor = Program.KioskApplication.BackgroundColor;
            lblAllowedMealsToBuyLunch.BackColor = Program.KioskApplication.BackgroundColor;
            lblMealsSpentTodayLunch.BackColor = Program.KioskApplication.BackgroundColor;
            lblHasMealsForNextMonthLunch.BackColor = Program.KioskApplication.BackgroundColor;

            lblHasMealsDinner.BackColor = Program.KioskApplication.BackgroundColor;
            lblAllowedMealsToBuyDinner.BackColor = Program.KioskApplication.BackgroundColor;
            lblMealsSpentTodayDinner.BackColor = Program.KioskApplication.BackgroundColor;
            lblHasMealsForNextMonthDinner.BackColor = Program.KioskApplication.BackgroundColor;
        }

        protected UserData userData;
        protected static HashSet<string> pictures;

        public bool DataShownOnScreen { get; protected set; }

        //private static UserDataForm instance = null;
        //public static UserDataForm getInstance()
        //{
        //    if (instance == null)
        //    {
        //        instance = new UserDataForm();
        //    }
        //    return instance;
        //}

        public virtual void readCardData() {}

        public virtual void showUserData()
        {
            lblCardOwner.Text = userData.UserName;
            lblCardNumberLabel.Text = guiLang.UserDataForm_Label_lblCardNumberLabel;
            lblCardNumber.Text = userData.CardNumber;
            lblCashAvailableLabel.Text = guiLang.UserDataForm_Label_lblCashAvailableLabel;
            lblCashAvailable.Text = Utils.getCurrencyString(getCashFromCard(), true);

            //todo
            lblHasMealsBreakfast.Text = "5";
            lblAllowedMealsToBuyBreakfast.Text = "5";
            lblMealsSpentTodayBreakfast.Text = "5";
            lblHasMealsForNextMonthBreakfast.Text = "5";

            lblHasMealsLunch.Text = "88";
            lblAllowedMealsToBuyLunch.Text = "88";
            lblMealsSpentTodayLunch.Text = "88";
            lblHasMealsForNextMonthLunch.Text = "88";

            lblHasMealsDinner.Text = "00";
            lblAllowedMealsToBuyDinner.Text = "00";
            lblMealsSpentTodayDinner.Text = "0";
            lblHasMealsForNextMonthDinner.Text = "0";

            showPhoto(userData.Photo);

            this.Refresh();
            Application.DoEvents();

            //Program.KioskApplication.writeToLog("Inserted card: " + userData.CardNumber + ", " + userData.UserName + ", cash available: " + lblCashAvailable.Text, PisLog.EventType.CardInserted); 
        }

        public virtual void clearUserData()
        {
            lblCardOwner.Text = "";
            lblCardNumber.Text = "";
            lblCashAvailable.Text = "";

            //todo
            lblHasMealsBreakfast.Text = "";
            lblAllowedMealsToBuyBreakfast.Text = "";
            lblMealsSpentTodayBreakfast.Text = "";
            lblHasMealsForNextMonthBreakfast.Text = "";

            lblHasMealsLunch.Text = "";
            lblAllowedMealsToBuyLunch.Text = "";
            lblMealsSpentTodayLunch.Text = "";
            lblHasMealsForNextMonthLunch.Text = "";

            lblHasMealsDinner.Text = "";
            lblAllowedMealsToBuyDinner.Text = "";
            lblMealsSpentTodayDinner.Text = "";
            lblHasMealsForNextMonthDinner.Text = "";

            showPhoto("");

            this.Refresh();
            Application.DoEvents();
        }

        public virtual double getCashFromCard()
        {
            double cashOnCard = 0;

            cashOnCard = userData.CashOnCard;

            return cashOnCard;
        }

        protected void showPhoto(string photoName)
        {
            //prikazivanje slike
            string photo = "";
            string defaultPhoto = "";
            try
            {
                photo = XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "photoPath") + photoName + ".jpg";
                defaultPhoto = XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "defaultPhoto");

                if (photoName.Equals(""))
                {
                    throw new Exception();
                }

                if (pictures.Contains(photoName + ".jpg"))
                {
                    // postoji fotografija u hash setu
                    photoBox.Load(photo);
                }
                else
                {
                    //                    LogFile.writeLog("Error in displaying photo. The HashSet does not contain photo: " + photoName, "Error");
                    photoBox.Load(defaultPhoto);
                }
            }
            catch (Exception)
            {
                //                LogFile.writeLog("Error in displaying photo " + photoName + ": " + ex.Message, "Error");
                photoBox.Load(defaultPhoto);
            }
            Thread.Sleep(300);  //da mu damo vremena da stigne da uradi load
        }

        //todo vidi gde treba da se nalazi ova funkcija - pre ce biti da treba da ide na glavnu formu
        protected void makePicturesHashSet()
        {
            pictures = new HashSet<string>();

            string photoDirectory = XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "photoPath");

            Uri uri;
            string filename;

            if (System.IO.Directory.Exists(photoDirectory))
            {
                try
                {
                    string[] files = System.IO.Directory.GetFiles(photoDirectory);

                    foreach (string s in files)
                    {
                        uri = new Uri(s);
                        filename = Path.GetFileName(uri.LocalPath);
                        pictures.Add(filename);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error in making photos hashset. " + ex.Message);
                }
            }
            else
            {
                throw new Exception("Photos directory does not exist!");
            }
            //LogFile.writeLog("Photos hashset made successfully.", "RegularEvent");
        }

        public void setLabels()
        {
            btnBuyFood.Text = guiLang.UserDataForm_Button_btnBuyFood;
            btnInsertCash.Text = guiLang.UserDataForm_Button_btnInsertCash;
            btnOtherServices.Text = guiLang.UserDataForm_Button_btnOtherServices;

            lblMealsTitle.Text = guiLang.UserDataForm_Label_lblMealsTitle.ToUpper() + " " + Utils.getMonthString(DateTime.Now.Month).ToUpper();
            lblBreakfast.Text = guiLang.Breakfast.ToUpper();
            lblLunch.Text = guiLang.Lunch.ToUpper();
            lblDinner.Text = guiLang.Dinner.ToUpper();
            lblHasMeals.Text = guiLang.UserDataForm_Label_lblHasMeals;
            lblAllowedMealsToBuy.Text = guiLang.UserDataForm_Label_lblAllowedMealsToBuy;
            lblMealsSpentToday.Text = guiLang.UserDataForm_Label_lblMealsSpentToday;
            lblHasMealsForNextMonth.Text = guiLang.UserDataForm_Label_lblHasMealsForNextMonth + " " + Utils.getMonthString(DateTime.Now.Month + 1).ToUpper();

            this.Refresh();
            Application.DoEvents();
        }

        private void panelMealsData_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.panelMealsData.ClientRectangle, Color.White, ButtonBorderStyle.Solid);
        }

        private void panelMealsData_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }


    }
}
