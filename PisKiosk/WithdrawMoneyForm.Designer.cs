﻿namespace PisKiosk
{
    partial class WithdrawMoneyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            PisKiosk.Office2010Blue office2010Blue1 = new PisKiosk.Office2010Blue();
            this.lblMoneyInKiosk = new System.Windows.Forms.Label();
            this.lblMoneyInKioskToday = new System.Windows.Forms.Label();
            this.btnWithdrawMoney = new PisKiosk.XButton();
            this.btnCancel = new PisKiosk.XButton();
            this.lblMoneyWithdrawn = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(0, 23);
            this.lblTitle.Size = new System.Drawing.Size(150, 68);
            // 
            // lblMoneyInKiosk
            // 
            this.lblMoneyInKiosk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneyInKiosk.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblMoneyInKiosk.Location = new System.Drawing.Point(42, 108);
            this.lblMoneyInKiosk.Name = "lblMoneyInKiosk";
            this.lblMoneyInKiosk.Size = new System.Drawing.Size(1061, 39);
            this.lblMoneyInKiosk.TabIndex = 10;
            this.lblMoneyInKiosk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMoneyInKioskToday
            // 
            this.lblMoneyInKioskToday.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneyInKioskToday.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblMoneyInKioskToday.Location = new System.Drawing.Point(42, 187);
            this.lblMoneyInKioskToday.Name = "lblMoneyInKioskToday";
            this.lblMoneyInKioskToday.Size = new System.Drawing.Size(1061, 39);
            this.lblMoneyInKioskToday.TabIndex = 10;
            this.lblMoneyInKioskToday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnWithdrawMoney
            // 
            this.btnWithdrawMoney.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnWithdrawMoney.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(97)))), ((int)(((byte)(54)))));
            office2010Blue1.BorderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonMouseOverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            office2010Blue1.ButtonMouseOverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            office2010Blue1.ButtonMouseOverColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(137)))));
            office2010Blue1.ButtonMouseOverColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(249)))), ((int)(((byte)(224)))));
            office2010Blue1.ButtonNormalColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.ButtonNormalColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonNormalColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            office2010Blue1.ButtonNormalColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            office2010Blue1.ButtonSelectedColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            office2010Blue1.ButtonSelectedColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            office2010Blue1.ButtonSelectedColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(117)))));
            office2010Blue1.ButtonSelectedColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(107)))));
            office2010Blue1.HoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            office2010Blue1.SelectedTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            office2010Blue1.TextColor = System.Drawing.Color.White;
            this.btnWithdrawMoney.ColorTable = office2010Blue1;
            this.btnWithdrawMoney.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.btnWithdrawMoney.ForeColor = System.Drawing.Color.White;
            this.btnWithdrawMoney.Location = new System.Drawing.Point(158, 469);
            this.btnWithdrawMoney.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnWithdrawMoney.Name = "btnWithdrawMoney";
            this.btnWithdrawMoney.Size = new System.Drawing.Size(368, 134);
            this.btnWithdrawMoney.TabIndex = 11;
            this.btnWithdrawMoney.Text = "Withdraw money";
            this.btnWithdrawMoney.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnWithdrawMoney.UseVisualStyleBackColor = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(97)))), ((int)(((byte)(54)))));
            this.btnCancel.ColorTable = office2010Blue1;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(636, 469);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(368, 134);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // lblMoneyWithdrawn
            // 
            this.lblMoneyWithdrawn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneyWithdrawn.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblMoneyWithdrawn.Location = new System.Drawing.Point(42, 266);
            this.lblMoneyWithdrawn.Name = "lblMoneyWithdrawn";
            this.lblMoneyWithdrawn.Size = new System.Drawing.Size(1061, 39);
            this.lblMoneyWithdrawn.TabIndex = 12;
            this.lblMoneyWithdrawn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WithdrawMoneyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1150, 650);
            this.Controls.Add(this.lblMoneyWithdrawn);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnWithdrawMoney);
            this.Controls.Add(this.lblMoneyInKioskToday);
            this.Controls.Add(this.lblMoneyInKiosk);
            this.Name = "WithdrawMoneyForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Controls.SetChildIndex(this.lblMoneyInKiosk, 0);
            this.Controls.SetChildIndex(this.lblMoneyInKioskToday, 0);
            this.Controls.SetChildIndex(this.btnWithdrawMoney, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblMoneyWithdrawn, 0);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Label lblMoneyInKiosk;
        protected System.Windows.Forms.Label lblMoneyInKioskToday;
        protected XButton btnWithdrawMoney;
        protected XButton btnCancel;
        protected System.Windows.Forms.Label lblMoneyWithdrawn;


    }
}
