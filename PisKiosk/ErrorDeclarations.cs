using System;
using System.Collections.Generic;
using System.Text;

namespace PisKiosk
{
    public class ErrorDeclarations
    {
        /*

	static const int WALLET_SELECT_FAILED						= 3001;  
	static const int WALLET_GET_ABONENT_FAILED					= 3002;  
	static const int WALLET_GET_EATEN_MEALS_FAILED				= 3003;  
	static const int WALLET_GET_ABONENT_UPDATED_DATE_FAILED		= 3004;
	static const int WALLET_GET_PERSONAL_LENGTH_FAILED			= 3005;
	static const int WALLET_GET_PERSONAL_FAILED					= 3006;
	static const int WALLET_GET_FACULTY_FAILED					= 3007;
	static const int WALLET_GET_SCHOOLYEARID_FAILED				= 3008;
	static const int WALLET_GET_BOARDING_INFO_FAILED			= 3009;
	static const int WALLET_GET_BOARDING_UPDATED_DATE_FAILED	= 3010;
	static const int WALLET_GET_CASH_FAILED						= 3011;
	static const int WALLET_GET_CARD_FAILED						= 3012;
	static const int WALLET_VERIFY_ADMIN_PIN_FAILED				= 3013;
	static const int WALLET_VERIFY_RESTAURANT_PIN_FAILED		= 3014;
	static const int WALLET_SET_PERSONAL_FAILED					= 3015;
	static const int WALLET_SET_FACULTY_FAILED					= 3016;
	static const int WALLET_SET_SCHOOLYEARID_FAILED				= 3017;
	static const int WALLET_SET_ABONENT_FAILED					= 3018;
	static const int WALLET_SET_EATEN_MEALS_FAILED				= 3019;
	static const int WALLET_SET_ABONENT_UPDATED_DATE_FAILED		= 3020;
	static const int WALLET_SET_CARD_FAILED						= 3021;
	static const int WALLET_SET_BOARDING_INFO_FAILED			= 3022;
	static const int WALLET_SET_BOARDING_UPDATED_DATE_FAILED	= 3023;
	static const int WALLET_SET_CASH_FAILED						= 3024;
	static const int WALLET_UPDATE_ADMIN_PIN_FAILED				= 3025;
	static const int WALLET_UPDATE_CASHIER_PIN_FAILED			= 3026;
	static const int WALLET_UPDATE_RESTAURANT_PIN_FAILED		= 3027;
	static const int WALLET_UPDATE_BOARDING_PIN_FAILED			= 3028;
	static const int WALLET_GET_TRANSACTION_AMOUNT_FAILED		= 3029;
	static const int WALLET_INC_TRANSACTION_AMOUNT_FAILED		= 3030;
	static const int WALLET_GET_ADDITIONAL_DATA_FAILED			= 3031;
	static const int WALLET_SET_ADDITIONAL_DATA_FAILED			= 3032;
	static const int WALLET_DECREASE_BALANCE_FAILED				= 3033;

	static const int INFO_ADDITIONAL_DATA_NOT_SET				= 3501;
		
	static const int OFFICIALID_SELECT_FAILED					= 4001;  
	static const int OFFICIALID_VERIFY_OFFICIAL_PIN_FAILED		= 4002;  
	static const int OFFICIALID_UPDATE_OFFICIAL_PIN_FAILED		= 4003;  
	static const int OFFICIALID_GET_OFFICIAL_PIN_INFO_FAILED	= 4004;  
	static const int OFFICIALID_VERIFY_ADMIN_PIN_FAILED			= 4005;  
	static const int OFFICIALID_UPDATE_ADMIN_PIN_FAILED			= 4006;  
	static const int OFFICIALID_GET_ADMIN_PIN_INFO_FAILED		= 4007;  
	static const int OFFICIALID_GET_CARD_FAILED					= 4008;  
	static const int OFFICIALID_SET_CARD_FAILED					= 4009;  
	static const int OFFICIALID_GET_SECURITY_DESCRIPTOR_FAILED	= 4010;  
	static const int OFFICIALID_SET_SECURITY_DESCRIPTOR_FAILED	= 4011;  
	static const int OFFICIALID_GET_SYSTEM_ID_FAILED			= 4012;  
	static const int OFFICIALID_SET_SYSTEM_ID_FAILED			= 4013;  
	static const int OFFICIALID_GET_RESTAURANT_SECRET_FAILED	= 4014;  
	static const int OFFICIALID_GET_CASHIER_SECRET_FAILED		= 4015;
	static const int OFFICIALID_GET_BOARDING_SECRET_FAILED		= 4016;
	static const int OFFICIALID_GET_ADMIN_SECRET_FAILED			= 4017;
	static const int OFFICIALID_SET_RESTAURANT_SECRET_FAILED	= 4018;
	static const int OFFICIALID_SET_CASHIER_SECRET_FAILED		= 4019;
	static const int OFFICIALID_SET_BOARDING_SECRET_FAILED		= 4020;
	static const int OFFICIALID_SET_ADMIN_SECRET_FAILED			= 4021;
	static const int OFFICIALID_UNBLOCK_OFFICIAL_PIN_FAILED		= 4022;

	static const int THREAD_ALREADY_STARTED						= 5001;
	static const int NOT_IMPLEMENTING_IRUNNABLE					= 5002;
	static const int THREAD_START								= 5003;
	static const int THREAD_SUSPEND								= 5004;
	static const int THREAD_RESUME								= 5005;
	static const int THREAD_JOIN								= 5006;
	static const int THREAD_NOT_CREATED							= 5007;
	static const int NO_THREAD_ALIVE							= 5008;

	static const int CARD_OBSERVER_ESTABLISHING_CONTEXT_FAILED	= 6001;
	static const int CARD_OBSERVER_RELEASING_CONTEXT_FAILED		= 6002;
	static const int CARD_OBSERVER_GET_STATUS_CHANGE_FAILED		= 6003;

	static const int INVALID_FILE_NUMBER_LENGTH					= 7001;
	static const int INVALID_HEALTH_STRING_LENGTH				= 7002;
	static const int PERSONAL_BASE_CHANGED						= 7003;
	static const int HEALTH_DATA_TOO_LONG						= 7004;
	static const int INVALID_ACCOMMODATION_PLACE_LENGTH			= 7005;

	static const int SELECT_CARD_MANAGER_FAILED					= 8001;	
	
};
             */
        public const int PROCESS_IN_PROGRESS = 1001;
        public const int TEST_PERIOD_EXPIRED = 1002;
        public const int CARD_TYPE_UNKNOWN = 1003;
        public const int UNALLOWED_CASH_INCREASING = 1004;
        public const int UNALLOWED_ABONENT_INCREASING = 1005;
        public const int UNALLOWED_CARD_NUMBER_CHANGE = 1006;
        public const int NOT_LOGGED_IN = 1007;
        public const int CARD_ALREADY_PERSONALIZED = 1008;
        public const int IS_LOGGED_IN_FAILED = 1009;

        public const int READER_OPEN_FAILED = 2001;
        public const int READER_CLOSE_FAILED = 2002;
        public const int READER_SEND_FAILED = 2003;
        public const int AVAILABLE_READERS_FAILED = 2004;
        public const int READER_CARD_CONNECT_FAILED = 2005;
        public const int READER_CARD_STATUS_FAILED = 2006;
        public const int READER_CARD_DISCONNECT_FAILED = 2007;
        public const int READER_LIST_READERS_FAILED = 2008;
        public const int READER_BEGIN_TRANSACTION_FAILED = 2009;
        public const int READER_END_TRANSACTION_FAILED = 2010;

        public const int WALLET_SELECT_FAILED = 3001;
        public const int WALLET_GET_ABONENT_FAILED = 3002;
        public const int WALLET_GET_EATEN_MEALS_FAILED = 3003;
        public const int WALLET_GET_ABONENT_UPDATED_DATE_FAILED = 3004;
        public const int WALLET_GET_PERSONAL_LENGTH_FAILED = 3005;
        public const int WALLET_GET_PERSONAL_FAILED = 3006;
        public const int WALLET_GET_FACULTY_FAILED = 3007;
        public const int WALLET_GET_SCHOOLYEARID_FAILED = 3008;
        public const int WALLET_GET_BOARDING_INFO_FAILED = 3009;
        public const int WALLET_GET_BOARDING_UPDATED_DATE_FAILED = 3010;
        public const int WALLET_GET_CASH_FAILED = 3011;
        public const int WALLET_GET_CARD_FAILED = 3012;
        public const int WALLET_VERIFY_ADMIN_PIN_FAILED = 3013;
        public const int WALLET_VERIFY_RESTAURANT_PIN_FAILED = 3014;
        public const int WALLET_SET_PERSONAL_FAILED = 3015;
        public const int WALLET_SET_FACULTY_FAILED = 3016;
        public const int WALLET_SET_SCHOOLYEARID_FAILED = 3017;
        public const int WALLET_SET_ABONENT_FAILED = 3018;
        public const int WALLET_SET_EATEN_MEALS_FAILED = 3019;
        public const int WALLET_SET_ABONENT_UPDATED_DATE_FAILED = 3020;
        public const int WALLET_SET_CARD_FAILED = 3021;
        public const int WALLET_SET_BOARDING_INFO_FAILED = 3022;
        public const int WALLET_SET_BOARDING_UPDATED_DATE_FAILED = 3023;
        public const int WALLET_SET_CASH_FAILED = 3024;
        public const int WALLET_UPDATE_ADMIN_PIN_FAILED = 3025;
        public const int WALLET_UPDATE_CASHIER_PIN_FAILED = 3026;
        public const int WALLET_UPDATE_RESTAURANT_PIN_FAILED = 3027;
        public const int WALLET_UPDATE_BOARDING_PIN_FAILED = 3028;

        public const int OFFICIALID_SELECT_FAILED = 4001;
        public const int OFFICIALID_VERIFY_OFFICIAL_PIN_FAILED = 4002;
        public const int OFFICIALID_UPDATE_OFFICIAL_PIN_FAILED = 4003;
        public const int OFFICIALID_GET_OFFICIAL_PIN_INFO_FAILED = 4004;
        public const int OFFICIALID_VERIFY_ADMIN_PIN_FAILED = 4005;
        public const int OFFICIALID_UPDATE_ADMIN_PIN_FAILED = 4006;
        public const int OFFICIALID_GET_ADMIN_PIN_INFO_FAILED = 4007;
        public const int OFFICIALID_GET_CARD_FAILED = 4008;
        public const int OFFICIALID_SET_CARD_FAILED = 4009;
        public const int OFFICIALID_GET_SECURITY_DESCRIPTOR_FAILED = 4010;
        public const int OFFICIALID_SET_SECURITY_DESCRIPTOR_FAILED = 4011;
        public const int OFFICIALID_GET_SYSTEM_ID_FAILED = 4012;
        public const int OFFICIALID_SET_SYSTEM_ID_FAILED = 4013;
        public const int OFFICIALID_GET_RESTAURANT_SECRET_FAILED = 4014;
        public const int OFFICIALID_GET_CASHIER_SECRET_FAILED = 4015;
        public const int OFFICIALID_GET_BOARDING_SECRET_FAILED = 4016;
        public const int OFFICIALID_GET_ADMIN_SECRET_FAILED = 4017;
        public const int OFFICIALID_SET_RESTAURANT_SECRET_FAILED = 4018;
        public const int OFFICIALID_SET_CASHIER_SECRET_FAILED = 4019;
        public const int OFFICIALID_SET_BOARDING_SECRET_FAILED = 4020;
        public const int OFFICIALID_SET_ADMIN_SECRET_FAILED = 4021;
        public const int OFFICIALID_UNBLOCK_OFFICIAL_PIN_FAILED = 4022;

        public const int THREAD_ALREADY_STARTED = 5001;
        public const int NOT_IMPLEMENTING_IRUNNABLE = 5002;
        public const int THREAD_START = 5003;
        public const int THREAD_SUSPEND = 5004;
        public const int THREAD_RESUME = 5005;
        public const int THREAD_JOIN = 5006;
        public const int THREAD_NOT_CREATED = 5007;
        public const int NO_THREAD_ALIVE = 5008;

        public const int CARD_OBSERVER_ESTABLISHING_CONTEXT_FAILED = 6001;
        public const int CARD_OBSERVER_RELEASING_CONTEXT_FAILED = 6002;
        public const int CARD_OBSERVER_GET_STATUS_CHANGE_FAILED = 6003;

        public const int WALLET_GET_TRANSACTION_AMOUNT_FAILED = 3029;
        public const int WALLET_INC_TRANSACTION_AMOUNT_FAILED = 3030;

        public const int INVALID_FILE_NUMBER_LENGTH = 7001;
        public const int INVALID_HEALTH_STRING_LENGTH = 7002;
    

        //greske koje prijavljuje dll fajl
	    
        
        
        public static string getErrorDescription(string err)
        {
            string result = "";
            int errInt = 0;
            try
            {
                errInt = Convert.ToInt32(err);
            }
            catch (Exception)
            {
                result = err;
                return result;
            }

            switch (errInt)
            {
                case PROCESS_IN_PROGRESS:
                    result = stCardError.Default.PROCESS_IN_PROGRESS;
                    break;
                case TEST_PERIOD_EXPIRED:
                    result = stCardError.Default.TEST_PERIOD_EXPIRED;
                    break;
                case CARD_TYPE_UNKNOWN:
                    result = stCardError.Default.CARD_TYPE_UNKNOWN;
                    break;
                case UNALLOWED_CASH_INCREASING:
                    result = stCardError.Default.UNALLOWED_CASH_INCREASING;
                    break;
                case UNALLOWED_ABONENT_INCREASING:
                    result = stCardError.Default.UNALLOWED_ABONENT_INCREASING;
                    break;
                case UNALLOWED_CARD_NUMBER_CHANGE:
                    result = stCardError.Default.UNALLOWED_CARD_NUMBER_CHANGE;
                    break;
                case NOT_LOGGED_IN:
                    result = stCardError.Default.NOT_LOGGED_IN;
                    break;
                case CARD_ALREADY_PERSONALIZED:
                    result = stCardError.Default.CARD_ALREADY_PERSONALIZED;
                    break;
                case IS_LOGGED_IN_FAILED:
                    result = stCardError.Default.IS_LOGGED_IN_FAILED;
                    break;
                case READER_OPEN_FAILED:
                    result = stCardError.Default.READER_OPEN_FAILED;
                    break;
                case READER_CLOSE_FAILED:
                    result = stCardError.Default.READER_CLOSE_FAILED;
                    break;
                case READER_SEND_FAILED:
                    result = stCardError.Default.READER_SEND_FAILED;
                    break;
                case AVAILABLE_READERS_FAILED:
                    result = stCardError.Default.AVAILABLE_READERS_FAILED;
                    break;
                case READER_CARD_CONNECT_FAILED:
                    result = stCardError.Default.GENERAL_READER_ERROR;  //nemam definisan opis ove greske, mozda jednog lepog dana
                    break;
                case READER_CARD_STATUS_FAILED:
                    result = stCardError.Default.GENERAL_READER_ERROR;  //nemam definisan opis ove greske, mozda jednog lepog dana
                    break;
                case READER_CARD_DISCONNECT_FAILED:
                    result = stCardError.Default.GENERAL_READER_ERROR;  //nemam definisan opis ove greske, mozda jednog lepog dana
                    break;
                case READER_LIST_READERS_FAILED:
                    result = stCardError.Default.GENERAL_READER_ERROR;  //nemam definisan opis ove greske, mozda jednog lepog dana
                    break;
                case READER_BEGIN_TRANSACTION_FAILED:
                    result = stCardError.Default.GENERAL_READER_ERROR;  //nemam definisan opis ove greske, mozda jednog lepog dana
                    break;
                case READER_END_TRANSACTION_FAILED:
                    result = stCardError.Default.GENERAL_READER_ERROR;  //nemam definisan opis ove greske, mozda jednog lepog dana
                    break;
                case WALLET_SELECT_FAILED:
                    result = stCardError.Default.WALLET_SELECT_FAILED;
                    break;
                case WALLET_GET_ABONENT_FAILED:
                    result = stCardError.Default.WALLET_GET_ABONENT_FAILED;
                    break;
                case WALLET_GET_EATEN_MEALS_FAILED:
                    result = stCardError.Default.WALLET_GET_EATEN_MEALS_FAILED;
                    break;
                case WALLET_GET_ABONENT_UPDATED_DATE_FAILED:
                    result = stCardError.Default.WALLET_GET_ABONENT_UPDATED_DATE_FAILED;
                    break;
                case WALLET_GET_PERSONAL_LENGTH_FAILED:
                    result = stCardError.Default.WALLET_GET_PERSONAL_LENGTH_FAILED;
                    break;
                case WALLET_GET_PERSONAL_FAILED:
                    result = stCardError.Default.WALLET_GET_PERSONAL_FAILED;
                    break;
                case WALLET_GET_FACULTY_FAILED:
                    result = stCardError.Default.WALLET_GET_FACULTY_FAILED;
                    break;
                case WALLET_GET_SCHOOLYEARID_FAILED:
                    result = stCardError.Default.WALLET_GET_SCHOOLYEARID_FAILED;
                    break;
                case WALLET_GET_BOARDING_INFO_FAILED:
                    result = stCardError.Default.WALLET_GET_BOARDING_INFO_FAILED;
                    break;
                case WALLET_GET_BOARDING_UPDATED_DATE_FAILED:
                    result = stCardError.Default.WALLET_GET_BOARDING_UPDATED_DATE_FAILED;
                    break;
                case WALLET_GET_CASH_FAILED:
                    result = stCardError.Default.WALLET_GET_CASH_FAILED;
                    break;
                case WALLET_GET_CARD_FAILED:
                    result = stCardError.Default.WALLET_GET_CARD_FAILED;
                    break;
                case WALLET_VERIFY_ADMIN_PIN_FAILED:
                    result = stCardError.Default.WALLET_VERIFY_ADMIN_PIN_FAILED;
                    break;
                case WALLET_VERIFY_RESTAURANT_PIN_FAILED:
                    result = stCardError.Default.WALLET_VERIFY_RESTAURANT_PIN_FAILED;
                    break;
                case WALLET_SET_PERSONAL_FAILED:
                    result = stCardError.Default.WALLET_SET_PERSONAL_FAILED;
                    break;
                case WALLET_SET_FACULTY_FAILED:
                    result = stCardError.Default.WALLET_SET_FACULTY_FAILED;
                    break;
                case WALLET_SET_SCHOOLYEARID_FAILED:
                    result = stCardError.Default.WALLET_SET_SCHOOLYEARID_FAILED;
                    break;
                case WALLET_SET_ABONENT_FAILED:
                    result = stCardError.Default.WALLET_SET_ABONENT_FAILED;
                    break;
                case WALLET_SET_EATEN_MEALS_FAILED:
                    result = stCardError.Default.WALLET_SET_EATEN_MEALS_FAILED;
                    break;
                case WALLET_SET_ABONENT_UPDATED_DATE_FAILED:
                    result = stCardError.Default.WALLET_SET_ABONENT_UPDATED_DATE_FAILED;
                    break;
                case WALLET_SET_CARD_FAILED:
                    result = stCardError.Default.WALLET_SET_CARD_FAILED;
                    break;
                case WALLET_SET_BOARDING_INFO_FAILED:
                    result = stCardError.Default.WALLET_SET_BOARDING_INFO_FAILED;
                    break;
                case WALLET_SET_BOARDING_UPDATED_DATE_FAILED:
                    result = stCardError.Default.WALLET_SET_BOARDING_UPDATED_DATE_FAILED;
                    break;
                case WALLET_SET_CASH_FAILED:
                    result = stCardError.Default.WALLET_SET_CASH_FAILED;
                    break;
                case WALLET_UPDATE_ADMIN_PIN_FAILED:
                    result = stCardError.Default.WALLET_UPDATE_ADMIN_PIN_FAILED;
                    break;
                case WALLET_UPDATE_CASHIER_PIN_FAILED:
                    result = stCardError.Default.WALLET_UPDATE_CASHIER_PIN_FAILED;
                    break;
                case WALLET_UPDATE_RESTAURANT_PIN_FAILED:
                    result = stCardError.Default.WALLET_UPDATE_RESTAURANT_PIN_FAILED;
                    break;
                case WALLET_UPDATE_BOARDING_PIN_FAILED:
                    result = stCardError.Default.WALLET_UPDATE_BOARDING_PIN_FAILED;
                    break;
                case OFFICIALID_SELECT_FAILED:
                    result = stCardError.Default.OFFICIALID_SELECT_FAILED;
                    break;
                case OFFICIALID_VERIFY_OFFICIAL_PIN_FAILED:
                    result = stCardError.Default.OFFICIALID_VERIFY_OFFICIAL_PIN_FAILED;
                    break;
                case OFFICIALID_UPDATE_OFFICIAL_PIN_FAILED:
                    result = stCardError.Default.OFFICIALID_UPDATE_OFFICIAL_PIN_FAILED;
                    break;
                case OFFICIALID_GET_OFFICIAL_PIN_INFO_FAILED:
                    result = stCardError.Default.OFFICIALID_GET_OFFICIAL_PIN_INFO_FAILED;
                    break;
                case OFFICIALID_VERIFY_ADMIN_PIN_FAILED:
                    result = stCardError.Default.OFFICIALID_VERIFY_ADMIN_PIN_FAILED;
                    break;
                case OFFICIALID_UPDATE_ADMIN_PIN_FAILED:
                    result = stCardError.Default.OFFICIALID_UPDATE_ADMIN_PIN_FAILED;
                    break;
                case OFFICIALID_GET_ADMIN_PIN_INFO_FAILED:
                    result = stCardError.Default.OFFICIALID_GET_ADMIN_PIN_INFO_FAILED;
                    break;
                case OFFICIALID_GET_CARD_FAILED:
                    result = stCardError.Default.OFFICIALID_GET_CARD_FAILED;
                    break;
                case OFFICIALID_SET_CARD_FAILED:
                    result = stCardError.Default.OFFICIALID_SET_CARD_FAILED;
                    break;
                case OFFICIALID_GET_SECURITY_DESCRIPTOR_FAILED:
                    result = stCardError.Default.OFFICIALID_GET_SECURITY_DESCRIPTOR_FAILED;
                    break;
                case OFFICIALID_SET_SECURITY_DESCRIPTOR_FAILED:
                    result = stCardError.Default.OFFICIALID_SET_SECURITY_DESCRIPTOR_FAILED;
                    break;
                case OFFICIALID_GET_SYSTEM_ID_FAILED:
                    result = stCardError.Default.OFFICIALID_GET_SYSTEM_ID_FAILED;
                    break;
                case OFFICIALID_SET_SYSTEM_ID_FAILED:
                    result = stCardError.Default.OFFICIALID_SET_SYSTEM_ID_FAILED;
                    break;
                case OFFICIALID_GET_RESTAURANT_SECRET_FAILED:
                    result = stCardError.Default.OFFICIALID_GET_RESTAURANT_SECRET_FAILED;
                    break;
                case OFFICIALID_GET_CASHIER_SECRET_FAILED:
                    result = stCardError.Default.OFFICIALID_GET_CASHIER_SECRET_FAILED;
                    break;
                case OFFICIALID_GET_BOARDING_SECRET_FAILED:
                    result = stCardError.Default.OFFICIALID_GET_BOARDING_SECRET_FAILED;
                    break;
                case OFFICIALID_GET_ADMIN_SECRET_FAILED:
                    result = stCardError.Default.OFFICIALID_GET_ADMIN_SECRET_FAILED;
                    break;
                case OFFICIALID_SET_RESTAURANT_SECRET_FAILED:
                    result = stCardError.Default.OFFICIALID_SET_RESTAURANT_SECRET_FAILED;
                    break;
                case OFFICIALID_SET_CASHIER_SECRET_FAILED:
                    result = stCardError.Default.OFFICIALID_SET_CASHIER_SECRET_FAILED;
                    break;
                case OFFICIALID_SET_BOARDING_SECRET_FAILED:
                    result = stCardError.Default.OFFICIALID_SET_BOARDING_SECRET_FAILED;
                    break;
                case OFFICIALID_SET_ADMIN_SECRET_FAILED:
                    result = stCardError.Default.OFFICIALID_SET_ADMIN_SECRET_FAILED;
                    break;
                case OFFICIALID_UNBLOCK_OFFICIAL_PIN_FAILED:
                    result = stCardError.Default.OFFICIALID_UNBLOCK_OFFICIAL_PIN_FAILED;
                    break;
                case THREAD_ALREADY_STARTED:
                    result = stCardError.Default.THREAD_ALREADY_STARTED;
                    break;
                case NOT_IMPLEMENTING_IRUNNABLE:
                    result = stCardError.Default.NOT_IMPLEMENTING_IRUNNABLE;
                    break;
                case THREAD_START:
                    result = stCardError.Default.THREAD_START;
                    break;
                case THREAD_SUSPEND:
                    result = stCardError.Default.THREAD_SUSPEND;
                    break;
                case THREAD_RESUME:
                    result = stCardError.Default.THREAD_RESUME;
                    break;
                case THREAD_JOIN:
                    result = stCardError.Default.THREAD_JOIN;
                    break;
                case THREAD_NOT_CREATED:
                    result = stCardError.Default.THREAD_NOT_CREATED;
                    break;
                case NO_THREAD_ALIVE:
                    result = stCardError.Default.NO_THREAD_ALIVE;
                    break;
                case CARD_OBSERVER_ESTABLISHING_CONTEXT_FAILED:
                    result = stCardError.Default.CARD_OBSERVER_ESTABLISHING_CONTEXT_FAILED;
                    break;
                case CARD_OBSERVER_RELEASING_CONTEXT_FAILED:
                    result = stCardError.Default.CARD_OBSERVER_RELEASING_CONTEXT_FAILED;
                    break;
                case CARD_OBSERVER_GET_STATUS_CHANGE_FAILED:
                    result = stCardError.Default.CARD_OBSERVER_GET_STATUS_CHANGE_FAILED;
                    break;
                case WALLET_GET_TRANSACTION_AMOUNT_FAILED:
                    result = stCardError.Default.WALLET_GET_TRANSACTION_AMOUNT_FAILED;
                    break;
                case WALLET_INC_TRANSACTION_AMOUNT_FAILED:
                    result = stCardError.Default.WALLET_INC_TRANSACTION_AMOUNT_FAILED;
                    break;
                case INVALID_FILE_NUMBER_LENGTH:
                    result = stCardError.Default.INVALID_FILE_NUMBER_LENGTH;
                    break;
                case INVALID_HEALTH_STRING_LENGTH:
                    result = stCardError.Default.INVALID_HEALTH_STRING_LENGTH;
                    break;
                default:
                    break;

            }
            return result;
        }
    }
}
