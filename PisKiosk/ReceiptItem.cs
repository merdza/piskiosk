﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk
{
    public class ReceiptItem
    {
        public string ItemName { get; set; }
        public int Amount { get; set; }
        public double ItemPrice { get; set; }
        public double TotalPrice { get; set; }

        public ReceiptItem(string n, int a, double p)
        {
            ItemName = n;
            Amount = a;
            ItemPrice = p;
            TotalPrice = a * p;
        }
    }
}
