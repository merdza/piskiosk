﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PisKiosk
{
    public class CardInterface
    {
        public static CardInterface instance = null;
        private IStudentCard cardInterface;

        public IStudentCard CardInterf
        {
            get { return cardInterface; }
            set { cardInterface = value; }
        }

        public static CardInterface getInstance()
        {
            if (instance == null)
            {
                instance = new CardInterface();
            }
            return instance;
        }

        protected CardInterface()
        {
            getCardInterface();
        }

        private void getCardInterface()
        {
            string ci = XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "cardInterface");
            string kioskApplication = XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "kioskApplication"); //1 - SCBeograd, 2 - Sombor, 3 - KSA
            if (ci.Equals("1"))
            {
                if (kioskApplication.Equals("1"))
	            {
                    cardInterface = new ScBgdKiosk.CardAdapterContactScBgd();
	            }
                else if (kioskApplication.Equals("3"))
                {
                    cardInterface = new KsaKiosk.CardAdapterContactKsa();
                }
                else if (kioskApplication.Equals("4"))
                {
                    cardInterface = new ScNSKiosk.CardAdapterContactScNS();
                }
                else
                {
                    throw new Exception("Wrong settings data - cardInterface or kioskApplication");
                }
                
            }
            else if (ci.Equals("2"))
            {
                if (kioskApplication.Equals("2"))
                {
                    cardInterface = new SomborKiosk.CardAdapterContactlessSombor();
                }
                else
                {
                    throw new Exception("Wrong settings data - cardInterface or kioskApplication");
                }
            }
            else
            {
                throw new Exception("Wrong settings data - cardInterface");
            }
        }
    }
}
