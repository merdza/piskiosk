﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PisValidatorPlus;
using System.Threading;

namespace PisKiosk
{
    public class ValidatorAdapter
    {
        private static ValidatorAdapter instance = null;

        public static ValidatorAdapter getInstance()
        {
            try
            {
                if (instance == null)
                {
                    instance = new ValidatorAdapter();
                }
                
            }
            catch (Exception ex)
            {
                //Program.KioskApplication.writeToLog("Error getting ValidatorAdapter Instance. " + ex.Message, ex);
                throw ex;
            }
            return instance;
        }

        private readonly object locker = new object();

        bool Running = false; // Indicates the status of the main poll loop
        int pollTimer = 250; // Timer in ms between polls
        int reconnectionAttempts = 10, reconnectionInterval = 3; // Connection info to deal with retrying connection to validator
        volatile bool Connected = false, ConnectionFail = false; // Threading bools to indicate status of connection with validator
        CValidator Validator; // The main validator class - used to send commands to the unit
        bool FormSetup = false; // Boolean so the form will only be setup once
        System.Windows.Forms.Timer reconnectionTimer = new System.Windows.Forms.Timer(); // Timer used to give a delay between reconnect attempts
        System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer();
        Thread ConnectionThread; // Thread used to connect to the validator
        Thread ValidatorWorkingThread;
        public List<Int64> ChanelValue { get; private set; }


        // Constructor
        private ValidatorAdapter()
        {
            Validator = new CValidator();
            timer1.Interval = pollTimer;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            reconnectionTimer.Tick += new EventHandler(reconnectionTimer_Tick);

            Validator.RejectingEvent += new EventHandler<RejectingEventArgs>(validator_RejectingEvent);
            Validator.RejectedEvent += new EventHandler<RejectedEventArgs>(validator_RejectedEvent);
            Validator.StackingEvent += new EventHandler<StackingEventArgs>(validator_StackingEvent);
            Validator.ReadNoteEvent += new EventHandler<ReadNoteEventArgs>(validator_ReadNoteEvent);
            Validator.CreditNoteEvent += new EventHandler<CreditNoteEventArgs>(validator_CreditNoteEvent);
            Validator.LogEvent += new EventHandler<LogEventArgs>(validator_LogEvent);
            Validator.StoredNoteEvent += new EventHandler<StoredNoteEventArgs>(validator_StoredNoteEvent);
            Validator.StackedEvent += new EventHandler<StackedEventArgs>(validator_StackedNoteEvent);
            
            Validator.ExceptionEvent += new EventHandler<ExceptionEventArgs>(validator_ExceptionEvent);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        // The main program loop, this is to control the validator, it polls at
        // a value set in this class (pollTimer).
        void MainLoop()
        {
            string logString = string.Empty;
//            btnRun.Enabled = false;
            //Validator.CommandStructure.ComPort = Global.ComPort;
            //Validator.CommandStructure.SSPAddress = Global.SSPAddress;
            //TODO  Program.KioskApplication.ValidatorPort
            Validator.setCommandStructure("COM" + Program.KioskApplication.ValidatorPort.ToString(), 0, 3000);

            // connect to the validator
            if (ConnectToValidator())
            {
                Running = true;
//                textBox1.AppendText(System.Environment.NewLine + "Poll Loop" + System.Environment.NewLine + "*********************************" + System.Environment.NewLine);
                Program.KioskApplication.log.Info("Pokrenuta petlja za očitavanje stanja validatora ");
//                btnHalt.Enabled = true;
            }

            while (Running)
            {
                bool pollResult = Validator.DoPoll(ref logString);
                //Program.KioskApplication.writeToLog("Poll result: " + log);
                // if the poll fails, try to reconnect
                if (!pollResult)
                {
                    Program.KioskApplication.log.Error("Nije uspelo očitavanje stanja validatora. Pokušava da se ponovo poveže... ");
                    Connected = false;
                    ConnectionThread = new Thread(ConnectToValidatorThreaded);
                    ConnectionThread.Start();
                    while (!Connected)
                    {
                        if (ConnectionFail)
                        {
                            Program.KioskApplication.log.Error("Nije uspelo ponovno očitavanje stanja validatora.");
                            return;
                        }
                        System.Windows.Forms.Application.DoEvents();
                    }
                    Program.KioskApplication.log.Info("Uspešno ponovno očitavanje stanja validatora.");
                }
                

                timer1.Enabled = true;

                // setup dynamic elements of win form once
                if (!FormSetup)
                {
                    SetupFormLayout();
                    FormSetup = true;
                }
                while (timer1.Enabled)
                {
                    System.Windows.Forms.Application.DoEvents();
                    Thread.Sleep(1); // Yield to free up CPU
                }
                if (!logString.Equals(string.Empty))
                {
                    Program.KioskApplication.log.Info(logString);
                    logString = string.Empty;
                }
                
            }

            //close com port and threads
            Validator.DisableValidator(ref logString);
            Validator.closeComPort();
            //btnRun.Enabled = true;
            //btnHalt.Enabled = false;

            if (!logString.Equals(string.Empty))
            {
                Program.KioskApplication.log.Info(logString);
            }
        }

        // This is a one off function that is called the first time the MainLoop()
        // function runs, it just sets up a few of the UI elements that only need
        // updating once.
        private void SetupFormLayout()
        {
            // need validator class instance
            if (Validator == null)
            {
                //todo
                //MessageBox.Show("Validator class is null.", "ERROR");
                return;
            }
        }

        // This function opens the com port and attempts to connect with the validator. It then negotiates
        // the keys for encryption and performs some other setup commands.
        private bool ConnectToValidator()
        {
            string log = string.Empty;
            // setup the timer
            reconnectionTimer.Interval = reconnectionInterval * 1000; // for ms

            // run for number of attempts specified
            for (int i = 0; i < reconnectionAttempts; i++)
            {
                // reset timer
                reconnectionTimer.Enabled = true;

                // close com port in case it was open
                Validator.closeComPort();

                // turn encryption off for first stage
                //Validator.CommandStructure.EncryptionStatus = false;
                Validator.setEncryptionStatus(false);

                // open com port and negotiate keys
                if (Validator.OpenComPort(ref log) && Validator.NegotiateKeys(ref log))
                {
                    //Validator.CommandStructure.EncryptionStatus = true; // now encrypting
                    Validator.setEncryptionStatus(true);
                    // find the max protocol version this validator supports
                    byte maxPVersion = FindMaxProtocolVersion();
                    if (maxPVersion > 6)
                    {
                        Validator.SetProtocolVersion(maxPVersion, ref log);
                    }
                    else
                    {
                        //todo
                        //MessageBox.Show("This program does not support units under protocol version 6, update firmware.", "ERROR");
                        return false;
                    }
                    // get info from the validator and store useful vars
                    Validator.SetupRequest(ref log);
                    // check this unit is supported by this program
                    if (!IsUnitTypeSupported(Validator.UnitType))
                    {
                        //todo
                        //MessageBox.Show("Unsupported unit type, this SDK supports the BV series and the NV series (excluding the NV11)");
                        //Application.Exit();
                        return false;
                    }
                    // inhibits, this sets which channels can receive notes
                    Validator.SetInhibits(ref log);
                    // enable, this allows the validator to receive and act on commands
                    Validator.EnableValidator(ref log);

                    //todo
                    //textBox1.AppendText(log);

                    return true;
                }
                
                while (reconnectionTimer.Enabled) System.Windows.Forms.Application.DoEvents(); // wait for reconnectionTimer to tick
            }
            return false;
        }

        // This is the same as the above function but set up differently for threading.
        private void ConnectToValidatorThreaded()
        {
            string log = string.Empty;

            // setup the timer
            reconnectionTimer.Interval = reconnectionInterval * 1000; // for ms

            // run for number of attempts specified
            for (int i = 0; i < reconnectionAttempts; i++)
            {
                // reset timer
                reconnectionTimer.Enabled = true;

                // close com port in case it was open
                //Validator.SSPComms.CloseComPort();
                Validator.closeComPort();

                // turn encryption off for first stage
                //Validator.CommandStructure.EncryptionStatus = false;
                Validator.setEncryptionStatus(false);

                // open com port and negotiate keys
                if (Validator.OpenComPort(ref log) && Validator.NegotiateKeys(ref log))
                {
                    //Validator.CommandStructure.EncryptionStatus = true; // now encrypting
                    Validator.setEncryptionStatus(true);
                    // find the max protocol version this validator supports
                    byte maxPVersion = FindMaxProtocolVersion();
                    if (maxPVersion > 6)
                    {
                        Validator.SetProtocolVersion(maxPVersion, ref log);
                    }
                    else
                    {
                        //todo
                        //MessageBox.Show("This program does not support units under protocol version 6, update firmware.", "ERROR");
                        Connected = false;
                        return;
                    }
                    // get info from the validator and store useful vars
                    Validator.SetupRequest(ref log);
                    // inhibits, this sets which channels can receive notes
                    Validator.SetInhibits(ref log);
                    // enable, this allows the validator to operate
                    Validator.EnableValidator(ref log);

                    //todo
                    //textBox1.AppendText(log);

                    Connected = true;
                    return;
                }
                
                while (reconnectionTimer.Enabled) System.Windows.Forms.Application.DoEvents(); // wait for reconnectionTimer to tick
            }
            Connected = false;
            ConnectionFail = true;

        }

        // This function finds the maximum protocol version that a validator supports. To do this
        // it attempts to set a protocol version starting at 6 in this case, and then increments the
        // version until error 0xF8 is returned from the validator which indicates that it has failed
        // to set it. The function then returns the version number one less than the failed version.
        private byte FindMaxProtocolVersion()
        {
            string log = string.Empty;
            // not dealing with protocol under level 6
            // attempt to set in validator
            byte b = 0x06;
            while (true)
            {
                Validator.SetProtocolVersion(b, ref log);
                if (Validator.getResponseData() == CCommands.SSP_RESPONSE_CMD_FAIL)
                    return --b;
                b++;
                if (b > 20)
                    return 0x06; // return default if protocol 'runs away'
            }
        }

        // This function checks whether the type of validator is supported by this program. This program only
        // supports Note Validators so any other type should be rejected.
        private bool IsUnitTypeSupported(char type)
        {
            if (type == (char)0x00)
                return true;
            return false;
        }

        public void Reject()
        {
            string log = string.Empty;
            if(!Validator.reject(ref log))
                throw new ItlSspErrorSspRejectException();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // create an instance of the validator info class
            Validator = new CValidator();
            //btnHalt.Enabled = false;

            //// Position comms window
            //Point p = new Point(Location.X, Location.Y);
            //p.X += this.Width;
            //Validator.CommsLog.Location = p;

            //if (Properties.Settings.Default.CommWindow)
            //{
            //    Validator.CommsLog.Show();
            //    logTickBox.Checked = true;
            //}
            //else
            //    logTickBox.Checked = false;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            //// hide this and show opening menu
            //Hide();
            //frmOpenMenu menu = new frmOpenMenu(this);
            //menu.Show();
        }


        public void Start()
        {
            Program.KioskApplication.log.Info("Startuje se validator.");
            ValidatorWorkingThread = new Thread(MainLoop);
            ValidatorWorkingThread.Start();
            //MainLoop(); sada se izvrsava u posebnoj niti
        }

        public void Stop()
        {
            Running = false;
        }

        //private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    Form formSettings = new frmSettings();
        //    formSettings.ShowDialog();
        //    Running = false;
        //}


        private void resetValidatorBtn_Click(object sender, EventArgs e)
        {
            string log = string.Empty;
            if (Validator != null)
                Validator.Reset(ref log);
            //todo
            //textBox1.AppendText(log);
        }

        //private void logTickBox_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (logTickBox.Checked)
        //        Validator.CommsLog.Show();
        //    else
        //        Validator.CommsLog.Hide();
        //}

        //todo
        //private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    Running = false;
        //    //Properties.Settings.Default.CommWindow = logTickBox.Checked;
        //    //Properties.Settings.Default.ComPort = Global.ComPort;
        //    //Properties.Settings.Default.Save();
        //}

        //private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    frmSettings f = new frmSettings();
        //    f.Show();
        //}

        //private void btnClear_Click(object sender, EventArgs e)
        //{
        //    Validator.NumberOfNotesStacked = 0;
        //}

        private void reconnectionTimer_Tick(object sender, EventArgs e)
        {
            reconnectionTimer.Enabled = false;
        }

        

//        private ValidatorAdapter(
//            int portNumber,
//            int readFrequency,
//            string valdiatorName,
//            bool generateLogMessages,
//            bool genereateValidatorThreadMessages)
//        {
//            ValidatorConfig config = new ValidatorConfig(
//                portNumber,
//                readFrequency,
//                valdiatorName,
//                generateLogMessages,
//                genereateValidatorThreadMessages);

//            validator = new PISNoteValidator(config);

//            validator.RejectingEvent += new EventHandler<PISNoteValidator.RejectingEventArgs>(validator_RejectingEvent);
//            validator.RejectedEvent += new EventHandler<PISNoteValidator.RejectedEventArgs>(validator_RejectedEvent);
//            validator.StackingEvent += new EventHandler<PISNoteValidator.StackingEventArgs>(validator_StackingEvent);
//            validator.ReadNoteEvent += new EventHandler<PISNoteValidator.ReadNoteEventArgs>(validator_ReadNoteEvent);
//            validator.CreditNoteEvent += new EventHandler<PISNoteValidator.CreditNoteEventArgs>(validator_CreditNoteEvent);
//            validator.LogEvent += new EventHandler<PISNoteValidator.LogEventArgs>(validator_LogEvent);
//            validator.StoredNoteEvent += new EventHandler<PISNoteValidator.StoredNoteEventArgs>(validator_StoredNoteEvent);
//            validator.StackedEvent += new EventHandler<PISNoteValidator.StackedEventArgs>(validator_StackedNoteEvent);
//            validator.ValidatorInitEvent += new EventHandler<PISNoteValidator.ValidatorInitEventArgs>(validator_ValidatorInitEvent);

//            validator.ExceptionEvent += new EventHandler<PISNoteValidator.ExceptionEventArgs>(validator_ExceptionEvent);

////            validator.OpenPort();

//            //Program.KioskApplication.writeToLog("Zavrsen poziv ValidatorAdapter");
//        }

//        public void OpenValidatorPort()
//        {
//            try
//            {
//                validator.OpenPort();
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Open Validator Port failed. " + ex.Message);
//            }
//        }

//        public void ExitValidator()
//        {
//            validator.MainThreadAbort();
//        }

//        public void Start()
//        {


//            Program.KioskApplication.writeToLog("Startuje se validator " + validator.CountyCode + ", " + validator.SerialNumber);
//            validator.Start();

//            string currencyCode = Program.KioskApplication.CurrencyCode;

//            //ovo je samo provera da li je validator programiran za potrebne novcanice
//            if (!validator.CountyCode.ToUpper().Equals(currencyCode.ToUpper()))
//            {
//                Stop();
//                throw new Exception("Wrong currency code. ");
//            }
//        }

//        public void Stop()
//        {
//            validator.Stop();
//        }

//        public void Reject()
//        {
//            validator.Reject();
//        }

        #region Validator events


        void validator_ExceptionEvent(object sender, ExceptionEventArgs e)
        {
            Monitor.Enter(locker);
            if (OnException != null)
                OnException(e.Exception);
            Monitor.Exit(locker);
        }

        void validator_RejectingEvent(object sender, RejectingEventArgs e)
        {
            Monitor.Enter(locker);
            OnValidatorStatusChange("Rejecting Note");
            Monitor.Exit(locker);
        }

        void validator_RejectedEvent(object sender, RejectedEventArgs e)
        {
            Monitor.Enter(locker);
            OnRejectedNote();
            Monitor.Exit(locker);
        }

        void validator_StackingEvent(object sender, StackingEventArgs e)
        {
            /*
             * The note is being moved from the escrow position to the host exit section of the device
             */
            Monitor.Enter(locker);
            OnValidatorStatusChange("Stacking Note");

            //otpoceto je uvlacenje novcanice u validator

            //if (NoteReaded)
            //{
            //    NeedPrinting = true;
            //    NeedWriteToCard = true;
            //}
            Monitor.Exit(locker);
        }

        void validator_StackedNoteEvent(object sender, StackedEventArgs e)
        {

            Monitor.Enter(locker);
            OnValidatorStatusChange("Stacked Note");
            Monitor.Exit(locker);
        }

        void validator_StoredNoteEvent(object sender, StoredNoteEventArgs e)
        {
            Monitor.Enter(locker);
            OnValidatorStatusChange("Stored Note");
            Monitor.Exit(locker);
        }


        void validator_ReadNoteEvent(object sender, ReadNoteEventArgs e)
        {
            /*
             * A note is in the process of being scanned by the device (byte value 0) 
             * or a valid note has been scanned and is in escrow (byte value gives the channel number)
             */
            Monitor.Enter(locker);
            int bankNote = 0;

            //OnLog("Cita se novcanica na kanalu: " + e.Chanel.ToString());

            if ((e != null) && (e.Chanel > -1))
            {
                int noteVal = Validator.GetChannelValue(e.Chanel);
                bankNote = noteVal / 100; //todo
                if (OnReadNote != null)
                    OnReadNote(bankNote);
            }

            OnValidatorStatusChange("Read Note", bankNote);
            Monitor.Exit(locker);
        }

        void validator_CreditNoteEvent(object sender, CreditNoteEventArgs e)
        {
            /*
             * A note has passed through the device, past the point of possible recovery and the host can safely issue its credit amount.
             * The byte value is the channel number of the note to credit. 
             */
            Monitor.Enter(locker);

            //int bankNote = (int)validator.ChanelValue[e.Chanel];
            int noteVal = Validator.GetChannelValue(e.Chanel);
            int bankNote = noteVal/100;

            OnValidatorStatusChange("Credit Note", bankNote);
            //novcanica je procitana i uvucena u validator



            if (OnCreditNote != null)
                OnCreditNote(bankNote);

            //iskljuci oznaku ubacivanja novcanice
            //OnReadNote(bankNote, false);

            Monitor.Exit(locker);
        }

        void validator_LogEvent(object sender, LogEventArgs e)
        {
            Monitor.Enter(locker);

            if (OnLog != null)
                OnLog(e.Text);

            Monitor.Exit(locker);
        }

        #endregion

        public delegate void LogDelegate(string text);
        public delegate void ExceptionDelegate(Exception ex);
        public delegate void ReadNoteDelegate(int bankNoteValue);
        public delegate void RejectedNoteDelegate();
        public delegate void CreditNoteDelegate(int bankNoteValue);
        public delegate void ValidatorStatusChangeDelegate(string status, int value = 0);

        public LogDelegate OnLog { get; set; }
        public ExceptionDelegate OnException { get; set; }
        public CreditNoteDelegate OnCreditNote { get; set; }
        public ReadNoteDelegate OnReadNote { get; set; }
        public RejectedNoteDelegate OnRejectedNote { get; set; }
        public ValidatorStatusChangeDelegate OnValidatorStatusChange { get; set; }
    }
}
