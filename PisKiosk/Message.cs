﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class Message : Form
    {
        public string Mess { get; protected set; }
        public MessageType MessType { get; protected set; }
        //public static System.Timers.Timer TurnOffTimer { get; set; }
        public Timer TurnOffTimer { get; set; }
        public int SecondsTillClose { get; set; }

        protected Message()
        {
            InitializeComponent();
            SetTurnOffTimer();
        }

        protected void setMessage()
        {
            lblMessage.Text = Mess;
            lblMessage.Refresh();
            lblMessage.Update();

            switch (MessType)
            {
                case MessageType.Error:
                    {
                        lblMessageTitle.Text = guiLang.Message_MessageType_Error;
                        break;
                    }
                case MessageType.Information:
                    {
                        lblMessageTitle.Text = guiLang.Message_MessageType_Information;
                        break;
                    }
                case MessageType.Warning:
                    {
                        lblMessageTitle.Text = guiLang.Message_MessageType_Warning;
                        break;
                    }
                case MessageType.Question:
                    {
                        lblMessageTitle.Text = guiLang.Message_MessageType_Warning;
                        break;
                    }
                default:
                    {
                        lblMessageTitle.Text = "";
                        break;
                    }
            }

            if (SecondsTillClose == 0)
            {
                lblSecondsTillClose.Text = "";
            }
            else
            {
                lblSecondsTillClose.Text = SecondsTillClose.ToString();
            }
            lblSecondsTillClose.Refresh();
            lblSecondsTillClose.Update();

            this.Refresh();
            Application.DoEvents();
            //Invalidate();
        }

        protected void hideMessage()
        {
            resetMessage();
            this.Hide();
        }
        

        protected void resetMessage()
        {
            Mess = string.Empty;
            MessType = MessageType.Information;
            SecondsTillClose = 0;
            setMessage();
        }

        /// <summary>
        /// All messages will turn off automatically after 10 seconds if the user don't turn them off prior to that
        /// </summary>
        protected void SetTurnOffTimer()
        {
            TurnOffTimer = new Timer();
            TurnOffTimer.Interval = 1000;
            SecondsTillClose = 10;
            TurnOffTimer.Tick += new EventHandler(TurnOffTimer_Tick);
        }

        protected void TurnOffTimer_Tick(object sender, EventArgs e)
        {
            if (SecondsTillClose > 0)
            {
                setMessage();
                SecondsTillClose--;
            }
            else
            {
                hideMessage();
            }
        }
    }

    public enum MessageType {Information, Warning, Error, Question};

    
}
