﻿namespace PisKiosk
{
    partial class CashInsertForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            PisKiosk.Office2010Blue office2010Blue1 = new PisKiosk.Office2010Blue();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CashInsertForm));
            this.listViewSupportedNotes = new System.Windows.Forms.ListView();
            this.listBoxInsertedNotes = new System.Windows.Forms.ListBox();
            this.groupBoxSupportedBankNotes = new System.Windows.Forms.GroupBox();
            this.groupBoxInsertedBankNotes = new System.Windows.Forms.GroupBox();
            this.lblEquals = new System.Windows.Forms.Label();
            this.lblTimes = new System.Windows.Forms.Label();
            this.lblInsertedTotal = new System.Windows.Forms.Label();
            this.groupBoxTotal = new System.Windows.Forms.GroupBox();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.listBoxValidatorStatus = new System.Windows.Forms.ListBox();
            this.timerBankNoteInserted = new System.Windows.Forms.Timer(this.components);
            this.lblKioskApp = new System.Windows.Forms.Label();
            this.lblCardMoneyBalance = new System.Windows.Forms.Label();
            this.btnEndInsertingMoney = new PisKiosk.XButton();
            this.groupBoxSupportedBankNotes.SuspendLayout();
            this.groupBoxInsertedBankNotes.SuspendLayout();
            this.groupBoxTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Size = new System.Drawing.Size(47, 47);
            // 
            // listViewSupportedNotes
            // 
            this.listViewSupportedNotes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listViewSupportedNotes.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold);
            this.listViewSupportedNotes.LabelWrap = false;
            this.listViewSupportedNotes.Location = new System.Drawing.Point(60, 45);
            this.listViewSupportedNotes.Name = "listViewSupportedNotes";
            this.listViewSupportedNotes.Scrollable = false;
            this.listViewSupportedNotes.Size = new System.Drawing.Size(162, 51);
            this.listViewSupportedNotes.TabIndex = 2;
            this.listViewSupportedNotes.UseCompatibleStateImageBehavior = false;
            this.listViewSupportedNotes.View = System.Windows.Forms.View.List;
            this.listViewSupportedNotes.SelectedIndexChanged += new System.EventHandler(this.listViewSupportedNotes_SelectedIndexChanged);
            // 
            // listBoxInsertedNotes
            // 
            this.listBoxInsertedNotes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBoxInsertedNotes.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold);
            this.listBoxInsertedNotes.FormattingEnabled = true;
            this.listBoxInsertedNotes.ItemHeight = 59;
            this.listBoxInsertedNotes.Location = new System.Drawing.Point(41, 45);
            this.listBoxInsertedNotes.MultiColumn = true;
            this.listBoxInsertedNotes.Name = "listBoxInsertedNotes";
            this.listBoxInsertedNotes.Size = new System.Drawing.Size(114, 59);
            this.listBoxInsertedNotes.TabIndex = 3;
            // 
            // groupBoxSupportedBankNotes
            // 
            this.groupBoxSupportedBankNotes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBoxSupportedBankNotes.Controls.Add(this.listViewSupportedNotes);
            this.groupBoxSupportedBankNotes.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Bold);
            this.groupBoxSupportedBankNotes.Location = new System.Drawing.Point(91, 46);
            this.groupBoxSupportedBankNotes.Name = "groupBoxSupportedBankNotes";
            this.groupBoxSupportedBankNotes.Size = new System.Drawing.Size(275, 126);
            this.groupBoxSupportedBankNotes.TabIndex = 6;
            this.groupBoxSupportedBankNotes.TabStop = false;
            this.groupBoxSupportedBankNotes.Text = "Supported banknotes";
            this.groupBoxSupportedBankNotes.Paint += new System.Windows.Forms.PaintEventHandler(this.groupBoxSupportedBankNotes_Paint);
            // 
            // groupBoxInsertedBankNotes
            // 
            this.groupBoxInsertedBankNotes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBoxInsertedBankNotes.Controls.Add(this.listBoxInsertedNotes);
            this.groupBoxInsertedBankNotes.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Bold);
            this.groupBoxInsertedBankNotes.Location = new System.Drawing.Point(458, 46);
            this.groupBoxInsertedBankNotes.Name = "groupBoxInsertedBankNotes";
            this.groupBoxInsertedBankNotes.Size = new System.Drawing.Size(174, 126);
            this.groupBoxInsertedBankNotes.TabIndex = 7;
            this.groupBoxInsertedBankNotes.TabStop = false;
            this.groupBoxInsertedBankNotes.Text = "Inserted";
            this.groupBoxInsertedBankNotes.Paint += new System.Windows.Forms.PaintEventHandler(this.groupBoxInsertedBankNotes_Paint);
            // 
            // lblEquals
            // 
            this.lblEquals.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblEquals.Font = new System.Drawing.Font("Segoe UI Light", 72F, System.Drawing.FontStyle.Bold);
            this.lblEquals.Location = new System.Drawing.Point(644, 91);
            this.lblEquals.Name = "lblEquals";
            this.lblEquals.Size = new System.Drawing.Size(63, 51);
            this.lblEquals.TabIndex = 8;
            this.lblEquals.Text = "=";
            this.lblEquals.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTimes
            // 
            this.lblTimes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTimes.Font = new System.Drawing.Font("Segoe UI Light", 72F, System.Drawing.FontStyle.Bold);
            this.lblTimes.Location = new System.Drawing.Point(386, 91);
            this.lblTimes.Name = "lblTimes";
            this.lblTimes.Size = new System.Drawing.Size(57, 51);
            this.lblTimes.TabIndex = 9;
            this.lblTimes.Text = "x";
            this.lblTimes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInsertedTotal
            // 
            this.lblInsertedTotal.Font = new System.Drawing.Font("Segoe UI Light", 72F, System.Drawing.FontStyle.Bold);
            this.lblInsertedTotal.Location = new System.Drawing.Point(16, 45);
            this.lblInsertedTotal.Name = "lblInsertedTotal";
            this.lblInsertedTotal.Size = new System.Drawing.Size(320, 51);
            this.lblInsertedTotal.TabIndex = 10;
            this.lblInsertedTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxTotal
            // 
            this.groupBoxTotal.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBoxTotal.Controls.Add(this.lblInsertedTotal);
            this.groupBoxTotal.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Bold);
            this.groupBoxTotal.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBoxTotal.Location = new System.Drawing.Point(719, 46);
            this.groupBoxTotal.Name = "groupBoxTotal";
            this.groupBoxTotal.Size = new System.Drawing.Size(352, 126);
            this.groupBoxTotal.TabIndex = 8;
            this.groupBoxTotal.TabStop = false;
            this.groupBoxTotal.Text = "Total";
            this.groupBoxTotal.Paint += new System.Windows.Forms.PaintEventHandler(this.groupBoxTotal_Paint);
            // 
            // listBoxLog
            // 
            this.listBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(72, 511);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(253, 108);
            this.listBoxLog.TabIndex = 12;
            this.listBoxLog.Visible = false;
            // 
            // listBoxValidatorStatus
            // 
            this.listBoxValidatorStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxValidatorStatus.FormattingEnabled = true;
            this.listBoxValidatorStatus.Location = new System.Drawing.Point(374, 511);
            this.listBoxValidatorStatus.Name = "listBoxValidatorStatus";
            this.listBoxValidatorStatus.Size = new System.Drawing.Size(264, 108);
            this.listBoxValidatorStatus.TabIndex = 13;
            this.listBoxValidatorStatus.Visible = false;
            // 
            // timerBankNoteInserted
            // 
            this.timerBankNoteInserted.Interval = 10000;
            this.timerBankNoteInserted.Tick += new System.EventHandler(this.timerBankNoteInserted_Tick);
            // 
            // lblKioskApp
            // 
            this.lblKioskApp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKioskApp.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.lblKioskApp.Location = new System.Drawing.Point(876, 457);
            this.lblKioskApp.Name = "lblKioskApp";
            this.lblKioskApp.Size = new System.Drawing.Size(262, 165);
            this.lblKioskApp.TabIndex = 15;
            this.lblKioskApp.Text = "lblKioskApp";
            this.lblKioskApp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblKioskApp.Visible = false;
            // 
            // lblCardMoneyBalance
            // 
            this.lblCardMoneyBalance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCardMoneyBalance.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold);
            this.lblCardMoneyBalance.Location = new System.Drawing.Point(85, 382);
            this.lblCardMoneyBalance.Name = "lblCardMoneyBalance";
            this.lblCardMoneyBalance.Size = new System.Drawing.Size(980, 64);
            this.lblCardMoneyBalance.TabIndex = 16;
            this.lblCardMoneyBalance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnEndInsertingMoney
            // 
            this.btnEndInsertingMoney.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            office2010Blue1.BorderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonMouseOverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.ButtonMouseOverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonMouseOverColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            office2010Blue1.ButtonMouseOverColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            office2010Blue1.ButtonNormalColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.ButtonNormalColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonNormalColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            office2010Blue1.ButtonNormalColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            office2010Blue1.ButtonSelectedColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.ButtonSelectedColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonSelectedColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            office2010Blue1.ButtonSelectedColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            office2010Blue1.HoverTextColor = System.Drawing.Color.White;
            office2010Blue1.SelectedTextColor = System.Drawing.Color.White;
            office2010Blue1.TextColor = System.Drawing.Color.White;
            this.btnEndInsertingMoney.ColorTable = office2010Blue1;
            this.btnEndInsertingMoney.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold);
            this.btnEndInsertingMoney.Image = ((System.Drawing.Image)(resources.GetObject("btnEndInsertingMoney.Image")));
            this.btnEndInsertingMoney.Location = new System.Drawing.Point(228, 457);
            this.btnEndInsertingMoney.Name = "btnEndInsertingMoney";
            this.btnEndInsertingMoney.Size = new System.Drawing.Size(691, 100);
            this.btnEndInsertingMoney.TabIndex = 17;
            this.btnEndInsertingMoney.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnEndInsertingMoney.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnEndInsertingMoney.UseVisualStyleBackColor = true;
            // 
            // CashInsertForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1150, 650);
            this.Controls.Add(this.btnEndInsertingMoney);
            this.Controls.Add(this.lblKioskApp);
            this.Controls.Add(this.listBoxValidatorStatus);
            this.Controls.Add(this.listBoxLog);
            this.Controls.Add(this.groupBoxTotal);
            this.Controls.Add(this.lblTimes);
            this.Controls.Add(this.lblEquals);
            this.Controls.Add(this.groupBoxInsertedBankNotes);
            this.Controls.Add(this.groupBoxSupportedBankNotes);
            this.Controls.Add(this.lblCardMoneyBalance);
            this.Name = "CashInsertForm";
            this.Text = "CashInsertForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CashInsertForm_FormClosing);
            this.Load += new System.EventHandler(this.CashInsertForm_Load);
            this.VisibleChanged += new System.EventHandler(this.CashInsertForm_VisibleChanged);
            this.Controls.SetChildIndex(this.lblCardMoneyBalance, 0);
            this.Controls.SetChildIndex(this.groupBoxSupportedBankNotes, 0);
            this.Controls.SetChildIndex(this.groupBoxInsertedBankNotes, 0);
            this.Controls.SetChildIndex(this.lblEquals, 0);
            this.Controls.SetChildIndex(this.lblTimes, 0);
            this.Controls.SetChildIndex(this.groupBoxTotal, 0);
            this.Controls.SetChildIndex(this.listBoxLog, 0);
            this.Controls.SetChildIndex(this.listBoxValidatorStatus, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblKioskApp, 0);
            this.Controls.SetChildIndex(this.btnEndInsertingMoney, 0);
            this.groupBoxSupportedBankNotes.ResumeLayout(false);
            this.groupBoxInsertedBankNotes.ResumeLayout(false);
            this.groupBoxTotal.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerBankNoteInserted;
        protected System.Windows.Forms.Label lblKioskApp;
        protected System.Windows.Forms.ListView listViewSupportedNotes;
        protected System.Windows.Forms.ListBox listBoxInsertedNotes;
        protected System.Windows.Forms.GroupBox groupBoxSupportedBankNotes;
        protected System.Windows.Forms.GroupBox groupBoxInsertedBankNotes;
        protected System.Windows.Forms.Label lblEquals;
        protected System.Windows.Forms.Label lblTimes;
        protected System.Windows.Forms.Label lblInsertedTotal;
        protected System.Windows.Forms.GroupBox groupBoxTotal;
        protected System.Windows.Forms.ListBox listBoxLog;
        protected System.Windows.Forms.ListBox listBoxValidatorStatus;
        protected System.Windows.Forms.Label lblCardMoneyBalance;
        protected XButton btnEndInsertingMoney;
    }
}