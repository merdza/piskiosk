﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class BuyMealsUC : UserControl
    {
        public BuyMealsUC(int month, int year)
        {
            InitializeComponent();

            Month = month;
            Year = year;

            this.BackColor = Program.KioskApplication.BackgroundColor;
            //lblTitle.ForeColor = Program.pkf.TextForeColor2;
        }

        public BuyMealsUC() { }

        public int Month { get; set; }
        public int Year { get; set; }

        protected virtual void takeCareOfMealTab(MealType mt) { }
                
        protected void addMealTabs()
        {
            tabControlMeal.TabPages.Clear();

            //tri obroka - dorucak, rucak, vecera
            takeCareOfMealTab(MealType.Breakfast);
            takeCareOfMealTab(MealType.Lunch);
            takeCareOfMealTab(MealType.Dinner);
        }


        private void tabControlMeal_DrawItem(object sender, DrawItemEventArgs e)
        {
            //
            Graphics g = e.Graphics;
            Brush _textBrush;

            // Get the item from the collection.
            TabPage _tabPage = tabControlMeal.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = tabControlMeal.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {

                // Draw a different background color, and don't paint a focus rectangle.
                _textBrush = new SolidBrush(Program.KioskApplication.TextForeColor);
                Brush brush = new SolidBrush(Program.KioskApplication.TextForeColor2);
                g.FillRectangle(brush, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                e.DrawBackground();
            }

            // Use our own font.
            Font _tabFont = new Font("Segoe UI Light", (float)20.0, FontStyle.Bold, GraphicsUnit.Pixel);

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));

            ChangeTabColor(sender, e);
            PaintTabsRectangle(sender, e);
        }

        private void ChangeTabColor(object sender, DrawItemEventArgs e)
        {
            Font TabFont;
            Brush BackBrush = new SolidBrush(Program.KioskApplication.BackgroundColor); //Set background color
            Brush BackBrushActive = new SolidBrush(Program.KioskApplication.TextForeColor2);
            Brush ForeBrush = new SolidBrush(Program.KioskApplication.TextForeColor2);//Set foreground color
            Brush ForeBrushActive = new SolidBrush(Program.KioskApplication.TextForeColor);
            if (e.Index == this.tabControlMeal.SelectedIndex)
            {
                TabFont = new Font(e.Font, /*FontStyle.Italic |*/ FontStyle.Bold);
                BackBrush = BackBrushActive;
                ForeBrush = ForeBrushActive;
            }
            else
            {
                TabFont = e.Font;
                //TabFont = new Font(e.Font, FontStyle.Strikeout);
            }
            string TabName = this.tabControlMeal.TabPages[e.Index].Text;
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            e.Graphics.FillRectangle(BackBrush, e.Bounds);
            Rectangle r = e.Bounds;
            r = new Rectangle(r.X, r.Y + 3, r.Width, r.Height - 3);
            e.Graphics.DrawString(TabName, TabFont, ForeBrush, r, sf);
            //Dispose objects
            sf.Dispose();
            if (e.Index == this.tabControlMeal.SelectedIndex)
            {
                TabFont.Dispose();
                BackBrush.Dispose();
            }
            else
            {
                BackBrush.Dispose();
                ForeBrush.Dispose();
            }

        }

        private void PaintTabsRectangle(object sender, DrawItemEventArgs e)
        {
            //ova funkcija farba pravougaonik sa leve strane tabova do kraja tabControl-a
            SolidBrush fillBrush = new SolidBrush(Program.KioskApplication.BackgroundColor);
            
            //draw rectangle behind the tabs
            Rectangle lasttabrect = tabControlMeal.GetTabRect(tabControlMeal.TabPages.Count - 1);
            Rectangle background = new Rectangle();
            background.Location = new Point(0, lasttabrect.Bottom);

            //pad the rectangle to cover the 1 pixel line between the top of the tabpage and the start of the tabs
            background.Size = new Size(lasttabrect.Width + 1, tabControlMeal.Bottom - background.Bottom);
            e.Graphics.FillRectangle(fillBrush, background);

            //string messOut = string.Format("tabControlMeal.Left: {0}, tabControlMeal.Top: {1}, tabControlMeal.Width: {2}, tabControlMeal.Height: {3}", tabControlMeal.Left.ToString(), tabControlMeal.Top.ToString(), tabControlMeal.Width.ToString(), tabControlMeal.Height.ToString());
            //MessageClassic.getInstance(messOut, MessageType.Information).Show();
        }

        private void BuyMealsUC_VisibleChanged(object sender, EventArgs e)
        {

        }

        

        private void BuyMealsUC_Load(object sender, EventArgs e)
        {
            addMealTabs();
        }

        private void BuyMealsUC_Paint(object sender, PaintEventArgs e)
        {
            Utils.drawTabControlBorder(sender, e, this);
        }

        
    }
}
