﻿namespace PisKiosk
{
    partial class BuyMealsPickAmountUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMealTitle = new System.Windows.Forms.Label();
            this.lblMealAmountSelected = new System.Windows.Forms.Label();
            this.lblAllowed = new System.Windows.Forms.Label();
            this.lblTenMinusReason = new System.Windows.Forms.Label();
            this.lblOnePlusReason = new System.Windows.Forms.Label();
            this.lblTenPlusReason = new System.Windows.Forms.Label();
            this.lblOneMinusReason = new System.Windows.Forms.Label();
            this.btnTenMinus = new PisKiosk.XButton();
            this.btnOnePlus = new PisKiosk.XButton();
            this.btnTenPlus = new PisKiosk.XButton();
            this.btnOneMinus = new PisKiosk.XButton();
            this.SuspendLayout();
            // 
            // lblMealTitle
            // 
            this.lblMealTitle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMealTitle.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold);
            this.lblMealTitle.Location = new System.Drawing.Point(14, 5);
            this.lblMealTitle.Name = "lblMealTitle";
            this.lblMealTitle.Size = new System.Drawing.Size(738, 58);
            this.lblMealTitle.TabIndex = 0;
            this.lblMealTitle.Text = "obrok";
            this.lblMealTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMealAmountSelected
            // 
            this.lblMealAmountSelected.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblMealAmountSelected.Font = new System.Drawing.Font("Segoe UI Light", 64F, System.Drawing.FontStyle.Bold);
            this.lblMealAmountSelected.Location = new System.Drawing.Point(290, 149);
            this.lblMealAmountSelected.Name = "lblMealAmountSelected";
            this.lblMealAmountSelected.Size = new System.Drawing.Size(184, 100);
            this.lblMealAmountSelected.TabIndex = 1;
            this.lblMealAmountSelected.Text = "kol";
            this.lblMealAmountSelected.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAllowed
            // 
            this.lblAllowed.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblAllowed.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblAllowed.Location = new System.Drawing.Point(14, 63);
            this.lblAllowed.Name = "lblAllowed";
            this.lblAllowed.Size = new System.Drawing.Size(738, 33);
            this.lblAllowed.TabIndex = 0;
            this.lblAllowed.Text = "allowed";
            this.lblAllowed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTenMinusReason
            // 
            this.lblTenMinusReason.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTenMinusReason.Font = new System.Drawing.Font("Segoe UI Light", 8F, System.Drawing.FontStyle.Bold);
            this.lblTenMinusReason.Location = new System.Drawing.Point(0, 98);
            this.lblTenMinusReason.Margin = new System.Windows.Forms.Padding(0);
            this.lblTenMinusReason.Name = "lblTenMinusReason";
            this.lblTenMinusReason.Size = new System.Drawing.Size(146, 50);
            this.lblTenMinusReason.TabIndex = 3;
            this.lblTenMinusReason.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lblOnePlusReason
            // 
            this.lblOnePlusReason.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblOnePlusReason.Font = new System.Drawing.Font("Segoe UI Light", 8F, System.Drawing.FontStyle.Bold);
            this.lblOnePlusReason.Location = new System.Drawing.Point(470, 98);
            this.lblOnePlusReason.Margin = new System.Windows.Forms.Padding(0);
            this.lblOnePlusReason.Name = "lblOnePlusReason";
            this.lblOnePlusReason.Size = new System.Drawing.Size(146, 50);
            this.lblOnePlusReason.TabIndex = 3;
            this.lblOnePlusReason.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lblTenPlusReason
            // 
            this.lblTenPlusReason.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTenPlusReason.Font = new System.Drawing.Font("Segoe UI Light", 8F, System.Drawing.FontStyle.Bold);
            this.lblTenPlusReason.Location = new System.Drawing.Point(621, 98);
            this.lblTenPlusReason.Margin = new System.Windows.Forms.Padding(0);
            this.lblTenPlusReason.Name = "lblTenPlusReason";
            this.lblTenPlusReason.Size = new System.Drawing.Size(146, 50);
            this.lblTenPlusReason.TabIndex = 3;
            this.lblTenPlusReason.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lblOneMinusReason
            // 
            this.lblOneMinusReason.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblOneMinusReason.Font = new System.Drawing.Font("Segoe UI Light", 8F, System.Drawing.FontStyle.Bold);
            this.lblOneMinusReason.Location = new System.Drawing.Point(151, 98);
            this.lblOneMinusReason.Margin = new System.Windows.Forms.Padding(0);
            this.lblOneMinusReason.Name = "lblOneMinusReason";
            this.lblOneMinusReason.Size = new System.Drawing.Size(146, 50);
            this.lblOneMinusReason.TabIndex = 3;
            this.lblOneMinusReason.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // btnTenMinus
            // 
            this.btnTenMinus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnTenMinus.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold);
            this.btnTenMinus.Location = new System.Drawing.Point(14, 149);
            this.btnTenMinus.Name = "btnTenMinus";
            this.btnTenMinus.Size = new System.Drawing.Size(120, 100);
            this.btnTenMinus.TabIndex = 2;
            this.btnTenMinus.Text = "-10";
            this.btnTenMinus.UseVisualStyleBackColor = true;
            this.btnTenMinus.Click += new System.EventHandler(this.btnTenMinus_Click);
            // 
            // btnOnePlus
            // 
            this.btnOnePlus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnOnePlus.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold);
            this.btnOnePlus.Location = new System.Drawing.Point(480, 149);
            this.btnOnePlus.Name = "btnOnePlus";
            this.btnOnePlus.Size = new System.Drawing.Size(120, 100);
            this.btnOnePlus.TabIndex = 2;
            this.btnOnePlus.Text = "+1";
            this.btnOnePlus.UseVisualStyleBackColor = true;
            this.btnOnePlus.Click += new System.EventHandler(this.btnOnePlus_Click);
            // 
            // btnTenPlus
            // 
            this.btnTenPlus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnTenPlus.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold);
            this.btnTenPlus.Location = new System.Drawing.Point(632, 149);
            this.btnTenPlus.Name = "btnTenPlus";
            this.btnTenPlus.Size = new System.Drawing.Size(120, 100);
            this.btnTenPlus.TabIndex = 2;
            this.btnTenPlus.Text = "+10";
            this.btnTenPlus.UseVisualStyleBackColor = true;
            this.btnTenPlus.Click += new System.EventHandler(this.btnTenPlus_Click);
            // 
            // btnOneMinus
            // 
            this.btnOneMinus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnOneMinus.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold);
            this.btnOneMinus.Location = new System.Drawing.Point(164, 149);
            this.btnOneMinus.Name = "btnOneMinus";
            this.btnOneMinus.Size = new System.Drawing.Size(120, 100);
            this.btnOneMinus.TabIndex = 2;
            this.btnOneMinus.Text = "-1";
            this.btnOneMinus.UseVisualStyleBackColor = true;
            this.btnOneMinus.Click += new System.EventHandler(this.btnOneMinus_Click);
            // 
            // BuyMealsPickAmountUC
            // 
            this.Controls.Add(this.lblTenPlusReason);
            this.Controls.Add(this.lblOnePlusReason);
            this.Controls.Add(this.lblOneMinusReason);
            this.Controls.Add(this.lblTenMinusReason);
            this.Controls.Add(this.btnTenMinus);
            this.Controls.Add(this.btnOnePlus);
            this.Controls.Add(this.btnTenPlus);
            this.Controls.Add(this.btnOneMinus);
            this.Controls.Add(this.lblMealAmountSelected);
            this.Controls.Add(this.lblAllowed);
            this.Controls.Add(this.lblMealTitle);
            this.Name = "BuyMealsPickAmountUC";
            this.Size = new System.Drawing.Size(770, 260);
            this.Load += new System.EventHandler(this.BuyMealsPickAmountUC_Load);
            this.VisibleChanged += new System.EventHandler(this.BuyMealsPickAmountUC_VisibleChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.BuyMealsPickAmountUC_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Label lblMealTitle;
        protected System.Windows.Forms.Label lblMealAmountSelected;
        protected XButton btnOneMinus;
        protected XButton btnTenMinus;
        protected XButton btnOnePlus;
        protected XButton btnTenPlus;
        protected System.Windows.Forms.Label lblAllowed;
        protected System.Windows.Forms.Label lblTenMinusReason;
        protected System.Windows.Forms.Label lblOnePlusReason;
        protected System.Windows.Forms.Label lblTenPlusReason;
        protected System.Windows.Forms.Label lblOneMinusReason;

    }
}
