﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.SomborKiosk
{
    public class ReceiptWithdrawSombor:Receipt
    {
        public double moneyInKiosk;
        public double moneyWithdrawn;

        public ReceiptWithdrawSombor(string rt, double mt, double mw, string ts)
            : base(rt, "", "", 0, ts, null)
        {
            moneyInKiosk = mt;
            moneyWithdrawn = mw;
        }

        protected override void preparePrinting(object sender, PrintPageEventArgs e)
        {
            using (Font font1 = new Font("Segoe UI Light", 12, FontStyle.Bold, GraphicsUnit.Point))
            {
                StringFormat stringFormat = new StringFormat();
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;

                StringFormat leftStringFormat = new StringFormat();
                leftStringFormat.Alignment = StringAlignment.Near;
                leftStringFormat.LineAlignment = StringAlignment.Center;

                StringFormat rightStringFormat = new StringFormat();
                rightStringFormat.Alignment = StringAlignment.Far;
                rightStringFormat.LineAlignment = StringAlignment.Center;

//                e.Graphics.DrawImage(logo, 115, 10);
//                Rectangle rectTitleArab = new Rectangle(10, 100, 280, 30);
//                Rectangle rectTitleEng = new Rectangle(10, 120, 280, 30);
//                e.Graphics.DrawString(@"جامعة الطائف", new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectTitleArab, stringFormat);
//                e.Graphics.DrawString(@"Taif University", new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectTitleEng, stringFormat);

                int y = 20;

                Rectangle rectOrg = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(guiLang.Receipt_Organization_ScBgd, new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectOrg, stringFormat);
                y += 20;
                
                rectOrg = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(guiLang.Receipt_Organization_Address_ScBgd, new Font(FontFamily.GenericSansSerif, 9), Brushes.Black, rectOrg, stringFormat);
                y += 40;
                

                Rectangle rectHeader = new Rectangle(10, y, 280, 30);
                e.Graphics.DrawString(guiLang.Receipt_Header, new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectHeader, stringFormat);
                y += 40;

                Rectangle rectTitle = new Rectangle(10, y, 280, 30);
                e.Graphics.DrawString(receiptTitle, new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectTitle, stringFormat);
                y += 40;
                
                //withdrawn
                y += 20;
                Rectangle rectTotalBalance = new Rectangle(10, y, 280, 25);
                string totalBalance = guiLang.Receipt_Withdrawal_MoneyInKiosk + ": " + Utils.getCurrencyString(moneyInKiosk, true);
                e.Graphics.DrawString(totalBalance, new Font(FontFamily.GenericSansSerif, 12, FontStyle.Bold), Brushes.Black, rectTotalBalance, leftStringFormat);
                y += 20;

                Rectangle rectWithdrawnBalance = new Rectangle(10, y, 280, 25);
                string withdrawnBalance = guiLang.Receipt_Withdrawal_MoneyWithdrawn + ": " + Utils.getCurrencyString(moneyWithdrawn, true);
                e.Graphics.DrawString(withdrawnBalance, new Font(FontFamily.GenericSansSerif, 12, FontStyle.Bold), Brushes.Black, rectWithdrawnBalance, leftStringFormat);
                y += 40;

                Rectangle rectTimeStamp = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(timeStamp, new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectTimeStamp, stringFormat);
                y += 20;

                Rectangle allOverRectangle = new Rectangle(10, 10, 280, y + 10);
                e.Graphics.DrawRectangle(Pens.Black, allOverRectangle);

            }
        }
    }
}
