﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MealsChecker;
using System.Threading;

namespace PisKiosk.SomborKiosk
{
    public partial class UserDataFormSombor : PisKiosk.UserDataForm
    {
        public UserDataFormSombor()
        {
            InitializeComponent();

            //userData = new UserDataSombor();
            ud = (UserDataSombor)Program.KioskApplication.UserData;  //ne znam kako drugacije da downcastujem nego da u svakoj izvedenoj klasi uvedem 
            ad = AbonentData.getInstance();
        }

        protected AbonentData ad;
        protected MealsChecker.MealsChecker mc = null;
        public UserDataSombor ud;

        public bool CardJustInserted { get; set; }  //oznacava da je kartica upravo ubacena i da treba procitati podatke sa kartice, pozvati MealsChecker da sredi bonove i ponovo upisati podatke na karticu. Cim se to uradi, sve dok se ne izvadi kartica ne treba vise da se sredjuju obroci i da se upisuju nazad na karticu

        private static UserDataFormSombor instance = null;
        public static UserDataFormSombor getInstance()
        {
            if (instance == null)
            {
                instance = new UserDataFormSombor();
            }
            return instance;
        }

        public override void readCardData()
        {
            ud.readCardData();
            if (CardJustInserted)
            {
                if (ud.CardType == CardType.Student)
                {
                    //kada je tek ubacena kartica, pa treba srediti stanje obroka na kartici
                    getMealsPricesFromDB(); //procitaj cene i ostale vazne podatke iz baze

                    initializeMealsChecker();
                    writeCardAbonentData();
                    CardJustInserted = false;
                }
            }
        }

        private void getMealsPricesFromDB()
        {
            //procitaj cene obroka iz baze za korisnika koji je ubacio karticu

            string cardNumber = ud.CardNumber;

            double breakfastPrice = 0;
            double lunchPrice = 0;
            double dinnerPrice = 0;
            double breakfastNextPrice = 0;
            double lunchNextPrice = 0;
            double dinnerNextPrice = 0;
            bool isResident = false;
            int abonentCategory = 0;  //todo ne mogu da se setim cemu mi ovo sluzi u programu za kiosk
            bool isCardActive = false;
            bool firstShoppingInMonth = false;

            WebServiceAdapter.getMealsPrices(cardNumber, out breakfastPrice, out lunchPrice, out dinnerPrice, out breakfastNextPrice, out lunchNextPrice, out dinnerNextPrice, out abonentCategory, out isCardActive, out isResident, out firstShoppingInMonth);

            ud.BreakfastPrice = breakfastPrice;
            ud.LunchPrice = lunchPrice;
            ud.DinnerPrice = dinnerPrice;
            ud.BreakfastNextPrice = breakfastNextPrice;
            ud.LunchNextPrice = lunchNextPrice;
            ud.DinnerNextPrice = dinnerNextPrice;
            ud.IsResident = isResident;
            ud.IsFirstShoppingInMonth = firstShoppingInMonth;

            if (!isCardActive)
            {
                throw new Exception(guiLang.UserDataFormScBgd_Error_CardNotActive);
            }
        }

        protected void initializeMealsChecker()
        {
            //TODO vidi koja instanca mealscheckera treba da se pozove za Sombor - pogledaj u domovima ucenika
            
            //inicijalizuje se MealsChecker
            ad.initializeMealsChecker(ud.MealsArray, ud.EatenMealsArray);
            getMealsCheckerInstance();
            mc.calculateMealsInitially();
            ad.getAbonentData(ud.MealsArray, ud.EatenMealsArray);
            ud.setGuiParameters();
        }

        private void getMealsCheckerInstance()
        {
            if (ud.IsStudent)
            {
                //ako su u pitanju studenti, moguci su razliciti scenariji
                //1. ako studenti kupuju obroke u kompletu za odredjeni period vremena, oni funkcionisu slicno kao ucenike kartice - automatski im se pune obroci, 
                //a da li imaju pravo da jedu, odnosno da li su kupili obroke za navedeni dan je regulisano fajlovima dozvola
                //2. ako studenti kupuju obroke na komad koji im vaze tokom celog meseca, onda je to slicno kao u studentskim domovima i koristi drugi MealsChecker
                //U ovom slucaju postoje dve varijante: da se obroci prenose u novi mesec i da se na kraju meseca ponistavaju
                //Koja varijanta je u pitanju se regulise u settings fajlu

                if (XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "studentiPrenosBonovaUSledeciMesec").Equals("0"))
                {
                    //studenti, ponistavaju se bonovi na kraju meseca
                    mc = new MealsCheckerCashierBelgrade(ud.IsFirstShoppingInMonth);
                        //Program.pkf.writeToLog("MealsChecker: Student, kupio bonove na komad. Bonovi se poništavaju na kraju meseca.");
                }
                else
                {
                    //studenti, prenose se bonovi na kraju meseca
                    mc = new MealsCheckerNoviSadCashier(DateTime.Now);
                        //Program.pkf.writeToLog("MealsChecker: Student, kupio bonove na komad. Nepotrošeni bonovi se prenose u sledeći mesec.");
                }
            }
            else
            {
                throw new Exception("Only students can use info kiosk.");
            }
        }

        public void changeMealsInMealsChecker(int m, int y, int b, int l, int d)
        {
            mc.calculateMealsOnChange(m, y, b, l, d);
            ad.getAbonentData(ud.MealsArray, ud.EatenMealsArray);
            ud.setGuiParameters();
        }

        public void writeCardAbonentData()
        {
            ud.writeCardAbonentData();
        }

        public override void showUserData()
        {
            lblCardOwner.Text = ud.UserName;
            lblCardNumberLabel.Text = guiLang.UserDataForm_Label_lblCardNumberLabel;
            lblCardNumber.Text = ud.CardNumber;
            lblCashAvailableLabel.Text = guiLang.UserDataForm_Label_lblCashAvailableLabel;
            lblCashAvailable.Text = Utils.getCurrencyString(getCashFromCard(), true);

            //todo
            lblHasMealsBreakfast.Text = ud.Breakfast;
            lblAllowedMealsToBuyBreakfast.Text = ud.BreakfastLeft;
            lblMealsSpentTodayBreakfast.Text = ud.EatenBreakfast;
            lblHasMealsForNextMonthBreakfast.Text = ud.BreakfastNext;

            lblHasMealsLunch.Text = ud.Lunch;
            lblAllowedMealsToBuyLunch.Text = ud.LunchLeft;
            lblMealsSpentTodayLunch.Text = ud.EatenLunch;
            lblHasMealsForNextMonthLunch.Text = ud.LunchNext;

            lblHasMealsDinner.Text = ud.Dinner;
            lblAllowedMealsToBuyDinner.Text = ud.DinnerLeft;
            lblMealsSpentTodayDinner.Text = ud.EatenDinner;
            lblHasMealsForNextMonthDinner.Text = ud.DinnerNext;

            showPhoto(ud.CardNumber);

            this.Refresh();
            Application.DoEvents();

            //Program.KioskApplication.writeToLog("Inserted card: " + ud.CardNumber + ", " + ud.UserName + ", cash available: " + lblCashAvailable.Text, PisLog.EventType.CardInserted);
        }

        public override double getCashFromCard()
        {
            double cashOnCard = 0;
            cashOnCard = WebServiceAdapter.getMoneyBalance(ud.CardNumber);
            ud.CashOnCard = cashOnCard;
            return cashOnCard;
        }

        private void UserDataFormSombor_Load(object sender, EventArgs e)
        {
            makePicturesHashSet();
        }

        //public void setDataOnScreen()
        //{
        //    setLabels();
        //    readCardData();
        //    showUserData();
        //    DataShownOnScreen = true;
        //}

        private void UserDataFormSombor_VisibleChanged(object sender, EventArgs e)
        {
            //if (this.Visible)
            //{
            //    //kada se ubaci kartica podaci bi trebalo da se prikazu pre nego sto forma postane vidljiva pozivom setDataOnScreen()
            //    //ali ako se dok je forma otvorena klikne na promenu jezika, to se ne bi dogodilo bez koda koji sledi
            //    if (!DataShownOnScreen)
            //    {
            //        setDataOnScreen();
            //    }
            //    DataShownOnScreen = false;//odmah ponisti informaciju da su postavljeni parametri
            //}
            //else
            //{
            //    DataShownOnScreen = false;
            //}

            if (this.Visible)
            {
                MessageReversedColors.getInstance((guiLang.WaitingForCardForm_Message_CardInsert).ToUpper(), MessageType.Information).Show();

                this.Refresh();
                Application.DoEvents();

                Thread.Sleep(2000); //zbog toga da se stabilizuje kartica u citacu kako bi je lepo procitao
                SomborKiosk.CardAdapterContactlessSombor ci = (SomborKiosk.CardAdapterContactlessSombor)CardInterface.getInstance().CardInterf;
                ci.connect();
                                
                setLabels();
                readCardData();
                //ako je sluzbenicka kartica ubacena, otisao je na login stranicu i dalje ne treba nista da radi
                if (ud.CardType == CardType.Student)
                {
                    showUserData();
                    MessageReversedColors.getInstance("", MessageType.Information).Hide();                    
                }
                else if (ud.CardType == CardType.Official)
                {
                    Login.getInstance().setOperaterInitiator(0, ud.OperaterCard);  //za Sombor, idOperatera nije bitan, vec samo serijski broj kartice
                    Login.getInstance().Show();
                    Program.KioskApplication.Hide();
                }
            }
            else
            {
                clearUserData();
            }

        }

        private void btnBuyFood_Click(object sender, EventArgs e)
        {
            bool openForm = true;
            try
            {
                Program.KioskApplication.checkPrinterStatus();
            }
            catch (Exception)
            {
                //racun nije dostupan, pitaj korisnika da li zeli da nastavi
                MessageChoice.getInstance(guiLang.MessageChoice_NoReceiptAvailable, MessageType.Warning).ShowDialog();
                if (!Program.KioskApplication.MessageChoiceResult)
                {
                    openForm = false;
                }
            }
            try
            {
                if (openForm)
                {
                    Program.KioskApplication.showFormInPanel(BuyMealsFormSombor.getInstance(true));
                }
            }
            catch (Exception ex)
            {
                MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
            }
        }

        private void btnInsertCash_Click(object sender, EventArgs e)
        {
            bool openForm = true;
            try
            {
                Program.KioskApplication.checkPrinterStatus();
            }
            catch (Exception)
            {
                //racun nije dostupan, pitaj korisnika da li zeli da nastavi
                MessageChoice.getInstance(guiLang.MessageChoice_NoReceiptAvailable, MessageType.Warning).ShowDialog();
                if (!Program.KioskApplication.MessageChoiceResult)
                {
                    openForm = false;
                }
            }
            try
            {
                if (openForm)
                {
                    Program.KioskApplication.showFormInPanel(CashInsertFormSombor.getInstance(this));
                }
            }
            catch (Exception ex)
            {
                MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
            }
        }
    }
}
