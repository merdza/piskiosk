﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.SomborKiosk
{
    public partial class WaitingForCardFormSombor : PisKiosk.WaitingForCardForm
    {
        public WaitingForCardFormSombor()
        {
            InitializeComponent();
        }

        private static WaitingForCardFormSombor instance = null;
        public static WaitingForCardFormSombor getInstance()
        {
            if (instance == null)
            {
                instance = new WaitingForCardFormSombor();
            }
            return instance;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cardInserted();
        }

        public override void cardInserted()
        {
            try
            {
                UserDataFormSombor ud = UserDataFormSombor.getInstance();
                ud.CardJustInserted = true;
                Program.KioskApplication.showFormInPanel(ud);
            }
            catch (Exception ex)
            {
                MessageReversedColors.getInstance("", MessageType.Information).Hide();
                MessageClassic.getInstance(guiLang.Message_MessageType_Error + ": " + ex.Message, MessageType.Error).Show();
                Program.KioskApplication.log.Error("Error : " + ex.Message, ex); 
            }
        }
    }
}
