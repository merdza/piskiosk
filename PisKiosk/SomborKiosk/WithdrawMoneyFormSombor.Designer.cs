﻿namespace PisKiosk.SomborKiosk
{
    partial class WithdrawMoneyFormSombor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // lblMoneyInKiosk
            // 
            this.lblMoneyInKiosk.Location = new System.Drawing.Point(82, 108);
            // 
            // lblMoneyInKioskToday
            // 
            this.lblMoneyInKioskToday.Location = new System.Drawing.Point(82, 218);
            // 
            // btnWithdrawMoney
            // 
            this.btnWithdrawMoney.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(191)))), ((int)(((byte)(243)))));
            this.btnWithdrawMoney.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(211)))));
            this.btnWithdrawMoney.Location = new System.Drawing.Point(204, 536);
            this.btnWithdrawMoney.Click += new System.EventHandler(this.btnWithdrawMoney_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(191)))), ((int)(((byte)(243)))));
            this.btnCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(246)))), ((int)(((byte)(211)))));
            this.btnCancel.Location = new System.Drawing.Point(682, 536);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // WithdrawMoneyFormSombor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1190, 643);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "WithdrawMoneyFormSombor";
            this.VisibleChanged += new System.EventHandler(this.WithdrawMoneyFormSombor_VisibleChanged);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
