﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.SomborKiosk
{
    public static class WebServiceAdapter
    {
        private static WebServiceDU.ServiceDUClient sc = new WebServiceDU.ServiceDUClient();

        //private static WebServiceDU.ServiceDUClient getServiceClient()
        //{
        //    try
        //    {
        //        if (sc != null)
        //        {
        //            if (sc.State != System.ServiceModel.CommunicationState.Opened)
        //            {
        //                sc = new WebServiceDU.ServiceDUClient();
        //            }
        //        }
        //        else
        //        {
        //            sc = new WebServiceDU.ServiceDUClient();
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        sc = new WebServiceDU.ServiceDUClient();
        //    }

        //    return sc;
        //}

        public static float getMoneyBalance(string brojKartice)
        {
            try
            {
                string errorMessage;
                float ret = 0;
                try
                {
                    ret = sc.GetMoneyBalance(brojKartice, out errorMessage);
                }
                catch (Exception)
                {
                    sc = new WebServiceDU.ServiceDUClient();
                    ret = sc.GetMoneyBalance(brojKartice, out errorMessage);
                }

                if (!errorMessage.Equals(""))
                {
                    throw new Exception(errorMessage);
                }
                return ret;
            }
            catch (Exception ex)
            {
//                LogFile.AppendLogFile(String.Format("Nije uspelo poziv web servisa. GetMoneyBalance. " + ex.Message));
                throw new Exception("Nije uspelo povezivanje sa bazom podataka. GetMoneyBalance. " + ex.Message);
            }


        }

        public static void getMealsPrices(string cardNumber, out double breakfastPrice, out double lunchPrice, out double dinnerPrice, out double breakfastNextPrice, out double lunchNextPrice, out double dinnerNextPrice, out int abonentCategory, out bool isCardActive, out bool isResident, out bool firstShoppingInMonth)
        {
            try
            {
                string errorMessage;
                try
                {
                    sc.GetFoodCost(cardNumber, out breakfastPrice, out lunchPrice, out dinnerPrice, out breakfastNextPrice, out lunchNextPrice, out dinnerNextPrice, out abonentCategory, out isCardActive, out isResident, out firstShoppingInMonth, out errorMessage);
                }
                catch (Exception)
                {
                    sc = new WebServiceDU.ServiceDUClient();
                    sc.GetFoodCost(cardNumber, out breakfastPrice, out lunchPrice, out dinnerPrice, out breakfastNextPrice, out lunchNextPrice, out dinnerNextPrice, out abonentCategory, out isCardActive, out isResident, out firstShoppingInMonth, out errorMessage);
                }
                if (!errorMessage.Equals(""))
                {
                    throw new Exception(errorMessage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GetFoodCost problem " + ex.Message);
            }
        }

        public static int buyMealsWithEMoney(string cardNumber, double totalAmount, int breakfast, int lunch, int dinner, int breakfastNext, int lunchNext, int dinnerNext, int month)
        {
            int ret = 0;
            try
            {
                string errorMessage;
                try
                {
                    ret = sc.BuyFood(cardNumber, Program.KioskApplication.KioskName, totalAmount, breakfast, lunch, dinner, breakfastNext, lunchNext, dinnerNext, month, out errorMessage);
                }
                catch (Exception)
                {
                    sc = new WebServiceDU.ServiceDUClient();
                    ret = sc.BuyFood(cardNumber, Program.KioskApplication.KioskName, totalAmount, breakfast, lunch, dinner, breakfastNext, lunchNext, dinnerNext, month, out errorMessage);
                }

                if (!errorMessage.Equals(""))
                {
                    throw new Exception(errorMessage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BuyFood problem " + ex.Message);
            }
            return ret;
        }

        public static void undoTransaction(int idTransaction)
        {
            try
            {
                string errorMessage;
                try
                {
                    sc.UndoBuyFood(idTransaction, out errorMessage);
                }
                catch (Exception)
                {
                    sc = new WebServiceDU.ServiceDUClient();
                    sc.UndoBuyFood(idTransaction, out errorMessage);
                }

                if (!errorMessage.Equals(""))
                {
                    throw new Exception(errorMessage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("UndoBuyFood problem " + ex.Message);
            }
        }

        public static void moneyDeposit(string cardNumber, int bankNote)
        {
            try
            {
                string errorMessage = "";
                try
                {
                    sc.BillingEMoney(Program.KioskApplication.KioskName, cardNumber, bankNote, out errorMessage);
                }
                catch (Exception)
                {
                    sc = new WebServiceDU.ServiceDUClient();
                    sc.BillingEMoney(Program.KioskApplication.KioskName, cardNumber, bankNote, out errorMessage);
                }

                if (!errorMessage.Equals(""))
                {
                    throw new Exception(errorMessage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BuyFood problem " + ex.Message);
            }
        }

        public static int getKioskOperator(string kioskName)
        {
            try
            {
                string errorMessage;
                int ret = 0;
                try
                {
                    ret = sc.OperaterKioska(kioskName, out errorMessage);
                }
                catch (Exception)
                {
                    sc = new WebServiceDU.ServiceDUClient();
                    ret = sc.OperaterKioska(kioskName, out errorMessage);
                }

                if (!errorMessage.Equals(""))
                {
                    throw new Exception(errorMessage);
                }
                return ret;
            }
            catch (Exception ex)
            {
                //                LogFile.AppendLogFile(String.Format("Nije uspelo poziv web servisa. GetMoneyBalance. " + ex.Message));
                throw new Exception("Nije uspelo povezivanje sa bazom podataka. getKioskOperator. " + ex.Message);
            }
        }

        public static bool isCardForThisKiosk(string card)
        {
            //da li operaterska kartica koja je ubacena sme da podize pare iz kioska
            //card je u SCBgd u stvari operaterID
            try
            {
                string errorMessage = "";
                bool ret = false;
                try
                {
                    ret = sc.IsCardForThisKiosk(Program.KioskApplication.KioskName, card, out errorMessage);
                }
                catch (Exception)
                {
                    sc = new WebServiceDU.ServiceDUClient();
                    ret = sc.IsCardForThisKiosk(Program.KioskApplication.KioskName, card, out errorMessage);
                }

                if (!errorMessage.Equals(""))
                {
                    throw new Exception(errorMessage);
                }
                return ret;
            }
            catch (Exception ex)
            {
                //                LogFile.AppendLogFile(String.Format("Nije uspelo poziv web servisa. GetMoneyBalance. " + ex.Message));
                throw new Exception("Nije uspelo povezivanje sa bazom podataka. getKioskOperator. " + ex.Message);
            }
        }

        public static void getMoneyInformation(out double total, out double totalUntilToday)
        {
            try
            {
                string errorMessage = "";
                string kioskName = Program.KioskApplication.KioskName;
                try
                {
                    sc.getMoneyInformation(kioskName, out total, out totalUntilToday, out errorMessage);
                }
                catch (Exception)
                {
                    sc = new WebServiceDU.ServiceDUClient();
                    sc.getMoneyInformation(kioskName, out total, out totalUntilToday, out errorMessage);
                }
                if (!errorMessage.Equals(""))
                {
                    throw new Exception(errorMessage);
                }
            }
            catch (Exception ex)
            {
                //                LogFile.AppendLogFile(String.Format("Nije uspelo poziv web servisa. GetMoneyBalance. " + ex.Message));
                throw new Exception("Nije uspelo povezivanje sa bazom podataka. getMoneyInformation. " + ex.Message);
            }
        }

        public static void dischargeKiosk(double amountToDischarge, double amountToLeave)
        {

            try
            {
                string errorMessage = "";
                string kioskName = Program.KioskApplication.KioskName;
                try
                {
                    sc.DischargeKiosks(kioskName, amountToDischarge, amountToLeave, out errorMessage);
                }
                catch (Exception)
                {
                    sc = new WebServiceDU.ServiceDUClient();
                    sc.DischargeKiosks(kioskName, amountToDischarge, amountToLeave, out errorMessage);
                }
                if (!errorMessage.Equals(""))
                {
                    throw new Exception(errorMessage);
                }
            }
            catch (Exception ex)
            {
                //                LogFile.AppendLogFile(String.Format("Nije uspelo poziv web servisa. GetMoneyBalance. " + ex.Message));
                throw new Exception("Nije uspelo povezivanje sa bazom podataka. dischargeKiosk. " + ex.Message);
            }
        }
    }
}
