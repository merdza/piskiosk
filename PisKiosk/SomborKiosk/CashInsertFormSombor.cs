﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PisKiosk.SomborKiosk
{
    public partial class CashInsertFormSombor : PisKiosk.CashInsertForm
    {
        public CashInsertFormSombor():base()
        {
            InitializeComponent();

            lblKioskApp.Text = "Sombor";
        }

        private static CashInsertFormSombor instance = null;

        public static CashInsertFormSombor getInstance(MainForm w)
        {
            if (instance == null)
            {
                instance = new CashInsertFormSombor();
            }
            instance.whoCalledMe = w;
            return instance;
        }

        protected override void setCashBalance()
        {
            lblCardMoneyBalance.Text = guiLang.BuyMealsFormScBgd_Label_lblCardMoneyBalance + " " + Utils.getCurrencyString(UserDataFormSombor.getInstance().ud.CashOnCard, true);
        }

        public override void bankNoteCredited(int bankNote)
        {
            try
            {
                InsertedBankNote newBankNote = new InsertedBankNoteSombor();

                newBankNote.insertBankNote(bankNote);
                insertedBankNotes.Add(newBankNote);

                emphasize(bankNote, true);

                addNoteToScreen(bankNote);
                bankNoteProcessed();
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Error inserting banknote. Banknote is in the kiosk, but it was not recorded at the card or in the database. " + ex.Message, ex);
                MessageClassic.getInstance(guiLang.CashInsertForm_LostBankNote_Warning, MessageType.Error).Show();
            }
        }

        private void btnEndInsertingMoney_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.log.Info("Clicked End Inserting Money button. ");
            if (insertedBankNotes.Count > 0)
            {
                //stampaj racun
                //prikazi dijalog sa pitanjem da li hoce da stampa
                if (Program.KioskApplication.PrinterAvailable)
                {
                    MessageChoice.getInstance(guiLang.MessageChoice_DoYouWantReceipt, MessageType.Question).ShowDialog();
                }

                if (Program.KioskApplication.MessageChoiceResult)
                {
                    //stampaj racun
                    printReceipt(); // ovde nema transactionId-a
                }
            }
            Program.KioskApplication.showFormInPanel(whoCalledMe);
        }

        public void printReceipt()
        {
            try
            {
                UserDataSombor ud = UserDataFormSombor.getInstance().ud;

                string receiptTitle = guiLang.Receipt_Title_Money_Inserted;

                string receiptNumber = "";

                DateTime now = DateTime.Now;

                string timeStamp = Utils.addLeadingZeros(now.Day, 2) + @"." + Utils.addLeadingZeros(now.Month, 2) + @"." + now.Year.ToString() + @". " + Utils.addLeadingZeros(now.Hour, 2) + @":" + Utils.addLeadingZeros(now.Minute, 2) + @":" + Utils.addLeadingZeros(now.Second, 2);//KSA date format dd/mm/yyy

                double insertedTotal = 0;
                List<ReceiptItem> receiptItems = new List<ReceiptItem>();

                foreach (var insertedNote in insertedBankNotes)
                {
                    receiptItems.Add(new ReceiptItem(guiLang.Receipt_BankNote + " " + Utils.getCurrencyString(Convert.ToInt32(insertedNote.Value), false), 1, Convert.ToDouble(insertedNote.Value)));
                    insertedTotal += insertedNote.Value;
                }

                Receipt rp = new ReceiptSombor(receiptTitle, receiptNumber, ud.CardNumber, insertedTotal, timeStamp, receiptItems);
                rp.printReceipt();
            }
            catch (Exception ex)
            {
                string err = "Error printing receipt. " + " " + ex.Message;
                Program.KioskApplication.log.Error(err, ex);
                throw new Exception(err);
            }
        }

    }
}
