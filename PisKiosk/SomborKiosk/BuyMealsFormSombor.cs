﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


/*
 logovanje operatera za test
 * 
USE [DomoviUcenikaBackup]
GO

DECLARE	@return_value int,
		@idLog int,
		@result int

EXEC	@return_value = [dbo].[spIKLogin]
		@idOperater = 26836,
		@imeKioska = N'KioskSO1',
		@idLog = @idLog OUTPUT,
		@result = @result OUTPUT
 */



namespace PisKiosk.SomborKiosk
{
    public partial class BuyMealsFormSombor : PisKiosk.BuyMealsForm
    {
        public BuyMealsFormSombor()
        {
            InitializeComponent();

            this.BackColor = Program.KioskApplication.BackgroundColor;

            gridShoppingCart.EnableHeadersVisualStyles = false;

            gridShoppingCart.ColumnHeadersDefaultCellStyle.BackColor = Program.KioskApplication.TextForeColor2;
            gridShoppingCart.ColumnHeadersDefaultCellStyle.ForeColor = Program.KioskApplication.BackgroundColor;

            gridShoppingCart.BackgroundColor = Program.KioskApplication.BackgroundColor;
            gridShoppingCart.GridColor = Program.KioskApplication.TextForeColor;
            gridShoppingCart.DefaultCellStyle.BackColor = Program.KioskApplication.BackgroundColor;
            gridShoppingCart.DefaultCellStyle.ForeColor = Program.KioskApplication.TextForeColor;

            //DataGridViewCellStyle style = gridShoppingCart.ColumnHeadersDefaultCellStyle;
            //style.BackColor = Color.Navy;
            //style.ForeColor = Color.White;
            //style.Font = new Font("Segoe UI Light", 30F, FontStyle.Bold);

            lblCardMoneyBalance.ForeColor = Program.KioskApplication.TextForeColor2;
            lblMoneyBalanceAfterShopping.ForeColor = Program.KioskApplication.TextForeColor2;

            btnBuyFood.BackColor = Program.KioskApplication.TextForeColor2;
            btnBuyFood.ForeColor = Program.KioskApplication.BackgroundColor;

            btnCancelBuyFood.BackColor = Program.KioskApplication.TextForeColor2;
            btnCancelBuyFood.ForeColor = Program.KioskApplication.BackgroundColor;

            btnInsertCash.BackColor = Program.KioskApplication.TextForeColor2;
            btnInsertCash.ForeColor = Program.KioskApplication.BackgroundColor;

            //nema sortiranja shopping carta
            foreach (DataGridViewColumn column in gridShoppingCart.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.showFormInPanel(UserDataFormSombor.getInstance());
        }

        private static BuyMealsFormSombor instance = null;
        public static BuyMealsFormSombor getInstance(bool newCard = false)
        {
            //new card se odnosi na to da li se ova forma inicijalizuje od pocetka sa novim objektima koji se na njoj koriste
            if (instance == null)
            {
                instance = new BuyMealsFormSombor();
                instance.buyMealsData = new BuyMealsDataSombor();
            }

            if (newCard)
            {
                instance.buyMealsData.resetBuyMealsData();
                instance.setDateBoundParameters();

                instance.initBuyMealsData();
            }

            return instance;
        }

        private void initBuyMealsData()
        {
            //procitaj cene obroka iz baze za korisnika koji je ubacio karticu

            UserDataSombor ud = UserDataFormSombor.getInstance().ud;

            DateTime now = DateTime.Now;
            buyMealsData.addMeal(new MealSombor(now.Month, now.Year, MealType.Breakfast, ud.BreakfastPrice));
            buyMealsData.addMeal(new MealSombor(now.Month, now.Year, MealType.Lunch, ud.LunchPrice));
            buyMealsData.addMeal(new MealSombor(now.Month, now.Year, MealType.Dinner, ud.DinnerPrice));
            if (MayBuyForNextMonth)
            {
                now = now.AddMonths(1);
                buyMealsData.addMeal(new MealSombor(now.Month, now.Year, MealType.Breakfast, ud.BreakfastNextPrice));
                buyMealsData.addMeal(new MealSombor(now.Month, now.Year, MealType.Lunch, ud.LunchNextPrice));
                buyMealsData.addMeal(new MealSombor(now.Month, now.Year, MealType.Dinner, ud.DinnerNextPrice));
            }
        }

        private void setCashValues()
        {
            //postavi koliko para ima u novcaniku
            buyMealsData.CardMoneyBalance = UserDataFormSombor.getInstance().ud.CashOnCard;
            buyMealsData.MoneyBalanceAfterBuyingSelected = buyMealsData.CardMoneyBalance - buyMealsData.getTotalPrice();
        }

        private void setDateBoundParameters()
        {
            //kupovina na dekade
            int stopBuyingOnDecades = 0; //kog datuma prestaje dekadna kupovina i pocinje kupovina bonova na komad
            try
            {
                stopBuyingOnDecades = Convert.ToInt32(XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "stopBuyingOnDecadesDay"));
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Error reading settings file, stopBuyingOnDecadesDay tag. " + ex.Message, ex);
                Application.Exit();
            }

            if (DateTime.Now.Day < stopBuyingOnDecades)
            {
                BuyingMealsByDecades = true;
            }
            else
            {
                BuyingMealsByDecades = false;
            }

            //da li sme da kupuje i za sledeci mesec?
            int buyNextMonth = 0; //kog datuma prestaje dekadna kupovina i pocinje kupovina bonova na komad
            try
            {
                buyNextMonth = Convert.ToInt32(XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "buyNextMonthDay"));
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Error reading settings file, buyNextMonthDay tag. " + ex.Message, ex);
                Application.Exit();
            }

            if (DateTime.Now.Day < buyNextMonth)
            {
                MayBuyForNextMonth = false;
            }
            else
            {
                MayBuyForNextMonth = true;
            }
        }

        private void takeCareOfMonthTab(int m, int y)
        {
            TabPage page = new TabPage(@"   " + Utils.getMonthString(m).ToUpper() + @"   ");

            //tabControlMonth.TabPages.Add(page);
            BuyMealsUC bmuc = new BuyMealsUCSombor(m, y);
            page.Controls.Add(bmuc);
            //bmuc.Anchor = (AnchorStyles.Top | AnchorStyles.Left);
            bmuc.Dock = DockStyle.Fill;
            tabControlMonth.TabPages.Add(page);
        }

        private void addMonthTabs()
        {
            tabControlMonth.TabPages.Clear();

            DateTime now = DateTime.Now;
            //mogu da se kupe obroci samo za tekuci i za naredni mesec
            takeCareOfMonthTab(now.Month, now.Year);
            if (MayBuyForNextMonth)
            {
                DateTime nextMonth = now.AddMonths(1);
                takeCareOfMonthTab(nextMonth.Month, nextMonth.Year);
            }
        }

        public void setCashAmounts()
        {
            lblCardMoneyBalance.Text = guiLang.BuyMealsFormScBgd_Label_lblCardMoneyBalance + " " + Utils.getCurrencyString(buyMealsData.CardMoneyBalance, true);
            lblMoneyBalanceAfterShopping.Text = guiLang.BuyMealsFormScBgd_Label_lblMoneyBalanceAfterShopping + " " + Utils.getCurrencyString(buyMealsData.MoneyBalanceAfterBuyingSelected, true);
        }

        private void BuyMealsFormSombor_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                //todo vidi da li je pametno da se stalno brisu i prave novi tabovi. Mozda bi trebalo nekako drugacije da se to uradi
                addMonthTabs();
                setGridShoppingCardTitles();
                setLabels();
                setCashValues();
            }
        }

        private void ChangeTabColor(object sender, DrawItemEventArgs e)
        {
            Font TabFont;
            Brush BackBrush = new SolidBrush(Program.KioskApplication.BackgroundColor); //Set background color
            Brush BackBrushActive = new SolidBrush(Program.KioskApplication.TextForeColor2);
            Brush ForeBrush = new SolidBrush(Program.KioskApplication.TextForeColor2);//Set foreground color
            Brush ForeBrushActive = new SolidBrush(Program.KioskApplication.TextForeColor);
            if (e.Index == this.tabControlMonth.SelectedIndex)
            {
                TabFont = new Font(e.Font, /*FontStyle.Italic |*/ FontStyle.Bold);
                BackBrush = BackBrushActive;
                ForeBrush = ForeBrushActive;
            }
            else
            {
                TabFont = e.Font;
                //TabFont = new Font(e.Font, FontStyle.Strikeout);
            }
            string TabName = this.tabControlMonth.TabPages[e.Index].Text;
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            e.Graphics.FillRectangle(BackBrush, e.Bounds);
            Rectangle r = e.Bounds;
            r = new Rectangle(r.X, r.Y + 3, r.Width, r.Height - 3);
            e.Graphics.DrawString(TabName, TabFont, ForeBrush, r, sf);
            //Dispose objects
            sf.Dispose();
            if (e.Index == this.tabControlMonth.SelectedIndex)
            {
                TabFont.Dispose();
                BackBrush.Dispose();
            }
            else
            {
                BackBrush.Dispose();
                ForeBrush.Dispose();
            }
        }

        private void PaintTabsRectangle(object sender, DrawItemEventArgs e)
        {
            //ova funkcija farba pravougaonik sa leve strane tabova do kraja tabControl-a
            SolidBrush fillBrush = new SolidBrush(Program.KioskApplication.BackgroundColor);

            //draw rectangle behind the tabs
            Rectangle lasttabrect = tabControlMonth.GetTabRect(tabControlMonth.TabPages.Count - 1);
            Rectangle background = new Rectangle();
            background.Location = new Point(lasttabrect.Right, 0);

            //pad the rectangle to cover the 1 pixel line between the top of the tabpage and the start of the tabs
            background.Size = new Size(tabControlMonth.Right - background.Left, lasttabrect.Height + 1);
            e.Graphics.FillRectangle(fillBrush, background);
        }
        
        private void tabControlMonth_DrawItem(object sender, DrawItemEventArgs e)
        {
            ChangeTabColor(sender, e);
            PaintTabsRectangle(sender, e);
        }

        public void fillShoppingCartGrid()
        {
            gridShoppingCart.Rows.Clear();

            bool cartVisible = false;

            foreach (var meal in buyMealsData.Meals)
            {
                if (meal.Amount > 0)
                {
                    gridShoppingCart.Rows.Add(meal.getMealTypeString() + " - " + Utils.getMonthString(meal.Month),
                                              Utils.getCurrencyString(meal.Price, true),
                                              meal.Amount.ToString(),
                                              Utils.getCurrencyString((meal.Amount * meal.Price), true));
                    cartVisible = true;
                }
            }

            DataGridViewRow row = new DataGridViewRow();
            row.DefaultCellStyle.BackColor = Program.KioskApplication.TextForeColor2;
            row.DefaultCellStyle.ForeColor = Program.KioskApplication.BackColor;
            for (int i = 0; i < 4; i++)
            {
                row.Cells.Add(new DataGridViewTextBoxCell());
            }
            row.Cells[0].Value = guiLang.BuyMealsFormScBgd_GridShoppingCart_Total.ToUpper();
            row.Cells[3].Value = Utils.getCurrencyString(buyMealsData.getTotalPrice(), true);
            gridShoppingCart.Rows.Add(row);

            pictureBoxShoppingCart.Visible = cartVisible;
            gridShoppingCart.Visible = cartVisible;
            btnBuyFood.Visible = cartVisible;
        }

        private void gridShoppingCart_SelectionChanged(object sender, EventArgs e)
        {
            gridShoppingCart.ClearSelection();
        }

        private void setGridShoppingCardTitles()
        {
            gridShoppingCart.Columns[0].HeaderText = guiLang.BuyMealsFormScBgd_GridShoppingCart_Header_0;
            gridShoppingCart.Columns[1].HeaderText = guiLang.BuyMealsFormScBgd_GridShoppingCart_Header_1;
            gridShoppingCart.Columns[2].HeaderText = guiLang.BuyMealsFormScBgd_GridShoppingCart_Header_2;
            gridShoppingCart.Columns[3].HeaderText = guiLang.BuyMealsFormScBgd_GridShoppingCart_Header_3;
        }

        private void setLabels()
        {
            btnBuyFood.Text = guiLang.BuyMealsFormScBgd_Button_btnBuyFood;
            btnCancelBuyFood.Text = guiLang.BuyMealsFormScBgd_Button_btnCancelBuyFood;
            btnInsertCash.Text = guiLang.UserDataForm_Button_btnInsertCash;
        }

        private void btnCancelBuyFood_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.showFormInPanel(UserDataFormSombor.getInstance());
        }

        private int processBuyingMeals()
        { 
        //Prvo se upisuje u bazu, zatim na karticu, a ako upis na karticu pukne, ponistava se transakcija u bazi
            int transactionId = 0;
            try
            {
                UserDataSombor ud = UserDataFormSombor.getInstance().ud;

                int b = 0, l = 0, d = 0, bn = 0, ln = 0, dn = 0;
                double p = 0, pn = 0;

                DateTime now = DateTime.Now;
                DateTime nextMonth = now.AddMonths(1);

                foreach (var meal in buyMealsData.Meals)
                {
                    switch (meal.Type)
                    {
                        case MealType.Breakfast:
                            if (meal.Month == now.Month)
                            {
                                b = meal.Amount;
                                p += meal.Price * b;
                            }
                            else
                            {
                                bn = meal.Amount;
                                pn += meal.Price * bn;
                            }
                            break;
                        case MealType.Lunch:
                            if (meal.Month == now.Month)
                            {
                                l = meal.Amount;
                                p += meal.Price * l;
                            }
                            else
                            {
                                ln = meal.Amount;
                                pn += meal.Price * ln;
                            }
                            break;
                        case MealType.Dinner:
                            if (meal.Month == now.Month)
                            {
                                d = meal.Amount;
                                p += meal.Price * d;
                            }
                            else
                            {
                                dn = meal.Amount;
                                pn += meal.Price * dn;
                            }
                            break;
                        default:
                            break;
                    }
                }

                //provera
                if (p + pn != buyMealsData.getTotalPrice())
                {
                    //todo vidi oko jezika
                    throw new Exception("There is a problem in buying meals process!");
                }

                if ((p > 0) || (pn > 0))
                {
                    transactionId = WebServiceAdapter.buyMealsWithEMoney(ud.CardNumber, p + pn, b, l, d, bn, ln, dn, now.Month);
                }
                
                if (transactionId == 0)
                {
                    //todo vidi oko jezika
                    throw new Exception("There is a problem in buying meals process!");
                }

                //upis na karticu
                try
                {
                    if ((b > 0) || (l > 0) || (d > 0))
                    {
                        //ima kupovine za tekuci mesec
                        UserDataFormSombor.getInstance().changeMealsInMealsChecker(now.Month, now.Year, b, l, d);
                    }
                    if ((bn > 0) || (ln > 0) || (dn > 0))
                    {
                        UserDataFormSombor.getInstance().changeMealsInMealsChecker(nextMonth.Month, nextMonth.Year, bn, ln, dn);
                    }

                    ud.writeCardAbonentData();
                }
                catch (Exception ex)
                {
                    //ako nije uspeo upis na karticu, ponisti transakciju u bazi
                    try
                    {
                        WebServiceAdapter.undoTransaction(transactionId);
                    }
                    catch (Exception ex1)
                    {
                        //nije uspelo ponistavanje u bazi
                        throw new Exception(ex1.Message);
                    }
                    throw new Exception(ex.Message/*nije uspeo upis na karticu*/);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return transactionId;
        }

        private void btnBuyFood_Click(object sender, EventArgs e)
        {
            //podaci ce se upisivati u bazu i na karticu u posebnom threadu (tasku),
            //a u ovom threadu cu da pitam korisnika da li hoce da mu se stampa racun
            //dok on odabere sta hoce, i upis ce da zavrsi - tu cekam da task zavrsi, 
            //a onda se stampa racun ako je korisnik to izabrao
            //naravno, ako nema papira ili ne radi stampac, ne treba nista pitati korisnika

            try
            {
                var taskProcessBuyingMeals = Task.Run(() => processBuyingMeals());

                //Task<int> taskProcessBuyingMeals = new Task<int>(processBuyingMeals);
                //taskProcessBuyingMeals.Start();

                //prikazi dijalog sa pitanjem da li hoce da stampa
                if (Program.KioskApplication.PrinterAvailable)
                {
                    MessageChoice.getInstance(guiLang.MessageChoice_DoYouWantReceipt, MessageType.Question).ShowDialog();
                }

                int transactionId = taskProcessBuyingMeals.Result;

                if (Program.KioskApplication.MessageChoiceResult)
                {
                    //stampaj racun
                    printReceipt(transactionId);
                }


                taskProcessBuyingMeals.Wait();
            }
            catch (AggregateException aex)
            {
                MessageClassic.getInstance(aex.InnerException.Message, MessageType.Error).ShowDialog();
            }
            catch (Exception ex)
            {
                MessageClassic.getInstance(ex.InnerException.Message, MessageType.Error).ShowDialog();
            }
            finally
            {
                //vrati se nazad
                btnCancelBuyFood_Click(null, null);                
            }
            
        }

        public void printReceipt(int transactionId)
        {
            try
            {
                DateTime now = DateTime.Now;
                string receiptTitle = guiLang.Receipt_Title_Shopping;
                string receiptNumber = transactionId.ToString();

                string timeStamp = Utils.addLeadingZeros(now.Day, 2) + @"." + Utils.addLeadingZeros(now.Month, 2) + @"." + now.Year.ToString() + @". " + Utils.addLeadingZeros(now.Hour, 2) + @":" + Utils.addLeadingZeros(now.Minute, 2) + @":" + Utils.addLeadingZeros(now.Second, 2);

                //stavke racuna
                List<ReceiptItem> receiptItems = new List<ReceiptItem>();

                foreach (var meal in buyMealsData.Meals)
                {
                    if (meal.Amount > 0)
                    {
                        receiptItems.Add(new ReceiptItem(meal.getMealTypeString() + " - " + Utils.getMonthString(meal.Month), meal.Amount, meal.Price));
                    }
                }
                //-------------

                Receipt rp = new ReceiptSombor(receiptTitle, receiptNumber, UserDataFormSombor.getInstance().ud.CardNumber, buyMealsData.getTotalPrice(), timeStamp, receiptItems);
                rp.printReceipt();
            }
            catch (Exception ex)
            {
                string err = "Error printing receipt. " + " " + ex.Message;
                Program.KioskApplication.log.Error(err, ex);
                throw new Exception(err);
            }
        }

        private void btnInsertCash_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.showFormInPanel(CashInsertFormSombor.getInstance(this));
        }
        
    }
}
