﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.SomborKiosk
{
    public class BuyMealsPickAmountUCSombor : BuyMealsPickAmountUC
    {
        public BuyMealsPickAmountUCSombor(Meal m)
            : base(m)
        {
        }

        public BuyMealsPickAmountUCSombor()
            : base()
        {
        }

        protected override void enableButtons(bool showLog)
        {
            //u zavisnosti od perioda u mesecu, od stanja novcanika i od ostalih parametara odredjuje se koje dugme moze da se pritisne, a koje nece biti enableovano

            ClickButtonStatus s = ClickButtonStatus.Allowed;

            //ako se obroci kupuju na dekade (pre 21. u mesecu), onda treba gledati koliko je studentu ostalo da kupi 
            //i ako mu je ostalo manje od 15, onda se prikazuje taj broj (npr. 8 za februar, 10 ili 11)
            //ako mu je ostalo vise od 15, onda se prikazuje 10
            if (BuyMealsFormSombor.getInstance().BuyingMealsByDecades)
            {
                if (MealChosen.MealsAllowed > 15)
                {
                    //btnTenMinus.Text = "-10";
                    btnTenPlus.Text = "+10";
                }
                else
                {
                    btnTenPlus.Text = "+" + (MealChosen.MealsAllowed).ToString();
                }

                btnOnePlus.Enabled = false;
                lblOnePlusReason.Text = guiLang.BuyMealsPickAmountUC_Message_BuyingOnDecades;

                btnOneMinus.Enabled = false;
                lblOneMinusReason.Text = guiLang.BuyMealsPickAmountUC_Message_BuyingOnDecades;
            }
            else
            {
                btnTenMinus.Text = "-10";
                btnTenPlus.Text = "+10";

                //one plus button
                s = checkIfEnoughMealsToBuy(1);
                if (s == ClickButtonStatus.Allowed)
                {
                    lblOnePlusReason.Text = "";
                    btnOnePlus.Enabled = true;
                }
                else
                {
                    //ispisi zasto ne moze da kupi
                    lblOnePlusReason.Text = getClickMessage(s);
                    btnOnePlus.Enabled = false;
                }

                //one minus button
                if (NumberOfOnesClicked > 0)
                {
                    lblOneMinusReason.Text = "";
                    btnOneMinus.Enabled = true;
                }
                else
                {
                    lblOneMinusReason.Text = guiLang.BuyMealsPickAmountUC_Message_PositiveNumberOnly;
                    btnOneMinus.Enabled = false;
                }
            }

            //ten plus button
            s = checkIfEnoughMealsToBuy(Convert.ToInt32(btnTenPlus.Text));
            if (s == ClickButtonStatus.Allowed)
            {
                lblTenPlusReason.Text = "";
                btnTenPlus.Enabled = true;
            }
            else
            {
                lblTenPlusReason.Text = getClickMessage(s);
                btnTenPlus.Text = "+10";
                btnTenPlus.Enabled = false;
            }

            //ten minus button
            if (TensPlusClicked.Count > 0)
            {
                lblTenMinusReason.Text = "";
                btnTenMinus.Text = (-TensPlusClicked.Peek()).ToString();
                btnTenMinus.Enabled = true;
            }
            else
            {
                lblTenMinusReason.Text = guiLang.BuyMealsPickAmountUC_Message_PositiveNumberOnly;
                btnTenMinus.Text = "-10";
                btnTenMinus.Enabled = false;
            }
        }
        protected override ClickButtonStatus checkIfEnoughMealsToBuy(int amount)
        {
            //Provera da li je treba omoguciti odredjene dugmice, odnosno da li je moguce kupiti odredjeni broj obroka:
            //1. treba proveriti da li ima dovoljno para
            //2. treba proveriti da li sme da kupi toliko obroka - MealsChecker

            ClickButtonStatus goForIt = ClickButtonStatus.Allowed;

            if (amount == 0)
            {
                goForIt = ClickButtonStatus.NotAllowed;
            }
            else // samo moze da se povecava broj obroka jer se smanjivanje regulise drugacije (stekom i brojem klikova na +1)
            {
                //1. da li ima dovoljno para
                if (!BuyMealsFormSombor.getInstance().buyMealsData.hasEnoughMoneyToBuy(MealChosen.Month, MealChosen.Year, MealChosen.Type, amount))
                {
                    goForIt = ClickButtonStatus.NotEnoughMoney;
                }
                else
                {
                    //2. da li sme da kupi toliko obroka
                    if (UserDataFormSombor.getInstance().ud.IsResident)
                    {
                        //ako je mestanin, onda za jedan mesec sme da kupi ukupno obroka koliko ima u left podacima
                        int allowedMealsResident = BuyMealsFormSombor.getInstance().buyMealsData.getAllowedMealsForResident(MealChosen.Month);
                        if (allowedMealsResident < amount)
                        {
                            goForIt = ClickButtonStatus.NotAllowed;
                        }
                    }
                    else
                    {
                        if (MealChosen.MealsAllowed < amount)
                        {
                            goForIt = ClickButtonStatus.NotAllowed;
                        }
                    }
                }
            }
            return goForIt;
        }

        protected override void setMealAmount(bool showLog)
        {
            lblMealAmountSelected.Text = MealChosen.Amount.ToString();
            lblAllowed.Text = guiLang.UserDataForm_Label_lblAllowedMealsToBuy + ": " + MealChosen.MealsAllowed.ToString();

            SomborKiosk.BuyMealsFormSombor.getInstance().fillShoppingCartGrid();
            SomborKiosk.BuyMealsFormSombor.getInstance().setCashAmounts();
            enableButtons(showLog);
        }

        protected override void btnOnePlus_Click(object sender, EventArgs e)
        {
            SomborKiosk.BuyMealsFormSombor.getInstance().buyMealsData.changeMealAmounts(MealChosen.Month, MealChosen.Year, MealChosen.Type, 1);
            MealChosen.MealsAllowed -= 1;
            NumberOfOnesClicked += 1;
            setMealAmount(true);
        }
        protected override void btnOneMinus_Click(object sender, EventArgs e)
        {
            SomborKiosk.BuyMealsFormSombor.getInstance().buyMealsData.changeMealAmounts(MealChosen.Month, MealChosen.Year, MealChosen.Type, -1);
            MealChosen.MealsAllowed -= 1;
            NumberOfOnesClicked -= 1;
            setMealAmount(true);
        }
        protected override void btnTenMinus_Click(object sender, EventArgs e)
        {
            SomborKiosk.BuyMealsFormSombor.getInstance().buyMealsData.changeMealAmounts(MealChosen.Month, MealChosen.Year, MealChosen.Type, Convert.ToInt32(btnTenMinus.Text));
            MealChosen.MealsAllowed -= Convert.ToInt32(btnTenMinus.Text);
            TensPlusClicked.Pop();
            setMealAmount(true);
        }
        protected override void btnTenPlus_Click(object sender, EventArgs e)
        {
            SomborKiosk.BuyMealsFormSombor.getInstance().buyMealsData.changeMealAmounts(MealChosen.Month, MealChosen.Year, MealChosen.Type, Convert.ToInt32(btnTenPlus.Text));
            MealChosen.MealsAllowed -= Convert.ToInt32(btnTenPlus.Text);
            TensPlusClicked.Push(Convert.ToInt32(btnTenPlus.Text));
            setMealAmount(true);
        }
    }
}
