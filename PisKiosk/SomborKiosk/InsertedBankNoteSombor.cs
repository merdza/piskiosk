﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.SomborKiosk
{
    public class InsertedBankNoteSombor : InsertedBankNote
    {
        public override void insertBankNote(int bankNoteValue)
        {
            Value = bankNoteValue;

            //todo
            //implementiraj kako se obradjuje novcanica koja je ocitana (upis na karticu, upis u xml, upis u bazu)
            
            //U kiosku u Somboru nema upisivanja na karticu - upisuje se samo u bazu.

            UserDataSombor ud = UserDataFormSombor.getInstance().ud;

            WebServiceAdapter.moneyDeposit(ud.CardNumber, bankNoteValue);

            ud.CashOnCard += bankNoteValue;
        }
    }
}
