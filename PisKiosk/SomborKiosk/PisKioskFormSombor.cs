﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.SomborKiosk
{
    public partial class PisKioskFormSombor : PisKiosk.PisKioskForm
    {
        private static PisKioskFormSombor instance = null;

        public static PisKioskFormSombor getInstance()
        {
            if (instance == null)
            {
                instance = new PisKioskFormSombor();
            }
            return instance;
        }

        private PisKioskFormSombor() : base()
        {
            InitializeComponent();

            UserData = new UserDataSombor();
        }

        public override void setKioskOperater()
        {
            int idOperater = WebServiceAdapter.getKioskOperator(Program.KioskApplication.KioskName);

            Operater = new Operator(idOperater);
        }

        public override WithdrawMoneyForm getWithdrawMoneyForm()
        {
            return WithdrawMoneyFormSombor.getInstance();
        }

        public override void loadKioskApplication(PanelFormType pft)
        {
            MainForm form = WaitingForCardFormSombor.getInstance();
            showFormInPanel(form);
        }
    }
}
