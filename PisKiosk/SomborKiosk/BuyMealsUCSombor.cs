﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PisKiosk.SomborKiosk
{
    public class BuyMealsUCSombor : BuyMealsUC
    {
        public BuyMealsUCSombor(int month, int year)
            : base(month, year)
        {}

        public BuyMealsUCSombor():base() { }

        protected override void takeCareOfMealTab(MealType mt)
        {
            Meal m = BuyMealsFormSombor.getInstance().buyMealsData.getMeal(Month, Year, mt);
            BuyMealsPickAmountUC bmuc = new BuyMealsPickAmountUCSombor(m);

            TabPage page = new TabPage(@"   " + mt.ToString().ToUpper() + @"   ");
            page.Text = m.getMealTypeString();

            page.BorderStyle = BorderStyle.None;
            bmuc.BorderStyle = BorderStyle.None;
            page.Controls.Add(bmuc);
            //bmuc.Anchor = (AnchorStyles.Top | AnchorStyles.Left);
            bmuc.Dock = DockStyle.Fill;
            tabControlMeal.TabPages.Add(page);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // BuyMealsUCSombor
            // 
            this.Name = "BuyMealsUCSombor";
            this.ResumeLayout(false);

        }
    }
}
