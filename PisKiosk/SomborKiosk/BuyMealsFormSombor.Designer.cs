﻿namespace PisKiosk.SomborKiosk
{
    partial class BuyMealsFormSombor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuyMealsFormSombor));
            this.tabControlMonth = new PisKiosk.TabControlExtended();
            this.lblMoneyBalanceAfterShopping = new System.Windows.Forms.Label();
            this.gridShoppingCart = new System.Windows.Forms.DataGridView();
            this.Item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCardMoneyBalance = new System.Windows.Forms.Label();
            this.btnCancelBuyFood = new PisKiosk.XButton();
            this.btnInsertCash = new PisKiosk.XButton();
            this.btnBuyFood = new PisKiosk.XButton();
            this.pictureBoxShoppingCart = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridShoppingCart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShoppingCart)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(59, 173);
            this.lblTitle.Size = new System.Drawing.Size(90, 40);
            this.lblTitle.Text = "Title - BuyMealsForm Sombor";
            // 
            // tabControlMonth
            // 
            this.tabControlMonth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMonth.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControlMonth.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.tabControlMonth.HotTrack = true;
            this.tabControlMonth.Location = new System.Drawing.Point(20, 84);
            this.tabControlMonth.Name = "tabControlMonth";
            this.tabControlMonth.SelectedIndex = 0;
            this.tabControlMonth.Size = new System.Drawing.Size(1110, 370);
            this.tabControlMonth.TabIndex = 8;
            this.tabControlMonth.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControlMonth_DrawItem);
            // 
            // lblMoneyBalanceAfterShopping
            // 
            this.lblMoneyBalanceAfterShopping.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneyBalanceAfterShopping.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblMoneyBalanceAfterShopping.Location = new System.Drawing.Point(385, 53);
            this.lblMoneyBalanceAfterShopping.Name = "lblMoneyBalanceAfterShopping";
            this.lblMoneyBalanceAfterShopping.Size = new System.Drawing.Size(539, 39);
            this.lblMoneyBalanceAfterShopping.TabIndex = 9;
            this.lblMoneyBalanceAfterShopping.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gridShoppingCart
            // 
            this.gridShoppingCart.AllowUserToAddRows = false;
            this.gridShoppingCart.AllowUserToDeleteRows = false;
            this.gridShoppingCart.AllowUserToResizeColumns = false;
            this.gridShoppingCart.AllowUserToResizeRows = false;
            this.gridShoppingCart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridShoppingCart.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridShoppingCart.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.gridShoppingCart.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridShoppingCart.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridShoppingCart.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridShoppingCart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridShoppingCart.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item,
            this.Price,
            this.Amount,
            this.Total});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridShoppingCart.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridShoppingCart.Location = new System.Drawing.Point(145, 478);
            this.gridShoppingCart.MultiSelect = false;
            this.gridShoppingCart.Name = "gridShoppingCart";
            this.gridShoppingCart.ReadOnly = true;
            this.gridShoppingCart.RowHeadersVisible = false;
            this.gridShoppingCart.Size = new System.Drawing.Size(795, 282);
            this.gridShoppingCart.TabIndex = 11;
            this.gridShoppingCart.Visible = false;
            this.gridShoppingCart.SelectionChanged += new System.EventHandler(this.gridShoppingCart_SelectionChanged);
            // 
            // Item
            // 
            this.Item.HeaderText = "Item";
            this.Item.Name = "Item";
            this.Item.ReadOnly = true;
            // 
            // Price
            // 
            this.Price.HeaderText = "Price";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // lblCardMoneyBalance
            // 
            this.lblCardMoneyBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCardMoneyBalance.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblCardMoneyBalance.Location = new System.Drawing.Point(385, 17);
            this.lblCardMoneyBalance.Name = "lblCardMoneyBalance";
            this.lblCardMoneyBalance.Size = new System.Drawing.Size(539, 38);
            this.lblCardMoneyBalance.TabIndex = 10;
            this.lblCardMoneyBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnCancelBuyFood
            // 
            this.btnCancelBuyFood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelBuyFood.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnCancelBuyFood.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelBuyFood.Image")));
            this.btnCancelBuyFood.Location = new System.Drawing.Point(954, 584);
            this.btnCancelBuyFood.Name = "btnCancelBuyFood";
            this.btnCancelBuyFood.Size = new System.Drawing.Size(176, 60);
            this.btnCancelBuyFood.TabIndex = 13;
            this.btnCancelBuyFood.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnCancelBuyFood.UseVisualStyleBackColor = true;
            this.btnCancelBuyFood.Click += new System.EventHandler(this.btnCancelBuyFood_Click);
            // 
            // btnInsertCash
            // 
            this.btnInsertCash.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInsertCash.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnInsertCash.Image = ((System.Drawing.Image)(resources.GetObject("btnInsertCash.Image")));
            this.btnInsertCash.Location = new System.Drawing.Point(942, 5);
            this.btnInsertCash.Name = "btnInsertCash";
            this.btnInsertCash.Size = new System.Drawing.Size(176, 100);
            this.btnInsertCash.TabIndex = 14;
            this.btnInsertCash.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnInsertCash.UseVisualStyleBackColor = true;
            this.btnInsertCash.Click += new System.EventHandler(this.btnInsertCash_Click);
            // 
            // btnBuyFood
            // 
            this.btnBuyFood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBuyFood.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnBuyFood.Image = ((System.Drawing.Image)(resources.GetObject("btnBuyFood.Image")));
            this.btnBuyFood.Location = new System.Drawing.Point(954, 478);
            this.btnBuyFood.Name = "btnBuyFood";
            this.btnBuyFood.Size = new System.Drawing.Size(176, 100);
            this.btnBuyFood.TabIndex = 15;
            this.btnBuyFood.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnBuyFood.UseVisualStyleBackColor = true;
            this.btnBuyFood.Click += new System.EventHandler(this.btnBuyFood_Click);
            // 
            // pictureBoxShoppingCart
            // 
            this.pictureBoxShoppingCart.Image = global::PisKiosk.Properties.Resources.shopping_cart_gray;
            this.pictureBoxShoppingCart.Location = new System.Drawing.Point(20, 478);
            this.pictureBoxShoppingCart.Name = "pictureBoxShoppingCart";
            this.pictureBoxShoppingCart.Size = new System.Drawing.Size(119, 106);
            this.pictureBoxShoppingCart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxShoppingCart.TabIndex = 12;
            this.pictureBoxShoppingCart.TabStop = false;
            // 
            // BuyMealsFormSombor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1150, 650);
            this.Controls.Add(this.pictureBoxShoppingCart);
            this.Controls.Add(this.gridShoppingCart);
            this.Controls.Add(this.lblMoneyBalanceAfterShopping);
            this.Controls.Add(this.lblCardMoneyBalance);
            this.Controls.Add(this.btnInsertCash);
            this.Controls.Add(this.tabControlMonth);
            this.Controls.Add(this.btnCancelBuyFood);
            this.Controls.Add(this.btnBuyFood);
            this.Name = "BuyMealsFormSombor";
            this.VisibleChanged += new System.EventHandler(this.BuyMealsFormSombor_VisibleChanged);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.btnBuyFood, 0);
            this.Controls.SetChildIndex(this.btnCancelBuyFood, 0);
            this.Controls.SetChildIndex(this.tabControlMonth, 0);
            this.Controls.SetChildIndex(this.btnInsertCash, 0);
            this.Controls.SetChildIndex(this.lblCardMoneyBalance, 0);
            this.Controls.SetChildIndex(this.lblMoneyBalanceAfterShopping, 0);
            this.Controls.SetChildIndex(this.gridShoppingCart, 0);
            this.Controls.SetChildIndex(this.pictureBoxShoppingCart, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gridShoppingCart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShoppingCart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblMoneyBalanceAfterShopping;
        private System.Windows.Forms.DataGridView gridShoppingCart;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.Label lblCardMoneyBalance;
        private TabControlExtended tabControlMonth;
        protected XButton btnCancelBuyFood;
        protected XButton btnInsertCash;
        protected XButton btnBuyFood;
        private System.Windows.Forms.PictureBox pictureBoxShoppingCart;
    }
}
