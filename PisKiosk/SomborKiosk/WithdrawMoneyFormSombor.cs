﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.SomborKiosk
{
    public partial class WithdrawMoneyFormSombor : PisKiosk.WithdrawMoneyForm
    {
        public WithdrawMoneyFormSombor()
        {
            InitializeComponent();
        }

        private static WithdrawMoneyFormSombor instance = null;
        public static WithdrawMoneyFormSombor getInstance()
        {
            if (instance == null)
            {
                instance = new WithdrawMoneyFormSombor();
            }
            return instance;
        }

        public override void getMoneyInformation(out double total, out double totalUntilToday)
        {
            total = 0;
            totalUntilToday = 0;
            try
            {
                WebServiceAdapter.getMoneyInformation(out total, out totalUntilToday);
            }
            catch (Exception)
            {
                close();
               // MessageClassic.getInstance("Podizu se pare. " + ex.Message, MessageType.Information).Show();
            }
        }

        private void WithdrawMoneyFormSombor_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                //pokupi iz baze informacije o stanju novca u kiosku, pa to prikazi
                double total = 0;
                double totalUntilToday = 0;
                getMoneyInformation(out total, out totalUntilToday);

                MoneyTotal = total;
                MoneyInsertedToday = totalUntilToday;

                lblMoneyInKiosk.Text = "Novca u kiosku: " + MoneyTotal.ToString();
                lblMoneyInKioskToday.Text = "Novca u kiosku danas: " + MoneyInsertedToday.ToString();
            }
        }

        private void btnWithdrawMoney_Click(object sender, EventArgs e)
        {
            int leaveTodaysMoney = Convert.ToInt16(XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "leaveTodaysMoney"));
            if (leaveTodaysMoney == 1)
            {
                MoneyWithdrawn = MoneyTotal - MoneyInsertedToday;
            }
            else
            {
                MoneyWithdrawn = MoneyTotal;
            }

            WebServiceAdapter.dischargeKiosk(MoneyWithdrawn, MoneyInsertedToday);

            //stampanje priznanice
            //prikazi dijalog sa pitanjem da li hoce da stampa
            if (Program.KioskApplication.PrinterAvailable)
            {
                MessageChoice.getInstance(guiLang.MessageChoice_DoYouWantReceipt, MessageType.Question).ShowDialog();
            }

            if (Program.KioskApplication.MessageChoiceResult)
            {
                //stampaj racun
                printReceipt();
            }

            close();
            MessageClassic.getInstance("Uspešno ispražnjen kiosk.", MessageType.Information).Show();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            close();
        }
        
        public void printReceipt()
        {
            try
            {
                DateTime now = DateTime.Now;
                string receiptTitle = guiLang.Receipt_Title_Withdraw;

                string timeStamp = Utils.addLeadingZeros(now.Day, 2) + @"." + Utils.addLeadingZeros(now.Month, 2) + @"." + now.Year.ToString() + @". " + Utils.addLeadingZeros(now.Hour, 2) + @":" + Utils.addLeadingZeros(now.Minute, 2) + @":" + Utils.addLeadingZeros(now.Second, 2);

                ////stavke racuna
                //List<ReceiptItem> receiptItems = new List<ReceiptItem>();

                //foreach (var meal in buyMealsData.Meals)
                //{
                //    if (meal.Amount > 0)
                //    {
                //        receiptItems.Add(new ReceiptItem(meal.getMealTypeString() + " - " + Utils.getMonthString(meal.Month), meal.Amount, meal.Price));
                //    }
                //}
                //-------------

                Receipt rp = new ReceiptWithdrawSombor(receiptTitle, MoneyTotal, MoneyWithdrawn, timeStamp);
                rp.printReceipt();
            }
            catch (Exception ex)
            {
                string err = "Error printing receipt. " + " " + ex.Message;
                Program.KioskApplication.log.Error(err, ex);
                throw new Exception(err);
            }
        }

        private void close()
        {
            Login.getInstance().Show();
            this.Hide();
        }
    }
}
