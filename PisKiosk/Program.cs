﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;

namespace PisKiosk
{
    static class Program
    {
        public static CardListener listener;
        public static Thread cardListeningThread;
        public static int Kiosk { get; private set; }
        public static PisKioskForm KioskApplication = null;

        [STAThread]
        static void Main()
        {
            //mutex koristim da bih obezbedio samo jednu instancu programa
            Mutex m = new Mutex(true, "PisKiosk", out bool ok);

            if (!ok)
            {
//                LogFile.writeLog("Application is already running and cannot be started again.", "Error");
                MessageBox.Show("Application is already running and cannot be started again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            listener = new CardListener();
            cardListeningThread = new Thread(listener.listen);


            // u slučaju nepredviđenih grešaka, odnosno unhandleovanih izuzetaka, probaj da upises u log, ali možda upisivanje u log još nije inicijalizovano, pa ispisi message box
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);


            //------------------------------------------------------




            try
            {
                Kiosk = Convert.ToInt32(XmlSettings.XmlSettings.getSetting(@"settings.xml", "kioskApplication"));
                setKioskFormInstance();

                string version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion + ", " + Application.ProductVersion;
                Program.KioskApplication.log.Info("PisKiosk application is starting. version: " + version);
                Program.KioskApplication.log.Info("Kiosk: " + KioskApplication.KioskName);

                Login.getInstance().Show();
                Program.KioskApplication.log.Info("Login form shown.");
                CardInterface.getInstance().CardInterf.initializeCommunicationAdapter();
                startListening();

                Application.Run();
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Error initializing application: " + ex.Message, ex);
                MessageBox.Show(ErrorDeclarations.getErrorDescription(ex.Message), "PisRestoran", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return;
            }
            finally
            {
                GC.KeepAlive(m); //potrebno zbog mutexa
                //                LogFile.writeLog("End", "ProgramClosed");
            }

            
        }

        
        static void Application_ThreadException(object sender, ThreadExceptionEventArgs ex)
        {
            try
            {
                Program.KioskApplication.log.Error("Dogodila se nepredviđena greška. Application_ThreadException " + ex.Exception.Message, ex.Exception);
            }
            catch (Exception)
            {}
            MessageBox.Show("Dogodila se nepredviđena greška. " + ex.Exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs ex)
        {
            try
            {
                Program.KioskApplication.log.Error("Dogodila se nepredviđena greška. CurrentDomain_UnhandledException" + (ex.ExceptionObject as Exception).Message, ex.ExceptionObject as Exception);
            }
            catch (Exception)
            { }
            MessageBox.Show("Dogodila se nepredviđena greška. " + (ex.ExceptionObject as Exception).Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
        }


        public static void startListening()
        {
            cardListeningThread.Start();
            while (!cardListeningThread.IsAlive) ;
            //listeningStarted = true;
        }


        public static void stopListening()
        {
            listener.requestStop();
        }

        public static void setKioskFormInstance()
        {
            if (KioskApplication == null)
            {
                try
                {
                    switch (Kiosk)
                    {
                        case 1:
                            KioskApplication = ScBgdKiosk.PisKioskFormScBgd.getInstance();
                            break;
                        case 2:
                            KioskApplication = SomborKiosk.PisKioskFormSombor.getInstance();
                            break;
                        case 3:
                            KioskApplication = KsaKiosk.PisKioskFormKsa.getInstance();
                            break;
                        case 4:
                            KioskApplication = ScNSKiosk.PisKioskFormScNS.getInstance();
                            break;
                        default:
                            throw new Exception("KioskApplication not set correctly.");
                    }
                }
                catch (Exception ex)
                {
                    MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
                }
            }
        }
    }
}
