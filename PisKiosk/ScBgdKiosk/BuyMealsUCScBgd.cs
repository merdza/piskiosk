﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PisKiosk.ScBgdKiosk
{
    public class BuyMealsUCScBgd : BuyMealsUC
    {
        public BuyMealsUCScBgd(int month, int year):base(month, year)
        {}

        protected override void takeCareOfMealTab(MealType mt)
        {
            Meal m = BuyMealsFormScBgd.getInstance().buyMealsData.getMeal(Month, Year, mt);
            BuyMealsPickAmountUC bmuc = new BuyMealsPickAmountUCScBgd(m);

            TabPage page = new TabPage(@"   " + mt.ToString().ToUpper() + @"   ");
            page.Text = m.getMealTypeString();

            page.BorderStyle = BorderStyle.None;
            bmuc.BorderStyle = BorderStyle.None;
            page.Controls.Add(bmuc);
            //bmuc.Anchor = (AnchorStyles.Top | AnchorStyles.Left);
            bmuc.Dock = DockStyle.Fill;
            tabControlMeal.TabPages.Add(page);
        }
    }
}
