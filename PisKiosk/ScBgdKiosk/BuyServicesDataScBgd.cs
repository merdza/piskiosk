﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.ScBgdKiosk
{
    public class BuyServicesDataScBgd : BuyServicesData
    {
        public BuyServicesDataScBgd() : base() { }

        public override void fillServicesFromDB()
        {
            DBAdapter.getServices(UserDataFormScBgd.getInstance().ud.CardNumber);
        }
    }
}
