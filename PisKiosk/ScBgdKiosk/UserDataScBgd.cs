﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.ScBgdKiosk
{
    public class UserDataScBgd : UserData
    {
        //personal data
        private bool valid;
        private DateTime expiryDate;
        //abonent data
            //meals
        private string abonentMonth;
        private string abonentYear;
        private string breakfast;
        private string lunch;
        private string dinner;
        private string breakfastLeft;
        private string lunchLeft;
        private string dinnerLeft;
        private string breakfastNext;
        private string lunchNext;
        private string dinnerNext;
            //eaten meals
        private DateTime eatenMealsDate;
        private string eatenBreakfast;
        private string eatenLunch;
        private string eatenDinner;
        //nizovi koji se koriste sa MealsCheckerom
        private byte[] mealsArray;
        private byte[] eatenMealsArray;

        //cene obroka
        private double breakfastPrice;
        private double lunchPrice;
        private double dinnerPrice;
        private double breakfastNextPrice;
        private double lunchNextPrice;
        private double dinnerNextPrice;
        private int abonentCategory;
        private bool isResident;
        private bool isFirstShoppingInMonth;  // vazno za Beogradjane jer treba da se prosledi kao argument za MealsCheckerCashierBelgrade
        private bool allowedBuyingThisMonth;
        private bool allowedBuyingNextMonth;

        //operaterska kartica
        public int OperaterID { get; private set; }

        public double BreakfastPrice
        {
            get { return breakfastPrice; }
            set { breakfastPrice = value; }
        }
        
        public double LunchPrice
        {
            get { return lunchPrice; }
            set { lunchPrice = value; }
        }
        
        public double DinnerPrice
        {
            get { return dinnerPrice; }
            set { dinnerPrice = value; }
        }
        
        public double BreakfastNextPrice
        {
            get { return breakfastNextPrice; }
            set { breakfastNextPrice = value; }
        }
        
        public double LunchNextPrice
        {
            get { return lunchNextPrice; }
            set { lunchNextPrice = value; }
        }
        
        public double DinnerNextPrice
        {
            get { return dinnerNextPrice; }
            set { dinnerNextPrice = value; }
        }

        public int AbonentCategory
        {
            get { return abonentCategory; }
            set { abonentCategory = value; }
        }

        public bool IsResident
        {
            get { return isResident; }
            set { isResident = value; }
        }

        public bool IsFirstShoppingInMonth
        {
            get { return isFirstShoppingInMonth; }
            set { isFirstShoppingInMonth = value; }
        }

        public bool AllowedBuyingThisMonth
        {
            get { return allowedBuyingThisMonth; }
            set { allowedBuyingThisMonth = value; }
        }
        
        public bool AllowedBuyingNextMonth
        {
            get { return allowedBuyingNextMonth; }
            set { allowedBuyingNextMonth = value; }
        }

        public bool Valid
        {
            get { return valid; }
            set { valid = value; }
        }
        
        public DateTime ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public byte[] MealsArray
        {
            get { return mealsArray; }
            set { mealsArray = value; }
        }

        public byte[] EatenMealsArray
        {
            get { return eatenMealsArray; }
            set { eatenMealsArray = value; }
        }

        public string AbonentMonth
        {
            get { return abonentMonth; }
            set { abonentMonth = value; }
        }

        public string AbonentYear
        {
            get { return abonentYear; }
            set { abonentYear = value; }
        }

        public string Breakfast
        {
            get { return breakfast; }
            set { breakfast = value; }
        }
        
        public string Lunch
        {
            get { return lunch; }
            set { lunch = value; }
        }
        
        public string Dinner
        {
            get { return dinner; }
            set { dinner = value; }
        }

        public string BreakfastLeft
        {
            get { return breakfastLeft; }
            set { breakfastLeft = value; }
        }

        public string LunchLeft
        {
            get { return lunchLeft; }
            set { lunchLeft = value; }
        }

        public string DinnerLeft
        {
            get { return dinnerLeft; }
            set { dinnerLeft = value; }
        }
        
        public string BreakfastNext
        {
            get { return breakfastNext; }
            set { breakfastNext = value; }
        }
        
        public string LunchNext
        {
            get { return lunchNext; }
            set { lunchNext = value; }
        }

        public string DinnerNext
        {
            get { return dinnerNext; }
            set { dinnerNext = value; }
        }

        public DateTime EatenMealsDate
        {
            get { return eatenMealsDate; }
            set { eatenMealsDate = value; }
        }
        
        public string EatenBreakfast
        {
            get { return eatenBreakfast; }
            set { eatenBreakfast = value; }
        }
        
        public string EatenLunch
        {
            get { return eatenLunch; }
            set { eatenLunch = value; }
        }
        
        public string EatenDinner
        {
            get { return eatenDinner; }
            set { eatenDinner = value; }
        }

        public UserDataScBgd()
        {
            try
            {
                MealsArray = new byte[11];
                EatenMealsArray = new byte[6];
                resetAbonentArrays();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void resetAbonentArrays()
        {
            for (int i = 0; i < MealsArray.Length; i++)
			{
			    MealsArray[i] = 0;
			}

            for (int i = 0; i < EatenMealsArray.Length; i++)
			{
			    EatenMealsArray[i] = 0;
			}
        }

        //public static UserDataScBgd getInstance()
        //{
        //    if (instance == null)
        //    {
        //        instance = new UserDataScBgd();
        //    }
        //    return instance;
        //}

        public override void readCardData()
        {
            try
            {
                ScBgdKiosk.CardAdapterContactScBgd cardInterface = (ScBgdKiosk.CardAdapterContactScBgd)CardInterface.getInstance().CardInterf;

                CardSerialNumber = cardInterface.getCardSerialAdapter(); 

                CardType = cardInterface.getCardTypeAdapter();
                if (CardType == CardType.Official)
                {
                    cardInterface.repeatLoginAdapter("korisnik", "korisnik");
                    OperaterID = cardInterface.getOperaterIDAdapter();
                    return; // ako je operaterska kartica, nisu potrebni ostali podaci
                }

                cardInterface.getDataRestaurantVersionAdapter(MealsArray, EatenMealsArray);

                if (cardInterface.getValidAdapter() == 1)
                {
                    Valid = true;
                }
                else
                {
                    Valid = false;
                    //ako je kartica nevazeca, sta ce mi ostali podaci
                    return;
                }

                int eYear = 0;
                int eMonth = 0;
                int eDay = 0;

                try
                {
                    eYear = cardInterface.getExpiryDateYearAdapter();
                    eMonth = cardInterface.getExpiryDateMonthAdapter();
                    eDay = cardInterface.getExpiryDateDayAdapter();
                    ExpiryDate = new DateTime(eYear, eMonth, eDay);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error reading Validity Date from the card. " + ex.Message + " " + eYear + "-" + eMonth + "-" +eDay);
                }

                CardNumber = cardInterface.getCardNumberAdapter();
                UserName = cardInterface.getCardOwnerNameAdapter();
                PersonalNumber = cardInterface.getPersonalNumberAdapter();
                if (PisKioskFormScBgd.getInstance().ActivePicturesInitialized)
                {
                    Photo = PisKioskFormScBgd.getInstance().getActivePicture(CardNumber);
                }
                else
                {
                    Photo = "";
                }
//                Photo = IDCapRestoranAdapter.getPhotoAdapter();

                CashOnCard = cardInterface.getCashDataAdapter();

                setGuiParameters();
            }
            catch (Exception ex)
            {
                throw new Exception("Reading card data failed. " + ex.Message);
            }
        }

        public override void resetUserData()
        {
            UserName = "";
            PersonalNumber = "";
            CardNumber = "";
            CardSerialNumber = "";
            CashOnCard = 0;
            Photo = "";
            CardType = CardType.Unknown;
          
            Valid = false;
            ExpiryDate = new DateTime(2000, 1, 1);
            
            resetAbonentArrays();
            setGuiParameters();
            resetPrices();
        }

        private void resetPrices()
        {
            BreakfastPrice = 0;
            LunchPrice = 0;
            DinnerPrice = 0;
            BreakfastNextPrice = 0;
            LunchNextPrice = 0;
            DinnerNextPrice = 0;
            IsResident = false;
            IsFirstShoppingInMonth = false;
        }

        public void writeCardAbonentData()
        {
            try
            {
                ScBgdKiosk.CardAdapterContactScBgd cardInterface = (ScBgdKiosk.CardAdapterContactScBgd)CardInterface.getInstance().CardInterf;
                cardInterface.setDataRestaurantVersionAdapter(MealsArray, EatenMealsArray);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //postavlja pojedinacne parametre za prikaz u GUIu
        public void setGuiParameters()
        {
            int offset = 0;
            
            //meals
            AbonentYear = (2000 + mealsArray[offset++]).ToString();
            AbonentMonth = mealsArray[offset++].ToString();

            Breakfast = mealsArray[offset++].ToString();
            Lunch = mealsArray[offset++].ToString();
            Dinner = mealsArray[offset++].ToString();

            BreakfastLeft = mealsArray[offset++].ToString();
            LunchLeft = mealsArray[offset++].ToString();
            DinnerLeft = mealsArray[offset++].ToString();
            
            BreakfastNext = mealsArray[offset++].ToString();
            LunchNext = mealsArray[offset++].ToString();
            DinnerNext = mealsArray[offset++].ToString();

            //eaten meals
            offset = 0;
            int day = eatenMealsArray[offset++];
            int month = eatenMealsArray[offset++];
            int year = 2000 + eatenMealsArray[offset++];
           // string dateString = month + "/" + day + "/" + year;

            DateTime result;
            try
            {
                result = new DateTime(year, month, day);
                //result = DateTime.Parse(dateString);
            }
            catch (Exception)
            {
                result = new DateTime(2000, 1, 1);
                //result = DateTime.Parse("01/01/2000");
            }

            EatenMealsDate = result;
            EatenBreakfast = eatenMealsArray[offset++].ToString();
            EatenLunch = eatenMealsArray[offset++].ToString();
            EatenDinner = eatenMealsArray[offset++].ToString();
        }

        public void updateArrays()
        {
            int offset = 0;

            mealsArray[offset++] = Convert.ToByte(Convert.ToInt32(AbonentYear) - 2000);
            mealsArray[offset++] = Convert.ToByte(AbonentMonth);

            mealsArray[offset++] = Convert.ToByte(Breakfast);
            mealsArray[offset++] = Convert.ToByte(Lunch);
            mealsArray[offset++] = Convert.ToByte(Dinner);

            mealsArray[offset++] = Convert.ToByte(BreakfastLeft);
            mealsArray[offset++] = Convert.ToByte(LunchLeft);
            mealsArray[offset++] = Convert.ToByte(DinnerLeft);

            mealsArray[offset++] = Convert.ToByte(BreakfastNext);
            mealsArray[offset++] = Convert.ToByte(LunchNext);
            mealsArray[offset++] = Convert.ToByte(DinnerNext);

            offset = 0;

            eatenMealsArray[offset++] = Convert.ToByte(EatenMealsDate.Day);
            eatenMealsArray[offset++] = Convert.ToByte(EatenMealsDate.Month);
            eatenMealsArray[offset++] = Convert.ToByte(Convert.ToInt32(EatenMealsDate.Year) - 2000);
            eatenMealsArray[offset++] = Convert.ToByte(EatenBreakfast);
            eatenMealsArray[offset++] = Convert.ToByte(EatenLunch);
            eatenMealsArray[offset++] = Convert.ToByte(EatenDinner);
        }

        public override bool isOperatorForThisKiosk(Operator op)
        {
            return DBAdapter.isCardForThisKiosk(op.OperaterId);
        }

        public override string ToString()
        {
            string ret = "";
            if (CardType.Equals(CardType.Student))
            {
                ret = "Student card: " + CardNumber + ", " + UserName + ", photo: " + Photo
                    + ", cash on card: " + CashOnCard.ToString() + ", AbonentYear: " + AbonentYear + ", AbonentMonth: " + AbonentMonth
                    + ", Breakfast: " + Breakfast + ", Lunch: " + Lunch + ", Dinner: " + Dinner
                    + ", BreakfastLeft: " + BreakfastLeft + ", LunchLeft: " + LunchLeft + ", DinnerLeft: " + DinnerLeft
                    + ", EatenMealsDate: " + EatenMealsDate.Date.ToString("dd.MM.yyyy") + ", BreakfastNext: " + BreakfastNext + ", LunchNext: " + LunchNext + ", DinnerNext: " + DinnerNext
                    + ", EatenBreakfast: " + EatenBreakfast + ", EatenLunch: " + EatenLunch + ", EatenDinner: " + EatenDinner;

                if (IsResident)
                {
                    ret += ", RESIDENT";
                }
                if (IsFirstShoppingInMonth)
                {
                    ret += ", firstShoppingInMonth";
                }
            }
            else if (CardType.Equals(CardType.Official))
            {
                ret = "Official card. Official ID: " + OperaterID;
            }
            return ret;
        }
    }
}
