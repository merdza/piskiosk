﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PisKiosk.ScBgdKiosk
{
    public partial class WithdrawMoneyFormScBgd : PisKiosk.WithdrawMoneyForm
    {
        public WithdrawMoneyFormScBgd()
        {
            InitializeComponent();

            this.BackColor = Program.KioskApplication.BackgroundColor;
            btnCancel.BackColor = Program.KioskApplication.TextForeColor2;
            btnCancel.ForeColor = Program.KioskApplication.BackgroundColor;
            btnWithdrawMoney.BackColor = Program.KioskApplication.TextForeColor2;
            btnWithdrawMoney.ForeColor = Program.KioskApplication.BackgroundColor;
            lblMoneyInKiosk.ForeColor = Program.KioskApplication.TextForeColor;
            lblMoneyInKioskToday.ForeColor = Program.KioskApplication.TextForeColor;
            lblMoneyWithdrawn.ForeColor = Program.KioskApplication.TextForeColor;
        }

        private static WithdrawMoneyFormScBgd instance = null;
        public static WithdrawMoneyFormScBgd getInstance()
        {
            if (instance == null)
            {
                instance = new WithdrawMoneyFormScBgd();
            }
            return instance;
        }

        public override void getMoneyInformation(out double total, out double totalUntilToday)
        {
            DBAdapter.getMoneyInformation(out total, out totalUntilToday);
        }

        private void WithdrawMoneyFormScBgd_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                try
                {

                    var taskGetMoneyData = Task.Run(() => getInfo());
                    
                    
                    //todo ovo ispisivanje ne radi kako treba, a ne znam zasto. uvek ispise citanje kartice, odnosno ono sto je prosli put bilo ispisano. Jednog dana proveri 
                    MessageReversedColors.getInstance(@"Čitanje podataka iz baze o uplaćenom novcu. Molimo Vas da sačekate.", MessageType.Information).Show();


                    taskGetMoneyData.Wait();

                    lblMoneyInKiosk.Text = @"Ukupno u kiosku ima: " + Utils.getCurrencyString(MoneyTotal, true);
                    lblMoneyInKioskToday.Text = @"Danas uplaćeno: " + Utils.getCurrencyString(MoneyInsertedToday, true);
                    lblMoneyWithdrawn.Text = @"Podiže se: " + Utils.getCurrencyString(MoneyWithdrawn, true);

                    MessageReversedColors.getInstance(string.Empty, MessageType.Information).Hide();
                }
                catch (AggregateException aex)
                {
                    Program.KioskApplication.log.Error("Error getting information from the database about inserted money." + aex.Message, aex);
                    MessageClassic.getInstance("Nije uspelo čitanje podataka iz baze o uplaćenom novcu. " + aex.Message, MessageType.Error).ShowDialog();
                    close();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Error getting information from the database about inserted money." + ex.Message, ex);
                    MessageClassic.getInstance("Nije uspelo čitanje podataka iz baze o uplaćenom novcu. " + ex.Message, MessageType.Error).ShowDialog();
                    close();
                }
            }
        }

        protected void getInfo()
        {
            //pokupi iz baze informacije o stanju novca u kiosku, pa to prikazi
            double total = 0;
            double totalUntilToday = 0;
            getMoneyInformation(out total, out totalUntilToday);

            MoneyTotal = total;
            MoneyInsertedToday = total - totalUntilToday;
            MoneyWithdrawn = totalUntilToday;
        }

        private void btnWithdrawMoney_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.log.Info("Button Withdraw Money clicked.");

            if (!FormBusy)
            {
                try
                {
                    try
                    {
                        int leaveTodaysMoney = Convert.ToInt16(XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "leaveTodaysMoney"));
                        if (leaveTodaysMoney == 1)
                        {
                            DBAdapter.dischargeKiosk(MoneyTotal, MoneyInsertedToday);
                        }
                        else
                        {
                            DBAdapter.dischargeKiosk(MoneyTotal, 0);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error Withdrawing Money." + ex.Message);
                    }

                    try
                    {
                        //stampanje priznanice
                        //prikazi dijalog sa pitanjem da li hoce da stampa
                        if (Program.KioskApplication.PrinterAvailable)
                        {
                            MessageChoice.getInstance(guiLang.MessageChoice_DoYouWantReceipt, MessageType.Question).ShowDialog();
                        }

                        if (Program.KioskApplication.MessageChoiceResult)
                        {
                            //stampaj racun
                            printReceipt();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error Printing Receipt but the transaction was sucessfully entered into database. " + ex.Message);
                    }

                    MessageClassic.getInstance("Uspešno ispražnjen kiosk.", MessageType.Information).ShowDialog();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Withdraw money - withdraw button click failed " + ex.Message, ex);
                    MessageClassic.getInstance(ex.Message, MessageType.Error).ShowDialog();
                }
                finally
                {
                    FormBusy = false;
                    close();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            close();
        }

        public void printReceipt()
        {
            try
            {
                DateTime now = DateTime.Now;
                string receiptTitle = guiLang.Receipt_Title_Withdraw;

                string timeStamp = Utils.addLeadingZeros(now.Day, 2) + @"." + Utils.addLeadingZeros(now.Month, 2) + @"." + now.Year.ToString() + @". " + Utils.addLeadingZeros(now.Hour, 2) + @":" + Utils.addLeadingZeros(now.Minute, 2) + @":" + Utils.addLeadingZeros(now.Second, 2);

                Receipt rp = new ReceiptWithdrawScBgd(receiptTitle, MoneyTotal, MoneyWithdrawn, timeStamp);
                rp.printReceipt();
            }
            catch (Exception ex)
            {
                string err = "Error printing receipt. " + " " + ex.Message;
                Program.KioskApplication.log.Error(err, ex);
                throw new Exception(err);
            }
        }

        private void close()
        {
            Login.getInstance().Show();
            Program.KioskApplication.Hide();
            //this.Hide();
        }

        private void WithdrawMoneyFormScBgd_Load(object sender, EventArgs e)
        {

        }
    }
}
