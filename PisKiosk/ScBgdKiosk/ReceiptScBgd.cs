﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.ScBgdKiosk
{
    public class ReceiptScBgd : Receipt
    {
        public ReceiptScBgd(string rt, string rn, string cn, double tp, string ts, List<ReceiptItem> itms)
            : base(rt, rn, cn, tp, ts, itms)
        { }

        protected override void preparePrinting(object sender, PrintPageEventArgs e)
        {
            using (Font font1 = new Font("Segoe UI Light", 12, FontStyle.Bold, GraphicsUnit.Point))
            {
                StringFormat stringFormat = new StringFormat();
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;

                StringFormat leftStringFormat = new StringFormat();
                leftStringFormat.Alignment = StringAlignment.Near;
                leftStringFormat.LineAlignment = StringAlignment.Center;

                StringFormat rightStringFormat = new StringFormat();
                rightStringFormat.Alignment = StringAlignment.Far;
                rightStringFormat.LineAlignment = StringAlignment.Center;

//                e.Graphics.DrawImage(logo, 115, 10);
//                Rectangle rectTitleArab = new Rectangle(10, 100, 280, 30);
//                Rectangle rectTitleEng = new Rectangle(10, 120, 280, 30);
//                e.Graphics.DrawString(@"جامعة الطائف", new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectTitleArab, stringFormat);
//                e.Graphics.DrawString(@"Taif University", new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectTitleEng, stringFormat);

                int y = 20;

                Rectangle rectOrg = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(guiLang.Receipt_Organization_ScBgd, new Font(FontFamily.GenericSansSerif, 10), Brushes.Black, rectOrg, stringFormat);
                y += 20;
                
                rectOrg = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(guiLang.Receipt_Organization_Address_ScBgd, new Font(FontFamily.GenericSansSerif, 9), Brushes.Black, rectOrg, stringFormat);
                y += 40;

                Rectangle rectHeader = new Rectangle(10, y, 280, 30);
                //e.Graphics.DrawString(guiLang.Receipt_Header, new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectHeader, stringFormat);
                e.Graphics.DrawString(Program.KioskApplication.KioskName, new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectHeader, stringFormat);
                y += 40;

                Rectangle rectTitle = new Rectangle(10, y, 280, 30);
                e.Graphics.DrawString(receiptTitle, new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectTitle, stringFormat);
                y += 40;

                Rectangle rectItemName = new Rectangle(10, y, 120, 20);
                Rectangle rectItemPrice = new Rectangle(130, y, 50, 20);
                Rectangle rectItemAmount = new Rectangle(180, y, 30, 20);
                Rectangle rectItemTotal = new Rectangle(210, y, 80, 20);
                
                //e.Graphics.DrawString(guiLang.Receipt_Item_Header, new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold), Brushes.Black, rectItemName, stringFormat);
                //e.Graphics.DrawString(guiLang.Receipt_Price_Header, new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold), Brushes.Black, rectItemPrice, stringFormat);
                //e.Graphics.DrawString(guiLang.Receipt_Amount_Header, new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold), Brushes.Black, rectItemAmount, stringFormat);
                //e.Graphics.DrawString(guiLang.Receipt_Total_Header, new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold), Brushes.Black, rectItemTotal, stringFormat);

                //e.Graphics.DrawRectangle(Pens.Black, rectItemName);
                //e.Graphics.DrawRectangle(Pens.Black, rectItemPrice);
                //e.Graphics.DrawRectangle(Pens.Black, rectItemAmount);
                //e.Graphics.DrawRectangle(Pens.Black, rectItemTotal);

                //y += 20;

                foreach (var item in items)
                {
                    //rectItemName = new Rectangle(10, y, 120, 20);
                    //rectItemPrice = new Rectangle(130, y, 50, 20);
                    //rectItemAmount = new Rectangle(180, y, 30, 20);
                    //rectItemTotal = new Rectangle(210, y, 80, 20);

                    rectItemName = new Rectangle(10, y, 280, 20);
                    y += 12; //u novom redu se ispisuju cene i kolicina

                    rectItemPrice = new Rectangle(10, y, 120, 20);
                    rectItemAmount = new Rectangle(130, y, 50, 20);
                    rectItemTotal = new Rectangle(180, y, 110, 20);
                    
                    e.Graphics.DrawString(Utils.makeFixedLengthString(item.ItemName, 50), new Font(FontFamily.GenericSansSerif, 8), Brushes.Black, rectItemName, leftStringFormat);
                    e.Graphics.DrawString(Utils.makeFixedLengthString(item.ItemPrice.ToString("N"), 9), new Font(FontFamily.GenericSansSerif, 8), Brushes.Black, rectItemPrice, stringFormat);
                    e.Graphics.DrawString("x " + Utils.makeFixedLengthString(item.Amount.ToString(), 4), new Font(FontFamily.GenericSansSerif, 8), Brushes.Black, rectItemAmount, stringFormat);
                    e.Graphics.DrawString(Utils.makeFixedLengthString(item.TotalPrice.ToString("N"), 9), new Font(FontFamily.GenericSansSerif, 8), Brushes.Black, rectItemTotal, stringFormat);

                    y += 20;
                }

                //price
                y += 20; //dodatni prored
                Rectangle rectPrice = new Rectangle(10, y, 280, 25);
                string currency = guiLang.Receipt_Total + ": " + Utils.getCurrencyString(totalPrice, true);
                e.Graphics.DrawString(currency, new Font(FontFamily.GenericSansSerif, 12, FontStyle.Bold), Brushes.Black, rectPrice, rightStringFormat);
                y += 40;

                Rectangle rectTimeStamp = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(timeStamp, new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectTimeStamp, stringFormat);
                y += 20;

                Rectangle rectCardNumber = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(guiLang.UserDataForm_Label_lblCardNumberLabel + ": " + cardNumber, new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectCardNumber, stringFormat);
                y += 20;

                Rectangle rectReceiptNumber = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(receiptNumber, new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectReceiptNumber, stringFormat);
                y += 20;

                Rectangle allOverRectangle = new Rectangle(10, 10, 280, y + 10);
                e.Graphics.DrawRectangle(Pens.Black, allOverRectangle);

            }
        }
    }
}
