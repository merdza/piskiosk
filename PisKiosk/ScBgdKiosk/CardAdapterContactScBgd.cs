﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace PisKiosk.ScBgdKiosk
{
    public class CardAdapterContactScBgd : CardAdapterContact
    {
        static object locker = new object();
        /*
        updateUsernameAndPassword
setPersonal
setFirstName
setLastName
setPersonalNumber
setCard
setCardNumber
setExpiryDate
setValid
personalizeStudent
personalizeOfficial
setStudentCenter
getAbonentYear
getAbonentMonth
getBreakfast
getLunch
getDinner
getBreakfastLeft
getLunchLeft
getDinnerLeft
getBreakfastNext
getLunchNext
getDinnerNext
getBreakfastEatenToday
getLunchEatenToday
getDinnerEatenToday
getTodayDay
getTodayMonth
getTodayYear
getAbonentUpdatedDateDay
getAbonentUpdatedDateMonth
getAbonentUpdatedDateYear
getStudentIndex
getFacultyId
getYearOfStudy
getSchoolYearId
getBoardingHouse
getBoardingBlock
getBoardingRoom
getBoardingUpdatedDateDay
getBoardingUpdatedDateMonth
getBoardingUpdatedDateYear
getCashAmount
getStudentCenter
getValid
getTransactionAmount
setStudentIndex
setFacultyId
setYearOfStudy
setSchoolYearId
setFaculty
setBreakfast
setLunch
setDinner
setBreakfastLeft
setLunchLeft
setDinnerLeft
setBreakfastNext
setLunchNext
setDinnerNext
setAbonentMonth
setAbonentYear
setBreakfastEatenToday
setLunchEatenToday
setDinnerEatenToday
setToday
setAbonentUpdatedDate
setAbonent
setBoarding
setBoardingHouse
setBoardingBlock
setBoardingRoom
setBoardingUpdatedDate
setCash
setCashAmount
getFileNumber
getDummyString
getSCDay
getSCMonth
getSCYear
getSCResult
getFacultyDay
getFacultyMonth
getFacultyYear
getFacultyResult
setFileNumber
setDummyString
setSCMedicalCheckUp
setFacultyMedicalCheckUp
setHealth
getMealsArray
setMealsArray
getEatenMealsArray
setEatenMealsArray
getDataRestaurantVersion
setDataRestaurantVersion
repeatLogin
getDataPersonalizationVersion
setDataPersonalizationVersion
*/

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        private static extern void getDataRestaurantVersion(byte[] mealsArray, byte[] eatenMealsArray);

        [DllImport(dllName, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.StdCall)]
        public static extern void setDataRestaurantVersion(byte[] mealsArray, byte[] eatenMealsArray);

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        private static extern int getValid();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        private static extern int isOfficial();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        private static extern int isGuest();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        private static extern int getFoodAppletType();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        private static extern double getCashData();

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        private static extern void setCashData(double amount);

        [DllImport(dllName, CharSet = CharSet.Unicode)]
        public static extern void repeatLogin([MarshalAs(UnmanagedType.BStr)] string username, [MarshalAs(UnmanagedType.BStr)] string password);


        public void getDataRestaurantVersionAdapter(byte[] mealsArray, byte[] eatenMealsArray)
        {
            lock (locker)
            {
                getDataRestaurantVersion(mealsArray, eatenMealsArray);
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                //umesto da cita mealsArray sa kartice (koji se uopste ne koristi za ucenicke domove, 
                //podesicu ga dinamicki tako da prikazuje fiktivne podatke
                //getPhonyMealsArray(mealsArray, eatenMealsArray);
            }
        }

        public void setDataRestaurantVersionAdapter(byte[] mealsArray, byte[] eatenMealsArray)
        {
            lock (locker)
            {
                setDataRestaurantVersion(mealsArray, eatenMealsArray);
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
            }
        }

        public int getValidAdapter()
        {
            lock (locker)
            {
                int result = getValid();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return result;
            }
        }

        public bool isOfficialAdapter()
        {//funkcija u dllu proverava da li je kartica istovremeno i sluzbenicka
            lock (locker)
            {
                try
                {
                    bool result = Convert.ToBoolean(isOfficial());
                    int err = getErrorCode();
                    if (err != 0)
                    {
                        throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                    }
                    return result;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public bool isStudentCardAdapter()
        {
            //da li je kartica koja je ubacena studentska
            //gleda se koji je aplet na kartici
            bool isStudentCard;
            int appletType = getFoodAppletType();
            if (appletType == 1)
            {
                //ucenicki aplet
                isStudentCard = false;
            }
            else if (appletType == 2)
            {
                isStudentCard = true;
            }
            else
            {
                throw new Exception("Nije uspelo određivanje tipa kartice.");
            }
            return isStudentCard;
        }

        public bool isGuestCardAdapter()
        {
            lock (locker)
            {
                int value = isGuest();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return Convert.ToBoolean(value);
            }
        }

        public int getFoodAppletTypeAdapter()
        {
            //vraca tip apleta koji se koristi za ishranu
            //za sada mogu da postoje dva tipa:
            //-ucenicki - ne gledaju se podaci o kupovini, nego samo podaci o potrosenim obrocima u toku dana
            //-studentski - vazno je i da li je kupljeno dovoljno obroka i koliko je potroseno u toku dana
            lock (locker)
            {
                int value = getFoodAppletType();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return value;
            }
        }

        public double getCashDataAdapter()
        {
            lock (locker)
            {
                double value = getCashData();
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
                return value;
            }
        }

        public void setCashDataAdapter(double amount)
        {
            lock (locker)
            {
                //setCashData u dllu poziva i setCashAmount i setCash kao grupnu funkciju
                setCashData(amount);
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
            }
        }

        public void incCashAmountAdapter(double amount)
        {
            double currentCashOnCard = getCashDataAdapter();
            setCashDataAdapter(currentCashOnCard + amount);
        }

        public void decCashAmountAdapter(double amount)
        {
            double currentCashOnCard = getCashDataAdapter();
            setCashDataAdapter(currentCashOnCard - amount);
        }

        public void repeatLoginAdapter(string username, string password)
        {
            lock (locker)
            {
                repeatLogin(username, password);
                int err = getErrorCode();
                if (err != 0)
                {
                    throw new Exception(ErrorDeclarations.getErrorDescription(Convert.ToString(err)));
                }
            }
        }
    }
}
