﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.ScBgdKiosk
{
    public partial class WaitingForCardFormScBgd : PisKiosk.WaitingForCardForm
    {
        public WaitingForCardFormScBgd()
        {
            InitializeComponent();
        }

        private static WaitingForCardFormScBgd instance = null;
        public static WaitingForCardFormScBgd getInstance()
        {
            if (instance == null)
            {
                instance = new WaitingForCardFormScBgd();
            }
            return instance;
        }

        private void WaitingForCardFormScBgd_Load(object sender, EventArgs e)
        {
            if (Program.KioskApplication.PrinterTesting)
            {
                btnPrinterStatus.Visible = true;
                btnPrintReceipt.Visible = true;
            }
            else
            {
                btnPrinterStatus.Visible = false;
                btnPrintReceipt.Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.showFormInPanel(UserDataFormScBgd.getInstance());
        }

        public override void cardInserted()
        {
            UserDataFormScBgd ud = UserDataFormScBgd.getInstance();
            ud.CardJustInserted = true;
            FormBusy = false;
            Program.KioskApplication.showFormInPanel(ud);
        }
    }
}
