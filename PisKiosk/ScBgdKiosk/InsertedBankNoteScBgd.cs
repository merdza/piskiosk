﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.ScBgdKiosk
{
    public class InsertedBankNoteScBgd : InsertedBankNote
    {
        public override void insertBankNote(int bankNoteValue)
        {
            
            Value = bankNoteValue;

            //Prvo se upisuje u bazu, zatim na karticu, a ako upis na karticu pukne, ponistava se transakcija u bazi

            int transactionId = 0;
            UserDataScBgd ud = UserDataFormScBgd.getInstance().ud;

            Program.KioskApplication.log.Debug("Stanje ud na pocetku insertBankNote: " + ud.ToString());

            try
            {
                DBAdapter.moneyDeposit(ud.CardNumber, bankNoteValue, out transactionId);
            }
            catch (Exception e)
            {
                throw new Exception("Inserting banknote to the database failed. " + e.Message);
            }


            try
            {
                ScBgdKiosk.CardAdapterContactScBgd cardInterface = (ScBgdKiosk.CardAdapterContactScBgd)CardInterface.getInstance().CardInterf;
                cardInterface.incCashAmountAdapter(bankNoteValue);
            }
            catch (Exception ex)
            {
                //ako nije uspeo upis na karticu, ponisti transakciju u bazi
                try
                {
                    DBAdapter.undoTransaction(transactionId);
                    throw new Exception("Writing to card failed. Database transaction was cancelled. " + ex.Message/*nije uspeo upis na karticu*/);
                }
                catch (Exception ex1)
                {
                    //nije uspelo ponistavanje u bazi
                    throw new Exception("Writing to card failed. Cancelation of database transaction ALSO FAILED. Card problem: " + ex.Message + " Database cancellation problem: " + ex1.Message);
                }
            }

            ud.CashOnCard += bankNoteValue;

            Program.KioskApplication.log.Debug("Stanje ud na kraju insertBankNote: " + ud.ToString());
           
        }
    }
}
