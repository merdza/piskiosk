﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using XmlSettings;


namespace PisKiosk.ScBgdKiosk
{
    public class DBAdapter : PisKiosk.DBAdapter
    {
        public DBAdapter():base() { }

        public static int getKioskOperaterID()
        {
            int kioskOperaterID = 0;
            try
            {
                string tableName = "InfoKiosk";
                string sqlConnectString = getProperty("connectionString");
                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlDataReader myReader = null;
                    SqlCommand myCommand = new SqlCommand("SELECT GlavniOperater FROM " + tableName + " WHERE ImeKioska = '" + Program.KioskApplication.KioskName + "'", connection);
                    connection.Open();
                    myReader = myCommand.ExecuteReader();

                    if (myReader.Read())
                    {
                        kioskOperaterID = Convert.ToInt32(myReader["GlavniOperater"]);
                    }
                    myReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return kioskOperaterID;
        }

        public static KioskStatuses getKioskStatus()
        {
            KioskStatuses kioskStatus = KioskStatuses.Info;  //moze samo da vidi stanje, ali ne i da radi nesto
            try
            {
                string tableName = "InfoKiosk";
                string sqlConnectString = getProperty("connectionString");
                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlDataReader myReader = null;
                    SqlCommand myCommand = new SqlCommand("SELECT IDStatusInfoKioska FROM " + tableName + " WHERE ImeKioska = '" + Program.KioskApplication.KioskName + "'", connection);
                    connection.Open();
                    myReader = myCommand.ExecuteReader();

                    if (myReader.Read())
                    {
                        kioskStatus = (KioskStatuses)Convert.ToInt32(myReader["IDStatusInfoKioska"]);
                    }
                    myReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return kioskStatus;
        }

        public static void getMealsPrices(string cardNumber, out double breakfastPrice, out double lunchPrice, out double dinnerPrice, out double breakfastNextPrice, out double lunchNextPrice, out double dinnerNextPrice, out int abonentCategory, out bool allowedBuyingThisMonth, out bool allowedBuyingNextMonth, out bool isResident, out bool firstShoppingInMonth)
        {
            try
            {
                string procName = "spNIKCeneObroka";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@brojKartice", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dorucak", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@rucak", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@vecera", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@dorucakSledeci", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@rucakSledeci", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@veceraSledeci", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@kategorijaAbonenta", SqlDbType.Int).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@dozvoljenaKupovinaOvogMeseca", SqlDbType.Bit).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@dozvoljenaKupovinaSledecegMeseca", SqlDbType.Bit).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@daLiJeBeogradjanin", SqlDbType.Bit).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@prvaKupovinaUMesecu", SqlDbType.Bit).Direction = ParameterDirection.Output;

                    command.Parameters["@brojKartice"].Value = cardNumber;
                    
                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);

                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", getMealsPrices");
                    }

                    breakfastPrice = Convert.ToDouble(command.Parameters["@dorucak"].Value);
                    lunchPrice = Convert.ToDouble(command.Parameters["@rucak"].Value);
                    dinnerPrice = Convert.ToDouble(command.Parameters["@vecera"].Value);
                    breakfastNextPrice = Convert.ToDouble(command.Parameters["@dorucakSledeci"].Value);
                    lunchNextPrice = Convert.ToDouble(command.Parameters["@rucakSledeci"].Value);
                    dinnerNextPrice = Convert.ToDouble(command.Parameters["@veceraSledeci"].Value);
                    abonentCategory = Convert.ToInt32(command.Parameters["@kategorijaAbonenta"].Value);
                    allowedBuyingThisMonth = Convert.ToBoolean(command.Parameters["@dozvoljenaKupovinaOvogMeseca"].Value);
                    allowedBuyingNextMonth = Convert.ToBoolean(command.Parameters["@dozvoljenaKupovinaSledecegMeseca"].Value);
                    isResident = Convert.ToBoolean(command.Parameters["@daLiJeBeogradjanin"].Value);
                    firstShoppingInMonth = Convert.ToBoolean(command.Parameters["@prvaKupovinaUMesecu"].Value);
                    
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public static int buyMealsWithEMoney(string cardNumber, double totalAmount, int breakfast, int lunch, int dinner, double totalAmountNext, int breakfastNext, int lunchNext, int dinnerNext, int month)
        {
            int idKupovina = 0;

            try
            {
                string procName = "spNIKKupovinaObrokaElektronskimNovcem";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@brKartice", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@iznos", SqlDbType.Float).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dorucak", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@rucak", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@vecera", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@iznosSledeciMesec", SqlDbType.Float).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dorucakSledeciMesec", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@rucakSledeciMesec", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@veceraSledeciMesec", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idMesec", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idKupovina", SqlDbType.Int).Direction = ParameterDirection.Output;

                    command.Parameters["@brKartice"].Value = cardNumber;
                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;
                    command.Parameters["@iznos"].Value = totalAmount;
                    command.Parameters["@dorucak"].Value = breakfast;
                    command.Parameters["@rucak"].Value = lunch;
                    command.Parameters["@vecera"].Value = dinner;
                    command.Parameters["@iznosSledeciMesec"].Value = totalAmountNext;
                    command.Parameters["@dorucakSledeciMesec"].Value = breakfastNext;
                    command.Parameters["@rucakSledeciMesec"].Value = lunchNext;
                    command.Parameters["@veceraSledeciMesec"].Value = dinnerNext;
                    command.Parameters["@idMesec"].Value = month;
                    
                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);
                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", buyMealsWithEMoney 1");
                    }

                    idKupovina = Convert.ToInt32(command.Parameters["@idKupovina"].Value);

                    if (!(idKupovina > 0))  // mozda je null ili ko zna sta
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", buyMealsWithEMoney 2");
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return idKupovina;
        }

        public static void undoTransaction(int idKupovina)
        {
            try
            {
                string procName = "spBrisanjeKupovine";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@idKupovina", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@resultStorno", SqlDbType.Int).Direction = ParameterDirection.Output;

                    command.Parameters["@idKupovina"].Value = idKupovina;
                    
                    connection.Open();
                    command.ExecuteNonQuery();

                    int result = Convert.ToInt32(command.Parameters["@resultStorno"].Value);
                    if (result != 0)
                    {
                        throw new Exception("db result = " + result.ToString());
                    } 

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(guiLang.DBAdapter_Error + ", undoTransaction" + ex.Message);
            }
        }

        public static void moneyDeposit(string cardNumber, int bankNote, out int transactionId)
        {
            try
            {
                string procName = "spNIKUplataElektronskogNovca";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@brojKartice", SqlDbType.NVarChar, 15).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@kolicina", SqlDbType.Float).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idKupovina", SqlDbType.Int).Direction = ParameterDirection.Output;
                    
                    command.Parameters["@brojKartice"].Value = cardNumber;
                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;
                    command.Parameters["@kolicina"].Value = bankNote;

                    connection.Open();
                    command.ExecuteNonQuery();

                    int result = Convert.ToInt32(command.Parameters["@rv"].Value);
                    if (result != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", moneyDeposit, " + result.ToString());
                    }

                    transactionId = Convert.ToInt32(command.Parameters["@idKupovina"].Value);
                    if (!(transactionId > 0))
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", moneyDeposit, idKupovina = " + transactionId.ToString());
                    }
                    
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(guiLang.DBAdapter_Error + ", moneyDeposit, " + ex.Message);
            }
        }

        public static bool isCardForThisKiosk(int opId)
        {
            //da li operaterska kartica koja je ubacena sme da podize pare iz kioska
            bool ret = false;

            if (opId == getKioskOperaterID())
            {
                ret = true;    
            }
            return ret;
        }

        public static void getMoneyInformation(out double total, out double totalUntilToday)
        {
            try
            {
                string procName = "spNIKProracunavanjePazaraZaPodizanje";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@ukupnaSuma", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@doDanasSuma", SqlDbType.Float).Direction = ParameterDirection.Output;

                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;

                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);

                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", getMoneyInformation");
                    }

                    total = Convert.ToDouble(command.Parameters["@ukupnaSuma"].Value);
                    totalUntilToday = Convert.ToDouble(command.Parameters["@doDanasSuma"].Value);

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void dischargeKiosk(double stanje, double ostatak)
        {
            try
            {
                //stanje - ukupno u kiosku
                //ostatak - koliko se ostavlja u kiosku
                //razlika ta dva je koliko se podize

                string procName = "spNIKPraznjenjeKioskaSaOstatkom";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@stanje", SqlDbType.Float).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@ostatak", SqlDbType.Float).Direction = ParameterDirection.Input;

                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;
                    command.Parameters["@stanje"].Value = stanje;
                    command.Parameters["@ostatak"].Value = ostatak;

                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);

                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", dischargeKiosk");
                    }

                    connection.Close();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void getServices(string cardNumber)
        {
            try
            {
                string procName = "spNIKOdredjivanjeUslugaZaPlacanje";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@brojKartice", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Input;

                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;
                    command.Parameters["@brojKartice"].Value = cardNumber;
                    
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    int result = Convert.ToInt32(command.Parameters["@rv"].Value);
                    if (result != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", getServices, " + result.ToString());
                    }

                    if (reader.HasRows)
                    {
                        Service s;

                        while (reader.Read())
                        {
                            s = new Service(Convert.ToString(reader[1]), Convert.ToDouble(reader[2]), Convert.ToInt32(reader[3]), Convert.ToInt32(reader[4]), Convert.ToInt32(reader[5]), Convert.ToBoolean(reader[6]));

                            BuyServicesFormScBgd.getInstance().bsd.addService(s);
                        }
                    }
                    else
                    {
                        throw new Exception("Ne postoje usluge za ubačenu karticu.");
                    }

                    
                    reader.Close();

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int InputPurchase(string brojKartice, int operater, int tipTransakcije)
        {
            int result;
            String procName = "spUnosKupovineNovo";
            string sqlConnectString = getProperty("connectionString");

            SqlConnection connection = null;
            try
            {
                using (connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandTimeout = 300;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@brKartice", SqlDbType.NVarChar, 15).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@operater", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idTipNovcaneTransakcije", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idKupovina", SqlDbType.Int).Direction = ParameterDirection.Output;

                    command.Parameters["@brKartice"].Value = brojKartice;
                    command.Parameters["@operater"].Value = operater;
                    command.Parameters["@idTipNovcaneTransakcije"].Value = tipTransakcije;

                    connection.Open();
                    int i = command.ExecuteNonQuery();

                    object val = command.Parameters["@idKupovina"].Value;
                    result = Convert.ToInt32(val);

                    if (result <= 0)
                    {
                        throw new Exception("Inserting purchase error. result = " + result);
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                connection.Close();
                connection.Dispose();
                throw ex;
            }

            return result;
        }

        //public static int UndoPurchase(int idKupovina)
        //{
        //    int result;
        //    String procName = "spStorno";
        //    string sqlConnectString = getProperty("connectionString");

        //    SqlConnection connection = null;
        //    try
        //    {
        //        using (connection = new SqlConnection(sqlConnectString))
        //        {
        //            SqlCommand command = new SqlCommand(procName, connection);
        //            command.CommandTimeout = 300;
        //            command.CommandType = CommandType.StoredProcedure;
        //            command.Parameters.Add("@idKupovina", SqlDbType.Int).Direction = ParameterDirection.Input;
        //            command.Parameters.Add("@resultStorno", SqlDbType.Int).Direction = ParameterDirection.Output;

        //            command.Parameters["@idKupovina"].Value = idKupovina;

        //            connection.Open();
        //            int i = command.ExecuteNonQuery();

        //            object val = command.Parameters["@resultStorno"].Value;
        //            result = Convert.ToInt32(val);

        //            connection.Close();

        //            if (result != 0)
        //            {
        //                throw new Exception("Undo purchase error. " + result);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        connection.Close();
        //        connection.Dispose();
        //        throw ex;
        //    }

        //    return result;
        //}

        public static void buyService(
            string brojKartice,
            int idKupovina,
            int idUsluga,
            int idPlacanjeBoravka,
            int idLicnaOstecenja,
            double ukupanIznos,
            int kolicina)
        {

            int result;
            string procName = "spNIKPlacanjeUsluge";
            string sqlConnectString = getProperty("connectionString");

            SqlConnection connection = null;
            try
            {
                using (connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandTimeout = 300;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@brojKartice", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idKupovina", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idUsluga", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idPlacanjeBoravka", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idLicnaOstecenja", SqlDbType.Int).Direction = ParameterDirection.Input;
                    SqlParameter p = command.Parameters.Add("@ukupanIznos", SqlDbType.Decimal);
                    p.Direction = ParameterDirection.Input;
                    p.Precision = 18;
                    p.Scale = 2;
                    command.Parameters.Add("@kolicina", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@result", SqlDbType.Int).Direction = ParameterDirection.Output;

                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;
                    command.Parameters["@brojKartice"].Value = brojKartice;
                    command.Parameters["@idKupovina"].Value = idKupovina;
                    command.Parameters["@idUsluga"].Value = idUsluga;
                    command.Parameters["@idPlacanjeBoravka"].Value = idPlacanjeBoravka;
                    command.Parameters["@idLicnaOstecenja"].Value = idLicnaOstecenja;
                    command.Parameters["@ukupanIznos"].Value = ukupanIznos;
                    command.Parameters["@kolicina"].Value = kolicina;

                    connection.Open();
                    int i = command.ExecuteNonQuery();

                    result = Convert.ToInt32(command.Parameters["@result"].Value);

                    if (result != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", buyService, " + result.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                connection.Close();
                connection.Dispose();
                throw ex;
            }
        }

        /*
        public static void getActivePictures(Hashtable activePictures)
        {
            SqlConnection connection = null;
            SqlDataReader myReader = null;

            try
            {
                string procName = "vAktivneFotografije";
                string sqlConnectString = getProperty("connectionString");

                using (connection = new SqlConnection(sqlConnectString))
                {
                    string sqlQuery = "SELECT Slika, BrojKartice FROM " + procName;
                    SqlCommand myCommand = new SqlCommand(sqlQuery, connection);
                    connection.Open();
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {
                        try
                        {
                            String cardNumber = myReader["BrojKartice"].ToString();
                            String picNumber = myReader["Slika"].ToString();

                            activePictures.Add(cardNumber, picNumber);
                        }
                        catch (Exception)
                        {
                            //ako je vec ubacen kljuc sa tim brojem kartice, bacice izuzetak ali ja ne zelim da prekinem punjenje tabele
                        }

                    }
                    myReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                if ((myReader != null) && (!myReader.IsClosed))
                {
                    myReader.Close();
                }
                if (connection != null)
                {
                    connection.Close();
                }
                throw new Exception("Nije uspelo prikupljanje podataka o aktivnim karticama. " + ex.Message);
            }
        }*/

        public static int fillCardsHashTable(HashSet<string> cardsTable)
        {
            int result = 0;
            SqlConnection connection = null;
            SqlCommand command = null;
            SqlDataReader dr = null;
            String procName = "spInitializeCardsHash";
            string sqlConnectString = getProperty("connectionString");
            try
            {
                using (connection = new SqlConnection(sqlConnectString))
                {
                    command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;

                    connection.Open();
                    dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        string cardNumber = Convert.ToString(dr["BrojKartice"]);
                        bool archive = Convert.ToBoolean(dr["Arhiva"]);

                        try
                        {
                            cardsTable.Add(cardNumber);
                            ++result;
                        }
                        catch (Exception)
                        {
                            //ako je vec ubacen kljuc sa tim brojem kartice, bacice izuzetak ali ja ne zelim da prekinem punjenje tabele
                        }
                    }

                    dr.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                result = 0;
                if ((dr != null) && (!dr.IsClosed))
                {
                    dr.Close();
                }
                if (connection != null)
                {
                    connection.Close();
                }
                throw ex;
            }

            return result;
        }

        public static int getActivePictures(Hashtable activePictures)
        {
            SqlConnection connection = null;
            SqlDataReader myReader = null;

            int numberOfPictures = 0;

            try
            {
                string procName = "vAktivneFotografije";
                string sqlConnectString = getProperty("connectionString");

                using (connection = new SqlConnection(sqlConnectString))
                {
                    string sqlQuery = "SELECT Slika, BrojKartice FROM " + procName;
                    SqlCommand myCommand = new SqlCommand(sqlQuery, connection);
                    connection.Open();
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {
                        try
                        {
                            String cardNumber = myReader["BrojKartice"].ToString();
                            String picNumber = myReader["Slika"].ToString();

                            activePictures.Add(cardNumber, picNumber);
                            numberOfPictures++;
                        }
                        catch (Exception)
                        {
                            //ako je vec ubacen kljuc sa tim brojem kartice, bacice izuzetak ali ja ne zelim da prekinem punjenje tabele
                        }

                    }
                    myReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                if ((myReader != null) && (!myReader.IsClosed))
                {
                    myReader.Close();
                }
                if (connection != null)
                {
                    connection.Close();
                }
                throw new Exception("Nije uspelo prikupljanje podataka o aktivnim karticama. " + ex.Message);
            }

            return numberOfPictures;
        }

        
    }
}

