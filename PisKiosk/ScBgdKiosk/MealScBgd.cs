﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.ScBgdKiosk
{
    public class MealScBgd : Meal
    {
        public MealScBgd(int m, int y, MealType mt, double p, int a = 0)
            : base(m, y, mt, p, a)
        {}
        public override void setMealsAllowed()
        {
            int allowedMeals = 0;
            if (Month == DateTime.Now.Month)
            {
                switch (Type)
                {
                    case MealType.Breakfast:
                        allowedMeals = Convert.ToInt32(ScBgdKiosk.UserDataFormScBgd.getInstance().ud.BreakfastLeft);
                        break;
                    case MealType.Lunch:
                        allowedMeals = Convert.ToInt32(ScBgdKiosk.UserDataFormScBgd.getInstance().ud.LunchLeft);
                        break;
                    case MealType.Dinner:
                        allowedMeals = Convert.ToInt32(ScBgdKiosk.UserDataFormScBgd.getInstance().ud.DinnerLeft);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                //sigurno je sledeci mesec, ne moze da bude ni jedan drugi
                switch (Type)
                {
                    case MealType.Breakfast:
                        allowedMeals = DateTime.DaysInMonth(Year, Month) - Convert.ToInt32(ScBgdKiosk.UserDataFormScBgd.getInstance().ud.BreakfastNext);
                        break;
                    case MealType.Lunch:
                        allowedMeals = DateTime.DaysInMonth(Year, Month) - Convert.ToInt32(ScBgdKiosk.UserDataFormScBgd.getInstance().ud.LunchNext);
                        break;
                    case MealType.Dinner:
                        allowedMeals = DateTime.DaysInMonth(Year, Month) - Convert.ToInt32(ScBgdKiosk.UserDataFormScBgd.getInstance().ud.DinnerNext);
                        break;
                    default:
                        break;
                }
            }

            MealsAllowed = allowedMeals;
        }
    }
}
