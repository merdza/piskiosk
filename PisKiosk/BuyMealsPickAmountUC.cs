﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class BuyMealsPickAmountUC : UserControl
    {
        public BuyMealsPickAmountUC(Meal m)
        {
            InitializeComponent();

            MealChosen = m;
            tensPlusClicked = new Stack<int>();
            NumberOfOnesClicked = 0;
            
            this.BackColor = Program.KioskApplication.BackgroundColor;
            lblMealTitle.ForeColor = Program.KioskApplication.TextForeColor2;
            lblMealAmountSelected.ForeColor = Program.KioskApplication.TextForeColor;
            lblAllowed.ForeColor = Program.KioskApplication.TextForeColor;

            lblTenMinusReason.ForeColor = Program.KioskApplication.TextForeColor;
            lblTenPlusReason.ForeColor = Program.KioskApplication.TextForeColor;
            lblOneMinusReason.ForeColor = Program.KioskApplication.TextForeColor;
            lblOnePlusReason.ForeColor = Program.KioskApplication.TextForeColor;

            btnOneMinus.BackColor = Program.KioskApplication.TextForeColor2;
            btnOneMinus.ForeColor = Program.KioskApplication.BackgroundColor;

            btnOnePlus.BackColor = Program.KioskApplication.TextForeColor2;
            btnOnePlus.ForeColor = Program.KioskApplication.BackgroundColor;

            btnTenMinus.BackColor = Program.KioskApplication.TextForeColor2;
            btnTenMinus.ForeColor = Program.KioskApplication.BackgroundColor;

            btnTenPlus.BackColor = Program.KioskApplication.TextForeColor2;
            btnTenPlus.ForeColor = Program.KioskApplication.BackgroundColor;

        }

        public BuyMealsPickAmountUC()
        { }

        protected Meal MealChosen { get; set; }

        protected Stack<int> tensPlusClicked; //lista u koju se upisuje redom na koje +10 (ili +8 ili +11) je korisnik kliknuo. Samo kada klikne na neko dugme, treba da mu se omoguci da kline na minus to isto dugme (na poslednji iz ove liste)
        protected int numberOfOnesClicked; //ovde ne mora da ide lista jer je ovde uvek ista vrednost = 1. Dovoljno je samo da pise koliko puta je korisnik klinuo na +1, pa dok je ova vrednost >0 dozvoli -1.

        public Stack<int> TensPlusClicked
        {
            get { return tensPlusClicked; }
            set { tensPlusClicked = value; }
        }

        public int NumberOfOnesClicked
        {
            get { return numberOfOnesClicked; }
            set { numberOfOnesClicked = value; }
        }

        

        protected virtual void setMealAmount(bool showLog)
        {
            lblMealAmountSelected.Text = MealChosen.Amount.ToString();
            lblAllowed.Text = guiLang.UserDataForm_Label_lblAllowedMealsToBuy + ": " + MealChosen.MealsAllowed.ToString();
            
            ScBgdKiosk.BuyMealsFormScBgd.getInstance().fillShoppingCartGrid();
            ScBgdKiosk.BuyMealsFormScBgd.getInstance().setCashAmounts();
            enableButtons(showLog);
        }
        protected virtual void enableButtons(bool showLog) { }

        protected virtual ClickButtonStatus checkIfEnoughMealsToBuy(int amount) 
        {
            return ClickButtonStatus.NotAllowed;
        }
        
        protected virtual void btnOnePlus_Click(object sender, EventArgs e){}

        protected virtual void btnOneMinus_Click(object sender, EventArgs e){}

        protected virtual void btnTenMinus_Click(object sender, EventArgs e){}

        protected virtual void btnTenPlus_Click(object sender, EventArgs e){}

        protected string getClickMessage(ClickButtonStatus s)
        {
            string msg = "";
            switch (s)
            {
                case ClickButtonStatus.NotEnoughMoney:
                    msg = guiLang.BuyMealsPickAmountUC_Message_NotEnoughMoney;
                    break;
                case ClickButtonStatus.NotAllowed:
                    msg = guiLang.BuyMealsPickAmountUC_Message_NotAllowed;
                    break;
                case ClickButtonStatus.PositiveNumberOnly:
                    msg = guiLang.BuyMealsPickAmountUC_Message_PositiveNumberOnly;
                    break;
                default:
                    break;
            }
            return msg;
        }

        private void BuyMealsPickAmountUC_Load(object sender, EventArgs e)
        {
            
        }

        private void BuyMealsPickAmountUC_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                userControlShown();
            }       
        }

        protected void userControlShown()
        {
            if (Visible)
            {
                lblMealTitle.Text = MealChosen.getMealTypeString().ToUpper() + " - " + Utils.getMonthString(MealChosen.Month).ToUpper();
                setMealAmount(true);
            }
        }

        private void BuyMealsPickAmountUC_Paint(object sender, PaintEventArgs e)
        {
            Utils.drawTabControlBorder(sender, e, this);
        }
    }

    public enum ClickButtonStatus { Allowed, NotEnoughMoney, NotAllowed, PositiveNumberOnly };

}
