﻿namespace PisKiosk
{
    partial class BuyServicesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            PisKiosk.Office2010Blue office2010Blue1 = new PisKiosk.Office2010Blue();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuyServicesForm));
            this.listViewService = new System.Windows.Forms.ListView();
            this.btnUp = new PisKiosk.XButton();
            this.btnDown = new PisKiosk.XButton();
            this.gridShoppingCart = new System.Windows.Forms.DataGridView();
            this.Item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblMoneyBalanceAfterShopping = new System.Windows.Forms.Label();
            this.lblCardMoneyBalance = new System.Windows.Forms.Label();
            this.panelServices = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnInsertCash = new PisKiosk.XButton();
            this.pictureBoxShoppingCart = new System.Windows.Forms.PictureBox();
            this.btnCancelBuyServices = new PisKiosk.XButton();
            this.btnBuyServices = new PisKiosk.XButton();
            this.btnMinusOne = new PisKiosk.XButton();
            this.btnPlusOne = new PisKiosk.XButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridShoppingCart)).BeginInit();
            this.panelServices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShoppingCart)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(127, 20);
            this.lblTitle.Size = new System.Drawing.Size(111, 35);
            // 
            // listViewService
            // 
            this.listViewService.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewService.AutoArrange = false;
            this.listViewService.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listViewService.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.listViewService.Font = new System.Drawing.Font("Segoe UI Light", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listViewService.FullRowSelect = true;
            this.listViewService.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewService.HideSelection = false;
            this.listViewService.LabelWrap = false;
            this.listViewService.Location = new System.Drawing.Point(8, 12);
            this.listViewService.MultiSelect = false;
            this.listViewService.Name = "listViewService";
            this.listViewService.Scrollable = false;
            this.listViewService.Size = new System.Drawing.Size(777, 234);
            this.listViewService.TabIndex = 1;
            this.listViewService.UseCompatibleStateImageBehavior = false;
            this.listViewService.View = System.Windows.Forms.View.Details;
            this.listViewService.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.listViewService_DrawItem);
            this.listViewService.SelectedIndexChanged += new System.EventHandler(this.listViewService_SelectedIndexChanged);
            this.listViewService.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listViewService_MouseDown);
            this.listViewService.MouseLeave += new System.EventHandler(this.listViewService_MouseLeave);
            this.listViewService.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listViewService_MouseUp);
            // 
            // btnUp
            // 
            this.btnUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            office2010Blue1.BorderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonMouseOverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            office2010Blue1.ButtonMouseOverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            office2010Blue1.ButtonMouseOverColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(137)))));
            office2010Blue1.ButtonMouseOverColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(249)))), ((int)(((byte)(224)))));
            office2010Blue1.ButtonNormalColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Blue1.ButtonNormalColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Blue1.ButtonNormalColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            office2010Blue1.ButtonNormalColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            office2010Blue1.ButtonSelectedColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            office2010Blue1.ButtonSelectedColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            office2010Blue1.ButtonSelectedColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(117)))));
            office2010Blue1.ButtonSelectedColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(107)))));
            office2010Blue1.HoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            office2010Blue1.SelectedTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            office2010Blue1.TextColor = System.Drawing.Color.White;
            this.btnUp.ColorTable = office2010Blue1;
            this.btnUp.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnUp.Location = new System.Drawing.Point(713, 9);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(75, 75);
            this.btnUp.TabIndex = 2;
            this.btnUp.Text = "gore";
            this.btnUp.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnDown
            // 
            this.btnDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDown.ColorTable = office2010Blue1;
            this.btnDown.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnDown.Location = new System.Drawing.Point(713, 169);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(75, 75);
            this.btnDown.TabIndex = 3;
            this.btnDown.Text = "dole";
            this.btnDown.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // gridShoppingCart
            // 
            this.gridShoppingCart.AllowUserToAddRows = false;
            this.gridShoppingCart.AllowUserToDeleteRows = false;
            this.gridShoppingCart.AllowUserToResizeColumns = false;
            this.gridShoppingCart.AllowUserToResizeRows = false;
            this.gridShoppingCart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridShoppingCart.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridShoppingCart.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.gridShoppingCart.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridShoppingCart.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridShoppingCart.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridShoppingCart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridShoppingCart.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item,
            this.Price,
            this.Amount,
            this.Total});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridShoppingCart.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridShoppingCart.Location = new System.Drawing.Point(137, 443);
            this.gridShoppingCart.MultiSelect = false;
            this.gridShoppingCart.Name = "gridShoppingCart";
            this.gridShoppingCart.ReadOnly = true;
            this.gridShoppingCart.RowHeadersVisible = false;
            this.gridShoppingCart.Size = new System.Drawing.Size(795, 282);
            this.gridShoppingCart.TabIndex = 16;
            this.gridShoppingCart.Visible = false;
            this.gridShoppingCart.SelectionChanged += new System.EventHandler(this.gridShoppingCart_SelectionChanged);
            // 
            // Item
            // 
            this.Item.HeaderText = "Item";
            this.Item.Name = "Item";
            this.Item.ReadOnly = true;
            // 
            // Price
            // 
            this.Price.HeaderText = "Price";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // lblMoneyBalanceAfterShopping
            // 
            this.lblMoneyBalanceAfterShopping.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneyBalanceAfterShopping.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblMoneyBalanceAfterShopping.Location = new System.Drawing.Point(407, 56);
            this.lblMoneyBalanceAfterShopping.Name = "lblMoneyBalanceAfterShopping";
            this.lblMoneyBalanceAfterShopping.Size = new System.Drawing.Size(539, 39);
            this.lblMoneyBalanceAfterShopping.TabIndex = 20;
            this.lblMoneyBalanceAfterShopping.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCardMoneyBalance
            // 
            this.lblCardMoneyBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCardMoneyBalance.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblCardMoneyBalance.Location = new System.Drawing.Point(407, 20);
            this.lblCardMoneyBalance.Name = "lblCardMoneyBalance";
            this.lblCardMoneyBalance.Size = new System.Drawing.Size(539, 38);
            this.lblCardMoneyBalance.TabIndex = 21;
            this.lblCardMoneyBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panelServices
            // 
            this.panelServices.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelServices.BackColor = System.Drawing.Color.Transparent;
            this.panelServices.Controls.Add(this.btnUp);
            this.panelServices.Controls.Add(this.btnDown);
            this.panelServices.Controls.Add(this.listViewService);
            this.panelServices.ForeColor = System.Drawing.Color.White;
            this.panelServices.Location = new System.Drawing.Point(137, 139);
            this.panelServices.Name = "panelServices";
            this.panelServices.Size = new System.Drawing.Size(795, 256);
            this.panelServices.TabIndex = 23;
            this.panelServices.Paint += new System.Windows.Forms.PaintEventHandler(this.panelServices_Paint);
            this.panelServices.Resize += new System.EventHandler(this.panelServices_Resize);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PisKiosk.Properties.Resources.HandScroll100x1201;
            this.pictureBox1.Location = new System.Drawing.Point(12, 205);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(119, 120);
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // btnInsertCash
            // 
            this.btnInsertCash.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInsertCash.ColorTable = office2010Blue1;
            this.btnInsertCash.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnInsertCash.Image = ((System.Drawing.Image)(resources.GetObject("btnInsertCash.Image")));
            this.btnInsertCash.Location = new System.Drawing.Point(946, 7);
            this.btnInsertCash.Name = "btnInsertCash";
            this.btnInsertCash.Size = new System.Drawing.Size(176, 100);
            this.btnInsertCash.TabIndex = 22;
            this.btnInsertCash.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnInsertCash.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnInsertCash.UseVisualStyleBackColor = true;
            this.btnInsertCash.Click += new System.EventHandler(this.btnInsertCash_Click);
            // 
            // pictureBoxShoppingCart
            // 
            this.pictureBoxShoppingCart.Image = global::PisKiosk.Properties.Resources.shopping_cart_gray;
            this.pictureBoxShoppingCart.Location = new System.Drawing.Point(12, 443);
            this.pictureBoxShoppingCart.Name = "pictureBoxShoppingCart";
            this.pictureBoxShoppingCart.Size = new System.Drawing.Size(119, 106);
            this.pictureBoxShoppingCart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxShoppingCart.TabIndex = 17;
            this.pictureBoxShoppingCart.TabStop = false;
            // 
            // btnCancelBuyServices
            // 
            this.btnCancelBuyServices.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelBuyServices.ColorTable = office2010Blue1;
            this.btnCancelBuyServices.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnCancelBuyServices.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelBuyServices.Image")));
            this.btnCancelBuyServices.Location = new System.Drawing.Point(946, 578);
            this.btnCancelBuyServices.Name = "btnCancelBuyServices";
            this.btnCancelBuyServices.Size = new System.Drawing.Size(176, 60);
            this.btnCancelBuyServices.TabIndex = 18;
            this.btnCancelBuyServices.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnCancelBuyServices.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnCancelBuyServices.UseVisualStyleBackColor = true;
            this.btnCancelBuyServices.Click += new System.EventHandler(this.btnCancelBuyServices_Click);
            // 
            // btnBuyServices
            // 
            this.btnBuyServices.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBuyServices.ColorTable = office2010Blue1;
            this.btnBuyServices.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnBuyServices.Image = ((System.Drawing.Image)(resources.GetObject("btnBuyServices.Image")));
            this.btnBuyServices.Location = new System.Drawing.Point(946, 443);
            this.btnBuyServices.Name = "btnBuyServices";
            this.btnBuyServices.Size = new System.Drawing.Size(176, 100);
            this.btnBuyServices.TabIndex = 19;
            this.btnBuyServices.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnBuyServices.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnBuyServices.UseVisualStyleBackColor = true;
            this.btnBuyServices.Click += new System.EventHandler(this.btnBuyServices_Click);
            // 
            // btnMinusOne
            // 
            this.btnMinusOne.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinusOne.ColorTable = office2010Blue1;
            this.btnMinusOne.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnMinusOne.Image = global::PisKiosk.Properties.Resources.recycle_bin_png32;
            this.btnMinusOne.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnMinusOne.Location = new System.Drawing.Point(983, 271);
            this.btnMinusOne.Name = "btnMinusOne";
            this.btnMinusOne.Size = new System.Drawing.Size(100, 100);
            this.btnMinusOne.TabIndex = 3;
            this.btnMinusOne.Text = "-1";
            this.btnMinusOne.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnMinusOne.UseVisualStyleBackColor = true;
            this.btnMinusOne.Click += new System.EventHandler(this.btnMinusOne_Click);
            // 
            // btnPlusOne
            // 
            this.btnPlusOne.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlusOne.ColorTable = office2010Blue1;
            this.btnPlusOne.Font = new System.Drawing.Font("Segoe UI Light", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPlusOne.Image = global::PisKiosk.Properties.Resources.shopping_cart_gray_32;
            this.btnPlusOne.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnPlusOne.Location = new System.Drawing.Point(983, 152);
            this.btnPlusOne.Name = "btnPlusOne";
            this.btnPlusOne.Size = new System.Drawing.Size(100, 100);
            this.btnPlusOne.TabIndex = 3;
            this.btnPlusOne.Text = "+1";
            this.btnPlusOne.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnPlusOne.UseVisualStyleBackColor = true;
            this.btnPlusOne.Click += new System.EventHandler(this.btnPlusOne_Click);
            // 
            // BuyServicesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1150, 650);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panelServices);
            this.Controls.Add(this.btnInsertCash);
            this.Controls.Add(this.lblMoneyBalanceAfterShopping);
            this.Controls.Add(this.lblCardMoneyBalance);
            this.Controls.Add(this.pictureBoxShoppingCart);
            this.Controls.Add(this.gridShoppingCart);
            this.Controls.Add(this.btnCancelBuyServices);
            this.Controls.Add(this.btnBuyServices);
            this.Controls.Add(this.btnMinusOne);
            this.Controls.Add(this.btnPlusOne);
            this.Name = "BuyServicesForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.VisibleChanged += new System.EventHandler(this.BuyServices_VisibleChanged);
            this.Controls.SetChildIndex(this.btnPlusOne, 0);
            this.Controls.SetChildIndex(this.btnMinusOne, 0);
            this.Controls.SetChildIndex(this.btnBuyServices, 0);
            this.Controls.SetChildIndex(this.btnCancelBuyServices, 0);
            this.Controls.SetChildIndex(this.gridShoppingCart, 0);
            this.Controls.SetChildIndex(this.pictureBoxShoppingCart, 0);
            this.Controls.SetChildIndex(this.lblCardMoneyBalance, 0);
            this.Controls.SetChildIndex(this.lblMoneyBalanceAfterShopping, 0);
            this.Controls.SetChildIndex(this.btnInsertCash, 0);
            this.Controls.SetChildIndex(this.panelServices, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gridShoppingCart)).EndInit();
            this.panelServices.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShoppingCart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.ListView listViewService;
        private XButton btnUp;
        private XButton btnDown;
        private System.Windows.Forms.PictureBox pictureBoxShoppingCart;
        private System.Windows.Forms.DataGridView gridShoppingCart;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        protected XButton btnCancelBuyServices;
        protected XButton btnBuyServices;
        private XButton btnPlusOne;
        private XButton btnMinusOne;
        private System.Windows.Forms.Label lblMoneyBalanceAfterShopping;
        private System.Windows.Forms.Label lblCardMoneyBalance;
        protected XButton btnInsertCash;
        private System.Windows.Forms.Panel panelServices;
        private System.Windows.Forms.PictureBox pictureBox1;
        

    }
}
