﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PisKiosk
{
    //Klasa TabControlExtended se koristi da bi tab kontroli obrisali borderi jer drugacije ne moze
    //onda posle, u onPaint metodi se nacrta pravougaonik kao border i on bude jednolican sa svih strana, a ne 3D kako je po defaultu
    public class TabControlExtended : TabControl
    {
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            const int TCM_ADJUSTRECT = 0x1328;

            try
            {
                if (m.Msg == TCM_ADJUSTRECT)
                {
                    RECT rc = (RECT)m.GetLParam(typeof(RECT));
                    rc.Left -= 4;
                    rc.Right += 4;
                    rc.Top -= 4;
                    rc.Bottom += 4;
                    Marshal.StructureToPtr(rc, m.LParam, true);
                }
            }
            catch (Exception)
            {}
            
            base.WndProc(ref m);
        }

    }
    internal struct RECT { public int Left, Top, Right, Bottom; }
}
