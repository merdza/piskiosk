﻿namespace PisKiosk
{
    partial class BuyMealsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlMeal = new PisKiosk.TabControlExtended();
            this.SuspendLayout();
            // 
            // tabControlMeal
            // 
            this.tabControlMeal.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControlMeal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMeal.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControlMeal.ItemSize = new System.Drawing.Size(90, 200);
            this.tabControlMeal.Location = new System.Drawing.Point(61, 29);
            this.tabControlMeal.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabControlMeal.Multiline = true;
            this.tabControlMeal.Name = "tabControlMeal";
            this.tabControlMeal.SelectedIndex = 0;
            this.tabControlMeal.Size = new System.Drawing.Size(987, 285);
            this.tabControlMeal.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControlMeal.TabIndex = 1;
            this.tabControlMeal.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControlMeal_DrawItem);
            // 
            // BuyMealsUC
            // 
            this.Controls.Add(this.tabControlMeal);
            this.Name = "BuyMealsUC";
            this.Size = new System.Drawing.Size(1098, 350);
            this.Load += new System.EventHandler(this.BuyMealsUC_Load);
            this.VisibleChanged += new System.EventHandler(this.BuyMealsUC_VisibleChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.BuyMealsUC_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        protected TabControlExtended tabControlMeal;

        //private TabControlExtended tabControlMeal;

    }
}
