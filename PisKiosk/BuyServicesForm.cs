﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class BuyServicesForm : PisKiosk.MainForm
    {
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;

        public BuyServicesForm()
        {
            InitializeComponent();

            LangsVisible = false;
            ReactOnCardRemoved = true;

            this.BackColor = Program.KioskApplication.BackgroundColor;

            listViewService.BackColor = Program.KioskApplication.BackgroundColor;
            listViewService.ForeColor = Program.KioskApplication.TextForeColor;

            gridShoppingCart.EnableHeadersVisualStyles = false;

            gridShoppingCart.ColumnHeadersDefaultCellStyle.BackColor = Program.KioskApplication.TextForeColor2;
            gridShoppingCart.ColumnHeadersDefaultCellStyle.ForeColor = Program.KioskApplication.BackgroundColor;

            gridShoppingCart.BackgroundColor = Program.KioskApplication.BackgroundColor;
            gridShoppingCart.GridColor = Program.KioskApplication.TextForeColor;
            gridShoppingCart.DefaultCellStyle.BackColor = Program.KioskApplication.BackgroundColor;
            gridShoppingCart.DefaultCellStyle.ForeColor = Program.KioskApplication.TextForeColor;

            lblCardMoneyBalance.ForeColor = Program.KioskApplication.TextForeColor2;
            lblMoneyBalanceAfterShopping.ForeColor = Program.KioskApplication.TextForeColor2;

            btnBuyServices.BackColor = Program.KioskApplication.TextForeColor2;
            btnBuyServices.ForeColor = Program.KioskApplication.BackgroundColor;

            btnCancelBuyServices.BackColor = Program.KioskApplication.TextForeColor2;
            btnCancelBuyServices.ForeColor = Program.KioskApplication.BackgroundColor;

            btnInsertCash.BackColor = Program.KioskApplication.TextForeColor2;
            btnInsertCash.ForeColor = Program.KioskApplication.BackgroundColor;

            btnPlusOne.BackColor = Program.KioskApplication.TextForeColor2;
            btnPlusOne.ForeColor = Program.KioskApplication.BackgroundColor;

            btnMinusOne.BackColor = Program.KioskApplication.TextForeColor2;
            btnMinusOne.ForeColor = Program.KioskApplication.BackgroundColor;

            //char character = (char)581;  //u ascii tabeli 581 je kod za obrnuto V - sto predstavlja strelicu na gore
            btnUp.Text = ((char)0x25B2).ToString();
            //btnDown.Text = "V"; //strelica na dole
            btnDown.Text = ((char)0x25BC).ToString();
            btnUp.BackColor = Program.KioskApplication.TextForeColor2;
            btnUp.ForeColor = Program.KioskApplication.BackgroundColor;
            btnDown.BackColor = Program.KioskApplication.TextForeColor2;
            btnDown.ForeColor = Program.KioskApplication.BackgroundColor;

            //nema sortiranja shopping carta
            foreach (DataGridViewColumn column in gridShoppingCart.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }



            columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));

            

            listViewService.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] { columnHeader1, columnHeader2 });
            
            listViewService.Columns[0].TextAlign = HorizontalAlignment.Left;
            listViewService.Columns[1].TextAlign = HorizontalAlignment.Center;

            


            //listViewService.Columns[0].Width = -2;
            //columnHeader1.Width = 800;
            //columnHeader2.Width = 200;
            //columnHeader1.Width = -2;

            //listViewService.Columns[0].Width = -2;
            //SizeLastColumn(listViewService);  //prva kolona, maksimalno koliko je ostalo prostora
        }

        private void SizeLastColumn(ListView lv)
        {
            lv.Columns[lv.Columns.Count - 1].Width = -2;
        }

        public BuyServicesData bsd = null;
        public Service SelectedService { get; set; }

        public virtual void setCashValues() { }

        protected void setGridShoppingCardTitles()
        {
            gridShoppingCart.Columns[0].HeaderText = guiLang.BuyMealsFormScBgd_GridShoppingCart_Header_0;
            gridShoppingCart.Columns[1].HeaderText = guiLang.BuyMealsFormScBgd_GridShoppingCart_Header_1;
            gridShoppingCart.Columns[2].HeaderText = guiLang.BuyMealsFormScBgd_GridShoppingCart_Header_2;
            gridShoppingCart.Columns[3].HeaderText = guiLang.BuyMealsFormScBgd_GridShoppingCart_Header_3;
        }
        private void setLabels()
        {
            btnBuyServices.Text = guiLang.BuyMealsFormScBgd_Button_btnBuyFood;
            btnCancelBuyServices.Text = guiLang.BuyMealsFormScBgd_Button_btnCancelBuyFood;
            btnInsertCash.Text = guiLang.UserDataForm_Button_btnInsertCash;
        }

        public void fillServices()
        {
            bsd.Services.Clear();
            bsd.fillServicesFromDB();
            SelectedService = bsd.Services[0];
            fillServicesListView();
        }

        public void fillServicesListView()
        { 
            //ovde treba izabrati 5 stavki i prikazati ih u listView
            listViewService.Items.Clear();

            //ColumnHeader header = new ColumnHeader();
            ////Same Width as Entire List Control
            //listViewService.Columns.Add(header);

            int serviceLen = Convert.ToInt32(XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "servicesDisplayLength"));

            Service second;
            Service first;
            Service fourth;
            Service fifth;

            if (bsd.Services.Count < 1)
            {
                throw new Exception("Nema usluga!"); // ovo se nece desiti jer ce se vec ranije baciti exception
            }
            if (bsd.Services.Count < 5)
            {
                //nemam dovoljno usluga da popunim listView
                if (bsd.Services.Count == 4)
                {
                    //second = bsd.getPreviousService(SelectedService);
                    //first = bsd.getPreviousService(second);
                    //fourth = bsd.getNextService(SelectedService);

                    //addServiceToListView(first);
                    //addServiceToListView(second);
                    //addServiceToListView(SelectedService);
                    //addServiceToListView(fourth);
                    //addServiceToListView(null);


                    second = bsd.getPreviousService(SelectedService);
                    first = bsd.getPreviousService(second);
                    fourth = bsd.getNextService(SelectedService);

                    System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(first.Name, serviceLen, false), Utils.getCurrencyString(first.Price.ToString()) }, -1);
                    System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(second.Name, serviceLen, false), Utils.getCurrencyString(second.Price.ToString()) }, -1);
                    System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(SelectedService.Name, serviceLen, false), Utils.getCurrencyString(SelectedService.Price.ToString()) }, -1);
                    System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(fourth.Name, serviceLen, false), Utils.getCurrencyString(fourth.Price.ToString()) }, -1);
                    System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] { "", "" }, -1);

                    listViewService.Items.AddRange(new System.Windows.Forms.ListViewItem[] { listViewItem1, listViewItem2, listViewItem3, listViewItem4, listViewItem5 });

                }
                if (bsd.Services.Count == 3)
                {
                    //second = bsd.getPreviousService(SelectedService);
                    //fourth = bsd.getNextService(SelectedService);
                    
                    //addServiceToListView(null);
                    //addServiceToListView(second);
                    //addServiceToListView(SelectedService);
                    //addServiceToListView(fourth);
                    //addServiceToListView(null);

                    second = bsd.getPreviousService(SelectedService);
                    fourth = bsd.getNextService(SelectedService);

                    System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] { "", "" }, -1);
                    System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(second.Name, serviceLen, false), Utils.getCurrencyString(second.Price.ToString()) }, -1);
                    System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(SelectedService.Name, serviceLen, false), Utils.getCurrencyString(SelectedService.Price.ToString()) }, -1);
                    System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(fourth.Name, serviceLen, false), Utils.getCurrencyString(fourth.Price.ToString()) }, -1);
                    System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] { "", "" }, -1);

                    listViewService.Items.AddRange(new System.Windows.Forms.ListViewItem[] { listViewItem1, listViewItem2, listViewItem3, listViewItem4, listViewItem5 });

                }
                if (bsd.Services.Count == 2)
                {
                    //second = bsd.getPreviousService(SelectedService);

                    //addServiceToListView(null);
                    //addServiceToListView(second);
                    //addServiceToListView(SelectedService);
                    //addServiceToListView(null);
                    //addServiceToListView(null);

                    second = bsd.getPreviousService(SelectedService);

                    System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] { "", "" }, -1);
                    System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(second.Name, serviceLen, false), Utils.getCurrencyString(second.Price.ToString()) }, -1);
                    System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(SelectedService.Name, serviceLen, false), Utils.getCurrencyString(SelectedService.Price.ToString()) }, -1);
                    System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] { "", "" }, -1);
                    System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] { "", "" }, -1);

                    listViewService.Items.AddRange(new System.Windows.Forms.ListViewItem[] { listViewItem1, listViewItem2, listViewItem3, listViewItem4, listViewItem5 });
                }
                if (bsd.Services.Count == 1)
                {
                    //addServiceToListView(null);
                    //addServiceToListView(null);
                    //addServiceToListView(SelectedService);
                    //addServiceToListView(null);
                    //addServiceToListView(null);

                    System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] { "", "" }, -1);
                    System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] { "", "" }, -1);
                    System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(SelectedService.Name, serviceLen, false), Utils.getCurrencyString(SelectedService.Price.ToString()) }, -1);
                    System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] { "", "" }, -1);
                    System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] { "", "" }, -1);

                    listViewService.Items.AddRange(new System.Windows.Forms.ListViewItem[] { listViewItem1, listViewItem2, listViewItem3, listViewItem4, listViewItem5 });

                }
            }
            else
            {
                second = bsd.getPreviousService(SelectedService);
                first = bsd.getPreviousService(second);
                fourth = bsd.getNextService(SelectedService);
                fifth = bsd.getNextService(fourth);

                System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(first.Name, serviceLen, false), Utils.getCurrencyString(first.Price.ToString()) }, -1);
                System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(second.Name, serviceLen, false), Utils.getCurrencyString(second.Price.ToString()) }, -1);
                System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(SelectedService.Name, serviceLen, false), Utils.getCurrencyString(SelectedService.Price.ToString()) }, -1);
                System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(fourth.Name, serviceLen, false), Utils.getCurrencyString(fourth.Price.ToString()) }, -1);
                System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] { getDisplayString(fifth.Name, serviceLen, false), Utils.getCurrencyString(fifth.Price.ToString()) }, -1);
                
                listViewService.Items.AddRange(new System.Windows.Forms.ListViewItem[] { listViewItem1, listViewItem2, listViewItem3, listViewItem4, listViewItem5 });

            }

            emphasize();

            listViewService.Columns[1].Width = 300;
            listViewService.Columns[0].Width = listViewService.Width - 300;
        }

        public void emphasize()
        {
            int cnt = 1;
            foreach (ListViewItem item in listViewService.Items)
            {
                switch (cnt)
                {
                    case 1:
                        item.BackColor = Program.KioskApplication.BackgroundColor;
                        item.Font = new Font(item.Font.FontFamily, 20, FontStyle.Bold);
                        break;
                    case 2:
                        item.BackColor = Program.KioskApplication.BackgroundColor;
                        item.Font = new Font(item.Font.FontFamily, 20, FontStyle.Bold);
                        break;
                    case 5:
                        item.BackColor = Program.KioskApplication.BackgroundColor;
                        item.Font = new Font(item.Font.FontFamily, 20, FontStyle.Bold);
                        break;
                    case 4:
                        item.BackColor = Program.KioskApplication.BackgroundColor;
                        item.Font = new Font(item.Font.FontFamily, 20, FontStyle.Bold);
                        break;
                    case 3:
                      // ListViewItem s = listViewService.FindItemWithText(getServiceDisplayString(SelectedService), true, 0, false);
                       // s.BackColor = Program.KioskApplication.TextForeColor2;
                        //s.Font = new Font(s.Font.FontFamily, 24, FontStyle.Bold);
                        item.BackColor = Program.KioskApplication.TextForeColor2;
                        item.Font = new Font(item.Font.FontFamily, 24, FontStyle.Bold);
                        break;
                    default:
                        break;
                }
                cnt++;
            }


        }

        private void addServiceToListView(Service s)
        {
            //if (s != null)
            //{
            //    if (listViewService.Items.Count > 0)
            //    {
            //        if (listViewService.FindItemWithText(getServiceDisplayString(s), true, 0, false) != null)
            //        {
            //            throw new Exception("Same servise is displayed more than once in the list. Probably the name of two different services is the same when shortened.");
            //        }
            //    }
            //}

            //listViewService.Items.Add(getServiceDisplayString(s), "XXX");
        }

        private string addSpacesToString(string s, int numberOfSpaces, bool inFront)
        {
            if (String.IsNullOrEmpty(s))
            {
                return "";
            }

            string ret = "";

            //if (s.Length > length)
            //{
            //    ret = Utils.makeFixedLengthString(s, length);
            //}
            //else
            //{
                string mask = "";

                for (int i = 0; i < numberOfSpaces; i++)
                {
                    mask += " ";
                }

                //string temp = "";
                if (inFront)
                {
                    ret = mask + s;
                    //ret = temp.Substring(temp.Length - numberOfSpaces, numberOfSpaces);
                }
                else
                {
                    ret = s + mask;
                    //ret = temp.Substring(0, numberOfSpaces);
                }
            //}

            return ret;
        }

        private string getDisplayString(string s, int length, bool inFront)
        {
            if (String.IsNullOrEmpty(s))
            {
                return "";
            }

            string ret = "";

            if (s.Length > length)
            {
                ret = Utils.makeFixedLengthString(s, length);
            }
            else
            {
                string mask = "";

                for (int i = 0; i < length; i++)
                {
                    mask += " ";
                }

                string temp = "";
                if (inFront)
                {
                    temp = mask + s;
                    ret = temp.Substring(temp.Length - length, length);
                }
                else
                {
                    temp = s + mask;
                    ret = temp.Substring(0, length);
                }
            }

            return ret;
            
        }

        

        

        private void listViewService_SelectedIndexChanged(object sender, EventArgs e)
        {
            listViewService.SelectedIndices.Clear(); //onemogucava selektovanje novcanica u ovom listView-u
        }

        protected void btnUp_Click(object sender, EventArgs e)
        {
            if (!FormBusy)
            {
                try
                {
                    SelectedService = bsd.getNextService(SelectedService);
                    fillServicesListView();
                    enableButtons();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Services button up click failed " + ex.Message, ex);
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }

        protected void btnDown_Click(object sender, EventArgs e)
        {
            if (!FormBusy)
            {
                try
                {
                    SelectedService = bsd.getPreviousService(SelectedService);
                    fillServicesListView();
                    enableButtons();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Services button up click failed " + ex.Message, ex);
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }

        private void listViewService_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            ////TextFormatFlags flags = TextFormatFlags.Left;
            
            //using (StringFormat sf = new StringFormat())
            //{
            //    sf.Alignment = StringAlignment.Center;
            ////    flags = TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter;

            //    e.DrawBackground();

            //    Brush brush = new SolidBrush(listViewService.ForeColor);

            //    e.Graphics.DrawString(e.Item.Text, e.Item.Font, brush, e.Bounds, sf);

            //    // Draw normal text for a subitem with a nonnegative 
            //    // or nonnumerical value.
            //    //e.DrawText(flags);
            //}

            ////e.DrawText(flags);
        }

        private void BuyServices_VisibleChanged(object sender, EventArgs e)
        {
            try 
	        {	        
		        if (this.Visible)
                    {
                        fillServices();
                        setGridShoppingCardTitles();
                        setLabels();
                        setCashValues();
                        setServiceAmount();
                    }
	        }
	        catch (Exception ex)
	        {
                btnCancelBuyServices_Click("Return", e);
                MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
	        }
        }

        public virtual void btnPlusOne_Click(object sender, EventArgs e){}

        public virtual void btnMinusOne_Click(object sender, EventArgs e){}

        public void setCashAmounts()
        {
            lblCardMoneyBalance.Text = guiLang.BuyMealsFormScBgd_Label_lblCardMoneyBalance + " " + Utils.getCurrencyString(bsd.CardMoneyBalance, true);
            lblMoneyBalanceAfterShopping.Text = guiLang.BuyMealsFormScBgd_Label_lblMoneyBalanceAfterShopping + " " + Utils.getCurrencyString(bsd.MoneyBalanceAfterBuyingSelected, true);
        }

        protected void setServiceAmount()
        {
            fillShoppingCartGrid();
            setCashAmounts();
            enableButtons();
        }

        protected void enableButtons()
        {
            bool btnPlusOneValue = true;

            //+1 bi trebalo da se pojavi ako ima dovoljno para da ga kupi
            if (!bsd.hasEnoughMoneyToBuy(SelectedService, 1))
            {
                btnPlusOneValue = false;
            }

            //ako je vec jednom kupio, pogledaj da li sme da kupi vise od jednom
            if (SelectedService.Amount == 1)
            {
                if (!SelectedService.MultiPurchasable)
	            {
                    btnPlusOneValue = false;
	            }
            }

            btnPlusOne.Enabled = btnPlusOneValue;
            
            //-1 treba da se se pojavi ako je vec ubacio bar 1 takvu uslugu u korpu
            if (SelectedService.Amount > 0)
            {
                btnMinusOne.Enabled = true;
            }
            else
            {
                btnMinusOne.Enabled = false;
            }

        }

        public void fillShoppingCartGrid()
        {
            gridShoppingCart.Rows.Clear();

            bool cartVisible = false;

            foreach (var service in bsd.Services)
            {
                if (service.Amount > 0)
                {
                    gridShoppingCart.Rows.Add(service.Name,
                                              Utils.getCurrencyString(service.Price, true),
                                              service.Amount.ToString(),
                                              Utils.getCurrencyString((service.Amount * service.Price), true));
                    cartVisible = true;
                }
            }

            DataGridViewRow row = new DataGridViewRow();
            row.DefaultCellStyle.BackColor = Program.KioskApplication.TextForeColor2;
            row.DefaultCellStyle.ForeColor = Program.KioskApplication.BackColor;
            for (int i = 0; i < 4; i++)
            {
                row.Cells.Add(new DataGridViewTextBoxCell());
            }
            row.Cells[0].Value = guiLang.BuyMealsFormScBgd_GridShoppingCart_Total.ToUpper();
            row.Cells[3].Value = Utils.getCurrencyString(bsd.getTotalPrice(), true);
            gridShoppingCart.Rows.Add(row);

            pictureBoxShoppingCart.Visible = cartVisible;
            gridShoppingCart.Visible = cartVisible;
            btnBuyServices.Visible = cartVisible;
        }

        private void gridShoppingCart_SelectionChanged(object sender, EventArgs e)
        {
            gridShoppingCart.ClearSelection();
        }

        public virtual void btnCancelBuyServices_Click(object sender, EventArgs e){}

        protected virtual void btnBuyServices_Click(object sender, EventArgs e){}

        protected virtual void btnInsertCash_Click(object sender, EventArgs e){}

        private void panelServices_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, this.panelServices.ClientRectangle, Color.White, ButtonBorderStyle.Solid);
        }

        private void panelServices_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }


        /* implementacija slajda */
        private int difference = 10;
        private int xPosition;
        private int yPosition;
        private bool slide = false; //da li je prethodno pritisnut taster u okviru forme i da li treba pratiti kretanje kursora
        private void listViewService_MouseDown(object sender, MouseEventArgs e)
        {
            //this.MouseMove += new MouseEventHandler(Form_MouseMove);
            Point mousePosition = System.Windows.Forms.Cursor.Position;
            xPosition = mousePosition.X;
            yPosition = mousePosition.Y;
            slide = true;
        }


        private void listViewService_MouseLeave(object sender, EventArgs e)
        {
            slideItems();
        }

        private void listViewService_MouseUp(object sender, MouseEventArgs e)
        {
            slideItems();
        }

        private void slideItems()
        {
            if (slide)
            {
                Point mousePosition = System.Windows.Forms.Cursor.Position;
                if (mousePosition.Y < yPosition - difference)
                {
                    slide = false;
                    btnUp_Click(null, null);
                }
                else if (mousePosition.Y > yPosition + difference)
                {
                    slide = false;
                    btnDown_Click(null, null);
                }
            }
            slide = false;
        }

        /*kraj slajdiranja*/
    }
}
