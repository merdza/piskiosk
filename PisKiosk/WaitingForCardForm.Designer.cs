﻿namespace PisKiosk
{
    partial class WaitingForCardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblInsertCard = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.timerLabelMove = new System.Windows.Forms.Timer(this.components);
            this.panelTitle = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnPrinterStatus = new System.Windows.Forms.Button();
            this.btnPrintReceipt = new System.Windows.Forms.Button();
            this.panelTitle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(19, 21);
            this.lblTitle.Text = "Title - WaitingForCardForm";
            // 
            // lblInsertCard
            // 
            this.lblInsertCard.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblInsertCard.AutoSize = true;
            this.lblInsertCard.Font = new System.Drawing.Font("Segoe UI Light", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblInsertCard.Location = new System.Drawing.Point(-167, 15);
            this.lblInsertCard.Name = "lblInsertCard";
            this.lblInsertCard.Size = new System.Drawing.Size(0, 86);
            this.lblInsertCard.TabIndex = 0;
            this.lblInsertCard.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(532, 417);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "cardInserted";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timerLabelMove
            // 
            this.timerLabelMove.Interval = 20000;
            this.timerLabelMove.Tick += new System.EventHandler(this.timerLabelMove_Tick);
            // 
            // panelTitle
            // 
            this.panelTitle.Controls.Add(this.pictureBox1);
            this.panelTitle.Controls.Add(this.lblInsertCard);
            this.panelTitle.Location = new System.Drawing.Point(446, 123);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(259, 404);
            this.panelTitle.TabIndex = 2;
            this.panelTitle.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTitle_Paint);
            this.panelTitle.Resize += new System.EventHandler(this.panelTitle_Resize);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::PisKiosk.Properties.Resources.StudentCardInsert;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(259, 404);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // btnPrinterStatus
            // 
            this.btnPrinterStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrinterStatus.Location = new System.Drawing.Point(810, 151);
            this.btnPrinterStatus.Name = "btnPrinterStatus";
            this.btnPrinterStatus.Size = new System.Drawing.Size(275, 124);
            this.btnPrinterStatus.TabIndex = 3;
            this.btnPrinterStatus.Text = "Check printer status";
            this.btnPrinterStatus.UseVisualStyleBackColor = true;
            this.btnPrinterStatus.Visible = false;
            this.btnPrinterStatus.Click += new System.EventHandler(this.btnPrinterStatus_Click);
            // 
            // btnPrintReceipt
            // 
            this.btnPrintReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrintReceipt.Location = new System.Drawing.Point(810, 291);
            this.btnPrintReceipt.Name = "btnPrintReceipt";
            this.btnPrintReceipt.Size = new System.Drawing.Size(275, 124);
            this.btnPrintReceipt.TabIndex = 3;
            this.btnPrintReceipt.Text = "Print receipt";
            this.btnPrintReceipt.UseVisualStyleBackColor = true;
            this.btnPrintReceipt.Visible = false;
            this.btnPrintReceipt.Click += new System.EventHandler(this.btnPrintReceipt_Click);
            // 
            // WaitingForCardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1150, 650);
            this.Controls.Add(this.btnPrintReceipt);
            this.Controls.Add(this.btnPrinterStatus);
            this.Controls.Add(this.panelTitle);
            this.Controls.Add(this.button1);
            this.Name = "WaitingForCardForm";
            this.Text = "Title - WaitingForCardForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.VisibleChanged += new System.EventHandler(this.WaitingForCardForm_VisibleChanged);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.panelTitle, 0);
            this.Controls.SetChildIndex(this.btnPrinterStatus, 0);
            this.Controls.SetChildIndex(this.btnPrintReceipt, 0);
            this.panelTitle.ResumeLayout(false);
            this.panelTitle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblInsertCard;
        protected System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timerLabelMove;
        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.PictureBox pictureBox1;
        protected System.Windows.Forms.Button btnPrinterStatus;
        protected System.Windows.Forms.Button btnPrintReceipt;
    }
}
