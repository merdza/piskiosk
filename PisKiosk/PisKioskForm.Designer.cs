﻿namespace PisKiosk
{
    partial class PisKioskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelMain = new System.Windows.Forms.Panel();
            this.listViewLanguages = new System.Windows.Forms.ListView();
            this.timerSyncUsers = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorkerCheckDBConnection = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelMain.Location = new System.Drawing.Point(17, 71);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1150, 673);
            this.panelMain.TabIndex = 3;
            // 
            // listViewLanguages
            // 
            this.listViewLanguages.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.listViewLanguages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewLanguages.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listViewLanguages.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Bold);
            this.listViewLanguages.Location = new System.Drawing.Point(17, 5);
            this.listViewLanguages.Name = "listViewLanguages";
            this.listViewLanguages.Size = new System.Drawing.Size(1150, 60);
            this.listViewLanguages.TabIndex = 6;
            this.listViewLanguages.UseCompatibleStateImageBehavior = false;
            this.listViewLanguages.View = System.Windows.Forms.View.List;
            this.listViewLanguages.SelectedIndexChanged += new System.EventHandler(this.listViewLanguages_SelectedIndexChanged);
            // 
            // timerSyncUsers
            // 
            this.timerSyncUsers.Tick += new System.EventHandler(this.timerSyncUsers_Tick);
            // 
            // backgroundWorkerCheckDBConnection
            // 
            this.backgroundWorkerCheckDBConnection.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerCheckDBConnection_DoWork);
            this.backgroundWorkerCheckDBConnection.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerCheckDBConnection_RunWorkerCompleted);
            // 
            // PisKioskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 750);
            this.Controls.Add(this.listViewLanguages);
            this.Controls.Add(this.panelMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PisKioskForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PisKioskForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PisKioskForm_FormClosed);
            this.Load += new System.EventHandler(this.PisKioskForm_Load);
            this.VisibleChanged += new System.EventHandler(this.PisKioskForm_VisibleChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.ListView listViewLanguages;
        protected System.Windows.Forms.Timer timerSyncUsers;
        protected System.ComponentModel.BackgroundWorker backgroundWorkerCheckDBConnection;
    }
}

