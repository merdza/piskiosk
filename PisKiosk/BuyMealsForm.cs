﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class BuyMealsForm : PisKiosk.MainForm
    {
        public BuyMealsForm()
        {
            InitializeComponent();

            LangsVisible = false;
            ReactOnCardRemoved = true;
        }

        //private static BuyMealsForm instance = null;
        //public static BuyMealsForm getInstance()
        //{
        //    if (instance == null)
        //    {
        //        instance = new BuyMealsForm();
        //    }
        //    return instance;
        //}

        public BuyMealsData buyMealsData { get; set; } //podaci o odabranim obrocima na formi
        public bool BuyingMealsByDecades { get; set; } // da li se obroci danas kupuju dekadno ili na komad
        public bool MayBuyForNextMonth { get; set; } // da li obroci mogu da se kupuju i za sledeci mesec

        private void button1_Click(object sender, EventArgs e)
        {
            //Program.pkf.showFormInPanel(CashInsertForm.getInstance(this));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Program.pkf.showFormInPanel(UserDataForm.getInstance());
        }
    }
}
