﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;

namespace PisKiosk
{
        public abstract class Receipt
        {
            public string receiptTitle;
            public string receiptNumber;
            public string cardNumber;
            public double totalPrice;
            public string timeStamp;
            public List<ReceiptItem> items;


            public Bitmap logo;

            public Receipt(string rt, string rn, string cn, double tp, string ts, List<ReceiptItem> itms)
            {
                receiptTitle = rt;
                receiptNumber = rn;
                cardNumber = cn;
                totalPrice = tp;
                timeStamp = ts;
                items = itms;
            }

            public Receipt()
            {
            }

            public void printReceipt()
            {
                try
                {
                    if (Program.KioskApplication.RestartSpooler)
                    {
                        PrintSupport.deletePrintingJobs();
                        PrintSupport.restartSpooler();
                        Program.KioskApplication.log.Error("Old printing jobs deleted and print spooler restarted. ");
                    }
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error(ex.Message, ex);
                }

                PrintDocument pd = new PrintDocument();
                pd.PrintPage += new PrintPageEventHandler(preparePrinting);
                pd.Print();
            }
            
            protected virtual void preparePrinting(object sender, PrintPageEventArgs e)
            {
                using (Font font1 = new Font("Segoe UI Light", 12, FontStyle.Bold, GraphicsUnit.Point))
                {
                    //Rectangle allOverRectangle = new Rectangle(10, 10, 280, 325);

                    //StringFormat stringFormat = new StringFormat();
                    //stringFormat.Alignment = StringAlignment.Center;
                    //stringFormat.LineAlignment = StringAlignment.Center;

                    //e.Graphics.DrawImage(logo, 115, 10);
                    //Rectangle rectTitleArab = new Rectangle(10, 100, 280, 30);
                    //Rectangle rectTitleEng = new Rectangle(10, 120, 280, 30);
                    //e.Graphics.DrawString(@"جامعة الطائف", new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectTitleArab, stringFormat);
                    //e.Graphics.DrawString(@"Taif University", new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectTitleEng, stringFormat);
                    

                    //Rectangle rectServiceArab = new Rectangle(10, 150, 280, 50);
                    //Rectangle rectServiceEng = new Rectangle(10, 200, 280, 50);
                    //if (serviceChosen == 1)
                    //{
                    //    e.Graphics.DrawString(@"فطور", new Font(FontFamily.GenericSansSerif, 30, FontStyle.Bold), Brushes.Black, rectServiceArab, stringFormat);
                    //    e.Graphics.DrawString(@"BREAKFAST", new Font(FontFamily.GenericSansSerif, 30, FontStyle.Bold), Brushes.Black, rectServiceEng, stringFormat);
                    //    //e.Graphics.DrawImage(CantinaCoupons.Properties.Resources.receiptBreakfast, 20, currentVertCoordinate += 100);
                    //}
                    //else if (serviceChosen == 2)
                    //{
                    //    e.Graphics.DrawString(@"غداء", new Font(FontFamily.GenericSansSerif, 30, FontStyle.Bold), Brushes.Black, rectServiceArab, stringFormat);
                    //    e.Graphics.DrawString(@"LUNCH", new Font(FontFamily.GenericSansSerif, 30, FontStyle.Bold), Brushes.Black, rectServiceEng, stringFormat);
                    //    //e.Graphics.DrawImage(CantinaCoupons.Properties.Resources.receiptLunch, 20, currentVertCoordinate += 100);
                    //}

                    ////price
                    //string currencyBeforeAmount = XmlSettings.XmlSettings.getSetting("xmlSettings.xml", "currencyBeforeAmount");
                    //string currency = XmlSettings.XmlSettings.getSetting("xmlSettings.xml", "currency");
                    //Rectangle rectPrice = new Rectangle(10, 250, 280, 25);
                    //if (currencyBeforeAmount.Equals("1"))
                    //{
                    //    e.Graphics.DrawString(currency + " " + totalPrice.ToString(), new Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold), Brushes.Black, rectPrice, stringFormat);
                    //}
                    //else
                    //{
                    //    e.Graphics.DrawString(totalPrice.ToString() + " " + currency, new Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold), Brushes.Black, rectPrice, stringFormat);
                    //}

                    //Rectangle rectTimeStamp = new Rectangle(10, 275, 280, 25);
                    //e.Graphics.DrawString(timeStamp, new Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold), Brushes.Black, rectTimeStamp, stringFormat);
                    //Rectangle rectCardNumber = new Rectangle(10, 300, 280, 25);
                    //e.Graphics.DrawString(receiptNumber, new Font(FontFamily.GenericSansSerif, 14, FontStyle.Bold), Brushes.Black, rectCardNumber, stringFormat);

                    //e.Graphics.DrawRectangle(Pens.Black, allOverRectangle);

                }
            }
           

            private static string makeStringDate(DateTime ts)
            {
                string timestamp = "";

                if (ts.Day < 10)
                {
                    timestamp += "0";
                }
                timestamp += ts.Day.ToString() + ".";

                if (ts.Month < 10)
                {
                    timestamp += "0";
                }
                timestamp += ts.Month.ToString() + ".";

                timestamp += ts.Year.ToString() + ". u ";


                if (ts.Hour < 10)
                {
                    timestamp += "0";
                }
                timestamp += ts.Hour.ToString() + ":";

                if (ts.Minute < 10)
                {
                    timestamp += "0";
                }
                timestamp += ts.Minute.ToString() + ":";

                if (ts.Second < 10)
                {
                    timestamp += "0";
                }
                timestamp += ts.Second.ToString();

                return timestamp;
            }
        }
}
