﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class MessageChoice : PisKiosk.Message
    {
        public MessageChoice() : base()
        {
            InitializeComponent();
        }

        private static MessageChoice instance = null;

        public static MessageChoice getInstance(string msg, MessageType mTp)
        {
            if (instance == null)
            {
                instance = new MessageChoice();
            }
            instance.Mess = msg;
            instance.MessType = mTp;
            instance.setMessage();
            return instance;
        }

        private void MessageChoice_Load(object sender, EventArgs e)
        {
            this.TransparencyKey = Color.Gainsboro;
            panelMessage.BackColor = Program.KioskApplication.TextForeColor2;
            lblMessage.ForeColor = Program.KioskApplication.TextForeColor;

            btnClose.BackColor = Program.KioskApplication.TextForeColor;
            btnClose.ForeColor = Program.KioskApplication.TextForeColor2;

            btnYes.BackColor = Program.KioskApplication.TextForeColor;
            btnYes.ForeColor = Program.KioskApplication.TextForeColor2;
            btnYes.Text = guiLang.Yes;

            btnNo.BackColor = Program.KioskApplication.TextForeColor;
            btnNo.ForeColor = Program.KioskApplication.TextForeColor2;
            btnNo.Text = guiLang.No;

            lblMessageTitle.BackColor = panelMessage.BackColor;
            lblMessageTitle.ForeColor = Program.KioskApplication.TextForeColor;

            lblSecondsTillClose.Text = "";
            lblSecondsTillClose.BackColor = panelMessage.BackColor;
            lblSecondsTillClose.ForeColor = lblMessage.ForeColor;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            btnNo_Click(sender, e);
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.MessageChoiceResult = true;

            hideMessage();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.MessageChoiceResult = false;

            hideMessage();
        }

        private void MessageChoice_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                SecondsTillClose = 10;
                TurnOffTimer.Start();
            }
            else
            {
                hideMessage();
                TurnOffTimer.Stop();
            }
        }
    }
}
