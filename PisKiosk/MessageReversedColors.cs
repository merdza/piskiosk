﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class MessageReversedColors : Message
    {
        public MessageReversedColors() : base()
        {
            InitializeComponent();
        }

        private static MessageReversedColors instance = null;

        public static MessageReversedColors getInstance(string msg, MessageType mTp)
        {
            if (instance == null)
            {
                instance = new MessageReversedColors();
            }
            instance.Mess = msg;
            instance.MessType = mTp;
            //instance.setMessage();
            return instance;
        }

        private void MessageReversedColors_Load(object sender, EventArgs e)
        {
            this.TransparencyKey = Color.Gainsboro;
            panelMessage.BackColor = Program.KioskApplication.TextForeColor;
            lblMessage.ForeColor = Program.KioskApplication.BackgroundColor;

            btnClose.BackColor = Program.KioskApplication.BackgroundColor;
            btnClose.ForeColor = Program.KioskApplication.TextForeColor;

            lblMessageTitle.BackColor = panelMessage.BackColor;
            lblMessageTitle.ForeColor = Program.KioskApplication.BackgroundColor;

            lblSecondsTillClose.Text = "";
            lblSecondsTillClose.BackColor = panelMessage.BackColor;
            lblSecondsTillClose.ForeColor = lblMessage.ForeColor;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            hideMessage();
        }

        private void MessageReversedColors_Deactivate(object sender, EventArgs e)
        {
            hideMessage();
        }

        private void MessageReversedColors_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                instance.setMessage();
                SecondsTillClose = 10;
                TurnOffTimer.Start();
            }
            else
            {
                hideMessage();
                TurnOffTimer.Stop();
            }
        }
    }
}
