﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class Login : Form
    {
        private Login()
        {
            InitializeComponent();
            // u settingsu ce se podesavati da li forma treba da bude Always On Top (1|0). To moze da bude problematicno jer ako se zaglavi, nece ni jedan drugi prozor moci da se otvori i jedino restart pomaze 
            //try
            //{
            //    if (Convert.ToInt32(XmlSettings.XmlSettings.getSetting(@"settings.xml", "loginFormAlwaysOnTop")) == 1)
            //    {
            //        this.TopMost = true;
            //    }
            //}
            //catch (Exception)
            //{}

            setOperaterInitiator(0);
        }

        private bool communicationInitialized = false;
        private bool alreadyLoggedIn = false; 
        public static Login instance = null;

        private Operator operaterInitiator;  //operater koji je uzrok otvaranja ove forme nakon sto se prethodno vec neko ulogovao

        public void setOperaterInitiator(int operaterID, string serialNumber = "")
        {
            operaterInitiator = new Operator(operaterID, serialNumber);
        }

        public static Login getInstance()
        {
            if (instance == null)
            {
                instance = new Login();
            }
            return instance;
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.BackColor = Program.KioskApplication.BackgroundColor;
            button1.BackColor = Program.KioskApplication.TextForeColor2;
            button1.ForeColor = Program.KioskApplication.BackgroundColor;
            button2.BackColor = Program.KioskApplication.TextForeColor2;
            button2.ForeColor = Program.KioskApplication.BackgroundColor;
            btnWithdrawMoney.BackColor = Program.KioskApplication.TextForeColor2;
            btnWithdrawMoney.ForeColor = Program.KioskApplication.BackgroundColor;
            lblVersion.BackColor = this.BackColor;
            lblVersion.ForeColor = Program.KioskApplication.TextForeColor;

            //            LogFile.writeLog("Startuje se program. Welcome!", "StartProgram");
            //ovaj deo koda radi automatsko logovanje
            //button1_Click(sender, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.log.Debug("Clicked Login button.");
            try
            {
                if (!alreadyLoggedIn)
                {
                    //logovanje se uvek radi sa kontaktnim cipom
                    CardAdapterContact cardLogin = new CardAdapterContact();

                    //int opId = 0;
                    //string opSN = "";

                    if (cardLogin.isLoggedInAdapter() != 1)
                    {
                        cardLogin.loginAdapter("korisnik", "korisnik");  //kartica za pokretanje ove aplikacije ce uvek imati fiksirano username i password
                        //opId = cardLogin.getOperaterIDAdapter();
                        //opSN = cardLogin.getCardSerialAdapter();
                    }
                    communicationInitialized = true;

                    //Program.KioskApplication.setKioskOperater(); //na kiosku nije bitno kojom karticom se ulogovao operater jer ce uvek da se postavi operater kioska
                    //prethodni red je iskomentarisan jer uopste ne mora ovde da se podesi operater kioska. Podesice se u background workeru koji proverava konekciju sa bazom. Pre nego sto se uspostavi konekcija, ionako niko ne moze nista da kupi
                    
                    alreadyLoggedIn = true;

                    Program.KioskApplication.log.Info("Operater se ulogovao karticom.");
                }
                Program.KioskApplication.ShouldLoginToDB = true;  // call db login procedure next time in the background thread
                Program.KioskApplication.loadKioskApplication(PanelFormType.UserApplication);
                Program.KioskApplication.Show();
                
                this.Hide();
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Error logging to the application. " + ex.Message, ex);
                MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.log.Info("Clicked Exit button. ");
            try
            {
                if (communicationInitialized)
                {
                    CardInterface.getInstance().CardInterf.finalizeCommunicationAdapter();
                }
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Error finalizing communication with the card. " + ex.Message, ex);
            }

            try
            {
                Utils.KillProcess("ProcessMonitor");
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Nije uspelo gašenje ProcessMonitor programa. " + ex.Message, ex);
            }

            Program.KioskApplication.log.Info("PisKiosk application is shutting down. ");
            Application.Exit();
        }

        private void enableButtons()
        {
            button1.Enabled = true;  //uvek moze da bude enableovan, pa ce kada se klikne na njega da se regulise sta se desava
            button2.Enabled = false; //izlaz treba da bude omogucen samo ako se operater vec ulogovao

            EnableButtonForKioskOperator(false);

            if (Program.KioskApplication.Operater != null)  //ako je prethodno inicijalizovan operater kioska
            {
                if (Program.KioskApplication.Operater.OperaterId == operaterInitiator.OperaterId)
                {
                    EnableButtonForKioskOperator(true);
                }
                button2.Enabled = true;
            }
            else
            {
                if (Program.KioskApplication.IsDBConnectionOk)
                {
                    if (Program.KioskApplication.UserData.isOperatorForThisKiosk(operaterInitiator))
                    {
                        EnableButtonForKioskOperator(true);
                    }
                }
            }
        }

        /// <summary>
        /// ako je ubacena kartica operatera kioksa, onda treba da se omoguci i dugme za podizanje para i dugme za izlaz i obrnuto
        /// </summary>
        private void EnableButtonForKioskOperator(bool enable)
        {
            btnWithdrawMoney.Enabled = enable;
        }

        private void Login_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                enableButtons();
            }
        }

        private void btnWithdrawMoney_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.log.Info("Clicked Withdraw Money button. ");
            bool cont = false;
            try  //desavala se greska da nece da prikaze poruku kada printer ima problem i poruka je vec prikazana. Zato, ako se to desi, samo nastavi dalje
            {
                try
                {
                    Program.KioskApplication.checkPrinterStatus();
                    cont = true;
                }
                catch (Exception)
                {
                    //racun nije dostupan, pitaj korisnika da li zeli da nastavi
                    MessageChoice.getInstance(guiLang.MessageChoice_NoReceiptAvailable, MessageType.Warning).ShowDialog();
                    if (Program.KioskApplication.MessageChoiceResult)
                    {
                        cont = true;
                    }
                }
            }
            catch (Exception)
            {
                cont = true;
            }

            if (cont)
            {
                this.Hide();
                //WithdrawMoneyForm wmf = Program.KioskApplication.getWithdrawMoneyForm();
                Program.KioskApplication.loadKioskApplication(PanelFormType.OperatorApplication);
                Program.KioskApplication.Show();
                //wmf.Show();
            }
        }

        public void cardInserted()
        {
            //prvo mora da se procita kartica, da se vidi da li je sluzbenicka
            //ako je sluzbenicka odmah se uloguj, pa radi po starom
            button1_Click(null, null);
            //if(isOpereatorsCard())
            //{
            //    enableButtonsOnStartup();
            //}
            
        }

        
    }
}
