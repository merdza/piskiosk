﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk
{
    //u ovoj klasi se cuvaju podaci o odabiru obroka za korisnika koju je uradio na stranici za odabir obroka
    //kao i ostali podaci o mogucim obrocima za kupovinu
    public abstract class BuyMealsData
    {
        public double CardMoneyBalance {get; set;} //kes na kartici pre kupovine
        public double MoneyBalanceAfterBuyingSelected { get; set; } //kes koji bi ostao ako bi korisnik kupio ono sto je odabrao

        public List<Meal> Meals { get; set; } // lista mogucih obroka za kupovinu obroka

        public BuyMealsData()
        {
            Meals = new List<Meal>();
        }

        public void resetBuyMealsData()
        {
            CardMoneyBalance = 0;
            MoneyBalanceAfterBuyingSelected = CardMoneyBalance;

            Meals.Clear();
        }

        public void addMeal(Meal m)
        {
            Meals.Add(m);
        }

        public double getTotalPrice()
        {
            double ret = 0;

            foreach (var meal in Meals)
            {
                ret += meal.Amount * meal.Price;
            }

            return ret;
        }

        public void changeMealAmounts(int month, int year, MealType type, int amountChange)
        {
            Meal m = getMeal(month, year, type);

            m.Amount += amountChange;
            MoneyBalanceAfterBuyingSelected -= amountChange * m.Price;
        }

        public bool hasEnoughMoneyToBuy(int month, int year, MealType type, int amount)
        {
            bool ret = false;
            Meal m = getMeal(month, year, type);
            if ((MoneyBalanceAfterBuyingSelected - amount * m.Price) >= 0)
            {
                ret = true;
            }
            return ret;
        }

        public Meal getMeal(int month, int year, MealType type)
        {
            foreach (Meal meal in Meals)
            {
                if ((meal.Month == month) && (meal.Year == year) && (meal.Type == type))
                {
                    return meal;
                }
            }
            return null;
        }

        public abstract int getAllowedMealsForResident(int month);

        public List<Meal> getMealsForMonth(int month)
        {
            List<Meal> mealsForMonth = new List<Meal>();

            foreach (var meal in Meals)
            {
                if (meal.Month == month)
                {
                    mealsForMonth.Add(meal);
                }
            }

            return mealsForMonth;
        }

    }   
}
