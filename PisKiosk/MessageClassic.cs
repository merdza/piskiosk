﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk
{
    public partial class MessageClassic : Message
    {
        private MessageClassic() : base()
        {
            InitializeComponent();
        }

        private static MessageClassic instance = null;

        public static MessageClassic getInstance(string msg, MessageType mTp)
        {
            if (instance == null)
            {
                instance = new MessageClassic();
            }
            instance.Mess = msg;
            instance.MessType = mTp;
            instance.setMessage();
            return instance;
        }

        private void MessageClassic_Load(object sender, EventArgs e)
        {
            this.TransparencyKey = Color.Gainsboro;
            panelMessage.BackColor = Program.KioskApplication.TextForeColor2;
            lblMessage.ForeColor = Program.KioskApplication.BackgroundColor;

            btnClose.BackColor = Program.KioskApplication.BackgroundColor;
            btnClose.ForeColor = Program.KioskApplication.TextForeColor2;

            lblMessageTitle.BackColor = panelMessage.BackColor;
            lblMessageTitle.ForeColor = Program.KioskApplication.BackgroundColor;

            lblSecondsTillClose.Text = "";
            lblSecondsTillClose.BackColor = panelMessage.BackColor;
            lblSecondsTillClose.ForeColor = lblMessage.ForeColor;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            hideMessage();
        }

        private void MessageClassic_Deactivate(object sender, EventArgs e)
        {
            hideMessage();
        }

        private void MessageClassic_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                SecondsTillClose = 10;
                TurnOffTimer.Start();
            }
            else
            {
                hideMessage();
                TurnOffTimer.Stop();
            }
        }

        
    }
}
