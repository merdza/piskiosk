﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.ScNSKiosk
{
    public partial class PisKioskFormScNS : PisKiosk.PisKioskForm
    {
        private static PisKioskFormScNS instance = null;
        

        public static PisKioskFormScNS getInstance()
        {
            if (instance == null)
            {
                instance = new PisKioskFormScNS();
            }
            return instance;
        }

        private PisKioskFormScNS():base()
        {
            InitializeComponent();

            UserData = new UserDataScNS();
        }

        private void PisKioskFormScNS_Load(object sender, EventArgs e)
        {
            //sta li sam ja sada hteo ovde da radim?
            //aha, da startujem timer

            //inicijalizuj background worker za proveru DB konekcije
            timerCallBcgWorkerToCheckDBConnection = new System.Windows.Forms.Timer();
            timerCallBcgWorkerToCheckDBConnection.Tick += new EventHandler(timerCallBcgWorkerToCheckDBConnection_Tick);
            timerCallBcgWorkerToCheckDBConnection.Interval = 60000;
            timerCallBcgWorkerToCheckDBConnection.Start();
            timerCallBcgWorkerToCheckDBConnection_Tick(this, null); //ne cekaj minut, odmah pozovi prvi put

            // inicijalizuj tajmer za prikupljanje aktivnih kartica i staruj ga

            //ideja je sledeca...
            //svakog minuta proveri da li su promenljivi podaci inicijalizovani
            //ako jesu, cekaj da prodje AuxiliaryDataInterval da ih sinhronizujes ponovo
            //a ako nisu, sinhronizuj ih odmah
            timerSyncUsers.Interval = 60000;
            try
            {
                string interval = XmlSettings.XmlSettings.getSetting(SettingsFile, "refreshActiveCardsInterval");
                AuxiliaryDataInterval = Convert.ToInt32(interval) * 60000;  //u fajlu je vrednost u minutima
            }
            catch (Exception)
            {
                //ako ne uspe citanje intervala iz fajla, postavi ga na pola sata
                AuxiliaryDataInterval = 1800000;
            }
            timerSyncUsers.Start();
            timerSyncUsers_Tick(this, null);  //ne cekaj minut, odmah pozovi prvi put
            //------------------------------------------------

            ActivePicturesInitialized = false;

            //getAuxiliaryData(); ovo ce da se uradi kada timer okine prvi put
        }

        public override void setKioskOperater()
        {
            if (Operater == null)  //ako jos nije postavljen operater
            {
                try
                {
                    if (IsDBConnectionOk)
                    {
                        int idOperater = DBAdapter.getKioskOperaterID();
                        Operater = new Operator(idOperater);
                        Program.KioskApplication.log.Debug("Postavljen operater kioska: " + idOperater.ToString());
                    }
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Nije moguće dobiti informaciju iz baze o operateru kioska. " + ex.Message, ex);
                }
            }
        }

        public override void LoginToDatabase()
        {
            DBAdapter.dbLogin(Operater.OperaterId);
        }

        public void setKioskStatus()
        {
            try
            {
                //if the kiosk operater wasn't set previously, then kiosk has to be in INFO state (read only)
                if (Operater == null)
                {
                    Program.KioskApplication.KioskStatus = KioskStatuses.Info;
                    Program.KioskApplication.log.Debug("Kiosk status set to Info because the operator for kiosk was not set.");
                }
                //if the connection to the DB could not be established previously in the background, do not try to check it again. Just set kiosk to status Info
                if (IsDBConnectionOk)
                {
                    Program.KioskApplication.KioskStatus = DBAdapter.getKioskStatus();
                }
                else
                {
                    Program.KioskApplication.KioskStatus = KioskStatuses.Info;
                    Program.KioskApplication.log.Debug("Kiosk status set to Info because the connection to the database could not be established previously in the background.");
                }
            }
            catch (Exception ex)
            {
                Program.KioskApplication.KioskStatus = KioskStatuses.Info;
                Program.KioskApplication.log.Error("Getting kiosk status failed: " + ex.Message, ex);
            }
        }

        public override WithdrawMoneyForm getWithdrawMoneyForm()
        {
            return WithdrawMoneyFormScNS.getInstance();
        }

        public override void loadKioskApplication(PanelFormType pft)
        {
            MainForm form = null;
            switch (pft)
            {
                case PanelFormType.UserApplication:
                    form = WaitingForCardFormScNS.getInstance();
                    break;
                case PanelFormType.OperatorApplication:
                    form = WithdrawMoneyFormScNS.getInstance();
                    break;
                default:
                    break;
            }   
            
            showFormInPanel(form);
        }

        public void getAuxiliaryData()
        {
            //periodicno se uzimaju podaci iz baze o:
            // - aktivnim karticama korisnika
            // - aktuelnim slikama korisnika
            //a mogu u istom cugu i da pokupim slike iz foldera jer je moguće da su snimljene nove

            

            try
            {
                initializeActiveCards();
                getActivePictures();
                makePicturesHashSet();
            }
            catch (Exception ex)
            {
                log.Info("Error in synchronizing data. " + ex.Message);
            }

            // ----------------------------------------------------------
            this.Invalidate();
        }

        protected override void timerSyncUsers_Tick(object sender, EventArgs e)
        {
            MinutesElapsedSinceLastSynch++; //tajmer se poziva svakog minuta

            lock (DataSyncLocker)
            {
                //ako su podaci prethodno sinhronizovani, onda cekaj da istekne period ponovne sinhronizacije AuxiliaryDataInterval
                //ako nisu sinhronizovani, odmah pokusaj ponovo
                if ((!DataSynchronized) || (MinutesElapsedSinceLastSynch * 60000 >= AuxiliaryDataInterval))
                {
                    try
                    {
                        getAuxiliaryData();

                        if ((CardsHashInitialized > 0) && (ActivePicturesInitialized) && (PicturesFromDirectoryTaken))//todo proveri sva tri parametra uspesnosti
                        {
                            DataSynchronized = true;
                            MinutesElapsedSinceLastSynch = 0;
                        }
                        else
                        {
                            DataSynchronized = false;
                        }
                    }
                    catch (Exception)
                    {
                        DataSynchronized = false;
                    }
                }
            }
        }
        
        public void initializeActiveCards()
        {
            try
            {
                //prvo prekini da proveravas da li su kartice koje se ubace tokom inicijalizacije hes tabele u tabeli
                CardsHashInitialized = 0;

                //hocu i da merim, da vidim koliko mu treba da ucita sve aktivne kartice
                System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
                stopWatch.Start();

                HashSet<string> tempCardsTable = new HashSet<string>();

                CardsHashInitialized = DBAdapter.fillCardsHashTable(tempCardsTable);
                if (CardsHashInitialized > 0)
                {
                    CardsTable = tempCardsTable;
                    ActiveCardsTaken++;
                }
                else
                {
                    throw new Exception("Nije uspela inicijalizacija aktivnih kartica iz baze. Nema ni jedne aktivne kartice?!?!?!");
                }
                

                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;

                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                log.Info("Active cards refreshed for the " + ActiveCardsTaken.ToString() + ". time. It lasted " + elapsedTime + " There are " + CardsHashInitialized.ToString() + " active cards. ");
                //CardsHashInitialized = 1;
            }
            catch (Exception ex)
            {
                throw new Exception("Initialization of active cards failed. " + ex.Message);
            }
        }

        private void makePicturesHashSet()
        {
            try
            {
                int numberOfPictures = 0;
                HashSet<string> picturesTemp = new HashSet<string>();
                //pictures = new HashSet<string>();

                string photoDirectory = XmlSettings.XmlSettings.getSetting(SettingsFile, "photoPath");

                Uri uri;
                string filename;

                if (System.IO.Directory.Exists(photoDirectory))
                {
                    try
                    {
                        string[] files = System.IO.Directory.GetFiles(photoDirectory);
                        numberOfPictures = files.Length;
                        foreach (string s in files)
                        {
                            uri = new Uri(s);
                            filename = System.IO.Path.GetFileName(uri.LocalPath);
                            picturesTemp.Add(filename);
                        }
                        pictures = picturesTemp;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Creating HashSet with photos from direcory failed. " + ex.Message);
                    }
                }
                else
                {
                    throw new Exception("Directory with photos does not exist!");
                }
                PicturesFromDirectoryTaken = true;
                log.Info("User pictures from directory taken successfully. There are " + numberOfPictures.ToString() + " pictures in the directory.");
            }
            catch (Exception ex)
            {
                PicturesFromDirectoryTaken = false;
                throw ex;
            }
        }
        private void getActivePictures()
        {
            Hashtable tempActivePictures = new Hashtable();  //koristim privremenu hes tabelu koju punim, pa kada se napuni, onda dodelim pravoj hes tabeli koja se dalje koristi
            int numberOfPictures = 0;
            try
            {
                numberOfPictures = DBAdapter.getActivePictures(tempActivePictures);
                activePictures = tempActivePictures;
                ActivePicturesInitialized = true;
                log.Info("Active user pictures from database taken successfully. There are " + numberOfPictures.ToString() + " pictures of active users in the database.");
            }
            catch (Exception ex)
            {
                ActivePicturesInitialized = false;
                throw new Exception("Error in taking active user pictures from database. " + ex.Message);
            }
        }

        public string getActivePicture(string cardNumber)
        {
            string activePicture = "";

            try
            {
                activePicture = activePictures[cardNumber].ToString();
            }
            catch (Exception)
            { }
            return activePicture;
        }

        public bool isCardActive(string cardNumber)
        {
            bool ret = false;

            //todo a sta da radim kada se ne inicijalizuju aktivne kartice?

            if (CardsHashInitialized > 0)
            {
                if (CardsTable.Contains(cardNumber))
                {
                    ret = true;
                }
            }
            else if((!IsDBConnectionOk) && (Program.KioskApplication.KioskStatus == KioskStatuses.Info))
            {
                //nema konekcije i kiosk samo prikazuje podatke, tako da mozemo da pustimo karticu da se ocita
                ret = true;
            }

            return ret;
        }

      

    }


}
