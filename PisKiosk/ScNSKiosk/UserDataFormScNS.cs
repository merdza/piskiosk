﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MealsChecker;

namespace PisKiosk.ScNSKiosk
{
    public partial class UserDataFormScNS : PisKiosk.UserDataForm
    {
        public UserDataFormScNS():base()
        {
            InitializeComponent();

            //userData = new UserDataScNS();
            ud = (UserDataScNS)Program.KioskApplication.UserData;  //ne znam kako drugacije da downcastujem nego da u svakoj izvedenoj klasi uvedem 
            ad = AbonentData.getInstance();
        }

       // protected static Hashtable activePictures;
       // public bool activePicturesInitialized = false;
        protected AbonentData ad;
        protected MealsChecker.MealsChecker mc = null;
        public UserDataScNS ud;
        public bool CardJustInserted {get; set;}  //oznacava da je kartica upravo ubacena i da treba procitati podatke sa kartice, pozvati MealsChecker da sredi bonove i ponovo upisati podatke na karticu. Cim se to uradi, sve dok se ne izvadi kartica ne treba vise da se sredjuju obroci i da se upisuju nazad na karticu

        private static UserDataFormScNS instance = null;
        public static UserDataFormScNS getInstance()
        {
            if (instance == null)
            {
                instance = new UserDataFormScNS();
            }
            return instance;
        }
      

        public override void readCardData() 
        {
            //await Task.Run(() => ud.readCardData());
            ud.readCardData();
            if (CardJustInserted)
            {
                Program.KioskApplication.log.Info("Card inserted. " + ud.ToString());
                if (ud.CardType == CardType.Official)
                {
                    Login.getInstance().setOperaterInitiator(ud.OperaterID, ud.CardSerialNumber);
                    Login.getInstance().Show();
                    Program.KioskApplication.Hide();
                }
                else
                {
                    //kada je tek ubacena kartica, pa treba srediti stanje obroka na kartici
                    ud.Valid = PisKioskFormScNS.getInstance().isCardActive(ud.CardNumber);
                    if (!ud.Valid)
                    {
                        throw new Exception(guiLang.UserDataFormScNS_Error_CardNotActive);
                    }

                    initializeMealsChecker();
                    writeCardAbonentData();
                    Program.KioskApplication.log.Info("Card state after MealsChecker: " + ud.ToString());
                    CardJustInserted = false;
                }
            }
        }


        private void getMealsPricesFromDB()
        {
            //procitaj cene obroka iz baze za korisnika koji je ubacio karticu

            //UserDataScNS ud = UserDataFormScNS.getInstance().ud;

            string cardNumber = ud.CardNumber;

            double breakfastPrice = 0;
            double lunchPrice = 0;
            double dinnerPrice = 0;
            double breakfastNextPrice = 0;
            double lunchNextPrice = 0;
            double dinnerNextPrice = 0;
            bool isResident = false;
            int abonentCategory = 0;  //ako je ekonomski korisnik, onda nema kupovine na dekade
            //bool isCardActive = false;
            bool allowedBuyingThisMonth = false;
            bool allowedBuyingNextMonth = false;
            bool firstShoppingInMonth = false;

            DBAdapter.getMealsPrices(cardNumber, out breakfastPrice, out lunchPrice, out dinnerPrice, out breakfastNextPrice, out lunchNextPrice, out dinnerNextPrice, out abonentCategory, out allowedBuyingThisMonth, out allowedBuyingNextMonth, out isResident, out firstShoppingInMonth);

            ud.BreakfastPrice = breakfastPrice;
            ud.LunchPrice = lunchPrice;
            ud.DinnerPrice = dinnerPrice;
            ud.BreakfastNextPrice = breakfastNextPrice;
            ud.LunchNextPrice = lunchNextPrice;
            ud.DinnerNextPrice = dinnerNextPrice;
            ud.AbonentCategory = abonentCategory;
            ud.IsResident = isResident;
            ud.AllowedBuyingThisMonth = allowedBuyingThisMonth;
            ud.AllowedBuyingNextMonth = allowedBuyingNextMonth;

            //if (!isCardActive)
            //{
            //    throw new Exception(guiLang.UserDataFormScNS_Error_CardNotActive);
            //}
        }

        private void UserDataFormScNS_Load(object sender, EventArgs e)
        {
            makePicturesHashSet();
        }

        protected void initializeMealsChecker(bool isResident = false)
        {
            //inicijalizuje se MealsChecker
            ad.initializeMealsChecker(ud.MealsArray, ud.EatenMealsArray);

            if (isResident)
            {
                mc = new MealsCheckerNoviSadResidents();
            }
            else
            {
                mc = new MealsCheckerNoviSadCashier();
            }
            mc.calculateMealsInitially();
            ad.getAbonentData(ud.MealsArray, ud.EatenMealsArray);
            ud.setGuiParameters();
        }

        public void changeMealsInMealsChecker(int m, int y, int b, int l, int d)
        {
            mc.calculateMealsOnChange(m, y, b, l, d);
            ad.getAbonentData(ud.MealsArray, ud.EatenMealsArray);
            ud.setGuiParameters();
        }

        private void UserDataFormScNS_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                Thread.CurrentThread.CurrentUICulture = Program.KioskApplication.CurrentCultureInfo;
                Program.KioskApplication.log.Debug("Current Culture: " + Thread.CurrentThread.CurrentUICulture.ToString());

                //kad god se prikaze ova forma, treba da se procitaju podaci sa kartice
                try
                {
                    MessageReversedColors.getInstance((guiLang.WaitingForCardForm_Message_CardInsert).ToUpper(), MessageType.Information).Show();
                    PisKioskFormScNS.getInstance().setKioskStatus();
                    setLabels();
                    //await Task.Run(() => readCardData());
                    readCardData();
                    showUserData();
                    MessageReversedColors.getInstance(string.Empty, MessageType.Information).Hide();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Error showing user data. " + ex.Message, ex);
                    //Program.KioskApplication.cardRemoved();
                    PisKioskFormScNS.getInstance().loadKioskApplication(PanelFormType.UserApplication);
                    MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
                }
            }
            else
            {
                clearUserData();
            }
        }

        public void writeCardAbonentData()
        {
            ud.writeCardAbonentData();
        }

        public override double getCashFromCard()
        {
            double cashOnCard = 0;

            cashOnCard = ud.CashOnCard;

            return cashOnCard;
        }
        //public override double getCashFromCard()
        //{
        //    double cashOnCard = 0;
        //    cashOnCard = WebServiceAdapter.getMoneyBalance(ud.CardNumber);
        //    ud.CashOnCard = cashOnCard;
        //    return cashOnCard;
        //}
        public override void showUserData()
        {
            if (!this.IsHandleCreated || this.IsDisposed)
            {
                return;
            }

            this.Invoke((MethodInvoker)delegate
            {
               lblCardOwner.Text = ud.UserName;
               lblCardNumberLabel.Text = guiLang.UserDataForm_Label_lblCardNumberLabel;
               lblCardNumber.Text = ud.CardNumber;
               lblCashAvailableLabel.Text = guiLang.UserDataForm_Label_lblCashAvailableLabel;
               lblCashAvailable.Text = Utils.getCurrencyString(getCashFromCard(), true);

                //todo
               lblHasMealsBreakfast.Text = ud.Breakfast;
               lblAllowedMealsToBuyBreakfast.Text = ud.BreakfastLeft;
               lblMealsSpentTodayBreakfast.Text = ud.EatenBreakfast;
               lblHasMealsForNextMonthBreakfast.Text = ud.BreakfastNext;

               lblHasMealsLunch.Text = ud.Lunch;
               lblAllowedMealsToBuyLunch.Text = ud.LunchLeft;
               lblMealsSpentTodayLunch.Text = ud.EatenLunch;
               lblHasMealsForNextMonthLunch.Text = ud.LunchNext;

               lblHasMealsDinner.Text = ud.Dinner;
               lblAllowedMealsToBuyDinner.Text = ud.DinnerLeft;
               lblMealsSpentTodayDinner.Text = ud.EatenDinner;
               lblHasMealsForNextMonthDinner.Text = ud.DinnerNext;

               showPhoto(PisKioskFormScNS.getInstance().getActivePicture(ud.CardNumber));

               //this.Refresh();
               //Application.DoEvents();
            });
        }

        //public string getActivePicture(string cardNumber)
        //{
        //    string activePicture = "";

        //    try
        //    {
        //        activePicture = activePictures[cardNumber].ToString();
        //    }
        //    catch (Exception)
        //    { }
        //    return activePicture;
        //}

        private void btnBuyFood_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.log.Info("Clicked Buy Food button.");
            if (!FormBusy)
            {
                try
                {
                    if (!Program.KioskApplication.isBuyingEnabled())
                    {
                        throw new Exception(guiLang.UserData_messageBuyingNotAllowed);
                    }

                    if (Program.KioskApplication.KioskStatus == KioskStatuses.Info)
                    {
                        throw new Exception(guiLang.KioskStatusDoesNotAllow);
                    }

                    bool openForm = true;
                    try
                    {
                        Program.KioskApplication.checkPrinterStatus();
                    }
                    catch (Exception)
                    {
                        //racun nije dostupan, pitaj korisnika da li zeli da nastavi
                        MessageChoice.getInstance(guiLang.MessageChoice_NoReceiptAvailable, MessageType.Warning).ShowDialog();
                        if (!Program.KioskApplication.MessageChoiceResult)
                        {
                            openForm = false;
                        }
                    }

                    if (openForm)
                    {
                        getMealsPricesFromDB();
                        //Samo ako je mestanin, onda cemo ponovo da inicijalizujemo MealsChecker jer ranije, kada se ubacila kartica i prikazali podaci, nismo imali podatak iz baze da li je Beogradjanin. Ako jeste, moramo da promenimo instancu MealsCheckera
                        if (ud.IsResident)
                        {
                            initializeMealsChecker(true);
                        }
                        
                        Program.KioskApplication.showFormInPanel(BuyMealsFormScNS.getInstance(true));
                    }
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("User data form - buy food button click failed " + ex.Message, ex);
                    MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }

        private void btnInsertCash_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.log.Info("Clicked Insert Cash button.");

            if (!FormBusy)
            {
                try
                {
                    if (!Program.KioskApplication.isBuyingEnabled())
                    {
                        throw new Exception(guiLang.UserData_messageBuyingNotAllowed);
                    }

                    if (Program.KioskApplication.KioskStatus != KioskStatuses.BuyingMealsInsertingMoney)
                    {
                        throw new Exception(guiLang.KioskStatusDoesNotAllow);
                    }

                    bool openForm = true;
                    try
                    {
                        Program.KioskApplication.checkPrinterStatus();
                    }
                    catch (Exception)
                    {
                        //racun nije dostupan, pitaj korisnika da li zeli da nastavi
                        MessageChoice.getInstance(guiLang.MessageChoice_NoReceiptAvailable, MessageType.Warning).ShowDialog();
                        if (!Program.KioskApplication.MessageChoiceResult)
                        {
                            openForm = false;
                        }
                    }

                    if (openForm)
                    {
                        Program.KioskApplication.showFormInPanel(CashInsertFormScNS.getInstance(this));
                    }
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("User data - insert cash button click failed " + ex.Message, ex);
                    MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }

        private void btnOtherServices_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.log.Info("Clicked Buy Other Services button.");

            if (!FormBusy)
            {
                try
                {
                    if (!Program.KioskApplication.isBuyingEnabled())
                    {
                        throw new Exception(guiLang.UserData_messageBuyingNotAllowed);
                    }

                    if (Program.KioskApplication.KioskStatus == KioskStatuses.Info)
                    {
                        throw new Exception(guiLang.KioskStatusDoesNotAllow);
                    }

                    bool openForm = true;
                    try
                    {
                        Program.KioskApplication.checkPrinterStatus();
                    }
                    catch (Exception)
                    {
                        //racun nije dostupan, pitaj korisnika da li zeli da nastavi
                        MessageChoice.getInstance(guiLang.MessageChoice_NoReceiptAvailable, MessageType.Warning).ShowDialog();
                        if (!Program.KioskApplication.MessageChoiceResult)
                        {
                            openForm = false;
                        }
                    }

                    if (openForm)
                    {
                        Program.KioskApplication.showFormInPanel(BuyServicesFormScNS.getInstance());
                    }
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("User data - buy other services button click failed " + ex.Message, ex);
                    MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }
    }
}