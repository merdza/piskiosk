﻿namespace PisKiosk.ScNSKiosk
{
    partial class CashInsertFormScNS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxSupportedBankNotes.SuspendLayout();
            this.groupBoxInsertedBankNotes.SuspendLayout();
            this.groupBoxTotal.SuspendLayout();
            this.SuspendLayout();

            // 
            // groupBoxSupportedBankNotes
            // 
            this.groupBoxSupportedBankNotes.Text = "Supported banknotes (SAR)";
            // 
            // groupBoxInsertedBankNotes
            // 
            this.groupBoxInsertedBankNotes.Text = "Inserted (pcs)";
            // 
            // groupBoxTotal
            // 
            this.groupBoxTotal.Text = "Total (SAR)";
            // 
            // btnEndInsertingMoney
            // 
            this.btnEndInsertingMoney.Text = "Inserting money finished";
            this.btnEndInsertingMoney.Click += new System.EventHandler(this.btnEndInsertingMoney_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "Please insert banknotes one by one\r\n(red glowing device below)";
            // 
            // CashInsertFormScNS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1150, 650);
            this.Name = "CashInsertFormScNS";
            this.groupBoxSupportedBankNotes.ResumeLayout(false);
            this.groupBoxInsertedBankNotes.ResumeLayout(false);
            this.groupBoxTotal.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
