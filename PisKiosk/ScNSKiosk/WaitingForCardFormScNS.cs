﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.ScNSKiosk
{
    public partial class WaitingForCardFormScNS : PisKiosk.WaitingForCardForm
    {
        public WaitingForCardFormScNS()
        {
            InitializeComponent();
        }

        private static WaitingForCardFormScNS instance = null;
        public static WaitingForCardFormScNS getInstance()
        {
            if (instance == null)
            {
                instance = new WaitingForCardFormScNS();
            }
            return instance;
        }

        private void WaitingForCardFormScNS_Load(object sender, EventArgs e)
        {
            if (Program.KioskApplication.PrinterTesting)
            {
                btnPrinterStatus.Visible = true;
                btnPrintReceipt.Visible = true;
            }
            else
            {
                btnPrinterStatus.Visible = false;
                btnPrintReceipt.Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.KioskApplication.showFormInPanel(UserDataFormScNS.getInstance());
        }

        public override void cardInserted()
        {
            UserDataFormScNS ud = UserDataFormScNS.getInstance();
            ud.CardJustInserted = true;
            FormBusy = false;
            Program.KioskApplication.showFormInPanel(ud);
        }
    }
}
