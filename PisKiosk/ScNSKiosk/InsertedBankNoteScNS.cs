﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.ScNSKiosk
{
    public class InsertedBankNoteScNS : InsertedBankNote
    {
        public override void insertBankNote(int bankNoteValue)
        {
            
            Value = bankNoteValue;

            //Prvo se upisuje u bazu, zatim na karticu, a ako upis na karticu pukne, ponistava se transakcija u bazi
            int transactionId;  //id transakcije iz baze
            UserDataScNS ud = UserDataFormScNS.getInstance().ud;
            ScNSKiosk.CardAdapterContactScNS cardInterface = (ScNSKiosk.CardAdapterContactScNS)CardInterface.getInstance().CardInterf;

            //upis u bazu
            try
            {
                //prvo se sa kartice cita broj transakcije koji mora da se upise u bazu
                int cardTransaction = cardInterface.getTransactionAmountAdapter() + 1;
                DBAdapter.moneyDeposit(ud.CardSerialNumber, bankNoteValue, ud.MealsArray, ud.CashOnCard, cardTransaction, out transactionId);
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Inserting transaction to database failed. " + ex.Message, ex);
                throw new Exception("There is a problem in buying meals process!");
            }

            //upis na karticu
            try
            {
                cardInterface.incCashAmountAdapter(bankNoteValue);
                ud.CashOnCard += bankNoteValue;
            }
            catch (Exception ex)
            {
                //ako nije uspeo upis na karticu, ponisti transakciju u bazi
                try
                {
                    DBAdapter.undoTransaction(transactionId);
                    throw new Exception("MONEY PROBLEM! More money in the kiosk that it should. Writing to card failed. Database transaction was cancelled. " + ex.Message/*nije uspeo upis na karticu*/);
                }
                catch (Exception ex1)
                {
                    //nije uspelo ponistavanje u bazi
                    throw new Exception("MONEY PROBLEM! Amount of money and report should match, but the money was not written to the card. Writing to card failed. Cancelation of database transaction ALSO FAILED. Card problem: " + ex.Message + " Database cancellation problem: " + ex1.Message);
                }
            }

            
            
                        
            Program.KioskApplication.log.Info(ud.ToString());
            
        }
    }
}
