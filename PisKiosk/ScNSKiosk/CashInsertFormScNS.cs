﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PisKiosk.ScNSKiosk
{
    public partial class CashInsertFormScNS : PisKiosk.CashInsertForm
    {
        public CashInsertFormScNS():base()
        {
            InitializeComponent();

            //lblKioskApp.Text = "ScNS";
        }


        private static CashInsertFormScNS instance = null;

        public static CashInsertFormScNS getInstance(MainForm w)
        {
            if (instance == null)
            {
                instance = new CashInsertFormScNS();
            }
            instance.whoCalledMe = w;
            return instance;
        }

        protected override void setCashBalance()
        {
            lblCardMoneyBalance.Text = guiLang.BuyMealsFormScNS_Label_lblCardMoneyBalance + " " + Utils.getCurrencyString(UserDataFormScNS.getInstance().ud.CashOnCard, true);
        }

        public override void bankNoteCredited(int bankNote)
        {
            try
            {
                Program.KioskApplication.log.Info("Inserted banknote " + bankNote.ToString());
                InsertedBankNote newBankNote = new InsertedBankNoteScNS();

                newBankNote.insertBankNote(bankNote);

                insertedBankNotes.Add(newBankNote);

                emphasize(bankNote, true);

                addNoteToScreen(bankNote);

                //btnEndInsertingMoney.Enabled = true;
                Program.KioskApplication.log.Info("Banknote successfully stored. " + bankNote.ToString());
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error(ex.Message, ex);
                MessageClassic.getInstance(guiLang.CashInsertForm_LostBankNote_Warning, MessageType.Error).Show();
            }
            finally 
            {
                bankNoteProcessed();
            }
        }

        public void printReceipt()
        {
            try
            {
                UserDataScNS ud = UserDataFormScNS.getInstance().ud;

                string receiptTitle = guiLang.Receipt_Title_Money_Inserted;

                string receiptNumber = "";

                DateTime now = DateTime.Now;

                string timeStamp = Utils.addLeadingZeros(now.Day, 2) + @"." + Utils.addLeadingZeros(now.Month, 2) + @"." + now.Year.ToString() + @". " + Utils.addLeadingZeros(now.Hour, 2) + @":" + Utils.addLeadingZeros(now.Minute, 2) + @":" + Utils.addLeadingZeros(now.Second, 2);//KSA date format dd/mm/yyy

                double insertedTotal = 0;
                List<ReceiptItem> receiptItems = new List<ReceiptItem>();

                foreach (var insertedNote in insertedBankNotes)
                {
                    receiptItems.Add(new ReceiptItem(guiLang.Receipt_BankNote + " " + Utils.getCurrencyString(Convert.ToInt32(insertedNote.Value), true), 1, Convert.ToDouble(insertedNote.Value)));
                    insertedTotal += insertedNote.Value;
                }

                Receipt rp = new ReceiptScNS(receiptTitle, receiptNumber, ud.CardNumber, insertedTotal, timeStamp, receiptItems);
                rp.printReceipt();
            }
            catch (Exception ex)
            {
                string err = "Error printing receipt. " + " " + ex.Message;
                Program.KioskApplication.log.Error(err, ex);
                throw new Exception(err);
            }
        }

        private void btnEndInsertingMoney_Click(object sender, EventArgs e)
        {
            if (!FormBusy)
            {
                if (!BankNoteInserted)
                {
                    try
                    {
                        Program.KioskApplication.log.Info("Clicked End Inserting Money button. ");
                        if (insertedBankNotes.Count > 0)
                        {
                            //stampaj racun
                            //prikazi dijalog sa pitanjem da li hoce da stampa
                            if (Program.KioskApplication.PrinterAvailable)
                            {
                                MessageChoice.getInstance(guiLang.MessageChoice_DoYouWantReceipt, MessageType.Question).ShowDialog();
                            }

                            if (Program.KioskApplication.MessageChoiceResult)
                            {
                                //stampaj racun
                                printReceipt(); // ovde nema transactionId-a
                            }
                        }
                        Program.KioskApplication.showFormInPanel(whoCalledMe);
                    }
                    catch (Exception ex)
                    {
                        Program.KioskApplication.log.Error("Buy services - insert cash button click failed " + ex.Message, ex);
                    }
                    finally
                    {
                        FormBusy = false;
                    }
                }
                else
                {
                    Program.KioskApplication.log.Debug("Kliknuto na dugme End Inserting Money button dok se novcanica jos ubacivala, tako da kao da nije kliknuo. ");
                }
            }

        }
    }
}
