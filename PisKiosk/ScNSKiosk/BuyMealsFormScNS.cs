﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PisKiosk.ScNSKiosk
{
    public partial class BuyMealsFormScNS : PisKiosk.BuyMealsForm
    {
        private BuyMealsFormScNS()
        {
            InitializeComponent();

            this.BackColor = Program.KioskApplication.BackgroundColor;

            gridShoppingCart.EnableHeadersVisualStyles = false;

            gridShoppingCart.ColumnHeadersDefaultCellStyle.BackColor = Program.KioskApplication.TextForeColor2;
            gridShoppingCart.ColumnHeadersDefaultCellStyle.ForeColor = Program.KioskApplication.BackgroundColor;

            gridShoppingCart.BackgroundColor = Program.KioskApplication.BackgroundColor;
            gridShoppingCart.GridColor = Program.KioskApplication.TextForeColor;
            gridShoppingCart.DefaultCellStyle.BackColor = Program.KioskApplication.BackgroundColor;
            gridShoppingCart.DefaultCellStyle.ForeColor = Program.KioskApplication.TextForeColor;

            //DataGridViewCellStyle style = gridShoppingCart.ColumnHeadersDefaultCellStyle;
            //style.BackColor = Color.Navy;
            //style.ForeColor = Color.White;
            //style.Font = new Font("Segoe UI Light", 30F, FontStyle.Bold);

            lblCardMoneyBalance.ForeColor = Program.KioskApplication.TextForeColor2;
            lblMoneyBalanceAfterShopping.ForeColor = Program.KioskApplication.TextForeColor2;

            btnBuyFood.BackColor = Program.KioskApplication.TextForeColor2;
            btnBuyFood.ForeColor = Program.KioskApplication.BackgroundColor;

            btnCancelBuyFood.BackColor = Program.KioskApplication.TextForeColor2;
            btnCancelBuyFood.ForeColor = Program.KioskApplication.BackgroundColor;

            btnInsertCash.BackColor = Program.KioskApplication.TextForeColor2;
            btnInsertCash.ForeColor = Program.KioskApplication.BackgroundColor;

            //nema sortiranja shopping carta
            foreach (DataGridViewColumn column in gridShoppingCart.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            
        }

        private static BuyMealsFormScNS instance = null;
        public static BuyMealsFormScNS getInstance(bool newCard = false)
        {
            //new card se odnosi na to da li se ova forma inicijalizuje od pocetka sa novim objektima koji se na njoj koriste
            if (instance == null)
            {
                instance = new BuyMealsFormScNS();
                instance.buyMealsData = new BuyMealsDataScNS();
            }

            if (newCard)
            {
                instance.buyMealsData.resetBuyMealsData();
                instance.setDateBoundParameters();

                instance.initBuyMealsData();
            }

            return instance;
        }

        private void initBuyMealsData()
        {
            //procitaj cene obroka iz baze za korisnika koji je ubacio karticu

            UserDataScNS ud = UserDataFormScNS.getInstance().ud;

            DateTime now = DateTime.Now;
            buyMealsData.addMeal(new MealScNS(now.Month, now.Year, MealType.Breakfast, ud.BreakfastPrice));
            buyMealsData.addMeal(new MealScNS(now.Month, now.Year, MealType.Lunch, ud.LunchPrice));
            buyMealsData.addMeal(new MealScNS(now.Month, now.Year, MealType.Dinner, ud.DinnerPrice));
            if (MayBuyForNextMonth)
            {
                now = now.AddMonths(1);
                buyMealsData.addMeal(new MealScNS(now.Month, now.Year, MealType.Breakfast, ud.BreakfastNextPrice));
                buyMealsData.addMeal(new MealScNS(now.Month, now.Year, MealType.Lunch, ud.LunchNextPrice));
                buyMealsData.addMeal(new MealScNS(now.Month, now.Year, MealType.Dinner, ud.DinnerNextPrice));
            }
        }

        private void setCashValues()
        {
            //postavi koliko para ima u novcaniku
            buyMealsData.CardMoneyBalance = UserDataFormScNS.getInstance().ud.CashOnCard;
            buyMealsData.MoneyBalanceAfterBuyingSelected = buyMealsData.CardMoneyBalance - buyMealsData.getTotalPrice();
        }

        private void setDateBoundParameters()
        {
            //da li uopste ima pravo da kupuje za tekuci mesec????
            if (!UserDataFormScNS.getInstance().ud.AllowedBuyingThisMonth)
            {
                throw new Exception(guiLang.BuyMealsFormScNS_Error_NotAllowedBuyingThisMonth);
            }

            //kupovina na dekade
            BuyingMealsByDecades = false;

            if (UserDataFormScNS.getInstance().ud.AbonentCategory == 1)
            {
                int stopBuyingOnDecades = 0; //kog datuma prestaje dekadna kupovina i pocinje kupovina bonova na komad
                try
                {
                    stopBuyingOnDecades = Convert.ToInt32(XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "stopBuyingOnDecadesDay"));
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Error reading settings file, stopBuyingOnDecadesDay tag. " + ex.Message, ex);
                    Application.Exit();
                }

                if (DateTime.Now.Day < stopBuyingOnDecades)
                {
                    BuyingMealsByDecades = true;
                    Program.KioskApplication.log.Debug("Kupuje se na dekade sve do " + stopBuyingOnDecades + ". u mesecu.");
                }
                else
                {
                    Program.KioskApplication.log.Debug("Kupuje se na komad jer je trenutno ili je prošao " + stopBuyingOnDecades + ". u mesecu.");
                }
            }
            else
            {
                Program.KioskApplication.log.Debug("Kupuje se na komad jer student nije na budžetu");
            }

            //da li sme da kupuje i za sledeci mesec?
            MayBuyForNextMonth = true;
            int buyNextMonthDay = 0; 
            try
            {
                buyNextMonthDay = Convert.ToInt32(XmlSettings.XmlSettings.getSetting(Program.KioskApplication.SettingsFile, "buyNextMonthDay"));
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Error reading settings file, buyNextMonthDay tag. " + ex.Message, ex);
                Application.Exit();
            }

            //ovde sada treba da se proveri da li student sme da kupuje za naredni mesec - ono sto je procitano iz procedure getMealsPrices
            if (!UserDataFormScNS.getInstance().ud.AllowedBuyingNextMonth)
            {
                MayBuyForNextMonth = false;
            }

            if (DateTime.Now.Day < buyNextMonthDay)
            {
                MayBuyForNextMonth = false;
            }
        }

        private void takeCareOfMonthTab(int m, int y)
        {
            TabPage page = new TabPage(@"   " + Utils.getMonthString(m).ToUpper() + @"   ");

            //tabControlMonth.TabPages.Add(page);
            BuyMealsUC bmuc = new BuyMealsUCScNS(m, y);
            page.Controls.Add(bmuc);
            //bmuc.Anchor = (AnchorStyles.Top | AnchorStyles.Left);
            bmuc.Dock = DockStyle.Fill;
            tabControlMonth.TabPages.Add(page);
        }

        private void addMonthTabs()
        {
            tabControlMonth.TabPages.Clear();

            DateTime now = DateTime.Now;
            //mogu da se kupe obroci samo za tekuci i za naredni mesec
            takeCareOfMonthTab(now.Month, now.Year);
            if (MayBuyForNextMonth)
            {
                DateTime nextMonth = now.AddMonths(1);
                takeCareOfMonthTab(nextMonth.Month, nextMonth.Year);
            }
        }

        public void setCashAmounts()
        {
            lblCardMoneyBalance.Text = guiLang.BuyMealsFormScNS_Label_lblCardMoneyBalance + " " + Utils.getCurrencyString(buyMealsData.CardMoneyBalance, true);
            lblMoneyBalanceAfterShopping.Text = guiLang.BuyMealsFormScNS_Label_lblMoneyBalanceAfterShopping + " " + Utils.getCurrencyString(buyMealsData.MoneyBalanceAfterBuyingSelected, true);
        }

        private void BuyMealsFormScNS_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                try
                {
                    //todo vidi da li je pametno da se stalno brisu i prave novi tabovi. Mozda bi trebalo nekako drugacije da se to uradi
                    setCashValues();
                    addMonthTabs();
                    setGridShoppingCardTitles();
                    setLabels();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Error in BuyMealsFormScNS_VisibleChanged. " + ex.Message, ex);
                    Program.KioskApplication.showFormInPanel(UserDataFormScNS.getInstance());
                    MessageClassic.getInstance(ex.Message, MessageType.Error).ShowDialog();
                }
            }
        }

        private void ChangeTabColor(object sender, DrawItemEventArgs e)
        {
            Font TabFont;
            Brush BackBrush = new SolidBrush(Program.KioskApplication.BackgroundColor); //Set background color
            Brush BackBrushActive = new SolidBrush(Program.KioskApplication.TextForeColor2);
            Brush ForeBrush = new SolidBrush(Program.KioskApplication.TextForeColor2);//Set foreground color
            Brush ForeBrushActive = new SolidBrush(Program.KioskApplication.TextForeColor);
            if (e.Index == this.tabControlMonth.SelectedIndex)
            {
                TabFont = new Font(e.Font, /*FontStyle.Italic |*/ FontStyle.Bold);
                BackBrush = BackBrushActive;
                ForeBrush = ForeBrushActive;
            }
            else
            {
                TabFont = e.Font;
                //TabFont = new Font(e.Font, FontStyle.Strikeout);
            }
            string TabName = this.tabControlMonth.TabPages[e.Index].Text;
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            e.Graphics.FillRectangle(BackBrush, e.Bounds);
            Rectangle r = e.Bounds;
            r = new Rectangle(r.X, r.Y + 3, r.Width, r.Height - 3);
            e.Graphics.DrawString(TabName, TabFont, ForeBrush, r, sf);
            //Dispose objects
            sf.Dispose();
            if (e.Index == this.tabControlMonth.SelectedIndex)
            {
                TabFont.Dispose();
                BackBrush.Dispose();
            }
            else
            {
                BackBrush.Dispose();
                ForeBrush.Dispose();
            }
        }

        private void PaintTabsRectangle(object sender, DrawItemEventArgs e)
        {
            //ova funkcija farba pravougaonik sa leve strane tabova do kraja tabControl-a
            SolidBrush fillBrush = new SolidBrush(Program.KioskApplication.BackgroundColor);

            //draw rectangle behind the tabs
            Rectangle lasttabrect = tabControlMonth.GetTabRect(tabControlMonth.TabPages.Count - 1);
            Rectangle background = new Rectangle();
            background.Location = new Point(lasttabrect.Right, 0);

            //pad the rectangle to cover the 1 pixel line between the top of the tabpage and the start of the tabs
            background.Size = new Size(tabControlMonth.Right - background.Left, lasttabrect.Height + 1);
            e.Graphics.FillRectangle(fillBrush, background);
        }

        private void tabControlMonth_DrawItem(object sender, DrawItemEventArgs e)
        {
            ChangeTabColor(sender, e);
            PaintTabsRectangle(sender, e);

        }

        public void fillShoppingCartGrid()
        {
            gridShoppingCart.Rows.Clear();
            
            bool cartVisible = false;

            foreach (var meal in buyMealsData.Meals)
            {
                if (meal.Amount > 0)
                {
                    gridShoppingCart.Rows.Add(meal.getMealTypeString() + " - " + Utils.getMonthString(meal.Month),
                                              Utils.getCurrencyString(meal.Price, true),
                                              meal.Amount.ToString(),
                                              Utils.getCurrencyString((meal.Amount * meal.Price), true));
                    cartVisible = true;
                }
            }

            DataGridViewRow row = new DataGridViewRow();
            row.DefaultCellStyle.BackColor = Program.KioskApplication.TextForeColor2;
            row.DefaultCellStyle.ForeColor = Program.KioskApplication.BackColor;
            for (int i = 0; i < 4; i++)
            {
                row.Cells.Add(new DataGridViewTextBoxCell());    
            }
            row.Cells[0].Value = guiLang.BuyMealsFormScNS_GridShoppingCart_Total.ToUpper();
            row.Cells[3].Value = Utils.getCurrencyString(buyMealsData.getTotalPrice(), true);
            gridShoppingCart.Rows.Add(row);

            pictureBoxShoppingCart.Visible = cartVisible;
            gridShoppingCart.Visible = cartVisible;
            btnBuyFood.Visible = cartVisible;
        }

        private void gridShoppingCart_SelectionChanged(object sender, EventArgs e)
        {
            gridShoppingCart.ClearSelection();
        }

        private void setGridShoppingCardTitles()
        {
            gridShoppingCart.Columns[0].HeaderText = guiLang.BuyMealsFormScNS_GridShoppingCart_Header_0;
            gridShoppingCart.Columns[1].HeaderText = guiLang.BuyMealsFormScNS_GridShoppingCart_Header_1;
            gridShoppingCart.Columns[2].HeaderText = guiLang.BuyMealsFormScNS_GridShoppingCart_Header_2;
            gridShoppingCart.Columns[3].HeaderText = guiLang.BuyMealsFormScNS_GridShoppingCart_Header_3;
        }

        private void setLabels()
        {
            btnBuyFood.Text = guiLang.BuyMealsFormScNS_Button_btnBuyFood;
            btnCancelBuyFood.Text = guiLang.BuyMealsFormScNS_Button_btnCancelBuyFood;
            btnInsertCash.Text = guiLang.UserDataForm_Button_btnInsertCash;
        }

        private void BuyMealsFormScNS_Paint(object sender, PaintEventArgs e)
        {
            //System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            //System.Drawing.Graphics formGraphics;
            //formGraphics = this.CreateGraphics();
            //formGraphics.FillRectangle(myBrush, new Rectangle(100, 100, 1000, 1000));
            //myBrush.Dispose();
            //formGraphics.Dispose();
        }

        private void btnCancelBuyFood_Click(object sender, EventArgs e)
        {
            if (!FormBusy)
            {
                try
                {
                    if (!sender.Equals("Return"))
                    {
                        Program.KioskApplication.log.Info("Clicked Cancel Buy Food button. ");
                    }

                    Program.KioskApplication.showFormInPanel(UserDataFormScNS.getInstance());
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Buy meals cancel click failed " + ex.Message, ex);
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }

        private int processBuyingMeals()
        {
            //Prvo se upisuje u bazu, zatim na karticu, a ako upis na karticu pukne, ponistava se transakcija u bazi
            int transactionId = 0;
            ScNSKiosk.CardAdapterContactScNS cardInterface = (ScNSKiosk.CardAdapterContactScNS)CardInterface.getInstance().CardInterf;

            try
            {
                UserDataScNS ud = UserDataFormScNS.getInstance().ud;

                int b = 0, l = 0, d = 0, bn = 0, ln = 0, dn = 0;
                double p = 0, pn = 0;

                DateTime now = DateTime.Now;
                DateTime nextMonth = now.AddMonths(1);

                foreach (var meal in buyMealsData.Meals)
                {
                    switch (meal.Type)
                    {
                        case MealType.Breakfast:
                            if (meal.Month == now.Month)
                            {
                                b = meal.Amount;
                                p += meal.Price * b;
                            }
                            else
                            {
                                bn = meal.Amount;
                                pn += meal.Price * bn;
                            }
                            break;
                        case MealType.Lunch:
                            if (meal.Month == now.Month)
                            {
                                l = meal.Amount;
                                p += meal.Price * l;
                            }
                            else
                            {
                                ln = meal.Amount;
                                pn += meal.Price * ln;
                            }
                            break;
                        case MealType.Dinner:
                            if (meal.Month == now.Month)
                            {
                                d = meal.Amount;
                                p += meal.Price * d;
                            }
                            else
                            {
                                dn = meal.Amount;
                                pn += meal.Price * dn;
                            }
                            break;
                        default:
                            break;
                    }
                }

                //provera
                if (p + pn != buyMealsData.getTotalPrice())
                {
                    //todo vidi oko jezika
                    Program.KioskApplication.log.Error("Doublecheck of meals prices failed. " + (p + pn).ToString() + " | " + buyMealsData.getTotalPrice().ToString());
                    throw new Exception("There is a problem in buying meals process!");
                }

                //upis u bazu
                if ((p > 0) || (pn > 0))
                {
                    //prvo se sa kartice cita broj transakcije koji mora da se upise u bazu
                    int cardTransactionNumber = cardInterface.getTransactionAmountAdapter() + 1;

                    transactionId = DBAdapter.insertBuyingMeals(ud.CardSerialNumber, ud.CardNumber, buyMealsData.MoneyBalanceAfterBuyingSelected, b, l, d, bn, ln, dn, 2, ud.MealsArray, cardTransactionNumber);
                }

                if (transactionId == 0)
                {
                    Program.KioskApplication.log.Error("Inserting transaction to database failed. ");
                    throw new Exception("There is a problem in buying meals process!");
                }


                //upis na karticu
                
                try
                {
                    if ((b > 0) || (l > 0) || (d > 0))
                    {
                        //ima kupovine za tekuci mesec
                        UserDataFormScNS.getInstance().changeMealsInMealsChecker(now.Month, now.Year, b, l, d);
                    }
                    if ((bn > 0) || (ln > 0) || (dn > 0))
                    {
                        UserDataFormScNS.getInstance().changeMealsInMealsChecker(nextMonth.Month, nextMonth.Year, bn, ln, dn);
                    }

                    
                    cardInterface.setDataCashierVersionAdapter(ud.MealsArray, ud.EatenMealsArray, buyMealsData.MoneyBalanceAfterBuyingSelected);

                    Program.KioskApplication.log.Info("Meals and decreased cash balance writen to the card.");
                }
                catch (Exception ex)
                {
                    //ako nije uspeo upis na karticu, ponisti transakciju u bazi
                    try
                    {
                        Program.KioskApplication.log.Error("Writing to card failed. Canceling purchase... Error message: " + ex.Message, ex);
                        DBAdapter.undoTransaction(transactionId);
                        Program.KioskApplication.log.Info("Purchase Canceled.");
                    }
                    catch (Exception ex1)
                    {
                        //nije uspelo ponistavanje u bazi
                        Program.KioskApplication.log.Error("Canceling purchase failed. Error message: " + ex1.Message, ex1);
                        throw new Exception(ex1.Message);
                    }
                    throw new Exception(ex.Message/*nije uspeo upis na karticu*/);
                }

                Program.KioskApplication.log.Info("Meals written to database. Now, writing to card is starting... ");

            }
            catch (Exception ex)
            {
                //MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
                throw ex;
            }

            return transactionId;
        }

        private void btnBuyFood_Click(object sender, EventArgs e)
        {
            //podaci ce se upisivati u bazu i na karticu u posebnom threadu (tasku),
            //a u ovom threadu cu da pitam korisnika da li hoce da mu se stampa racun
            //dok on odabere sta hoce, i upis ce da zavrsi - tu cekam da task zavrsi, 
            //a onda se stampa racun ako je korisnik to izabrao
            //naravno, ako nema papira ili ne radi stampac, ne treba nista pitati korisnika
            if (!FormBusy)
            {
                FormBusy = true;

                string logMessage = "Clicked Buy Food button. Shopping cart content: ";
                foreach (var meal in buyMealsData.Meals)
                {
                    logMessage += meal.getMealTypeString() + " - " + meal.Month + ". month, " + meal.Amount + " pcs; ";
                }
                logMessage += "Total price: " + Utils.getCurrencyString(buyMealsData.getTotalPrice(), true);
                Program.KioskApplication.log.Info(logMessage);

                try
                {
                    var taskProcessBuyingMeals = Task.Run(() => processBuyingMeals());

                    //Task<int> taskProcessBuyingMeals = new Task<int>(processBuyingMeals);
                    //taskProcessBuyingMeals.Start();

                    //prikazi dijalog sa pitanjem da li hoce da stampa
                    if (Program.KioskApplication.PrinterAvailable)
                    {
                        MessageChoice.getInstance(guiLang.MessageChoice_DoYouWantReceipt, MessageType.Question).ShowDialog();
                    }

                    int transactionId = taskProcessBuyingMeals.Result;

                    if (Program.KioskApplication.MessageChoiceResult)
                    {
                        //stampaj racun
                        printReceipt(transactionId);
                    }

                    taskProcessBuyingMeals.Wait();

                }
                catch (AggregateException aex)
                {
                    Program.KioskApplication.log.Error(aex.InnerException.Message, aex);
                    MessageClassic.getInstance(aex.InnerException.Message, MessageType.Error).ShowDialog();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error(ex.InnerException.Message, ex);
                    MessageClassic.getInstance(ex.InnerException.Message, MessageType.Error).ShowDialog();
                }
                finally
                {
                    //vrati se nazad
                    FormBusy = false;
                    btnCancelBuyFood_Click("Return", null);  //Retun oznacava da se vracamo nazad, i da nije stvarno kliknuto na dugme
                }
            }
        }

        public void printReceipt(int transactionId)
        {
            try
            {
                DateTime now = DateTime.Now;
                string receiptTitle = guiLang.Receipt_Title_Shopping;
                string receiptNumber = transactionId.ToString();

                string timeStamp = Utils.addLeadingZeros(now.Day, 2) + @"." + Utils.addLeadingZeros(now.Month, 2) + @"." + now.Year.ToString() + @". " + Utils.addLeadingZeros(now.Hour, 2) + @":" + Utils.addLeadingZeros(now.Minute, 2) + @":" + Utils.addLeadingZeros(now.Second, 2);

                //stavke racuna
                List<ReceiptItem> receiptItems = new List<ReceiptItem>();

                foreach (var meal in buyMealsData.Meals)
                {
                    if (meal.Amount > 0)
                    {
                        receiptItems.Add(new ReceiptItem(meal.getMealTypeString() + " - " + Utils.getMonthString(meal.Month), meal.Amount, meal.Price));
                    }
                }
                //-------------

                Receipt rp = new ReceiptScNS(receiptTitle, receiptNumber, UserDataFormScNS.getInstance().ud.CardNumber, buyMealsData.getTotalPrice(), timeStamp, receiptItems);
                rp.printReceipt();
            }
            catch (Exception ex)
            {
                string err = "Error printing receipt. " + " " + ex.Message;
                Program.KioskApplication.log.Error(err, ex);
                throw new Exception(err);
            }
        }

        private void btnInsertCash_Click(object sender, EventArgs e)
        {
            if (!FormBusy)
            {
                try
                {
                    Program.KioskApplication.log.Info("Clicked Insert Cash button. ");
                    Program.KioskApplication.showFormInPanel(CashInsertFormScNS.getInstance(this));
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Buy meals - insert cash click failed " + ex.Message, ex);
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }
    }
}
