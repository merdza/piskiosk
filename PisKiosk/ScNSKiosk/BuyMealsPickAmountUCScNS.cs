﻿using System;
using System.Windows.Forms;

namespace PisKiosk.ScNSKiosk
{
    public class BuyMealsPickAmountUCScNS : BuyMealsPickAmountUC
    {
        public BuyMealsPickAmountUCScNS(Meal m):base(m)
        {
        }

        protected override void enableButtons(bool showLog)
        {
            //u zavisnosti od perioda u mesecu, od stanja novcanika i od ostalih parametara odredjuje se koje dugme moze da se pritisne, a koje nece biti enableovano

            ClickButtonStatus s = ClickButtonStatus.Allowed;

            //ako se obroci kupuju na dekade (pre 21. u mesecu), onda treba gledati koliko je studentu ostalo da kupi 
            //i ako mu je ostalo manje od 15, onda se prikazuje taj broj (npr. 8 za februar, 10 ili 11)
            //ako mu je ostalo vise od 15, onda se prikazuje 10
            if ((!BuyMealsFormScNS.getInstance().BuyingMealsByDecades) && (MealChosen.Month == DateTime.Now.Month))
            {
                //one plus button
                s = checkIfEnoughMealsToBuy(1);
                if (s == ClickButtonStatus.Allowed)
                {
                    lblOnePlusReason.Text = "";
                    btnOnePlus.Enabled = true;
                }
                else
                {
                    //ispisi zasto ne moze da kupi
                    lblOnePlusReason.Text = getClickMessage(s);
                    btnOnePlus.Enabled = false;
                }

                //one minus button
                if (NumberOfOnesClicked > 0)
                {
                    lblOneMinusReason.Text = "";
                    btnOneMinus.Enabled = true;
                }
                else
                {
                    lblOneMinusReason.Text = guiLang.BuyMealsPickAmountUC_Message_PositiveNumberOnly;
                    btnOneMinus.Enabled = false;
                }
            }
            else
            {
                btnOnePlus.Enabled = false;
                lblOnePlusReason.Text = guiLang.BuyMealsPickAmountUC_Message_BuyingOnDecades;

                btnOneMinus.Enabled = false;
                lblOneMinusReason.Text = guiLang.BuyMealsPickAmountUC_Message_BuyingOnDecades;
            }

            //ten plus button
            if (MealChosen.MealsAllowed > 15)
            {
                btnTenPlus.Text = "+10";
            }
            else
            {
                btnTenPlus.Text = "+" + (MealChosen.MealsAllowed).ToString();
            }

            s = checkIfEnoughMealsToBuy(Convert.ToInt32(btnTenPlus.Text));
            if (s == ClickButtonStatus.Allowed)
            {
                lblTenPlusReason.Text = "";
                btnTenPlus.Enabled = true;
            }
            else
            {
                lblTenPlusReason.Text = getClickMessage(s);
                btnTenPlus.Text = "+10";
                btnTenPlus.Enabled = false;
            }

            //ten minus button
            if (TensPlusClicked.Count > 0)
            {
                lblTenMinusReason.Text = "";
                btnTenMinus.Text = (-TensPlusClicked.Peek()).ToString();
                btnTenMinus.Enabled = true;
            }
            else
            {
                lblTenMinusReason.Text = guiLang.BuyMealsPickAmountUC_Message_PositiveNumberOnly;
                btnTenMinus.Text = "-10";
                btnTenMinus.Enabled = false;
            }

            this.Refresh();
            Application.DoEvents();

            //ispis statusa dugmica u log
            if (showLog)
            {
                string logEntry = MealChosen.Type.ToString() + ": ";
                string reason = string.Empty;
                if (!btnOnePlus.Enabled)
                {
                    reason = lblOnePlusReason.Text;
                }
                logEntry += "Status dugmeta +1: " + btnOnePlus.Enabled + ". " + reason + ". ";

                reason = string.Empty;
                if (!btnOneMinus.Enabled)
                {
                    reason = lblOneMinusReason.Text;
                }
                logEntry += "Status dugmeta -1: " + btnOneMinus.Enabled + ". " + reason + ". ";

                if (!btnTenPlus.Enabled)
                {
                    reason = lblTenPlusReason.Text;
                }
                else
                {
                    reason = "Za kupovinu omoguceno " + btnTenPlus.Text + " obroka.";
                }
                logEntry += "Status dugmeta +10: " + btnTenPlus.Enabled + ". " + reason + ". ";

                if (!btnTenMinus.Enabled)
                {
                    reason = lblTenMinusReason.Text;
                }
                else
                {
                    reason = "Za ponistavanje omoguceno " + btnTenMinus.Text + " obroka.";
                }
                logEntry += "Status dugmeta -10: " + btnTenMinus.Enabled + ". " + reason + ". ";

                Program.KioskApplication.log.Debug(logEntry);
            }
            //-----------------------------------
        }
        protected override ClickButtonStatus checkIfEnoughMealsToBuy(int amount)
        {
            //Provera da li je treba omoguciti odredjene dugmice, odnosno da li je moguce kupiti odredjeni broj obroka:
            //1. treba proveriti da li ima dovoljno para
            //2. treba proveriti da li sme da kupi toliko obroka - MealsChecker

            ClickButtonStatus goForIt = ClickButtonStatus.Allowed;

            if (amount == 0)
            {
                goForIt = ClickButtonStatus.NotAllowed;
            }
            else // samo moze da se povecava broj obroka jer se smanjivanje regulise drugacije (stekom i brojem klikova na +1)
            {
                //1. da li ima dovoljno para
                if (!BuyMealsFormScNS.getInstance().buyMealsData.hasEnoughMoneyToBuy(MealChosen.Month, MealChosen.Year, MealChosen.Type, amount))
                {
                    goForIt = ClickButtonStatus.NotEnoughMoney;
                }
                else
                {
                    //2. da li sme da kupi toliko obroka
                    if (UserDataFormScNS.getInstance().ud.IsResident)
                    {
                        //ako je mestanin, onda za jedan mesec sme da kupi ukupno obroka koliko ima u left podacima
                        int allowedMealsResident = BuyMealsFormScNS.getInstance().buyMealsData.getAllowedMealsForResident(MealChosen.Month);
                        if (allowedMealsResident < amount)
                        {
                            goForIt = ClickButtonStatus.NotAllowed;
                        }
                    }
                    else
                    {
                        if (MealChosen.MealsAllowed < amount)
                        {
                            goForIt = ClickButtonStatus.NotAllowed;
                        }
                    }
                }
            }
            return goForIt;
        }

        protected override void setMealAmount(bool showLog)
        {
            //if (UserDataFormScNS.getInstance().ud.IsResident)
            //{
            //    BuyMealsFormScNS.getInstance().buyMealsData.getAllowedMealsForResident(MealChosen.Month);
            //}
            enableButtons(showLog);
            lblMealAmountSelected.Text = MealChosen.Amount.ToString();
            lblAllowed.Text = guiLang.UserDataForm_Label_lblAllowedMealsToBuy + ": " + MealChosen.MealsAllowed.ToString();

            ScNSKiosk.BuyMealsFormScNS.getInstance().fillShoppingCartGrid();
            ScNSKiosk.BuyMealsFormScNS.getInstance().setCashAmounts();
            
        }

        protected override void btnOnePlus_Click(object sender, EventArgs e)
        {
            if (!ScNSKiosk.BuyMealsFormScNS.getInstance().FormBusy)
            {
                Program.KioskApplication.log.Debug(lblMealTitle.Text + ": Button +1 clicked");
                try
                {
                    ScNSKiosk.BuyMealsFormScNS.getInstance().buyMealsData.changeMealAmounts(MealChosen.Month, MealChosen.Year, MealChosen.Type, 1);
                    MealChosen.MealsAllowed -= 1;
                    NumberOfOnesClicked += 1;
                    setMealAmount(true);
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Buy meals pick amount - plus one button click failed " + ex.Message, ex);
                }
                finally
                {
                    ScNSKiosk.BuyMealsFormScNS.getInstance().FormBusy = false;
                }
            }
            
        }
        protected override void btnOneMinus_Click(object sender, EventArgs e)
        {
            if (!ScNSKiosk.BuyMealsFormScNS.getInstance().FormBusy)
            {
                Program.KioskApplication.log.Debug(lblMealTitle.Text + ": Button -1 clicked");
                try
                {
                    ScNSKiosk.BuyMealsFormScNS.getInstance().buyMealsData.changeMealAmounts(MealChosen.Month, MealChosen.Year, MealChosen.Type, -1);
                    MealChosen.MealsAllowed += 1;
                    NumberOfOnesClicked -= 1;
                    setMealAmount(true);
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Buy meals pick amount - minus one button click failed " + ex.Message, ex);
                }
                finally
                {
                    ScNSKiosk.BuyMealsFormScNS.getInstance().FormBusy = false;
                }
            }

            
        }
        protected override void btnTenMinus_Click(object sender, EventArgs e)
        {
            if (!ScNSKiosk.BuyMealsFormScNS.getInstance().FormBusy)
            {
                Program.KioskApplication.log.Debug(lblMealTitle.Text + ": Button  " + btnTenMinus.Text + "  clicked");
                try
                {
                    ScNSKiosk.BuyMealsFormScNS.getInstance().buyMealsData.changeMealAmounts(MealChosen.Month, MealChosen.Year, MealChosen.Type, Convert.ToInt32(btnTenMinus.Text));
                    MealChosen.MealsAllowed -= Convert.ToInt32(btnTenMinus.Text);
                    TensPlusClicked.Pop();
                    setMealAmount(true);
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Buy meals pick amount - minus ten button click failed " + ex.Message, ex);
                }
                finally
                {
                    ScNSKiosk.BuyMealsFormScNS.getInstance().FormBusy = false;
                }
            }

            
        }
        protected override void btnTenPlus_Click(object sender, EventArgs e)
        {
            if (!ScNSKiosk.BuyMealsFormScNS.getInstance().FormBusy)
            {
                Program.KioskApplication.log.Debug(lblMealTitle.Text + ": Button " + btnTenPlus.Text +" clicked");
                try
                {
                    ScNSKiosk.BuyMealsFormScNS.getInstance().buyMealsData.changeMealAmounts(MealChosen.Month, MealChosen.Year, MealChosen.Type, Convert.ToInt32(btnTenPlus.Text));
                    MealChosen.MealsAllowed -= Convert.ToInt32(btnTenPlus.Text);
                    TensPlusClicked.Push(Convert.ToInt32(btnTenPlus.Text));
                    setMealAmount(true);
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Buy meals pick amount - plus ten button click failed " + ex.Message, ex);
                }
                finally
                {
                    ScNSKiosk.BuyMealsFormScNS.getInstance().FormBusy = false;
                }
            }
            
        }
    }
}
