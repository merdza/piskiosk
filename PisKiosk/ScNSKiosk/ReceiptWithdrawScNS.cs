﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.ScNSKiosk
{
    public class ReceiptWithdrawScNS : Receipt
    {
        public double moneyInKiosk;
        public double moneyWithdrawn;
        public double moneyLeftInKiosk;

        public ReceiptWithdrawScNS(string rt, double mt, double mw, string ts)
            : base(rt, "", "", 0, ts, null)
        {
            moneyInKiosk = mt;
            moneyWithdrawn = mw;
            moneyLeftInKiosk = mt - mw;
        }

        protected override void preparePrinting(object sender, PrintPageEventArgs e)
        {
            using (Font font1 = new Font("Segoe UI Light", 12, FontStyle.Bold, GraphicsUnit.Point))
            {
                StringFormat stringFormat = new StringFormat();
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;

                StringFormat leftStringFormat = new StringFormat();
                leftStringFormat.Alignment = StringAlignment.Near;
                leftStringFormat.LineAlignment = StringAlignment.Center;

                StringFormat rightStringFormat = new StringFormat();
                rightStringFormat.Alignment = StringAlignment.Far;
                rightStringFormat.LineAlignment = StringAlignment.Center;

                int y = 20;

                Rectangle rectOrg = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(guiLang.Receipt_Organization_ScNS, new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectOrg, stringFormat);
                y += 20;

                rectOrg = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(guiLang.Receipt_Organization_Address_ScNS, new Font(FontFamily.GenericSansSerif, 9), Brushes.Black, rectOrg, stringFormat);
                y += 40;

                Rectangle rectHeader = new Rectangle(10, y, 280, 30);
                //e.Graphics.DrawString(guiLang.Receipt_Header, new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectHeader, stringFormat);
                e.Graphics.DrawString(Program.KioskApplication.KioskName, new Font(FontFamily.GenericSansSerif, 14), Brushes.Black, rectHeader, stringFormat);
                y += 40;

                Rectangle rectTitle = new Rectangle(10, y, 280, 30);
                e.Graphics.DrawString(receiptTitle, new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectTitle, stringFormat);
                y += 40;

                y += 20;
                Rectangle rectTotalBalanceLabel = new Rectangle(20, y, 120, 35);
                Rectangle rectTotalBalance = new Rectangle(140, y, 145, 35);

                e.Graphics.DrawString(guiLang.Receipt_Withdrawal_MoneyInKiosk + ": ", new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectTotalBalanceLabel, leftStringFormat);
                e.Graphics.DrawString(Utils.getCurrencyString(moneyInKiosk, true), new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectTotalBalance, rightStringFormat);
                y += 45;

                Rectangle rectWithdrawnBalanceLabel = new Rectangle(20, y, 120, 35);
                Rectangle rectWithdrawnBalance = new Rectangle(140, y, 145, 35);

                e.Graphics.DrawString(guiLang.Receipt_Withdrawal_MoneyWithdrawn + ": ", new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectWithdrawnBalanceLabel, leftStringFormat);
                e.Graphics.DrawString(Utils.getCurrencyString(moneyWithdrawn, true), new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectWithdrawnBalance, rightStringFormat);
                y += 45;


                Rectangle rectLeftBalanceLabel = new Rectangle(20, y, 120, 35);
                Rectangle rectLeftBalance = new Rectangle(140, y, 145, 35);

                e.Graphics.DrawString(guiLang.Receipt_Withdrawal_MoneyLeft + ": ", new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectLeftBalanceLabel, leftStringFormat);
                e.Graphics.DrawString(Utils.getCurrencyString(moneyLeftInKiosk, true), new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectLeftBalance, rightStringFormat);
                y += 50;

                Rectangle rectTimeStamp = new Rectangle(10, y, 280, 20);
                e.Graphics.DrawString(timeStamp, new Font(FontFamily.GenericSansSerif, 12), Brushes.Black, rectTimeStamp, stringFormat);
                y += 20;

                Rectangle allOverRectangle = new Rectangle(10, 10, 280, y + 10);
                e.Graphics.DrawRectangle(Pens.Black, allOverRectangle);

            }
        }
    }
}
