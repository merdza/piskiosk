﻿namespace PisKiosk.ScNSKiosk
{
    partial class BuyMealsFormScNS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            PisKiosk.Colortable colortable1 = new PisKiosk.Colortable();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuyMealsFormScNS));
            PisKiosk.Colortable colortable2 = new PisKiosk.Colortable();
            PisKiosk.Colortable colortable3 = new PisKiosk.Colortable();
            this.tabControlMonth = new PisKiosk.TabControlExtended();
            this.lblCardMoneyBalance = new System.Windows.Forms.Label();
            this.lblMoneyBalanceAfterShopping = new System.Windows.Forms.Label();
            this.gridShoppingCart = new System.Windows.Forms.DataGridView();
            this.Item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pictureBoxShoppingCart = new System.Windows.Forms.PictureBox();
            this.btnBuyFood = new PisKiosk.XButton();
            this.btnCancelBuyFood = new PisKiosk.XButton();
            this.btnInsertCash = new PisKiosk.XButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridShoppingCart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShoppingCart)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI Light", 6F, System.Drawing.FontStyle.Bold);
            this.lblTitle.Location = new System.Drawing.Point(143, 9);
            this.lblTitle.Size = new System.Drawing.Size(161, 51);
            this.lblTitle.Text = "Title - BuyMealsForm ScNS";
            // 
            // tabControlMonth
            // 
            this.tabControlMonth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMonth.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControlMonth.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.tabControlMonth.HotTrack = true;
            this.tabControlMonth.Location = new System.Drawing.Point(27, 84);
            this.tabControlMonth.Name = "tabControlMonth";
            this.tabControlMonth.SelectedIndex = 0;
            this.tabControlMonth.Size = new System.Drawing.Size(1094, 370);
            this.tabControlMonth.TabIndex = 3;
            this.tabControlMonth.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControlMonth_DrawItem);
            // 
            // lblCardMoneyBalance
            // 
            this.lblCardMoneyBalance.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCardMoneyBalance.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblCardMoneyBalance.Location = new System.Drawing.Point(376, 17);
            this.lblCardMoneyBalance.Name = "lblCardMoneyBalance";
            this.lblCardMoneyBalance.Size = new System.Drawing.Size(539, 38);
            this.lblCardMoneyBalance.TabIndex = 4;
            this.lblCardMoneyBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMoneyBalanceAfterShopping
            // 
            this.lblMoneyBalanceAfterShopping.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMoneyBalanceAfterShopping.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            this.lblMoneyBalanceAfterShopping.Location = new System.Drawing.Point(376, 53);
            this.lblMoneyBalanceAfterShopping.Name = "lblMoneyBalanceAfterShopping";
            this.lblMoneyBalanceAfterShopping.Size = new System.Drawing.Size(539, 39);
            this.lblMoneyBalanceAfterShopping.TabIndex = 4;
            this.lblMoneyBalanceAfterShopping.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gridShoppingCart
            // 
            this.gridShoppingCart.AllowUserToAddRows = false;
            this.gridShoppingCart.AllowUserToDeleteRows = false;
            this.gridShoppingCart.AllowUserToResizeColumns = false;
            this.gridShoppingCart.AllowUserToResizeRows = false;
            this.gridShoppingCart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridShoppingCart.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridShoppingCart.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.gridShoppingCart.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridShoppingCart.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridShoppingCart.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridShoppingCart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridShoppingCart.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item,
            this.Price,
            this.Amount,
            this.Total});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Light", 20F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridShoppingCart.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridShoppingCart.Location = new System.Drawing.Point(152, 465);
            this.gridShoppingCart.MultiSelect = false;
            this.gridShoppingCart.Name = "gridShoppingCart";
            this.gridShoppingCart.ReadOnly = true;
            this.gridShoppingCart.RowHeadersVisible = false;
            this.gridShoppingCart.Size = new System.Drawing.Size(779, 282);
            this.gridShoppingCart.TabIndex = 5;
            this.gridShoppingCart.Visible = false;
            this.gridShoppingCart.SelectionChanged += new System.EventHandler(this.gridShoppingCart_SelectionChanged);
            // 
            // Item
            // 
            this.Item.HeaderText = "Item";
            this.Item.Name = "Item";
            this.Item.ReadOnly = true;
            // 
            // Price
            // 
            this.Price.HeaderText = "Price";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // pictureBoxShoppingCart
            // 
            this.pictureBoxShoppingCart.Image = global::PisKiosk.Properties.Resources.shopping_cart_gray;
            this.pictureBoxShoppingCart.Location = new System.Drawing.Point(27, 465);
            this.pictureBoxShoppingCart.Name = "pictureBoxShoppingCart";
            this.pictureBoxShoppingCart.Size = new System.Drawing.Size(119, 106);
            this.pictureBoxShoppingCart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxShoppingCart.TabIndex = 6;
            this.pictureBoxShoppingCart.TabStop = false;
            // 
            // btnBuyFood
            // 
            this.btnBuyFood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            colortable1.BorderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            colortable1.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            colortable1.ButtonMouseOverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            colortable1.ButtonMouseOverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            colortable1.ButtonMouseOverColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(137)))));
            colortable1.ButtonMouseOverColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(249)))), ((int)(((byte)(224)))));
            colortable1.ButtonNormalColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            colortable1.ButtonNormalColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            colortable1.ButtonNormalColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            colortable1.ButtonNormalColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            colortable1.ButtonSelectedColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            colortable1.ButtonSelectedColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            colortable1.ButtonSelectedColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(117)))));
            colortable1.ButtonSelectedColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(107)))));
            colortable1.HoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            colortable1.SelectedTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            colortable1.TextColor = System.Drawing.Color.White;
            this.btnBuyFood.ColorTable = colortable1;
            this.btnBuyFood.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnBuyFood.Image = ((System.Drawing.Image)(resources.GetObject("btnBuyFood.Image")));
            this.btnBuyFood.Location = new System.Drawing.Point(945, 465);
            this.btnBuyFood.Name = "btnBuyFood";
            this.btnBuyFood.Size = new System.Drawing.Size(176, 100);
            this.btnBuyFood.TabIndex = 7;
            this.btnBuyFood.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnBuyFood.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnBuyFood.UseVisualStyleBackColor = true;
            this.btnBuyFood.Click += new System.EventHandler(this.btnBuyFood_Click);
            // 
            // btnCancelBuyFood
            // 
            this.btnCancelBuyFood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            colortable2.BorderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            colortable2.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            colortable2.ButtonMouseOverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            colortable2.ButtonMouseOverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            colortable2.ButtonMouseOverColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(137)))));
            colortable2.ButtonMouseOverColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(249)))), ((int)(((byte)(224)))));
            colortable2.ButtonNormalColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            colortable2.ButtonNormalColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            colortable2.ButtonNormalColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            colortable2.ButtonNormalColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            colortable2.ButtonSelectedColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            colortable2.ButtonSelectedColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            colortable2.ButtonSelectedColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(117)))));
            colortable2.ButtonSelectedColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(107)))));
            colortable2.HoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            colortable2.SelectedTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            colortable2.TextColor = System.Drawing.Color.White;
            this.btnCancelBuyFood.ColorTable = colortable2;
            this.btnCancelBuyFood.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnCancelBuyFood.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelBuyFood.Image")));
            this.btnCancelBuyFood.Location = new System.Drawing.Point(945, 590);
            this.btnCancelBuyFood.Name = "btnCancelBuyFood";
            this.btnCancelBuyFood.Size = new System.Drawing.Size(176, 60);
            this.btnCancelBuyFood.TabIndex = 7;
            this.btnCancelBuyFood.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnCancelBuyFood.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnCancelBuyFood.UseVisualStyleBackColor = true;
            this.btnCancelBuyFood.Click += new System.EventHandler(this.btnCancelBuyFood_Click);
            // 
            // btnInsertCash
            // 
            this.btnInsertCash.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            colortable3.BorderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            colortable3.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            colortable3.ButtonMouseOverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            colortable3.ButtonMouseOverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            colortable3.ButtonMouseOverColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(137)))));
            colortable3.ButtonMouseOverColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(249)))), ((int)(((byte)(224)))));
            colortable3.ButtonNormalColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            colortable3.ButtonNormalColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            colortable3.ButtonNormalColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(97)))), ((int)(((byte)(181)))));
            colortable3.ButtonNormalColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(125)))), ((int)(((byte)(219)))));
            colortable3.ButtonSelectedColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            colortable3.ButtonSelectedColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            colortable3.ButtonSelectedColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(117)))));
            colortable3.ButtonSelectedColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(107)))));
            colortable3.HoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            colortable3.SelectedTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            colortable3.TextColor = System.Drawing.Color.White;
            this.btnInsertCash.ColorTable = colortable3;
            this.btnInsertCash.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold);
            this.btnInsertCash.Image = ((System.Drawing.Image)(resources.GetObject("btnInsertCash.Image")));
            this.btnInsertCash.Location = new System.Drawing.Point(933, 5);
            this.btnInsertCash.Name = "btnInsertCash";
            this.btnInsertCash.Size = new System.Drawing.Size(176, 100);
            this.btnInsertCash.TabIndex = 7;
            this.btnInsertCash.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnInsertCash.Theme = PisKiosk.Theme.MSOffice2010_BLUE;
            this.btnInsertCash.UseVisualStyleBackColor = true;
            this.btnInsertCash.Click += new System.EventHandler(this.btnInsertCash_Click);
            // 
            // BuyMealsFormScNS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1150, 650);
            this.Controls.Add(this.btnCancelBuyFood);
            this.Controls.Add(this.btnInsertCash);
            this.Controls.Add(this.btnBuyFood);
            this.Controls.Add(this.lblMoneyBalanceAfterShopping);
            this.Controls.Add(this.pictureBoxShoppingCart);
            this.Controls.Add(this.gridShoppingCart);
            this.Controls.Add(this.lblCardMoneyBalance);
            this.Controls.Add(this.tabControlMonth);
            this.Name = "BuyMealsFormScNS";
            this.VisibleChanged += new System.EventHandler(this.BuyMealsFormScNS_VisibleChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.BuyMealsFormScNS_Paint);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.tabControlMonth, 0);
            this.Controls.SetChildIndex(this.lblCardMoneyBalance, 0);
            this.Controls.SetChildIndex(this.gridShoppingCart, 0);
            this.Controls.SetChildIndex(this.pictureBoxShoppingCart, 0);
            this.Controls.SetChildIndex(this.lblMoneyBalanceAfterShopping, 0);
            this.Controls.SetChildIndex(this.btnBuyFood, 0);
            this.Controls.SetChildIndex(this.btnInsertCash, 0);
            this.Controls.SetChildIndex(this.btnCancelBuyFood, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gridShoppingCart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxShoppingCart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        //private System.Windows.Forms.TabControl tabControlMonth;
        private TabControlExtended tabControlMonth;
        
        private System.Windows.Forms.Label lblCardMoneyBalance;
        private System.Windows.Forms.Label lblMoneyBalanceAfterShopping;
        private System.Windows.Forms.DataGridView gridShoppingCart;
        private System.Windows.Forms.PictureBox pictureBoxShoppingCart;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        protected XButton btnBuyFood;
        protected XButton btnCancelBuyFood;
        protected XButton btnInsertCash;
    }
}
