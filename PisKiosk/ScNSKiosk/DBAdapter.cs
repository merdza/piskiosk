﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using XmlSettings;


namespace PisKiosk.ScNSKiosk
{
    public class DBAdapter:PisKiosk.DBAdapter
    {
        public DBAdapter():base() { }

        

        public static int getKioskOperaterID()
        {
            int kioskOperaterID = 0;
            try
            {
                string tableName = "vOperaterKioska";
                string sqlConnectString = getProperty("connectionString");
                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlDataReader myReader = null;
                    SqlCommand myCommand = new SqlCommand("SELECT GlavniOperater FROM " + tableName + " WHERE ImeKioska = '" + Program.KioskApplication.KioskName + "'", connection);
                    connection.Open();
                    myReader = myCommand.ExecuteReader();

                    if (myReader.Read())
                    {
                        kioskOperaterID = Convert.ToInt32(myReader["GlavniOperater"]);
                    }
                    myReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return kioskOperaterID;
        }

        public static KioskStatuses getKioskStatus()
        {
            KioskStatuses kioskStatus = KioskStatuses.Info;  //moze samo da vidi stanje, ali ne i da radi nesto
            try
            {
                string procName = "spNIKStatusKioska";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@ImeKioska", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@IDStatusInfoKioska", SqlDbType.Int).Direction = ParameterDirection.Output;

                    command.Parameters["@ImeKioska"].Value = Program.KioskApplication.KioskName;

                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);
                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ": " + err.ToString() + ", dbLogin");
                    }

                    kioskStatus = (KioskStatuses)Convert.ToInt32(command.Parameters["@IDStatusInfoKioska"].Value);

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return kioskStatus;
        }

        public static void getMealsPrices(string cardNumber, out double breakfastPrice, out double lunchPrice, out double dinnerPrice, out double breakfastNextPrice, out double lunchNextPrice, out double dinnerNextPrice, out int abonentCategory, out bool allowedBuyingThisMonth, out bool allowedBuyingNextMonth, out bool isResident, out bool firstShoppingInMonth)
        {
            try
            {
                string procName = "spNIKCeneObroka";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@brojKartice", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dorucak", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@rucak", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@vecera", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@dorucakSledeci", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@rucakSledeci", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@veceraSledeci", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@kategorijaAbonenta", SqlDbType.Int).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@dozvoljenaKupovinaOvogMeseca", SqlDbType.Bit).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@dozvoljenaKupovinaSledecegMeseca", SqlDbType.Bit).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@daLiJeBeogradjanin", SqlDbType.Bit).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@prvaKupovinaUMesecu", SqlDbType.Bit).Direction = ParameterDirection.Output;

                    command.Parameters["@brojKartice"].Value = cardNumber;
                    
                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);

                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ": " + err.ToString() + ", getMealsPrices");
                    }

                    breakfastPrice = Convert.ToDouble(command.Parameters["@dorucak"].Value);
                    lunchPrice = Convert.ToDouble(command.Parameters["@rucak"].Value);
                    dinnerPrice = Convert.ToDouble(command.Parameters["@vecera"].Value);
                    breakfastNextPrice = Convert.ToDouble(command.Parameters["@dorucakSledeci"].Value);
                    lunchNextPrice = Convert.ToDouble(command.Parameters["@rucakSledeci"].Value);
                    dinnerNextPrice = Convert.ToDouble(command.Parameters["@veceraSledeci"].Value);
                    abonentCategory = Convert.ToInt32(command.Parameters["@kategorijaAbonenta"].Value);
                    allowedBuyingThisMonth = Convert.ToBoolean(command.Parameters["@dozvoljenaKupovinaOvogMeseca"].Value);
                    allowedBuyingNextMonth = Convert.ToBoolean(command.Parameters["@dozvoljenaKupovinaSledecegMeseca"].Value);
                    isResident = Convert.ToBoolean(command.Parameters["@daLiJeBeogradjanin"].Value);
                    firstShoppingInMonth = Convert.ToBoolean(command.Parameters["@prvaKupovinaUMesecu"].Value);
                    
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public static int insertBuyingMeals(string cardSerial, string cardNum, double eWalletBalance, int b, int l, int d, int bn, int ln, int dn, int paymentType, byte[] meals, int transNumber)
        {
            try
            {
                int idTransakcija;

                string procName = "blSpTransakcijaKupovinaBonova";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@serijskiBrojKartice", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@brojKartice", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@brojTransakcije", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@novac", SqlDbType.Decimal).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@kupujeDorucak", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@kupujeRucak", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@kupujeVecera", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@kupujeDorucakNaredni", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@kupujeRucakNaredni", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@kupujeVeceraNaredni", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@tipPlacanja", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@mesec", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@godina", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dorucakTekuci", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@rucakTekuci", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@veceraTekuci", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dorucakDozvoljeno", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@rucakDozvoljeno", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@veceraDozvoljeno", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dorucakNaredni", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@rucakNaredni", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@veceraNaredni", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@operater", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dodatniOpis", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idTransakcija", SqlDbType.Int).Direction = ParameterDirection.Output;

                    int offset = 0;

                    command.Parameters["@serijskiBrojKartice"].Value = cardSerial;
                    command.Parameters["@brojKartice"].Value = cardNum;
                    command.Parameters["@brojTransakcije"].Value = transNumber;
                    command.Parameters["@novac"].Value = eWalletBalance;
                    command.Parameters["@novac"].Precision = 10;
                    command.Parameters["@novac"].Scale = 2;
                    command.Parameters["@kupujeDorucak"].Value = b;
                    command.Parameters["@kupujeRucak"].Value = l;
                    command.Parameters["@kupujeVecera"].Value = d;
                    command.Parameters["@kupujeDorucakNaredni"].Value = bn;
                    command.Parameters["@kupujeRucakNaredni"].Value = ln;
                    command.Parameters["@kupujeVeceraNaredni"].Value = dn;
                    command.Parameters["@tipPlacanja"].Value = paymentType;
                    command.Parameters["@godina"].Value = 2000 + meals[offset++];
                    command.Parameters["@mesec"].Value = meals[offset++];
                    command.Parameters["@dorucakTekuci"].Value = meals[offset++];
                    command.Parameters["@rucakTekuci"].Value = meals[offset++];
                    command.Parameters["@veceraTekuci"].Value = meals[offset++];
                    command.Parameters["@dorucakDozvoljeno"].Value = meals[offset++];
                    command.Parameters["@rucakDozvoljeno"].Value = meals[offset++];
                    command.Parameters["@veceraDozvoljeno"].Value = meals[offset++];
                    command.Parameters["@dorucakNaredni"].Value = meals[offset++];
                    command.Parameters["@rucakNaredni"].Value = meals[offset++];
                    command.Parameters["@veceraNaredni"].Value = meals[offset++];
                    command.Parameters["@operater"].Value = Program.KioskApplication.Operater.OperaterId;
                    command.Parameters["@dodatniOpis"].Value = string.Empty;

                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);

                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ": " + err.ToString() + ", buyMealsWithEMoney");
                    }

                    idTransakcija = Convert.ToInt32(command.Parameters["@idTransakcija"].Value);

                    connection.Close();
                    return idTransakcija;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void undoTransaction(int idTransakcija)
        {
            try
            {
                string procName = "pisSpStornoTransakcijaKupovinaObroka";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@idTransakcija", SqlDbType.Int).Direction = ParameterDirection.Input;

                    command.Parameters["@idTransakcija"].Value = idTransakcija;
                    
                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);

                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ": " + err.ToString() + ", undoTransaction");
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(guiLang.DBAdapter_Error + ", undoTransaction" + ex.Message);
            }
        }

        public static void moneyDeposit(string cardSerialNumber, int bankNote, byte[] meals, double cashOnCard, int cardTransaction, out int transactionId)
        {
            try
            {
                string procName = "spNIKUplataElektronskogNovca";
                string sqlConnectString = getProperty("connectionString");

                //Program.KioskApplication.log.Debug("sqlConnectString: " + sqlConnectString); 

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@serijskiBrojKartice", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@kolicina", SqlDbType.Int).Direction = ParameterDirection.Input;

                    command.Parameters.Add("@brojTransakcije", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@novac", SqlDbType.Decimal).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@mesec", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@godina", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dorucakTekuci", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@rucakTekuci", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@veceraTekuci", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dorucakDozvoljeno", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@rucakDozvoljeno", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@veceraDozvoljeno", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@dorucakNaredni", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@rucakNaredni", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@veceraNaredni", SqlDbType.Int).Direction = ParameterDirection.Input;

                    command.Parameters.Add("@idTransakcija", SqlDbType.Int).Direction = ParameterDirection.Output;

                    int offset = 0;

                    command.Parameters["@serijskiBrojKartice"].Value = cardSerialNumber;
                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;
                    command.Parameters["@kolicina"].Value = bankNote;

                    command.Parameters["@brojTransakcije"].Value = cardTransaction;
                    command.Parameters["@novac"].Value = cashOnCard;
                    command.Parameters["@novac"].Precision = 10;
                    command.Parameters["@novac"].Scale = 2;
                    command.Parameters["@godina"].Value = 2000 + meals[offset++];
                    command.Parameters["@mesec"].Value = meals[offset++];
                    command.Parameters["@dorucakTekuci"].Value = meals[offset++];
                    command.Parameters["@rucakTekuci"].Value = meals[offset++];
                    command.Parameters["@veceraTekuci"].Value = meals[offset++];
                    command.Parameters["@dorucakDozvoljeno"].Value = meals[offset++];
                    command.Parameters["@rucakDozvoljeno"].Value = meals[offset++];
                    command.Parameters["@veceraDozvoljeno"].Value = meals[offset++];
                    command.Parameters["@dorucakNaredni"].Value = meals[offset++];
                    command.Parameters["@rucakNaredni"].Value = meals[offset++];
                    command.Parameters["@veceraNaredni"].Value = meals[offset++];

                    //additional debugging code - should be removed
                    offset = 0;
                    string procParams = "@serijskiBrojKartice: " + cardSerialNumber;
                    procParams += ", @imeKioska " + Program.KioskApplication.KioskName;
                    procParams += ", @kolicina " + bankNote.ToString();
                    procParams += ", @brojTransakcije " + cardTransaction.ToString();
                    procParams += ", @novac " + cashOnCard.ToString(); 
                    procParams += ", @godina " + (2000 + meals[offset++]).ToString();
                    procParams += ", @mesec " + meals[offset++].ToString();
                    procParams += ", @dorucakTekuci " + meals[offset++].ToString();
                    procParams += ", @rucakTekuci " + meals[offset++].ToString();
                    procParams += ", @veceraTekuci " + meals[offset++].ToString();
                    procParams += ", @dorucakDozvoljeno" + meals[offset++].ToString();
                    procParams += ", @rucakDozvoljeno" + meals[offset++].ToString();
                    procParams += ", @veceraDozvoljeno" + meals[offset++].ToString();
                    procParams += ", @dorucakNaredni" + meals[offset++].ToString();
                    procParams += ", @rucakNaredni" + meals[offset++].ToString();
                    procParams += ", @veceraNaredni " + meals[offset++].ToString();

                    Program.KioskApplication.log.Debug("spNIKUplataElektronskogNovca is called with parameters: " + procParams);
                    ////////////////////////////////////////////////


                    connection.Open();
                    command.ExecuteNonQuery();

                    int result = Convert.ToInt32(command.Parameters["@rv"].Value);
                    if (result != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", moneyDeposit, " + result.ToString());
                    }

                    transactionId = Convert.ToInt32(command.Parameters["@idTransakcija"].Value);
                    if (!(transactionId > 0))
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", moneyDeposit, idTransakcija = " + transactionId.ToString());
                    }
                    
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(guiLang.DBAdapter_Error + ", moneyDeposit, " + ex.Message);
            }
        }

        public static bool isCardForThisKiosk(int opId)
        {
            //da li operaterska kartica koja je ubacena sme da podize pare iz kioska
            bool ret = false;

            if (opId == getKioskOperaterID())
            {
                ret = true;    
            }
            return ret;
        }

        public static void dbLogin(int opId)
        {
            try
            {
                //opId = 22139; za testiranje da bi moglo da se pokrene bilo kojom sluzbenickom

                string procName = "pisSpLogin";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@operater", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@nazivRadneStanice", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Input;

                    command.Parameters["@operater"].Value = opId;
                    command.Parameters["@nazivRadneStanice"].Value = Program.KioskApplication.KioskName;

                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);

                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ": " + err.ToString() + ", dbLogin");
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void getMoneyInformation(out double total, out double totalUntilToday)
        {
            try
            {
                string procName = "spNIKProracunavanjePazaraZaPodizanje";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@ukupnaSuma", SqlDbType.Float).Direction = ParameterDirection.Output;
                    command.Parameters.Add("@doDanasSuma", SqlDbType.Float).Direction = ParameterDirection.Output;

                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;

                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);

                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ": " + err.ToString() + ", getMoneyInformation");
                    }

                    total = Convert.ToDouble(command.Parameters["@ukupnaSuma"].Value);
                    totalUntilToday = Convert.ToDouble(command.Parameters["@doDanasSuma"].Value);

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void dischargeKiosk(double stanje, double ostatak)
        {
            try
            {
                //stanje - ukupno u kiosku
                //ostatak - koliko se ostavlja u kiosku
                //razlika ta dva je koliko se podize

                string procName = "spNIKPraznjenjeKioskaSaOstatkom";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@stanje", SqlDbType.Float).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@ostatak", SqlDbType.Float).Direction = ParameterDirection.Input;

                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;
                    command.Parameters["@stanje"].Value = stanje;
                    command.Parameters["@ostatak"].Value = ostatak;

                    connection.Open();
                    command.ExecuteNonQuery();

                    int err = Convert.ToInt32(command.Parameters["@rv"].Value);

                    if (err != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ": " + err.ToString() + ", dischargeKiosk");
                    }

                    connection.Close();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //za sada se ne prodaju ostale usluge na kiosku u Novom Sadu
        public static void getServices(string cardNumber)
        {
            try
            {
                string procName = "spNIKOdredjivanjeUslugaZaPlacanje";
                string sqlConnectString = getProperty("connectionString");

                using (SqlConnection connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@rv", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@brojKartice", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Input;

                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;
                    command.Parameters["@brojKartice"].Value = cardNumber;
                    
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    int result = Convert.ToInt32(command.Parameters["@rv"].Value);
                    if (result != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", getServices, " + result.ToString());
                    }

                    if (reader.HasRows)
                    {
                        Service s;

                        while (reader.Read())
                        {
                            s = new Service(Convert.ToString(reader[1]), Convert.ToDouble(reader[2]), Convert.ToInt32(reader[3]), Convert.ToInt32(reader[4]), Convert.ToInt32(reader[5]), Convert.ToBoolean(reader[6]));

                            BuyServicesFormScNS.getInstance().bsd.addService(s);
                        }
                    }
                    else
                    {
                        throw new Exception("Ne postoje usluge za ubačenu karticu.");
                    }

                    
                    reader.Close();

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int InputPurchase(string brojKartice, int operater, int tipTransakcije)
        {
            int result;
            String procName = "spUnosKupovineNovo";
            string sqlConnectString = getProperty("connectionString");

            SqlConnection connection = null;
            try
            {
                using (connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandTimeout = 300;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@brKartice", SqlDbType.NVarChar, 15).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@operater", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idTipNovcaneTransakcije", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idKupovina", SqlDbType.Int).Direction = ParameterDirection.Output;

                    command.Parameters["@brKartice"].Value = brojKartice;
                    command.Parameters["@operater"].Value = operater;
                    command.Parameters["@idTipNovcaneTransakcije"].Value = tipTransakcije;

                    connection.Open();
                    int i = command.ExecuteNonQuery();

                    object val = command.Parameters["@idKupovina"].Value;
                    result = Convert.ToInt32(val);

                    if (result <= 0)
                    {
                        throw new Exception("Inserting purchase error. result = " + result);
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                connection.Close();
                connection.Dispose();
                throw ex;
            }

            return result;
        }

        //public static int UndoPurchase(int idKupovina)
        //{
        //    int result;
        //    String procName = "spStorno";
        //    string sqlConnectString = getProperty("connectionString");

        //    SqlConnection connection = null;
        //    try
        //    {
        //        using (connection = new SqlConnection(sqlConnectString))
        //        {
        //            SqlCommand command = new SqlCommand(procName, connection);
        //            command.CommandTimeout = 300;
        //            command.CommandType = CommandType.StoredProcedure;
        //            command.Parameters.Add("@idKupovina", SqlDbType.Int).Direction = ParameterDirection.Input;
        //            command.Parameters.Add("@resultStorno", SqlDbType.Int).Direction = ParameterDirection.Output;

        //            command.Parameters["@idKupovina"].Value = idKupovina;

        //            connection.Open();
        //            int i = command.ExecuteNonQuery();

        //            object val = command.Parameters["@resultStorno"].Value;
        //            result = Convert.ToInt32(val);

        //            connection.Close();

        //            if (result != 0)
        //            {
        //                throw new Exception("Undo purchase error. " + result);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        connection.Close();
        //        connection.Dispose();
        //        throw ex;
        //    }

        //    return result;
        //}

        public static void buyService(
            string brojKartice,
            int idKupovina,
            int idUsluga,
            int idPlacanjeBoravka,
            int idLicnaOstecenja,
            double ukupanIznos,
            int kolicina)
        {

            int result;
            string procName = "spNIKPlacanjeUsluge";
            string sqlConnectString = getProperty("connectionString");

            SqlConnection connection = null;
            try
            {
                using (connection = new SqlConnection(sqlConnectString))
                {
                    SqlCommand command = new SqlCommand(procName, connection);
                    command.CommandTimeout = 300;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@imeKioska", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@brojKartice", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idKupovina", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idUsluga", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idPlacanjeBoravka", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@idLicnaOstecenja", SqlDbType.Int).Direction = ParameterDirection.Input;
                    SqlParameter p = command.Parameters.Add("@ukupanIznos", SqlDbType.Decimal);
                    p.Direction = ParameterDirection.Input;
                    p.Precision = 18;
                    p.Scale = 2;
                    command.Parameters.Add("@kolicina", SqlDbType.Int).Direction = ParameterDirection.Input;
                    command.Parameters.Add("@result", SqlDbType.Int).Direction = ParameterDirection.Output;

                    command.Parameters["@imeKioska"].Value = Program.KioskApplication.KioskName;
                    command.Parameters["@brojKartice"].Value = brojKartice;
                    command.Parameters["@idKupovina"].Value = idKupovina;
                    command.Parameters["@idUsluga"].Value = idUsluga;
                    command.Parameters["@idPlacanjeBoravka"].Value = idPlacanjeBoravka;
                    command.Parameters["@idLicnaOstecenja"].Value = idLicnaOstecenja;
                    command.Parameters["@ukupanIznos"].Value = ukupanIznos;
                    command.Parameters["@kolicina"].Value = kolicina;

                    connection.Open();
                    int i = command.ExecuteNonQuery();

                    result = Convert.ToInt32(command.Parameters["@result"].Value);

                    if (result != 0)
                    {
                        throw new Exception(guiLang.DBAdapter_Error + ", buyService, " + result.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                connection.Close();
                connection.Dispose();
                throw ex;
            }
        }

        /*
        public static void getActivePictures(Hashtable activePictures)
        {
            SqlConnection connection = null;
            SqlDataReader myReader = null;

            try
            {
                string procName = "vAktivneFotografije";
                string sqlConnectString = getProperty("connectionString");

                using (connection = new SqlConnection(sqlConnectString))
                {
                    string sqlQuery = "SELECT Slika, BrojKartice FROM " + procName;
                    SqlCommand myCommand = new SqlCommand(sqlQuery, connection);
                    connection.Open();
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {
                        try
                        {
                            String cardNumber = myReader["BrojKartice"].ToString();
                            String picNumber = myReader["Slika"].ToString();

                            activePictures.Add(cardNumber, picNumber);
                        }
                        catch (Exception)
                        {
                            //ako je vec ubacen kljuc sa tim brojem kartice, bacice izuzetak ali ja ne zelim da prekinem punjenje tabele
                        }

                    }
                    myReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                if ((myReader != null) && (!myReader.IsClosed))
                {
                    myReader.Close();
                }
                if (connection != null)
                {
                    connection.Close();
                }
                throw new Exception("Nije uspelo prikupljanje podataka o aktivnim karticama. " + ex.Message);
            }
        }*/

        public static int fillCardsHashTable(HashSet<string> cardsTable)
        {
            int result = 0;
            SqlConnection connection = null;
            SqlCommand command = null;
            SqlDataReader dr = null;
            String procName = "spInitializeCardsHash";
            string sqlConnectString = getProperty("connectionString");
            try
            {
                using (connection = new SqlConnection(sqlConnectString))
                {
                    command = new SqlCommand(procName, connection);
                    command.CommandType = CommandType.StoredProcedure;

                    connection.Open();
                    dr = command.ExecuteReader();

                    while (dr.Read())
                    {
                        string cardNumber = Convert.ToString(dr["BrojKartice"]);
                        bool archive = Convert.ToBoolean(dr["Arhiva"]);

                        try
                        {
                            cardsTable.Add(cardNumber);
                            ++result;
                        }
                        catch (Exception)
                        {
                            //ako je vec ubacen kljuc sa tim brojem kartice, bacice izuzetak ali ja ne zelim da prekinem punjenje tabele
                        }
                    }

                    dr.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                result = 0;
                if ((dr != null) && (!dr.IsClosed))
                {
                    dr.Close();
                }
                if (connection != null)
                {
                    connection.Close();
                }
                throw ex;
            }

            return result;
        }

        public static int getActivePictures(Hashtable activePictures)
        {
            SqlConnection connection = null;
            SqlDataReader myReader = null;

            int numberOfPictures = 0;

            try
            {
                string procName = "vAktivneFotografije";
                string sqlConnectString = getProperty("connectionString");

                using (connection = new SqlConnection(sqlConnectString))
                {
                    string sqlQuery = "SELECT Slika, BrojKartice FROM " + procName;
                    SqlCommand myCommand = new SqlCommand(sqlQuery, connection);
                    connection.Open();
                    myReader = myCommand.ExecuteReader();

                    while (myReader.Read())
                    {
                        try
                        {
                            String cardNumber = myReader["BrojKartice"].ToString();
                            String picNumber = myReader["Slika"].ToString();

                            activePictures.Add(cardNumber, picNumber);
                            numberOfPictures++;
                        }
                        catch (Exception)
                        {
                            //ako je vec ubacen kljuc sa tim brojem kartice, bacice izuzetak ali ja ne zelim da prekinem punjenje tabele
                        }

                    }
                    myReader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                if ((myReader != null) && (!myReader.IsClosed))
                {
                    myReader.Close();
                }
                if (connection != null)
                {
                    connection.Close();
                }
                throw new Exception("Nije uspelo prikupljanje podataka o aktivnim karticama. " + ex.Message);
            }

            return numberOfPictures;
        }

    }
}

