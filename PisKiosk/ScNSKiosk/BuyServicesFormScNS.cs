﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PisKiosk.ScNSKiosk
{
    public partial class BuyServicesFormScNS : PisKiosk.BuyServicesForm
    {
        private static BuyServicesFormScNS instance = null;
        public static BuyServicesFormScNS getInstance()
        {
            if (instance == null)
            {
                instance = new BuyServicesFormScNS();
            }
            return instance;
        }

        public BuyServicesFormScNS():base()
        {
            InitializeComponent();

            bsd = new BuyServicesDataScNS();
        }

        public override void setCashValues()
        {
            //postavi koliko para ima u novcaniku
            bsd.CardMoneyBalance = UserDataFormScNS.getInstance().ud.CashOnCard;
            bsd.MoneyBalanceAfterBuyingSelected = bsd.CardMoneyBalance - bsd.getTotalPrice();
        }

        public override void btnPlusOne_Click(object sender, EventArgs e)
        {
            if (!FormBusy)
            {
                try
                {
                    bsd.changeServiceAmounts(SelectedService, 1);
                    setServiceAmount();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Buy services - plus one button click failed " + ex.Message, ex);
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }

        public override void btnMinusOne_Click(object sender, EventArgs e)
        {
            if (!FormBusy)
            {
                try
                {
                    bsd.changeServiceAmounts(SelectedService, -1);
                    setServiceAmount();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Buy services - minus one button click failed " + ex.Message, ex);
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }

        public override void btnCancelBuyServices_Click(object sender, EventArgs e)
        {
            if (!FormBusy)
            {
                try
                {
                    if (!sender.Equals("Return"))
                    {
                        Program.KioskApplication.log.Info("Clicked Cancel Buy Services button.");
                    }
                    Program.KioskApplication.showFormInPanel(UserDataFormScNS.getInstance());
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Buy services - cancel button click failed " + ex.Message, ex);
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }

        private int processBuyingServices()
        {

            int idKupovina = DBAdapter.InputPurchase(UserDataFormScNS.getInstance().ud.CardNumber, Program.KioskApplication.Operater.OperaterId, 1);

            Program.KioskApplication.log.Info("Purchase inserted into DB. ID=" + idKupovina.ToString());

            foreach (var service in bsd.Services)
            {
                if (service.Amount > 0)
                {
                    try
                    {
                        DBAdapter.buyService(UserDataFormScNS.getInstance().ud.CardNumber, idKupovina, service.ServiceDBId, service.BoardingDBId, service.PersonalDamageDBId, service.Amount * service.Price, service.Amount);
                    }
                    catch (Exception ex)
                    {
                        //nije uspeo upis usluge u bazu
                        Program.KioskApplication.log.Error("Inserting service to DB failed. Canceling purchase... Service=" + service.Name + ", ID=" + service.ServiceDBId + ", error message: " + ex.Message, ex);
                        //ponisti idKupovine
                        DBAdapter.undoTransaction(idKupovina);
                        Program.KioskApplication.log.Info("Purchase Canceled.");
                        throw ex;             
                    }
                }
            }

            Program.KioskApplication.log.Info("Services successfully inserted into DB. Now, the money from the card will be taken. ");

            //treba i da se skine novac sa kartice
            try
            {
                ScNSKiosk.CardAdapterContactScNS cardInterface = (ScNSKiosk.CardAdapterContactScNS)CardInterface.getInstance().CardInterf;
                cardInterface.decCashAmountAdapter(bsd.getTotalPrice());    
            }
            catch (Exception ex)
            {
                //ako nije uspeo upis na karticu, ponisti transakciju u bazi
                try
                {
                    Program.KioskApplication.log.Error("Writing to card failed. Canceling purchase... Error message: " + ex.Message, ex);
                    DBAdapter.undoTransaction(idKupovina);
                    Program.KioskApplication.log.Info("Purchase Canceled.");
                }
                catch (Exception ex1)
                {
                    //nije uspelo ponistavanje u bazi
                    Program.KioskApplication.log.Error("Canceling purchase failed. Error message: " + ex1.Message, ex1);
                    throw new Exception(ex1.Message);
                }
                throw new Exception(ex.Message/*nije uspeo upis na karticu*/);
            }

            return idKupovina;
        }
                                                                                                              
        protected override void btnBuyServices_Click(object sender, EventArgs e) 
        {
            //podaci ce se upisivati u bazu i na karticu u posebnom threadu (tasku),
            //a u ovom threadu cu da pitam korisnika da li hoce da mu se stampa racun
            //dok on odabere sta hoce, i upis ce da zavrsi - tu cekam da task zavrsi, 
            //a onda se stampa racun ako je korisnik to izabrao
            //naravno, ako nema papira ili ne radi stampac, ne treba nista pitati korisnika
            if (!FormBusy)
            {
                Program.KioskApplication.log.Info("Clicked Buy Services button.");
                try
                {
                    var taskProcessBuyingServices = Task.Run(() => processBuyingServices());

                    //Task<int> taskProcessBuyingMeals = new Task<int>(processBuyingMeals);
                    //taskProcessBuyingMeals.Start();

                    //prikazi dijalog sa pitanjem da li hoce da stampa
                    if (Program.KioskApplication.PrinterAvailable)
                    {
                        MessageChoice.getInstance(guiLang.MessageChoice_DoYouWantReceipt, MessageType.Question).ShowDialog();
                    }

                    int transactionId = taskProcessBuyingServices.Result;

                    if (Program.KioskApplication.MessageChoiceResult)
                    {
                        //stampaj racun
                        printReceipt(transactionId);
                    }

                    taskProcessBuyingServices.Wait();


                    Program.KioskApplication.log.Info("Successful Buying services.");
                }
                catch (AggregateException aex)
                {
                    Program.KioskApplication.log.Error(aex.InnerException.Message, aex);
                    MessageClassic.getInstance(aex.InnerException.Message, MessageType.Error).ShowDialog();
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error(ex.InnerException.Message, ex);
                    MessageClassic.getInstance(ex.InnerException.Message, MessageType.Error).ShowDialog();
                }
                finally
                {
                    //vrati se nazad
                    FormBusy = false;
                    btnCancelBuyServices_Click("Return", null);
                }
            }
        }

        protected override void btnInsertCash_Click(object sender, EventArgs e)
        {
            if (!FormBusy)
            {
                try
                {
                    Program.KioskApplication.log.Info("Clicked Insert Cash button.");
                    Program.KioskApplication.showFormInPanel(CashInsertFormScNS.getInstance(this));
                }
                catch (Exception ex)
                {
                    Program.KioskApplication.log.Error("Buy services - insert cash button click failed " + ex.Message, ex);
                }
                finally
                {
                    FormBusy = false;
                }
            }
        }

        public void printReceipt(int transactionId)
        {
            try
            {
                DateTime now = DateTime.Now;
                string receiptTitle = guiLang.Receipt_Title_Shopping;
                string receiptNumber = transactionId.ToString();

                string timeStamp = Utils.addLeadingZeros(now.Day, 2) + @"." + Utils.addLeadingZeros(now.Month, 2) + @"." + now.Year.ToString() + @". " + Utils.addLeadingZeros(now.Hour, 2) + @":" + Utils.addLeadingZeros(now.Minute, 2) + @":" + Utils.addLeadingZeros(now.Second, 2);

                //stavke racuna
                List<ReceiptItem> receiptItems = new List<ReceiptItem>();

                foreach (var service in bsd.Services)
                {
                    if (service.Amount > 0)
                    {
                        receiptItems.Add(new ReceiptItem(service.Name, service.Amount, service.Price));
                    }
                }
                //-------------

                Receipt rp = new ReceiptScNS(receiptTitle, receiptNumber, UserDataFormScNS.getInstance().ud.CardNumber, bsd.getTotalPrice(), timeStamp, receiptItems);
                rp.printReceipt();
            }
            catch (Exception ex)
            {
                string err = "Error printing receipt. " + " " + ex.Message;
                Program.KioskApplication.log.Error(err, ex);
                throw new Exception(err);
            }
        }



    }
}
