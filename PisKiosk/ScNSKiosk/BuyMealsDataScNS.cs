﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.ScNSKiosk
{
    public class BuyMealsDataScNS : BuyMealsData
    {
        public override int getAllowedMealsForResident(int month)
        {
            //mestani smeju ukupno da kupe onoliko obroka koliko im pise u LEFT podacima za sve obroke
            //ovde treba da pronadjem sve obroke za odabrani mesec, pa da vidim koliko 
            int allowed = 0;
            DateTime now = DateTime.Now;
            if (month == now.Month)
            {
                allowed = Convert.ToInt32(ScNSKiosk.UserDataFormScNS.getInstance().ud.LunchLeft);
            }
            else
            {
                allowed = DateTime.DaysInMonth(now.AddMonths(1).Year, now.AddMonths(1).Month) - Convert.ToInt32(ScNSKiosk.UserDataFormScNS.getInstance().ud.LunchNext);
            }

            foreach (Meal meal in Meals)
            {
                if (meal.Month == month)
                {
                    allowed -= meal.Amount;
                }
            }
            foreach (Meal meal in Meals)
            {
                if (meal.Month == month)
                {
                    meal.MealsAllowed = allowed;
                }
            }
            return allowed;
        }
    }
}
