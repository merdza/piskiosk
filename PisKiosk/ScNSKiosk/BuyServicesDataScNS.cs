﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk.ScNSKiosk
{
    public class BuyServicesDataScNS : BuyServicesData
    {
        public BuyServicesDataScNS() : base() { }

        public override void fillServicesFromDB()
        {
            DBAdapter.getServices(UserDataFormScNS.getInstance().ud.CardNumber);
        }
    }
}
