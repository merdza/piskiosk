﻿namespace PisKiosk.ScNSKiosk
{
    partial class WithdrawMoneyFormScNS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // btnWithdrawMoney
            // 
            this.btnWithdrawMoney.Text = "Podizanje novca";
            this.btnWithdrawMoney.Click += new System.EventHandler(this.btnWithdrawMoney_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Text = "Odustani";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // WithdrawMoneyFormScNS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(1190, 643);
            this.Name = "WithdrawMoneyFormScNS";
            this.Load += new System.EventHandler(this.WithdrawMoneyFormScNS_Load);
            this.VisibleChanged += new System.EventHandler(this.WithdrawMoneyFormScNS_VisibleChanged);
            this.ResumeLayout(false);

        }

        

        #endregion
    }
}
