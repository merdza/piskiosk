﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisKiosk
{
    public abstract class InsertedBankNote
    {
        //ova klasa se izvodi za svaku posebnu implementaciju info kioska
        public int Value {get; protected set;}

        public InsertedBankNote()
        {
            
        }

        public virtual void insertBankNote(int bankNoteValue)
        {
            Value = bankNoteValue;

            //todo
            //svaka izvedena klasa implementira kako se obradjuje novcanica koja je ocitana (upis na karticu, upis u xml, upis u bazu)
        }
    }
}
