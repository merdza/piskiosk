﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Windows.Forms;
using log4net;

namespace PisKiosk
{

    //public enum KioskApplication { SCBeograd, Sombor, KSA, SCNoviSad}

    public enum KioskStatuses { Info = 1, BuyingMeals, BuyingMealsInsertingMoney }  //Provera stanja, Kupovina bonova, Kupovina bonova i uplata novca

    public partial class PisKioskForm : Form
    {
        public readonly ILog log;
        public string SettingsFile { get; private set; }
        //public KioskApplication KioskApplication { get; private set; }
        public string KioskName { get; private set; }
        public Operator Operater { get; set; }
        public int KioskOperater { get; set; }  // operater koji je zaduzen za podizanje novca sa kioska - podatak se cita iz baze
        public string SelectedLanguage { get; set; }
        public CultureInfo CurrentCultureInfo { get; set; }
        public string CurrencyCode { get; private set; }
        public Color BackgroundColor { get; private set; }
        public Color TextForeColor { get; private set; }
        public Color TextForeColor2 { get; private set; }
        public int ValidatorPort { get; private set; }
        public MainForm ActivePanelForm { get; private set; }

        public int ActiveCardsTaken { get; protected set; } //koliko puta su uzeti podaci o aktivnim karticama iz baze

        public bool PrinterAvailable { get; set; }
        public bool PrinterTesting { get; set; }
        public bool RestartSpooler { get; set; }

        public KioskStatuses KioskStatus { get; set; }

        /// <summary>
        /// In the background the connection to the database will be checked periodically and IsDBConnectionOk variable will contain information of the last checkup.
        /// </summary>
        public bool IsDBConnectionOk { get; set; }
        protected System.Windows.Forms.Timer timerCallBcgWorkerToCheckDBConnection;
        /// <summary>
        /// When the operator clicked login button, this parameter is set to true as indication that in background thread should call login procedure in DB when the connection is available
        /// </summary>
        public bool ShouldLoginToDB { get; set; }

        public bool MessageChoiceResult { get; set; }

        public UserData UserData { get; set; }

        protected static bool listeningStarted = false;

        public int CardsHashInitialized { get; protected set; }
        public HashSet<string> CardsTable { get; protected set; }
        protected static HashSet<string> pictures; //spisak slika iz foldera koje služe za prikaz na formi
        protected static Hashtable activePictures; //spisak aktivnih slika iz baze za svakog korisnika
        public bool ActivePicturesInitialized { get; protected set; }
        public bool PicturesFromDirectoryTaken { get; set; }
        public int AuxiliaryDataInterval { get; set; } //period u minutima kada treba uzimati promenljive podatke kao sto su aktivne kartice i slike
        public bool DataSynchronized { get; set; } //da li je uspela sinhronizacija svih promenljivih podataka
        public int MinutesElapsedSinceLastSynch { get; set; } //posto ce tajmer da se poziva svakog minuta, ova promenljiva cuva informacije koliko minuta je proslo od poslednje uspesne sinhronizacije
        public static object DataSyncLocker { get; set; }  //ako ne zavrsi za minut, sledeci poziv tajmera mora da saceka


        //public delegate void LogWriter(string input, PisLog.EventType messType);
        //public void writeToLog(string message, PisLog.EventType et, string card = "", bool shouldWriteEmptyOrCorrupted = false)
        //{
        //    if (!String.IsNullOrWhiteSpace(message))
        //    {
        //        if (String.IsNullOrEmpty(card))
        //        {
        //            PisLogAdapter.getInstance().writeToLog(message, et);
        //        }
        //        else
        //        {
        //            PisLogAdapter.getInstance().writeToLog(message, et, card);
        //        }
        //    }
        //    else
        //    {
        //        if (shouldWriteEmptyOrCorrupted)
        //        {
        //            PisLogAdapter.getInstance().writeToLog(@"The message which was sent to log was empty or corrupted.", ex);    
        //        }
        //    }


        //}

        public PisKioskForm()
        {
            InitializeComponent();

            //this.TopLevel = true;

            DataSyncLocker = new object();

            log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            SettingsFile = AppDomain.CurrentDomain.BaseDirectory + "settings.xml";

            try
            {
                KioskName = XmlSettings.XmlSettings.getSetting(SettingsFile, "kioskDBName");
                CurrencyCode = XmlSettings.XmlSettings.getSetting(SettingsFile, "currency");
                //boja pozadine se cita iz settings fajla
                BackgroundColor = ColorTranslator.FromHtml(XmlSettings.XmlSettings.getSetting(SettingsFile, "backgroundColor"));
                TextForeColor = ColorTranslator.FromHtml(XmlSettings.XmlSettings.getSetting(SettingsFile, "foreColor"));
                TextForeColor2 = ColorTranslator.FromHtml(XmlSettings.XmlSettings.getSetting(SettingsFile, "foreColor2"));

                ValidatorPort = Convert.ToInt32(XmlSettings.XmlSettings.getSetting(SettingsFile, "validatorPort"));

                PrinterTesting = Convert.ToBoolean(Convert.ToInt16(XmlSettings.XmlSettings.getSetting(SettingsFile, "printerTesting")));
                RestartSpooler = Convert.ToBoolean(Convert.ToInt16(XmlSettings.XmlSettings.getSetting(SettingsFile, "restartSpooler")));
            }
            catch (Exception ex)
            {
                throw new Exception("Neispravan konfiguracioni fajl. " + ex.Message);
            }

            ActiveCardsTaken = 0;
            IsDBConnectionOk = false;
        }


        public virtual void setKioskOperater()
        { }

        public virtual void LoginToDatabase()
        { }

        public virtual WithdrawMoneyForm getWithdrawMoneyForm()
        {
            return null;
        }

        private void PisKioskForm_Load(object sender, EventArgs e)
        {
            fillLanguages();

            loadKioskApplication(PanelFormType.UserApplication);

            //Program.startListening();
            //            setListeningStarted(true);

            this.BackColor = Program.KioskApplication.BackgroundColor;
            listViewLanguages.BackColor = Program.KioskApplication.BackgroundColor;
        }

        //public void setListeningStarted(bool started)
        //{
        //    listeningStarted = started;
        //}

        public void cardInserted()
        {
            try
            {
                WaitingForCardForm wcf = null;
                try
                {
                    wcf = (WaitingForCardForm)ActivePanelForm;
                }
                catch (Exception)
                {
                    //ovo znaci da je kao aktivna forma postavljen operaterski deo aplikacije - za podizanje para. Nemoj da reagujes na ubacivanje kartice
                }
                if (wcf != null)
                {
                    wcf.cardInserted();
                }
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Error Card inserted. " + ex.Message, ex);
                MessageClassic.getInstance(ex.Message, MessageType.Error).Show();
            }
        }

        public void cardRemoved()
        {
            try
            {
                if (ActivePanelForm.ReactOnCardRemoved)
                {
                    UserData.resetUserData();
                    Program.KioskApplication.log.Info("Card removed.");
                    loadKioskApplication(PanelFormType.UserApplication);
                }
            }
            catch (Exception ex)
            {
                Program.KioskApplication.log.Error("Error: cardRemoved " + ex.Message, ex);
            }
            //            changeFormStatus(0);
        }

        public void checkPrinterStatus()
        {
            try
            {
                PrintSupport.getPrinterErrors();
                Program.KioskApplication.PrinterAvailable = true;
            }
            catch (Exception ex)
            {
                Program.KioskApplication.PrinterAvailable = false;
                Program.KioskApplication.log.Error("Printer error: " + ex.Message, ex);
                throw new Exception(ex.Message);
            }
        }

        public virtual void loadKioskApplication(PanelFormType pft)
        { }

        private void listViewLanguages_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Windows.Forms.ListView.SelectedListViewItemCollection lang = new System.Windows.Forms.ListView.SelectedListViewItemCollection(listViewLanguages);
            lang = listViewLanguages.SelectedItems;
            if (lang.Count > 0)
            {
                string selectedLang = lang[0].Text;

                if (!selectedLang.Equals(SelectedLanguage)) //iz nekog razloga, dva puta poziva ovu funkciju, pa sam morao da proveravam da li je selektovani jezik vec aktuelni i da u tom slucaju nista ne radim
                {
                    try
                    {
                        switch (selectedLang)
                        {
                            case "Srpski - latinica":
                                {
                                    CurrentCultureInfo = CultureInfo.GetCultureInfo("sr-Latn");
                                    Thread.CurrentThread.CurrentUICulture = CurrentCultureInfo;
                                    SelectedLanguage = @"Srpski - latinica";
                                    break;
                                }
                            case "Српски - ћирилица":
                                {
                                    CurrentCultureInfo = CultureInfo.GetCultureInfo("sr-Cyrl");
                                    Thread.CurrentThread.CurrentUICulture = CurrentCultureInfo;
                                    SelectedLanguage = @"Српски - ћирилица";
                                    break;
                                }
                            default:
                                {
                                    CurrentCultureInfo = CultureInfo.GetCultureInfo("en-US");
                                    Thread.CurrentThread.CurrentUICulture = CurrentCultureInfo;
                                    SelectedLanguage = @"English";
                                    break;
                                }
                        }

                        log.Debug("Selected language: " + SelectedLanguage + " " + Thread.CurrentThread.CurrentUICulture.ToString());

                        showFormInPanel(ActivePanelForm);
                    }
                    catch (Exception)
                    { }
                }

                listViewLanguages.SelectedIndices.Clear();
            }
        }

        private void stopListening()
        {
            Program.stopListening();
        }

        private void PisKioskForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            stopListening();
        }

        private void PisKioskForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void fillLanguages()
        {
            List<string> langs = new List<string>();
            List<string> flags = new List<string>();

            XmlSettings.XmlSettings.getArraySettings(SettingsFile, "languageName", ref langs);
            XmlSettings.XmlSettings.getArraySettings(SettingsFile, "languageFlagImg", ref flags);

            ListViewItem item = null;
            ImageList imageList = new ImageList();

            int cnt = 0;
            foreach (var lang in langs)
            {
                item = new ListViewItem(lang, cnt++);
                listViewLanguages.Items.Add(item);
            }

            foreach (var flag in flags)
            {
                //imageList.Images.Add(Image.FromFile(@"D:\Aktuelne verzije na kojima se radi\PisKioskAKTUELNO\PisKiosk\PisKiosk\" + flag + @".png"));

                ResourceManager rm = PisKiosk.Properties.Resources.ResourceManager;
                Bitmap myImage = (Bitmap)rm.GetObject(flag);

                imageList.Images.Add(myImage);
            }

            listViewLanguages.View = View.Tile;

            imageList.ImageSize = new Size(32, 32);
            imageList.ColorDepth = ColorDepth.Depth32Bit;

            listViewLanguages.LargeImageList = imageList;
            listViewLanguages.Items[0].Selected = true;
        }

        public void showFormInPanel(MainForm form)
        {
            string formName = form.GetType().Name;
            Program.KioskApplication.log.Debug("Pokrenuto prikazivanje forme: " + formName);
            IEnumerator ien = panelMain.Controls.GetEnumerator();
            ien.Reset();
            ien.MoveNext();
            for (int i = 0; i < panelMain.Controls.Count; i++)
            {
                ((MainForm)ien.Current).Hide();
                ien.MoveNext();
            }

            //Program.KioskApplication.writeToLog("Sakrivene sve forme na panelu", PisLog.EventType.Test);

            panelMain.Controls.Clear();
            panelMain.Controls.Add(form);
            ActivePanelForm = form;
            listViewLanguages.Visible = form.LangsVisible;  //na svakoj formi se definise da li na njoj moze da se menja jezik ili ne

            form.Show();
            Program.KioskApplication.log.Debug("Prikazana forma: " + formName);
        }

        private void PisKioskForm_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
            {
                //ako se kojim slucajem sakriva ova forma, kada se vrati, neka ne budu podaci o ubacenoj kartici na njoj
                Program.KioskApplication.cardRemoved();
            }
        }

        protected virtual void timerSyncUsers_Tick(object sender, EventArgs e)
        { }




        //zabrana kupovine u vremenskim intervalima
        public bool isBuyingEnabled()
        {
            bool ret = true;
            List<TimePeriod> timePeriods = new List<TimePeriod>();
            List<string> disableBuyingFrom = new List<string>();
            List<string> disableBuyingTo = new List<string>();

            DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            XmlSettings.XmlSettings.getArraySettings(SettingsFile, "disableBuyingFrom", ref disableBuyingFrom);
            XmlSettings.XmlSettings.getArraySettings(SettingsFile, "disableBuyingTo", ref disableBuyingTo);

            if (disableBuyingFrom.Count != disableBuyingTo.Count)
            {
                throw new Exception("Reading off times from settings failed. Check settings file");
            }

            string offTimesOutput = string.Empty;  //za ispis u log svih vremena kada ne moze da se kupi
            for (int i = 0; i < disableBuyingFrom.Count; i++)
            {
                TimePeriod period = new TimePeriod()
                {
                    TimeFrom = parseTimeFromString(disableBuyingFrom[i]),
                    TimeTo = parseTimeFromString(disableBuyingTo[i])
                };
                timePeriods.Add(period);
                offTimesOutput += period.ToString() + ", ";
            }

            //sad kad su napravljeni periodi u kojima je zabranjena kupovina, treba da vidimo da li trenutno vreme upada u neki od perioda
            TimeSpan now = new TimeSpan(dt.Hour, dt.Minute, dt.Second);

            foreach (var period in timePeriods)
            {
                if (period.isTimeInPeriod(now))
                {
                    ret = false;
                    break;
                }
            }

            if (ret)
            {
                Program.KioskApplication.log.Debug("Time for buying is OK. Off times: " + offTimesOutput);
            }
            else
            {
                Program.KioskApplication.log.Debug("Buying is not allowed because currently is off time. Off times: " + offTimesOutput);
            }

            return ret;
        }

        private TimeSpan parseTimeFromString(string timeString)
        {
            int h = -1;
            int m = -1;

            try
            {
                int index = timeString.IndexOf(":");
                h = Convert.ToInt32((index > 0 ? timeString.Substring(0, index) : ""));
                m = Convert.ToInt32((index > 0 ? timeString.Substring(index + 1) : ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Wrong time in settings file: " + timeString + ". " + ex.Message);
            }

            if ((h < 0) || (h > 23))
            {
                throw new Exception("Wrong time in settings file: " + timeString);
            }

            if ((m < 0) || (m > 59))
            {
                throw new Exception("Wrong interval in settings file: " + timeString);
            }

            return new TimeSpan(h, m, 59);
        }

        public bool isHandleCreated()
        {
            return this.IsHandleCreated;
        }

        /// <summary>
        /// The idea is to check connection to the database in the background and to keep whether it is ok or not in a variable
        /// Then, when card is inserted, if the connection is not ok, it will not even try to connect and it will work in "offline" mode
        /// If the connection is ok, it will try to connect as usual although the connection might not work in that moment
        /// This will prevent the frequent connection requests when db connection is not available. This has caused program to stop working in the past.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void backgroundWorkerCheckDBConnection_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            e.Result = DBAdapter.IsServerConnected();
        }

        protected virtual void backgroundWorkerCheckDBConnection_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                IsDBConnectionOk = false;
                log.Error("Error checking connection to DataBase" + e.Error.Message, e.Error);
            }
            else
            {
                try
                {
                    IsDBConnectionOk = Convert.ToBoolean(e.Result);

                    //if kiosk operater is not set yet (if there was no connection on login), set it now
                    if (IsDBConnectionOk)
                    {
                        if (Operater == null)
                        {
                            Program.KioskApplication.setKioskOperater();
                        }

                        if (ShouldLoginToDB)
                        {
                            try
                            {
                                Program.KioskApplication.LoginToDatabase();
                                ShouldLoginToDB = false;
                            }
                            catch (Exception ex)
                            {
                                ShouldLoginToDB = false;  //za svaki slucaj, posto se nije ulogovao, mora da proba u sledecem prolazu
                                log.Error("Error logging to a database. " + ex.Message, ex);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    IsDBConnectionOk = false;
                }
                
                log.Info("Connection to DataBase checked. The connection " + (IsDBConnectionOk ? "is OK." : "is NOT established."));
            }
        }

        protected void timerCallBcgWorkerToCheckDBConnection_Tick(object sender, EventArgs e)
        {
            if (!backgroundWorkerCheckDBConnection.IsBusy)
                backgroundWorkerCheckDBConnection.RunWorkerAsync();
        }
    }

    public class TimePeriod
    {
        public TimeSpan TimeFrom { get; set; }
        public TimeSpan TimeTo { get; set; }

        public bool isTimeInPeriod(TimeSpan time)
        {
            if (TimeSpan.Compare(TimeFrom, time) <= 0 && TimeSpan.Compare(TimeTo, time) >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return TimeFrom.ToString(@"hh\:mm") + " - " + TimeTo.ToString(@"hh\:mm");
        }
    }

    public enum PanelFormType { UserApplication, OperatorApplication};
}
